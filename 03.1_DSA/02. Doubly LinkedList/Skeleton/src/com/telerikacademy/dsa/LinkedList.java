package com.telerikacademy.dsa;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class LinkedList<T> implements List<T> {
    private Node head;
    private Node tail;
    private int size = 0;

    public LinkedList() {
    }

    public LinkedList(Iterable<T> iterable) {
        iterable.forEach(item -> this.addLast(item));
    }

    @Override
    public void addFirst(T value) {
        var newNode = new Node(value);

        newNode.next = head;
        head = newNode;
        if (isEmpty()) {
            tail = head;
        } else {
            head.next.prev = head;
        }
        size++;
    }

    @Override
    public void addLast(T value) {
        var newNode = new Node(value);

        newNode.prev = tail;
        tail = newNode;

        if (isEmpty()) {
            head = tail;
        } else {
            tail.prev.next = tail;
        }
        size++;
    }

    @Override
    public void add(int index, T value) {
        if (index < 0 || index > size()) {
            throw new NoSuchElementException();
        }

        /** if we are adding to the tail */
        if (size() == index) {
            addLast(value);
            return;
        }

        /** if we are adding to the head */
        if (0 == index) {
            addFirst(value);
            return;
        }

        size++;

        var neighbour = getNode(index);
        var newNode = new Node(value);
        newNode.prev = neighbour.prev;
        newNode.next = neighbour;
        neighbour.prev = newNode;

        if (newNode.prev != null) {
            newNode.prev.next = newNode;
        }

        if (isEmpty()) {
            head = newNode;
        }
    }

    @Override
    public T getFirst() {
        return get(0);
    }

    @Override
    public T getLast() {
        return get(size() - 1);
    }

    @Override
    public T get(int index) {
        return getNode(index).value;

    }

    @Override
    public int indexOf(T value) {
        int index = 0;
        var current = head;

        while (current != null) {
            if (current.value.equals(value)) {
                return index;
            }
            current = current.next;
            index++;
        }
        return -1;
    }

    @Override
    public T removeFirst() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }

        var oldHead = head;
        head = head.next;
        if (head == null) {
            tail = null;
        } else {
            head.prev = null;
        }
        size--;
        return oldHead.value;
    }

    @Override
    public T removeLast() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        size--;
        var oldTail = tail;
        tail = tail.prev;

        if (tail == null) {
            head = null;
        } else {
            tail.next = null;
        }

        return oldTail.value;

    }

    @Override
    public int size() {
        return this.size;
    }

    private boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public Iterator<T> iterator() {
        return new MyLinkedListIterator();
    }

    private class Node {
        T value;
        Node prev;
        Node next;

        Node(T value) {
            this.value = value;
        }
    }

    private class MyLinkedListIterator implements Iterator<T> {

        private Node current = head;


        @Override
        public boolean hasNext() {
            if (current != null) {
                return true;
            }
            return false;
        }

        @Override
        public T next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }

            T temp = current.value;
            current = current.next;
            return temp;
        }
    }

    private Node getNode(int index) {
        if (index < 0 || index > size() - 1) {
            throw new NoSuchElementException();
        }
        if (index == 0) {
            return head;
        }
        if (index == size() - 1) {
            return tail;

        }

        var iterCopy = head;
        while (index > 0) {
            iterCopy = iterCopy.next;
            index--;
        }
        return iterCopy;


    }
}
