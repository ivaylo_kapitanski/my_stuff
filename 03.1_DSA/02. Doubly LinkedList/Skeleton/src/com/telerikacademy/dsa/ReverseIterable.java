package com.telerikacademy.dsa;

public interface ReverseIterable<T> {
    public ReverseIterable<T> reverseIterable();
}
