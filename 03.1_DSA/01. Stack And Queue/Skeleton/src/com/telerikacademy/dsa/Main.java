package com.telerikacademy.dsa;

import com.telerikacademy.dsa.stack.ArrayStack;
import com.telerikacademy.dsa.stack.LinkedStack;
import com.telerikacademy.dsa.stack.Stack;

public class Main {
    public static void main(String[] args) {
//        Stack<Integer> newStack=new LinkedStack<>();
//
//       newStack.push(3);
//       newStack.push(2);
//        System.out.println(newStack.pop());
//        System.out.println(newStack.size());
//        System.out.println(newStack.peek());
//        System.out.println(newStack.isEmpty());

        Stack<Integer> newStack=new ArrayStack<>();

        newStack.push(3);
        newStack.push(2);
        newStack.push(17);
        System.out.println(newStack.pop());
        System.out.println(newStack.size());
        System.out.println(newStack.peek());
        System.out.println(newStack.isEmpty());
    }
}
