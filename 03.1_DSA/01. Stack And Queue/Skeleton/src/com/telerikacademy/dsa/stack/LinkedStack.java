package com.telerikacademy.dsa.stack;

import com.telerikacademy.dsa.Node;

import java.util.NoSuchElementException;

public class LinkedStack<E> implements Stack<E> {
    private Node<E> top;
    private int size;

    public LinkedStack(Node<E> top) {
        this.size = 0;
        this.top = new Node<>();
    }

    public LinkedStack() {

    }

    /**
     * Adds <code>element</code> to the end of LinkedStack.
     *
     * @param element The element to add
     */
    @Override
    public void push(E element) {
        Node<E> newNode = new Node<>();
        newNode.data = element;
        newNode.next = top;
        top = newNode;
        size++;
    }

    /**
     * Removes the last added element from the LinkedStack.
     *
     * @return the removed element from the stack.
     */
    @Override
    public E pop() {
        if (isEmpty()) {
            throw new NoSuchElementException("Stack is empty!");
        }

        E value = top.data;
        top = top.next;
        size--;
        return value;
    }

    /**
     * Shows the element that was added last to the LinkedStack.
     */
    @Override
    public E peek() {
        if (isEmpty()) {
            throw new NoSuchElementException("Stack is empty!");
        }
        return top.data;
    }

    /**
     * Shows the number of elements currently present in the LinkedStack.
     *
     * @return the size of the LinkedStack.
     */
    @Override
    public int size() {
        if (isEmpty()) {
            return 0;
        }
        return size;
    }

    /**
     * Checks if the LinkedStack contains any elements.
     *
     * @return True if LinkedStack is empty or False if it is not.
     */
    @Override
    public boolean isEmpty() {
        return top == null;
    }
}
