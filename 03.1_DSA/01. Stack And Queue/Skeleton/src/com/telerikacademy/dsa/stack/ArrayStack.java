package com.telerikacademy.dsa.stack;

import java.util.Arrays;
import java.util.NoSuchElementException;

public class ArrayStack<E> implements Stack<E> {
    private E[] data;

    private static final int DEFAULT_INITIAL_CAPACITY = 10;

    /**
     * The counter showing the index of the last element of the array `elements`.
     */
    private int top;

    public ArrayStack() {
        this.top = 0;
        this.data = (E[]) new Object[DEFAULT_INITIAL_CAPACITY];
    }

    /**
     * Adds <code>element</code> to the end of ArrayStack.
     *
     * @param element The element to add
     */
    @Override
    public void push(E element) {
        if (top == data.length) {
            this.data = Arrays.copyOf(this.data, size() * 2);
        }
        this.data[top++] = element;

    }

    /**
     * Removes the last added element from the ArrayStack.
     *
     * @return the removed element from the stack.
     */
    @Override
    public E pop() {
        if (isEmpty()) {
            throw new NoSuchElementException("Stack is empty!");
        }
        E temp = data[top - 1];
        data[top - 1] = null;
        top--;
        return temp;
    }

    /**
     * Shows the element that was added last to the ArrayStack.
     */
    @Override
    public E peek() {
        if (isEmpty()) {
            throw new NoSuchElementException("Stack is empty!");
        }
        return data[top - 1];
    }

    /**
     * Shows the number of elements currently present in the ArrayStack.
     *
     * @return the size of the ArrayStack.
     */
    @Override
    public int size() {
        if (isEmpty()) {
            return 0;
        }
        return top;
    }

    /**
     * Checks if the ArrayStack contains any elements.
     *
     * @return True if ArrayStack is empty or False if it is not.
     */
    @Override
    public boolean isEmpty() {
        return top ==0;
    }
}
