package com.telerikacademy.dsa.queue;

import java.util.Arrays;
import java.util.NoSuchElementException;

public class ArrayQueue<E> implements Queue<E> {
    private E[] data;
    private int head, tail, size;

    private static final int DEFAULT_INITIAL_CAPACITY = 10;

    public ArrayQueue() {
        this.head = 0;
        this.tail = 0;
        this.size=0;
        this.data = (E[]) new Object[DEFAULT_INITIAL_CAPACITY];
    }

    @Override
    public void enqueue(E element) {
        if (tail == data.length) {
            this.data = Arrays.copyOf(this.data, size() * 2);
        }

        this.data[tail++] = element;
    }

    @Override
    public E dequeue() {
        if (isEmpty()) {
            throw new NoSuchElementException("Queue is empty!");
        }
        E temp = data[tail - 1];
//        data[top - 1] = null;
//        top--;
       return temp;
    }

    @Override
    public E peek() {
        if (isEmpty()) {
            throw new NoSuchElementException("Queue is empty!");
        }
        return data[head];
    }

    @Override
    public int size() {
        if (isEmpty()) {
            return 0;
        }
        return tail;
    }

    @Override
    public boolean isEmpty() {
       return tail == 0;
    }

}
