package com.telerikacademy.dsa.queue;

import com.telerikacademy.dsa.Node;

public class LinkedQueue<E> implements Queue<E> {
    private Node<E> head, tail;
    private int size;

    public LinkedQueue() {
    size=0;
    }

    @Override
    public void enqueue(E element) {
        Node<E> newNode = new Node<>();
        newNode.data = element;
        newNode.next = tail;


    }

    @Override
    public E dequeue() {
        throw new UnsupportedOperationException();
    }

    @Override
    public E peek() {
        if(isEmpty()){
            throw new IllegalArgumentException("Queue is empty");
        }
       return head.data;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return tail==null;
    }
}
