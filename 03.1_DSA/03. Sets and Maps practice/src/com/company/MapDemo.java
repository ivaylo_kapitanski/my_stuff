package com.company;

import java.util.*;
import java.util.stream.Collectors;

public class MapDemo {
    public static void main(String[] args) {

        Map<String, String> phonebook = new HashMap<>();
        phonebook.put("Joey", "0123");
        phonebook.put("Steve", "4567");
        phonebook.put("Tom", "7890");
        phonebook.put("David", "111111");

        System.out.println(phonebook.get("Tom"));
        System.out.println(phonebook.getOrDefault("Carl", "No Number"));
        phonebook.putIfAbsent("Pesho", "123456789");

        System.out.println(phonebook);

        Collection<String> numbersWith78 = phonebook.values().stream()
                .filter(value -> value.startsWith("78")).collect(Collectors.toList());

        System.out.println(numbersWith78);

        Collection<String> namesWithT = phonebook.keySet().stream().filter(key -> key.startsWith("T"))
                .collect(Collectors.toList());

        System.out.println(namesWithT);

        System.out.println("******");

        Map<Person,String> peopleFavColors=new TreeMap<>();
        peopleFavColors.put(new Person("Amy"),"Blue");
        peopleFavColors.put(new Person("Zoey"),"Green");
        peopleFavColors.put(new Person("Andy"),"Yellow");


        System.out.println(peopleFavColors);
    }
}
