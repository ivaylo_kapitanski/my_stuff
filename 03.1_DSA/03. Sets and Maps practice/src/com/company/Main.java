package com.company;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {

	Set<String> nameSet=new HashSet<>();
	nameSet.add("Joey");
	nameSet.add("Ross");
	nameSet.add("Chandler");

        System.out.println("NameSet size1:"+nameSet.size());

        nameSet.add("Ross");
        System.out.println("NameSet size2:"+nameSet.size());

        System.out.println("Is Ross added?:"+nameSet.add("Ross"));
        System.out.println("Is Monica added?:"+nameSet.add("Monica"));
        System.out.println("Is Phoebe in the set?:"+nameSet.contains("Phoebe"));
        System.out.println("Is Ross removed?:"+nameSet.remove("Ross"));

       List<String> nameSetList= nameSet.stream().filter(name->name.contains("e"))
                .collect(Collectors.toList());

        System.out.println(nameSetList);

        List<Character> characters=List.of('a','b','c','c','b','d');

        Set<Character> charactersSet=characters
                .stream().collect(Collectors.toSet());
        System.out.println(charactersSet);

        System.out.println("********");


        Set<String> treeSet=new TreeSet<>(nameSet);

        System.out.println(treeSet);

        Set<Person> personSet=new TreeSet<>();

        personSet.add(new Person("Pesho"));
        personSet.add(new Person("Gosho"));
        personSet.add(new Person("Tosho"));

        System.out.println(personSet);

    }
}
