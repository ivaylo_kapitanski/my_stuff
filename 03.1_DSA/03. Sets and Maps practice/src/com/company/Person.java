package com.company;

import java.util.Comparator;

public class Person implements Comparable<Person> {
    private String name;
//    private String color;
//
//    public Person(String name,String color) {
//        this.name = name;
//        this.color=color;
//    }
    public Person(String name) {
        this.name = name;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("Person {name=%s}",getName());
    }

    @Override
    public int compareTo(Person o) {
        return Comparator.comparing(Person::getName).compare(this,o);
    }
}
