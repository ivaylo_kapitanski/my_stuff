package com.telerikacademy.dsa;

import java.io.File;
import java.util.*;

public class FilesUtils {
    public static void traverseDirectories(String path) {
        File file = new File(path);
        if (file.isDirectory()) {
            traverseDirectoriesWithNesting(file, 0);
        } else {
            System.out.printf("\t%s%n", file.getName());
        }
    }

    public static List<String> findFiles(String path, String extension) {
        File file = new File(path);
        ArrayList<String> files = new ArrayList<>();

        for (File currentFile : file.listFiles()) {

            if (currentFile.isDirectory()) {
                files.addAll(findFiles(currentFile.getPath(), extension));
            } else {
                if (currentFile.getName().endsWith(extension)) {
                    files.add(currentFile.getName());
                }
            }
        }
        return files;
    }

    public static boolean fileExists(String path, String fileName) {
        File file = new File(path);
        if (file.isFile()) {
            return file.getName().equals(fileName);
        }
        for (File listFile : file.listFiles()) {
            if (fileExists(listFile.getPath(), fileName))
                return true;
        }
        return false;
    }

    public static Map<String, Integer> getDirectoryStats(String path) {
        File file = new File(path);
        Map<String, Integer> fileMap = new HashMap<>();

        for (File currentFile : file.listFiles()) {

            if (currentFile.isFile()) {
                int count = fileMap.getOrDefault(getFileExtension(currentFile.getName()), 0);
                fileMap.put(getFileExtension(currentFile.getName()), count + 1);
            } else {
                fileMap.putAll(getDirectoryStats(currentFile.getPath()));
            }
        }
        return fileMap;


    }

    private static String getFileExtension(String fileName) {
        int lastDot = fileName.lastIndexOf(".");
        return fileName.substring(lastDot + 1);

    }

    private static void traverseDirectoriesWithNesting(File dir, int level) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < level; i++) {
            builder.append("\t");
        }
        System.out.printf("%s%s:%n", builder, dir.getName());
        List<File> directories = new ArrayList<>();
        for (File listFile : dir.listFiles()) {
            if (listFile.isDirectory())
                directories.add(listFile);
            else
                System.out.printf("\t%s%s%n", builder.toString(), listFile.getName());
        }
        for (File directory : directories) {
            traverseDirectoriesWithNesting(directory, level + 1);
        }
    }
}