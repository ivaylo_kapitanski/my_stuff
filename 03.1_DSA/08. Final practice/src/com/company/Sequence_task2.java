package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Sequence_task2 {
    public static void main(String[] args) throws IOException {
        BufferedReader br  =new BufferedReader(new InputStreamReader(System.in));

        String[] input=(br.readLine().split(" "));

        int k=Integer.parseInt(input[0]);
        int target=Integer.parseInt((input[1]));

        int[] storage=new int[target];
        storage[0]=k;

        int[] current=new int[1];
        current[0]=k;
        int targetChaser=1;
        int countTo3=0;
        int trackCurrent=0;

        solve(target,storage,current,targetChaser,countTo3,trackCurrent);
    }

    private static void solve(int target, int[] storage, int[] current,int targetChaser,int countTo3,int trackCurrent) {
        if(targetIsReached(target,targetChaser)){
            System.out.println(storage[target-1]);
            return;
        }
        if(countTo3>=3){
            trackCurrent++;
            current[0]=storage[trackCurrent];
            countTo3=0;

        }
        firstCalculation(storage,current,targetChaser);

        targetChaser++;
        countTo3++;
        if(targetIsReached(target,targetChaser)){
            System.out.println(storage[target-1]);
            return;
        }
        secondCalculation(storage,current,targetChaser);

        targetChaser++;
        countTo3++;
        if(targetIsReached(target,targetChaser)){
            System.out.println(storage[target-1]);
            return;
        }

        thirdCalculation(storage,current,targetChaser);

        targetChaser++;
        countTo3++;

        if(targetIsReached(target,targetChaser)){
            System.out.println(storage[target-1]);
            return;
        }
        solve(target,storage,current,targetChaser,countTo3,trackCurrent);

    }

    private static boolean targetIsReached(int target, int targetChaser){
        return (target==targetChaser);
    }

    private static void firstCalculation(int[] storage, int[] current, int targetChaser){
        storage[targetChaser]=current[0]+1;
    }
    private static void secondCalculation(int[] storage,int[] current,int targetChaser){
        storage[targetChaser]=2*current[0]+1;
    }
    private static void thirdCalculation(int[] storage,int[] current,int targetChaser){
        storage[targetChaser]=current[0]+2;
    }

}
