package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Doctors_office_task3 {
    public static void main(String[] args) throws IOException {

        BufferedReader br   =new BufferedReader(new InputStreamReader(System.in));

        ArrayList<String> theQueue=new ArrayList<>();
        HashMap<String,Integer> getNames=new HashMap<>();


        while(true){
            String[] commandLine=br.readLine().split(" ");

            switch (commandLine[0]){
                case "Append":
                    addToQueue(commandLine[1],theQueue,getNames);
                    break;
                case "Examine":
                    examineFromQueue(Integer.parseInt(commandLine[1]),theQueue,getNames);
                    break;
                case "Find":
                    findPerson(commandLine[1],getNames);
                    break;
                case "Insert":
                    insertPerson(Integer.parseInt(commandLine[1]),commandLine[2],theQueue,getNames);
                    break;
                case "End":
                    return;

            }
        }
    }

    private static void insertPerson(int position,String name, ArrayList<String> theQueue,
                                     Map<String, Integer> getNames) {

        if(position> theQueue.size()){
            System.out.println("Error");
            return;
        }

        theQueue.add(position,name);

        System.out.println("OK");

        getNames.put(name, getNames.getOrDefault(name,0)+1);
    }

    private static void findPerson(String personToFind, HashMap<String, Integer> getNames) {
        if(!getNames.containsKey(personToFind)){
            System.out.println("0");
            return;
        }
        System.out.println(getNames.get(personToFind));
    }

    private static void examineFromQueue(int numOfPatients, ArrayList<String> theQueue, HashMap<String, Integer> getNames) {
        if(numOfPatients>theQueue.size()){
            System.out.println("Error");
            return;
        }
        for (int i = 0; i <numOfPatients ; i++) {
            getNames.put(theQueue.get(0), getNames.get(theQueue.get(0))-1);

            System.out.print(theQueue.get(0)+" ");
            theQueue.remove(0);

        }
        System.out.println();

    }

    private static void addToQueue(String name, ArrayList<String> theQueue, HashMap<String, Integer> getNames) {
        theQueue.add(name);
        System.out.println("OK");

        getNames.put(name, getNames.getOrDefault(name,0)+1);

    }
}
