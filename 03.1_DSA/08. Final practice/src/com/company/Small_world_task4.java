package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Small_world_task4 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int[] mazeSize = Arrays.stream(br.readLine().split(" ")).mapToInt(Integer::parseInt).toArray();

        int[][] maze = new int[mazeSize[0]][mazeSize[1]];

        for (int row = 0; row < mazeSize[0]; row++) {
        String[] temp=br.readLine().split("");
            for (int col = 0; col < temp.length; col++) {
                maze[row][col] = Integer.parseInt(temp[col]);
            }
        }
        boolean[][] memoMaze=new boolean[mazeSize[0]][mazeSize[1]];
        ArrayList<Integer> conquestSize=new ArrayList<>();

        for (int row = 0; row < maze.length; row++) {
            for (int col = 0; col < maze[row].length; col++) {
                int currentConquest=checkRegion(maze, row, col, memoMaze);
                if(currentConquest!=0) {
                    conquestSize.add(currentConquest);
                }
            }
        }
        conquestSize.sort(Collections.reverseOrder());
        for (Integer integer : conquestSize) {
            System.out.println(integer);
        }

    }
    private static int checkRegion(int[][] maze, int rowInput, int colInput,boolean[][] memoMaze) {

        if (isOutOfMaze(maze, rowInput, colInput)) {
            return 0;
        }

        if (isVisited(rowInput, colInput,memoMaze)) {
            return 0;
        }
        if(maze[rowInput][colInput] !=1){
            return 0;
        }

        memoMaze[rowInput][colInput] = true;
        int size = 1;

        size+= checkRegion(maze, rowInput, colInput-1,memoMaze);
        size+= checkRegion(maze, rowInput, colInput+1,memoMaze);
        size+= checkRegion(maze, rowInput+1, colInput,memoMaze);
        size+= checkRegion(maze, rowInput-1, colInput,memoMaze);

        return size;

    }

    private static boolean isOutOfMaze(int[][] maze, int row, int col) {
        return (row<0||col<0||row>=maze.length||col>=maze[0].length);

    }

    private static boolean isVisited(int row, int col,boolean[][] memoMaze) {
        return memoMaze[row][col] == true;
    }
}
