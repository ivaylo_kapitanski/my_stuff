package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayDeque;
import java.util.Queue;

public class Cafe_task1 {

    public static void main(String[] args) throws IOException {
        BufferedReader br  =new BufferedReader(new InputStreamReader(System.in));

        int total=Integer.parseInt(br.readLine());
        String[] cafeLine=br.readLine().split(" ");
        Queue<String> sergants=new ArrayDeque<>();
        Queue<String> corporals=new ArrayDeque<>();
        Queue<String> privates=new ArrayDeque<>();


        for (int i = 0; i < cafeLine.length ; i++) {
            if(cafeLine[i].contains("S")){
                sergants.offer(cafeLine[i]);
            }else if(cafeLine[i].contains("C")){
                corporals.offer(cafeLine[i]);
            }else {
                privates.offer(cafeLine[i]);
            }
        }
      print(sergants);
        print(corporals);
        print(privates);


    }
    private static void print(Queue<String> input){
        if(!input.isEmpty()){
            for (String s : input) {
                System.out.print(s+" ");
            }
        }
    }


}
