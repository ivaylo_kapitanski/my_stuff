package com.telerikacademy.dsa;


import java.util.*;

public class BinarySearchTreeImpl<E extends Comparable<E>> implements BinarySearchTree<E> {
    private E value;
    private BinarySearchTreeImpl<E> left;
    private BinarySearchTreeImpl<E> right;


    public BinarySearchTreeImpl(E value) {
        this.value = value;

    }

    public BinarySearchTreeImpl(BSTIterator<E> iterable) {
        iterable.queue.forEach(item -> this.insert(item));
    }


    @Override
    public E getRootValue() {
        return value;
    }

    @Override
    public BinarySearchTree<E> getLeftTree() {
        return left;
    }

    @Override
    public BinarySearchTree<E> getRightTree() {
        return right;
    }

    @Override
    public void insert(E value) {
        BinarySearchTreeImpl<E> newNode = new BinarySearchTreeImpl<>(value);
        insertNode(this, newNode);

    }

    @Override
    public boolean search(E value) {
        BinarySearchTreeImpl<E> searched = new BinarySearchTreeImpl<>(value);

        return searchRec(this, searched);
    }

    @Override
    public List<E> inOrder() {
        List<E> tempList = new ArrayList<>();
        inOrderRec(tempList, this);
        return tempList;
    }

    @Override
    public List<E> postOrder() {
        List<E> tempList = new ArrayList<>();
        postOrderRec(tempList, this);
        return tempList;
    }

    @Override
    public List<E> preOrder() {
        List<E> tempList = new ArrayList<>();
        preOrderRec(tempList, this);
        return tempList;
    }

    @Override
    public List<E> bfs() {

        List<E> tempList = new ArrayList<>();
        Queue<BinarySearchTreeImpl<E>> queue = new LinkedList<>();
        queue.offer(this);
        bfsRec(tempList, this, queue);

        return tempList;

    }

    @Override
    public int height() {
        int left = 0;
        int right = 0;

        return heightRec(this, left, right);
    }

    // Advanced task: implement remove method. To test, uncomment the commented tests in BinaryTreeImplTests
//    @Override
//    public boolean remove(E value) {
//        throw new UnsupportedOperationException();
//    }


    private void bfsRec(List<E> tempList, BinarySearchTreeImpl<E> root, Queue<BinarySearchTreeImpl<E>> queue) {
        if (root == null) {
            return;
        }

        while (!queue.isEmpty()) {
            BinarySearchTreeImpl<E> current = queue.poll();
            tempList.add(current.value);

            if (current.left != null) {
                queue.offer(current.left);
            }
            if (current.right != null) {
                queue.offer(current.right);
            }
        }
    }

    private void inOrderRec(List<E> tempList, BinarySearchTreeImpl<E> root) {
        if (root == null) {
            return;
        }
        inOrderRec(tempList, root.left);
        tempList.add(root.value);
        inOrderRec(tempList, root.right);

    }

    private void preOrderRec(List<E> tempList, BinarySearchTreeImpl<E> root) {
        if (root == null) {
            return;
        }
        tempList.add(root.value);
        preOrderRec(tempList, root.left);
        preOrderRec(tempList, root.right);

    }

    private void insertNode(BinarySearchTreeImpl<E> root, BinarySearchTreeImpl<E> newNode) {

        if (newNode.value.compareTo(root.value) < 0) {
            if (root.left == null) {
                root.left = newNode;

            } else {
                insertNode(root.left, newNode);
            }

        } else if (newNode.value.compareTo(root.value) > 0) {
            if (root.right == null) {
                root.right = newNode;
            } else {
                insertNode(root.right, newNode);
            }
        }
    }

    private void postOrderRec(List<E> tempList, BinarySearchTreeImpl<E> root) {
        if (root == null) {
            return;
        }
        postOrderRec(tempList, root.left);
        postOrderRec(tempList, root.right);
        tempList.add(root.value);
    }

    private boolean searchRec(BinarySearchTreeImpl<E> root, BinarySearchTreeImpl<E> searched) {
        if (root == null) {
            return false;
        }

        if (searched.value == root.value) {
            return true;
        }

        return searched.value.compareTo(root.value) < 0 ?
                searchRec(root.left, searched)
                : searchRec(root.right, searched);
    }

    private class BSTIterator<E extends Comparable<E>> implements Iterator<E> {

        Queue<E> queue;

        public BSTIterator(BinarySearchTreeImpl<E> root) {
            queue = new LinkedList<>();
            inOrderIterator(root);

        }

        private void inOrderIterator(BinarySearchTreeImpl<E> root) {
            if (root == null) {
                return;
            }
            inOrderIterator(root.left);
            queue.offer(root.value);
            inOrderIterator(root.right);

        }

        @Override
        public boolean hasNext() {
            return queue.peek() != null;
        }

        @Override
        public E next() {
            return queue.poll();
        }
    }

    private int heightRec(BinarySearchTreeImpl<E> root, int left, int right) {
        if (root == null) {
            return -1;
        }
        left = heightRec(root.left, left, right);
        right = heightRec(root.right, left, right);

        return left < right ? right + 1 : left + 1;
    }

}



