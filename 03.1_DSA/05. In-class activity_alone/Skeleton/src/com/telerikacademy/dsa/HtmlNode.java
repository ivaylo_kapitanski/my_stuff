package com.telerikacademy.dsa;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class HtmlNode {
    private String name;
    private List<HtmlAttribute> attributes;
    private List<HtmlNode> childNodes;

    public HtmlNode(String name) {
        this.name = name;
        attributes = new ArrayList<>();
        childNodes = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<HtmlAttribute> getAttributes() {
        return attributes;
    }

    public List<HtmlNode> getChildNodes() {
        return childNodes;
    }

    @Override
    public String toString() {
        if (attributes.isEmpty()) {
            return String.format("<%s>", name);

        } else {
            return String.format("<%s ", name) +
                    attributes
                            .stream()
                            .map(HtmlAttribute::toString)
                            .collect(Collectors.joining(" ")) +">";
        }


//
//        return (!attributes.isEmpty()) ? new StringBuilder(String.format("<%s ",name))
//                .append(attributes
//                        .stream()
//                        .map(HtmlAttribute::toString)
//                        .collect(Collectors.joining(" ")))
//                .append(String.format(">")).toString()
//                : String.format("<%s>", name);
    }
}
