package com.telerikacademy.dsa;

public class SolveExpression2 {
    public static void main(String[] args) {
        System.out.println(solveExpression("45+(55)"));
        System.out.println(solveExpression("45+(24*(12+3))"));
        System.out.println(solveExpression("12*(35-(46*(5+15)))"));
    }

    public static int solveExpression(String entry) {
        int signCounter;
        int nested = 0;

        if (entry.charAt(0) == '(') {
            return solveExpression(entry.substring(1, entry.length() - 1));
        }

        for (int i = 0; i < entry.length(); i++) {
            char ch = entry.charAt(i);
            if (Character.isDigit(ch)) {
                continue;
            }

            if (ch == '(') {
                nested++;
            }
            if (ch == ')') {
                nested--;
            }

            if (ch == '+' && nested == 0) {
                signCounter = i;
                return getResultOfOperation(entry, signCounter, ch);
            }

            if (ch == '*' && nested == 0) {
                signCounter = i;
                return getResultOfOperation(entry, signCounter, ch);
            }

            if (ch == '/' && nested == 0) {
                signCounter = i;

                return getResultOfOperation(entry, signCounter, ch);
            }

            if (ch == '-' && nested == 0) {
                signCounter = i;
                return getResultOfOperation(entry, signCounter, ch);
            }
        }


            return Integer.parseInt(entry);
    }

    private static int getResultOfOperation(String entry, int signCounter, char sign) {

        String beforeSign = entry.substring(0, signCounter);
        String afterSign = entry.substring(signCounter + 1);

        int resultBeforeSign = solveExpression(beforeSign);
        int remainingPart = solveExpression(afterSign);

        if (sign == '-') {
            return resultBeforeSign - remainingPart;
        } else if (sign == '+') {
            return resultBeforeSign + remainingPart;
        } else if (sign == '*') {
            return resultBeforeSign * remainingPart;
        } else {
            return resultBeforeSign / remainingPart;
        }
    }
}
