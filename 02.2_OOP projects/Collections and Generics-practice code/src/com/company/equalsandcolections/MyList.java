package com.company.equalsandcolections;

public interface MyList extends Iterable<Person>{
    void add(Person element);

    Person get(int index);

    boolean contains(Person person);
}
