package com.company.equalsandcolections;

public class Main {
    public static void main(String[] args) {
      MyList people=new MyPersonList();
      Person pesho=new Person("Pesho",34);
      Person pesho2=new Person("Pesho",34);
      Person gosho=new Person("Gosho",45);
      people.add(pesho);
      if(!people.contains(pesho2)){
          people.add(pesho2);
      }
      people.add(gosho);

        for (Person person : people) {
            System.out.println(person);
        }
    }
}
