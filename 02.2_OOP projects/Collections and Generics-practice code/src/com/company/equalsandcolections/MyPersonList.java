package com.company.equalsandcolections;

import java.util.Iterator;

public class MyPersonList implements MyList {

    private static final int CAPACITY = 0;
    private final Person[]elements;
private int size;

    public MyPersonList() {
      elements=new Person[5];
      size= CAPACITY;
    }

    @Override
    public void add(Person element) {
        elements[size]=element;
        size++;

    }

    @Override
    public Person get(int index) {
        return elements[index];
    }

    @Override
    public boolean contains(Person person) {
        for (int i = 0; i <size ; i++) {
            if(elements[i].equals(person)){
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<Person> iterator() {
        return new MyArrayListIterator();
    }

    private class MyArrayListIterator implements Iterator<Person    >{
        private int currentIndex;

        public MyArrayListIterator(){
            currentIndex=0;

        }

        @Override
        public boolean hasNext() {
            return currentIndex<size;
        }

        @Override
        public Person next() {
            Person currentElement=elements[currentIndex];
            currentIndex++;
            return currentElement;
        }
    }
}
