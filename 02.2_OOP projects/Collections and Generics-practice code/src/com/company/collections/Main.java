package com.company.collections;

public class Main {
    public static void main(String[] args) {
        MyList list = new MyIntegerList();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        System.out.printf("Element at index 1: %d%n",list.get(1));

        System.out.println("All elements: ");
        for (int integer : list) {
            System.out.print(integer+" ");
        }
        System.out.println();
    }
}
