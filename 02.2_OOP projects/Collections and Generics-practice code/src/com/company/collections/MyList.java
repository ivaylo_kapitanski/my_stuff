package com.company.collections;

public interface MyList extends Iterable<Integer>{
    void add(int element);

    int get(int index);
}
