package com.company.generics;

public class Person implements Comparable<Person> {
    private final String name;
    private final int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public boolean equals(Object obj) {
        if(this==obj){
            return true;
        }
        if(!(obj instanceof Person)){
            return false;
        }
        Person person=(Person)obj;
        return age==person.age && name.equals(person.name);
    }

    @Override
    public String toString() {
        return String.format("Person{name= %s, age= %d}",name,age);
    }

    @Override
    public int compareTo(Person obj) {
        return Integer.compare(age,obj.age);
    }
}
