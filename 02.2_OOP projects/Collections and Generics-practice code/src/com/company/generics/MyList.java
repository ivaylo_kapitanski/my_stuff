package com.company.generics;

public interface MyList<E> extends Iterable<E>{
    void add(E element);

    E get(int index);
}
