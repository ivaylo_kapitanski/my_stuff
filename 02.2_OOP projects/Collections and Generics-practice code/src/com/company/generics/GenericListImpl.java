package com.company.generics;

import java.util.Iterator;
import java.util.Objects;

public class GenericListImpl<E> implements MyList<E> {

    private static final int CAPACITY = 5;
    private final E[]elements;
private int size;

    public GenericListImpl() {

      elements=(E[])new Object[CAPACITY];
      size= 0;
    }

    @Override
    public void add(E element) {
        elements[size]=element;
        size++;

    }

    @Override
    public E get(int index) {
        return elements[index];
    }

    @Override
    public Iterator<E> iterator() {
        return new MyArrayListIterator();
    }

    private class MyArrayListIterator implements Iterator<E>{
        private int currentIndex;

        public MyArrayListIterator(){
            currentIndex=0;

        }

        @Override
        public boolean hasNext() {
            return currentIndex<size;
        }

        @Override
        public E next() {
            E currentElement=elements[currentIndex];
            currentIndex++;
            return currentElement;
        }
    }
}
