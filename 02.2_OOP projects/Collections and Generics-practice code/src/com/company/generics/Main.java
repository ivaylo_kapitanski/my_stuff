package com.company.generics;


public class Main {
    public static void main(String[] args) {
        MyList<Person> people = new GenericListImpl<>();
        Person pesho = new Person("Pesho", 34);
        Person gosho = new Person("Gosho", 45);

        people.add(pesho);

        people.add(gosho);

        for (Person person : people) {
            System.out.println(person);
        }
        if(pesho.compareTo(gosho)<0){
            System.out.println(pesho+" is younger than "+gosho);
        }
    }
}

