package com.telerikacademy.oop.dealership.utils;

import com.telerikacademy.dealership.models.*;
import com.telerikacademy.oop.dealership.models.*;
import com.telerikacademy.oop.dealership.models.common.enums.Role;
import com.telerikacademy.dealership.models.contracts.*;
import com.telerikacademy.oop.dealership.models.contracts.*;

public class Factory {
    
    public static Car createCar() {
        return new CarImpl("Make", "Model", 20, 4);
    }
    
    public static User createNormalUser() {
        return new UserImpl("username", "firstname", "lastname", "password", Role.NORMAL);
    }
    
    public static Motorcycle createMotorcycle() {
        return new MotorcycleImpl("Kawasaki", "Z1000", 2500, "Race");
    }
    
    public static Truck createTruck() {
        return new TruckImpl("Volvo", "FH4", 11800, 50);
    }
    
    public static Comment createComment(User user) {
        return new CommentImpl("{{I like this vehicle!}}", user.getUsername());
    }
    
}
