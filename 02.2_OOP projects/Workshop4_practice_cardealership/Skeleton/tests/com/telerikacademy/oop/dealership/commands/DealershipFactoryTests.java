package com.telerikacademy.oop.dealership.commands;

import com.telerikacademy.oop.dealership.commands.contracts.Command;
import com.telerikacademy.oop.dealership.core.DealershipRepositoryImpl;
import com.telerikacademy.oop.dealership.core.contracts.CommandFactory;
import com.telerikacademy.oop.dealership.core.contracts.DealershipFactory;
import com.telerikacademy.oop.dealership.core.contracts.DealershipRepository;
import com.telerikacademy.oop.dealership.core.factories.CommandFactoryImpl;
import com.telerikacademy.oop.dealership.core.factories.DealershipFactoryImpl;
import com.telerikacademy.dealership.models.contracts.*;
import com.telerikacademy.oop.dealership.models.contracts.*;
import com.telerikacademy.oop.dealership.utils.Factory;
import com.telerikacademy.oop.dealership.utils.Mapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;


public class DealershipFactoryTests {
    
    private DealershipRepository dealershipRepository;
    private DealershipFactory dealershipFactory;
    private CommandFactory commandFactory;
    
    @BeforeEach
    public void before() {
        this.commandFactory = new CommandFactoryImpl();
        this.dealershipFactory = new DealershipFactoryImpl();
        this.dealershipRepository = new DealershipRepositoryImpl();
    }
    
    @Test
    public void createCar_ShouldCreate_WhenInputIsValid() {
        logInUser(Factory.createNormalUser());
        
        // Act
        Command createVehicle = commandFactory.createCommand("AddVehicle", dealershipFactory, dealershipRepository);
        Car testCar = Factory.createCar();
        List<String> params = Mapper.mapCarToParamsList(testCar);
        
        // Act
        createVehicle.execute(params);
        
        //Assert
        Car toCompare = (Car) dealershipRepository.getLoggedUser().getVehicles().get(0);
        Assertions.assertEquals(testCar.getMake(), toCompare.getMake());
        Assertions.assertEquals(testCar.getSeats(), toCompare.getSeats());
        
    }
    
    @Test
    public void createMotorcycle_ShouldCreate_WhenInputIsValid() {
        logInUser(Factory.createNormalUser());
        
        // Act
        Command createVehicle = commandFactory.createCommand("AddVehicle", dealershipFactory, dealershipRepository);
        Motorcycle testMotorcycle = Factory.createMotorcycle();
        List<String> params = Mapper.mapMotorcycleToParamsList(testMotorcycle);
        
        // Act
        createVehicle.execute(params);
        
        //Assert
        Motorcycle toCompare = (Motorcycle) dealershipRepository.getLoggedUser().getVehicles().get(0);
        Assertions.assertEquals(testMotorcycle.getMake(), toCompare.getMake());
        Assertions.assertEquals(testMotorcycle.getCategory(), toCompare.getCategory());
        
    }
    
    @Test
    public void createTruck_ShouldCreate_WhenInputIsValid() {
        logInUser(Factory.createNormalUser());
        
        // Act
        Command createVehicle = commandFactory.createCommand("AddVehicle", dealershipFactory, dealershipRepository);
        Truck testTruck = Factory.createTruck();
        List<String> params = Mapper.mapTruckToParamsList(testTruck);
        
        // Act
        createVehicle.execute(params);
        
        //Assert
        Truck toCompare = (Truck) dealershipRepository.getLoggedUser().getVehicles().get(0);
        Assertions.assertEquals(testTruck.getMake(), toCompare.getMake());
        Assertions.assertEquals(testTruck.getWeightCapacity(), toCompare.getWeightCapacity());
        
    }
    
    @Test
    public void createUser_ShouldCreate_WhenInputIsValid() {
        // Act
        Command createVehicle = commandFactory.createCommand("RegisterUser",
                dealershipFactory,
                dealershipRepository);
        User testUser = Factory.createNormalUser();
        List<String> params = Mapper.mapUserToParamsList(testUser);
        
        // Act
        createVehicle.execute(params);
        
        //Assert
        User toCompare = dealershipRepository.getUsers().get(0);
        Assertions.assertEquals(toCompare.getUsername(), testUser.getUsername());
    }
    
    @Test
    public void createComment_ShouldCreate_WhenInputIsValid() {
        User user = Factory.createNormalUser();
        Vehicle vehicle = Factory.createCar();
        logInUser(user);
        addVehicleToUser(user, vehicle);
        
        // Act
        Command createVehicle = commandFactory.createCommand("AddComment",
                dealershipFactory,
                dealershipRepository);
        Comment testComment = Factory.createComment(user);
        List<String> params = Mapper.mapCommentToParamsList(testComment);
        
        // Act
        createVehicle.execute(params);
        
        //Assert
        Comment toCompare = dealershipRepository.getLoggedUser().getVehicles().get(0).getComments().get(0);
        Assertions.assertEquals(toCompare.getContent(), testComment.getContent());
        Assertions.assertEquals(toCompare.getAuthor(), testComment.getAuthor());
    }
    
    private void logInUser(User userToLogIn) {
        dealershipRepository.addUser(userToLogIn);
        Command login = new Login(dealershipFactory, dealershipRepository);
        List<String> params = new ArrayList<>();
        params.add(userToLogIn.getUsername());
        params.add(userToLogIn.getPassword());
        login.execute(params);
    }
    
    private void addVehicleToUser(User user, Vehicle vehicle) {
        Command createVehicle = commandFactory.createCommand("AddVehicle",
                dealershipFactory,
                dealershipRepository);
        Car testCar = Factory.createCar();
        List<String> params = Mapper.mapCarToParamsList(testCar);
        
        createVehicle.execute(params);
    }
    
}
