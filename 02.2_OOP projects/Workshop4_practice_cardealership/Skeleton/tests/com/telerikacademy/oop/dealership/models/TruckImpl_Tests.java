package com.telerikacademy.oop.dealership.models;

import com.telerikacademy.oop.dealership.models.contracts.Truck;
import com.telerikacademy.oop.dealership.models.contracts.Vehicle;
import com.telerikacademy.oop.dealership.models.common.Constants;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TruckImpl_Tests {
    
    @Test
    public void TruckImpl_ShouldImplementTruckInterface() {
        TruckImpl truck = new TruckImpl("make", "model", 100, 10);
        Assertions.assertTrue(truck instanceof Truck);
    }
    
    @Test
    public void TruckImpl_ShouldImplementVehicleInterface() {
        TruckImpl truck = new TruckImpl("make", "model", 100, 10);
        Assertions.assertTrue(truck instanceof Vehicle);
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenMakeIsNull() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new TruckImpl(null, "model", 100, 10));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenModelIsNull() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new TruckImpl("make", null, 100, 10));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenMakeLengthIsBelow2() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new TruckImpl(new String(new char[Constants.MAKE_NAME_LEN_MIN - 1]), "model", 100, 10));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenMakeLengthIsAbove15() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new TruckImpl(new String(new char[Constants.MAKE_NAME_LEN_MAX + 1]), "model", 100, 10));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenModelLengthIsBelow_1() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new TruckImpl("make", new String(new char[Constants.MODEL_NAME_LEN_MIN - 1]), 100, 10));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenModelLengthIsAbove15() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new TruckImpl("make", new String(new char[Constants.MODEL_NAME_LEN_MAX + 1]), 100, 10));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenPriceIsNegative() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new TruckImpl("make", "model", -100, 10));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenPriceIsAbove1000000() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new TruckImpl("make", "model", 1000001, 10));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenWeightCapacityIsNegative() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new TruckImpl("make", "model", 100, -10));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenWeightCapacityIsAbove100() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new TruckImpl("make", "model", 100, 101));
    }
    
}
