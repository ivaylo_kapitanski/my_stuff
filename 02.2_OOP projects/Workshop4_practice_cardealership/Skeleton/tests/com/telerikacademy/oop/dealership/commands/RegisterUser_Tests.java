package com.telerikacademy.oop.dealership.commands;

import com.telerikacademy.oop.dealership.commands.contracts.Command;
import com.telerikacademy.oop.dealership.core.DealershipRepositoryImpl;
import com.telerikacademy.oop.dealership.core.contracts.CommandFactory;
import com.telerikacademy.oop.dealership.core.contracts.DealershipFactory;
import com.telerikacademy.oop.dealership.core.contracts.DealershipRepository;
import com.telerikacademy.oop.dealership.core.factories.CommandFactoryImpl;
import com.telerikacademy.oop.dealership.core.factories.DealershipFactoryImpl;
import com.telerikacademy.oop.dealership.models.contracts.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class RegisterUser_Tests {
    
    private DealershipRepository dealershipRepository;
    private DealershipFactory dealershipFactory;
    private CommandFactory commandFactory;
    
    @BeforeEach
    public void before() {
        this.commandFactory = new CommandFactoryImpl();
        this.dealershipFactory = new DealershipFactoryImpl();
        this.dealershipRepository = new DealershipRepositoryImpl();
    }
    
    @Test
    public void Execute_ShouldRegisterUser_WhenUserDoesNotExist() {
        // Arrange
        Command registerUser = commandFactory.createCommand("registeruser", dealershipFactory, dealershipRepository);
        List<String> params = new ArrayList<>();
        params.add("pesho123");
        params.add("petar");
        params.add("petrov");
        params.add("password");
        
        // Act
        registerUser.execute(params);
        
        // Assert
        User pesho = dealershipRepository.getUsers().get(0);
        Assertions.assertEquals("pesho123", pesho.getUsername());
    }
    
    @Test
    public void Execute_ShouldNotRegisterUser_WhenUserAlreadyExist() {
        // Arrange
        Command registerUser = commandFactory.createCommand("registeruser", dealershipFactory, dealershipRepository);
        List<String> params = new ArrayList<>();
        params.add("pesho123");
        params.add("petar");
        params.add("petrov");
        params.add("password");
        
        // Act
        registerUser.execute(params);
        registerUser.execute(params);
        
        // Assert
        Assertions.assertEquals(1, dealershipRepository.getUsers().size());
    }
    
}
