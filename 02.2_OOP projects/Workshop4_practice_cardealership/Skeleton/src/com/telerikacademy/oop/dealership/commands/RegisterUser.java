package com.telerikacademy.oop.dealership.commands;

import com.telerikacademy.oop.dealership.commands.contracts.Command;
import com.telerikacademy.oop.dealership.core.contracts.DealershipFactory;
import com.telerikacademy.oop.dealership.core.contracts.DealershipRepository;
import com.telerikacademy.oop.dealership.models.common.enums.Role;
import com.telerikacademy.oop.dealership.models.contracts.User;

import java.util.List;

import static com.telerikacademy.oop.dealership.commands.constants.CommandConstants.*;

public class RegisterUser implements Command {

    private final DealershipFactory dealershipFactory;
    private final DealershipRepository dealershipRepository;

    public RegisterUser(DealershipFactory dealershipFactory, DealershipRepository dealershipRepository) {
        this.dealershipFactory = dealershipFactory;
        this.dealershipRepository = dealershipRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        String username = parameters.get(0);
        String firstName = parameters.get(1);
        String lastName = parameters.get(2);
        String password = parameters.get(3);

        Role role = Role.NORMAL;

        if (parameters.size() > NUMBER_OF_PARAMETERS_FOR_NORMAL_ROLE) {
            role = Role.valueOf(parameters.get(4).toUpperCase());
        }

        return registerUser(username, firstName, lastName, password, role);
    }

    private String registerUser(String username, String firstName, String lastName, String password, Role role) {
        if (dealershipRepository.getLoggedUser() != null) {
            return String.format(USER_LOGGED_IN_ALREADY, dealershipRepository.getLoggedUser().getUsername());
        }

        if (dealershipRepository.getUsers().stream().anyMatch(u -> u.getUsername().toLowerCase().equals(username.toLowerCase()))) {

            return String.format(USER_ALREADY_EXIST, username);
        }

        User user = dealershipFactory.createUser(username, firstName, lastName, password, role.toString());
        dealershipRepository.setLoggedUser(user);
        dealershipRepository.addUser(user);

        return String.format(USER_REGISTERED, username);
    }

}
