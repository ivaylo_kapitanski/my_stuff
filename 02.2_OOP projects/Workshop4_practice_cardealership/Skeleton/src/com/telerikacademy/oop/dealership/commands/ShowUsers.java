package com.telerikacademy.oop.dealership.commands;

import com.telerikacademy.oop.dealership.commands.contracts.Command;
import com.telerikacademy.oop.dealership.core.contracts.DealershipFactory;
import com.telerikacademy.oop.dealership.core.contracts.DealershipRepository;
import com.telerikacademy.oop.dealership.models.common.enums.Role;
import com.telerikacademy.oop.dealership.models.contracts.User;

import java.util.List;

import static com.telerikacademy.oop.dealership.commands.constants.CommandConstants.YOU_ARE_NOT_AN_ADMIN;

public class ShowUsers implements Command {

    private final DealershipFactory dealershipFactory;
    private final DealershipRepository dealershipRepository;

    public ShowUsers(DealershipFactory dealershipFactory, DealershipRepository dealershipRepository) {
        this.dealershipFactory = dealershipFactory;
        this.dealershipRepository = dealershipRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        return showAllUsers();
    }

    private String showAllUsers() {
        if (dealershipRepository.getLoggedUser().getRole() != Role.ADMIN) {
            return YOU_ARE_NOT_AN_ADMIN;
        }

        StringBuilder builder = new StringBuilder();
        builder.append("--USERS--").append(System.lineSeparator());
        int counter = 1;
        for (User user : dealershipRepository.getUsers()) {
            builder.append(String.format("%d. %s", counter, user.toString()))
                    .append(System.lineSeparator());
            counter++;
        }

        return builder.toString().trim();
    }

}
