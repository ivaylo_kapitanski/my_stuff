package com.telerikacademy.oop.dealership.models.common.enums;

public enum Role {
    NORMAL,
    VIP,
    ADMIN;

    @Override
    public String toString() {
        switch (this) {
            case ADMIN:
                return "Admin";
            case NORMAL:
                return "Normal";
            case VIP:
                return "VIP";
            default:
                return "";
        }
    }
}
