package com.telerikacademy.oop.dealership.commands;

import com.telerikacademy.oop.dealership.commands.contracts.Command;
import com.telerikacademy.oop.dealership.core.contracts.DealershipFactory;
import com.telerikacademy.oop.dealership.core.contracts.DealershipRepository;
import com.telerikacademy.oop.dealership.models.common.enums.VehicleType;
import com.telerikacademy.oop.dealership.models.contracts.Vehicle;
import com.telerikacademy.oop.dealership.commands.constants.CommandConstants;

import java.util.List;

public class AddVehicle implements Command {
    
    private final DealershipFactory dealershipFactory;
    private final DealershipRepository dealershipRepository;
    
    public AddVehicle(DealershipFactory dealershipFactory, DealershipRepository dealershipRepository) {
        this.dealershipFactory = dealershipFactory;
        this.dealershipRepository = dealershipRepository;
    }
    
    @Override
    public String execute(List<String> parameters) {
        String type = parameters.get(0);
        String make = parameters.get(1);
        String model = parameters.get(2);
        double price = Double.parseDouble((parameters.get(3)));
        String additionalParam = parameters.get(4);
        
        VehicleType typeEnum = VehicleType.valueOf(type.toUpperCase());
        
        return addVehicle(typeEnum, make, model, price, additionalParam);
    }
    
    private String addVehicle(VehicleType type, String make, String model, double price, String additionalParam) {
        Vehicle vehicle = null;
        
        if (type == VehicleType.CAR) {
            vehicle = dealershipFactory.createCar(make, model, price, Integer.parseInt(additionalParam));
        } else if (type == VehicleType.MOTORCYCLE) {
            vehicle = dealershipFactory.createMotorcycle(make, model, price, additionalParam);
        } else if (type == VehicleType.TRUCK) {
            vehicle = dealershipFactory.createTruck(make, model, price, Integer.parseInt(additionalParam));
        }
        
        dealershipRepository.getLoggedUser().addVehicle(vehicle);
        
        return String.format(CommandConstants.VEHICLE_ADDED_SUCCESSFULLY, dealershipRepository.getLoggedUser().getUsername());
    }
    
}
