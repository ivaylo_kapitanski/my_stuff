package com.telerikacademy.oop.dealership.models.common;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {
    
    public static void validateIntRange(int value, int min, int max, String message) {
        if (value < min || value > max) {
            throw new IllegalArgumentException(message);
        }
    }
    
    public static void validateDecimalRange(double value, double min, double max, String message) {
        if (value < min || value > max) {
            throw new IllegalArgumentException(message);
        }
    }
    
    public static void validatePattern(String value, String pattern, String message) {
        Pattern patternToMatch = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
        Matcher matcher = patternToMatch.matcher(value);
        
        if (!matcher.matches()) // returns !true only if the whole value matches the pattern
        //if (m.find()) // if we want to find if there is substring of value that matches the pattern
        {
            throw new IllegalArgumentException(message);
        }
    }
    
    public static void validateArgumentIsNotNull(Object arg, String message) {
        if (arg == null) {
            throw new IllegalArgumentException(message);
        }
    }
    
}
