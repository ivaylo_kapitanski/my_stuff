package com.telerikacademy.oop.dealership.models.contracts;

import com.telerikacademy.oop.dealership.models.common.enums.VehicleType;

public interface Vehicle extends Priceable, Commentable {
    
    int getWheels();
    
    VehicleType getType();
    
    String getMake();
    
    String getModel();
    
}
