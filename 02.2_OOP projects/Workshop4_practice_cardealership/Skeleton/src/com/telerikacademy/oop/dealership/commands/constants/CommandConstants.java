package com.telerikacademy.oop.dealership.commands.constants;

public class CommandConstants {

    public static final String INVALID_COMMAND = "Invalid command! %s";

    public static final String USER_ALREADY_EXIST = "User %s already exist. Choose a different username!";
    public static final String USER_LOGGED_IN_ALREADY = "User %s is logged in! Please log out first!";
    public static final String USER_REGISTERED = "User %s registered successfully!";
    public static final String NO_SUCH_USER = "There is no user with username %s!";
    public static final String USER_LOGGED_OUT = "You logged out!";
    public static final String USER_LOGGED_IN = "User %s successfully logged in!";
    public static final String WRONG_USERNAME_OR_PASSWORD = "Wrong username or password!";
    public static final String YOU_ARE_NOT_AN_ADMIN = "You are not an admin!";

    public static final String COMMENT_ADDED_SUCCESSFULLY = "%s added comment successfully!";
    public static final String COMMENT_REMOVED_SUCCESSFULLY = "%s removed comment successfully!";

    public static final String VEHICLE_REMOVED_SUCCESSFULLY = "%s removed vehicle successfully!";
    public static final String VEHICLE_ADDED_SUCCESSFULLY = "%s added vehicle successfully!";

    public static final String REMOVED_VEHICLE_DOES_NOT_EXIST = "Cannot remove comment! The vehicle does not exist!";
    public static final String REMOVED_COMMENT_DOES_NOT_EXIST = "Cannot remove comment! The comment does not exist!";

    public static final String COMMENT_DOES_NOT_EXIST = "The comment does not exist!";
    public static final String VEHICLE_DOES_NOT_EXIST = "The vehicle does not exist!";
    public static final String REPORT_SEPARATOR = "####################";
    public static final int NUMBER_OF_PARAMETERS_FOR_NORMAL_ROLE = 4;

}
