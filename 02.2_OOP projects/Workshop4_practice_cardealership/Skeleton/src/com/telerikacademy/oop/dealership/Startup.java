package com.telerikacademy.oop.dealership;

import com.telerikacademy.oop.dealership.core.DealershipEngineImpl;

public class Startup {
    
    public static void main(String[] args) {
        DealershipEngineImpl engine = new DealershipEngineImpl();
        engine.start();
    }
    
}
