package com.telerikacademy.oop.dealership.core.factories;

import com.telerikacademy.oop.dealership.core.contracts.DealershipFactory;
import com.telerikacademy.oop.dealership.models.contracts.*;

public class DealershipFactoryImpl implements DealershipFactory {
    
    public Car createCar(String make, String model, double price, int seats) {
        // TODO: Implement this
        throw new UnsupportedOperationException("Not implemented yet.");
    }
    
    public Motorcycle createMotorcycle(String make, String model, double price, String category) {
        // TODO: Implement this
        throw new UnsupportedOperationException("Not implemented yet.");
    }
    
    public Truck createTruck(String make, String model, double price, int weightCapacity) {
        // TODO: Implement this
        throw new UnsupportedOperationException("Not implemented yet.");
    }
    
    public User createUser(String username, String firstName, String lastName, String password, String role) {
        // TODO: Implement this
        throw new UnsupportedOperationException("Not implemented yet.");
    }
    
    public Comment createComment(String content, String author) {
        // TODO: Implement this
        throw new UnsupportedOperationException("Not implemented yet.");
    }
    
}
