package com.telerikacademy.oop.dealership.commands;

import com.telerikacademy.oop.dealership.commands.contracts.Command;
import com.telerikacademy.oop.dealership.core.contracts.DealershipFactory;
import com.telerikacademy.oop.dealership.core.contracts.DealershipRepository;
import com.telerikacademy.oop.dealership.commands.constants.CommandConstants;

import java.util.List;

public class Logout implements Command {
    
    private final DealershipFactory dealershipFactory;
    private final DealershipRepository dealershipRepository;

    public Logout(DealershipFactory dealershipFactory, DealershipRepository dealershipRepository) {
        this.dealershipFactory = dealershipFactory;
        this.dealershipRepository = dealershipRepository;
    }
    
    @Override
    public String execute(List<String> parameters) {
        return logout();
    }
    
    private String logout() {
        dealershipRepository.setLoggedUser(null);
        return CommandConstants.USER_LOGGED_OUT;
    }
    
}
