package com.telerikacademy.oop.dealership.core;

import com.telerikacademy.oop.dealership.commands.contracts.Command;
import com.telerikacademy.oop.dealership.core.factories.CommandFactoryImpl;
import com.telerikacademy.oop.dealership.core.factories.DealershipFactoryImpl;
import com.telerikacademy.oop.dealership.core.providers.CommandParserImpl;
import com.telerikacademy.oop.dealership.core.providers.ConsoleReader;
import com.telerikacademy.oop.dealership.core.providers.ConsoleWriter;
import com.telerikacademy.oop.dealership.core.contracts.*;

import java.util.List;

public class DealershipEngineImpl implements Engine {
    
    private static final String TERMINATION_COMMAND = "Exit";
    private static final String USER_NOT_LOGGED = "You are not logged! Please login first!";
    private static final String REPORT_SEPARATOR = "####################";
    
    private final DealershipFactory dealershipFactory;
    private final Writer writer;
    private final Reader reader;
    private final CommandParser commandParser;
    private final DealershipRepository dealershipRepository;
    private final CommandFactory commandFactory;
    
    public DealershipEngineImpl() {
        dealershipFactory = new DealershipFactoryImpl();
        writer = new ConsoleWriter();
        reader = new ConsoleReader();
        commandParser = new CommandParserImpl();
        dealershipRepository = new DealershipRepositoryImpl();
        commandFactory = new CommandFactoryImpl();
    }
    
    public void start() {
        
        while (true) {
            try {
                String commandAsString = reader.readLine();
                if (commandAsString.equalsIgnoreCase(TERMINATION_COMMAND)) {
                    break;
                }
                processCommand(commandAsString);
                
            } catch (Exception ex) {
                writer.writeLine(ex.getMessage() != null && !ex.getMessage().isEmpty() ? ex.getMessage() : ex.toString());
                writer.writeLine(REPORT_SEPARATOR);
            }
        }
    }
    
    private void processCommand(String commandAsString) {
        if (commandAsString == null || commandAsString.trim().equals("")) {
            throw new IllegalArgumentException("Command cannot be null or empty.");
        }
        
        String commandTypeAsString = commandParser.parseCommand(commandAsString);
        checkPermissionToExecute(commandTypeAsString);
        Command command = commandFactory.createCommand(commandTypeAsString, dealershipFactory, dealershipRepository);
        List<String> parameters = commandParser.parseParameters(commandAsString);
        String executionResult = command.execute(parameters);
        writer.writeLine(executionResult);
        writer.writeLine(REPORT_SEPARATOR);
        
    }
    
    private void checkPermissionToExecute(String commandTypeAsString) {
        if (!(commandTypeAsString.toUpperCase().equals("REGISTERUSER")) && !commandTypeAsString.toUpperCase().equals("LOGIN")) {
            if (dealershipRepository.getLoggedUser() == null) {
                throw new IllegalArgumentException(USER_NOT_LOGGED);
            }
        }
    }
    
}
