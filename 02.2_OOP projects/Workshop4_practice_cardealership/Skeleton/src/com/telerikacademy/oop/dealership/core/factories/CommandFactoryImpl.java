package com.telerikacademy.oop.dealership.core.factories;

import com.telerikacademy.oop.dealership.commands.*;
import com.telerikacademy.oop.dealership.commands.contracts.Command;
import com.telerikacademy.oop.dealership.commands.enums.CommandType;
import com.telerikacademy.oop.dealership.core.contracts.CommandFactory;
import com.telerikacademy.oop.dealership.core.contracts.DealershipFactory;
import com.telerikacademy.oop.dealership.core.contracts.DealershipRepository;

public class CommandFactoryImpl implements CommandFactory {
    
    private static final String INVALID_COMMAND = "Invalid command name: %s!";
    
    @Override
    public Command createCommand(String commandTypeAsString, DealershipFactory dealershipFactory, DealershipRepository dealershipRepository) {
        CommandType commandType = CommandType.valueOf(commandTypeAsString.toUpperCase());
        switch (commandType) {
            case LOGIN:
                return new Login(dealershipFactory, dealershipRepository);
            case LOGOUT:
                return new Logout(dealershipFactory, dealershipRepository);
            case SHOWUSERS:
                return new ShowUsers(dealershipFactory, dealershipRepository);
            case ADDCOMMENT:
                return new AddComment(dealershipFactory, dealershipRepository);
            case ADDVEHICLE:
                return new AddVehicle(dealershipFactory, dealershipRepository);
            case REGISTERUSER:
                return new RegisterUser(dealershipFactory, dealershipRepository);
            case SHOWVEHICLES:
                return new ShowVehicles(dealershipFactory, dealershipRepository);
            case REMOVECOMMENT:
                return new RemoveComment(dealershipFactory, dealershipRepository);
            case REMOVEVEHICLE:
                return new RemoveVehicle(dealershipFactory, dealershipRepository);
        }
        throw new IllegalArgumentException(String.format(INVALID_COMMAND, commandTypeAsString));
    }
    
}
