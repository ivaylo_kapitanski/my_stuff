package com.telerikacademy.oop;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Objects;

public class MyListImpl<T> implements MyList<T> {

    private static final int INITIAL_CAPACITY = 4;
    private T[] elements;
    private int size;
    //  private int capacity;

    public MyListImpl() {
        this(INITIAL_CAPACITY);
    }

    public MyListImpl(int newCapacity) {
        initialize(newCapacity);

    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public int capacity() {
        return this.elements.length;
    }

    @Override
    public T get(int index) {
        return elements[index];
    }

    @Override
    public void add(T element) {
        if (capacity() == size()) {
            this.elements = Arrays.copyOf(this.elements, capacity() * 2);
        }
        this.elements[size()] = element;
        size++;
    }

    @Override
    public boolean contains(T element) {
        if (indexOf(element) != -1) {
            return true;
        }
        return false;
    }

    @Override
    public int indexOf(T element) {
        for (int i = 0; i < size(); i++) {
            T currentElement = get(i);
            if (element == null && currentElement == null) {
                return i;
            }
            if (currentElement.equals(element)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public int lastIndexOf(T element) {
        for (int i = size() - 1; i >= 0; i--) {
            T currentElement = get(i);
            if (element == null && currentElement == null) {
                return i;
            }
            if (currentElement.equals(element)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public boolean remove(T element) {
        if (contains(element)) {
            removeAt(indexOf(element));
            return true;
        }

        return false;
    }


    @Override
    public boolean removeAt(int index) {
        if (index < 0 || index > size()) {
            return false;
        }
        for (int i = index; i < size() - 1; i++) {
            elements[i] = elements[i + 1];
        }
        elements[size() - 1] = null;
        size--;
        return true;
    }

    @Override
    public void clear() {
        for (int i = 0; i < size(); i++) {
            elements[i] = null;
        }
        initialize(INITIAL_CAPACITY);
    }

    @Override
    public void swap(int from, int to) {
        T temp = get(from);
        elements[from] = elements[to];
        elements[to] = temp;

    }

    @Override
    public void print() {
        StringBuilder sb = new StringBuilder("[");
        for (int i = 0; i < size(); i++) {
            sb.append(elements[i].toString());
            sb.append(", ");
        }
        sb.append(elements[size() - 1]).append("]");
        System.out.println(sb.toString());

    }

    @Override
    public Iterator<T> iterator() {
        return new MyListIterator();
    }


    private void initialize(int newCapacity) {
        this.size = 0;
        this.elements = (T[]) new Object[newCapacity];
    }

    private class MyListIterator implements Iterator<T> {
        int currentIndex = 0;

        @Override
        public boolean hasNext() {
            return currentIndex < size;
        }

        @Override
        public T next() {
            T result = elements[currentIndex];
            currentIndex++;
            return result;
        }
    }
}
