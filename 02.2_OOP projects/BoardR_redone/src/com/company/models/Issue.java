package com.company.models;

import com.company.enums.Status;

import java.time.LocalDate;

public class Issue extends BoardItem {
    private String description;


    public Issue(String title, String description, LocalDate dueDate) {
        super(title, dueDate, Status.OPEN);
        setDescription(description);
    }

    public String getDescription() {
        return description;
    }

    private void setDescription(String description) {
        if (description == null) {
            throw new IllegalArgumentException("No description");
        }
        this.description = description;
    }

    @Override
    public void revertStatus() {
        if (super.getStatus() == Status.OPEN) {
            getItemLogs().add(new EventLog("Issue status already Open"));
        } else {
            getItemLogs().add(new EventLog(String.format("Issue status set to %s", Status.OPEN.toString())));
            super.setStatus(Status.OPEN);
        }
    }

    @Override
    public void advanceStatus() {
        if (super.getStatus() == Status.VERIFIED) {
            getItemLogs().add(new EventLog("Issue status already Verified"));
        } else {
            getItemLogs().add(new EventLog(String.format("Issue status set to %s", Status.VERIFIED.toString())));
            super.setStatus(Status.VERIFIED);
        }
    }

    @Override
    public String viewInfo() {
        String baseInfo = super.viewInfo();

        return String.format("Issue: %s, Description: %s", baseInfo, this.description);
    }
}
