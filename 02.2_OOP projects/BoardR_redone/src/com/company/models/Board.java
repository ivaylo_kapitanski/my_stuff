package com.company.models;

import com.company.contracts.Logger;

import java.util.ArrayList;
import java.util.List;

public class Board {
    private static final List<BoardItem> items = new ArrayList<>();


    private Board(List<BoardItem> items) {
    }

    public static void addItem(BoardItem item) {
        if (items.contains(item)) {
            throw new IllegalArgumentException("This item has already been added");
        }
        items.add(item);
    }

    public static int totalItems() {
        return items.size();
    }

    public static List<BoardItem> getItems() {
        return items;
    }

    public static void displayHistory(Logger logger) {
        for (BoardItem item : items) {
            logger.log(item.getHistory());
        }
    }


}
