package com.company.models;

import com.company.enums.Status;

import java.time.LocalDate;
import java.util.ArrayList;

public abstract class BoardItem {
    private static final int MIN_TITLE_LENGTH = 5;
    private static final int MAX_TITLE_LENGTH = 30;
    protected final Status initialStatus = Status.OPEN;
    protected final Status finalStatus = Status.VERIFIED;
    private String title;
    private LocalDate dueDate;
    private Status status = initialStatus;
    private ArrayList<EventLog> logs;

    //All setters are package-private

    public BoardItem(String title, LocalDate dueDate) {
        this(title, dueDate, Status.OPEN);
    }

    public BoardItem(String title, LocalDate dueDate, Status status) {
        validateTitle(title);
        validateDueDate(dueDate);
        validateStatus(status);
        logs = new ArrayList<>();
        this.status = status;
        logs.add(new EventLog(String.format("Item created: %s, [%s | %s]", title, status, dueDate)));
    }

    void addLog(EventLog log) {
        logs.add(log);
    }

    public ArrayList<EventLog> getLog() {
        return new ArrayList<>(logs);
    }

    public String getTitle() {
        return title;
    }

    void validateTitle(String title) {
        if (title == null) {
            throw new IllegalArgumentException("Please provide a non-empty title");
        }
        if (title.length() < MIN_TITLE_LENGTH || title.length() > MAX_TITLE_LENGTH) {
            throw new IllegalArgumentException("Please provide a title with length between 5 and 30 chars");
        }
        this.title = title;
    }

    void setTitle(String title) {
        logs.add(new EventLog(String.format("Title changed from %s to %s", this.title, title)));
        this.title = title;
    }

    protected ArrayList<EventLog> getItemLogs() {
        return logs;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    void validateDueDate(LocalDate dueDate) {
        if (dueDate.isAfter(LocalDate.now())) {
            this.dueDate = dueDate;
        } else {
            throw new IllegalArgumentException("Please enter a valid date");
        }
    }

    void setDueDate(LocalDate dueDate) {
        logs.add(new EventLog(String.format("DueDate changed from %s to %s", this.dueDate, dueDate)));
        this.dueDate = dueDate;
    }


    void validateStatus(Status status) {
        this.status = status;
    }

    public Status getStatus() {
        return status;
    }

    void setStatus(Status status) {
        this.status = status;
    }

    protected abstract void revertStatus();

    protected abstract void advanceStatus();

    public String getHistory() {
        StringBuilder sb = new StringBuilder();
        for (EventLog event : logs) {
            sb.append(event.viewInfo()).append(System.lineSeparator());
        }
        return sb.toString();
    }

    public String viewInfo() {
        return String.format("'%s', [%s | %s]", getTitle(), getStatus(), getDueDate());
    }
}
