package com.company.models;

import com.company.enums.Status;

import java.time.LocalDate;

public class Task extends BoardItem {
    private static final int ASIGNEE_MIN_LENGTH = 5;
    private static final int ASIGNEE_MAX_LENGTH = 30;
    private final Status childFirstStatus = Status.TODO;
    private String assignee;

    public Task(String title, String assignee, LocalDate dueDate) {
        super(title, dueDate, Status.TODO);
        validateAssignee(assignee);

    }

    private void validateAssignee(String assignee) {
        if (assignee == null) {
            throw new IllegalArgumentException("Input cannot be empty");
        }
        if (assignee.length() < ASIGNEE_MIN_LENGTH || assignee.length() > ASIGNEE_MAX_LENGTH) {
            throw new IllegalArgumentException("Input should be between 5 and 30 characters");
        }
        this.assignee = assignee;
    }

    public String getAssignee() {
        return assignee;
    }

    protected void setAssignee(String assignee) {

        getItemLogs().add(new EventLog(String.format("Assignee changed from %s to %s", this.assignee, assignee)));
        this.assignee = assignee;
        validateAssignee(assignee);
    }

    @Override
    public void revertStatus() {
        if (super.getStatus() == Status.OPEN) {
            getItemLogs().add(new EventLog("Task status already at Open"));
        } else {
            getItemLogs().add(new EventLog(String.format("Task status changed from %s to %s", super.getStatus(), Status.values()[getStatus().ordinal() - 1])));
        }
        if (super.getStatus() != Status.OPEN) {
            super.setStatus(Status.values()[getStatus().ordinal() - 1]);
        }
    }

    @Override
    public void advanceStatus() {
        if (super.getStatus() == Status.VERIFIED) {
            getItemLogs().add(new EventLog("Task status already at Verified"));
        } else {
            getItemLogs().add(new EventLog(String.format("Task status changed from %s to %s", super.getStatus(), Status.values()[getStatus().ordinal() + 1])));
        }
        if (super.getStatus() != Status.VERIFIED) {
            super.setStatus(Status.values()[getStatus().ordinal() + 1]);
        }

    }

    @Override
    public String viewInfo() {
        String baseInfo = super.viewInfo();

        return String.format("Task: %s, Assignee: %s", baseInfo, this.assignee);
    }
}
