package com.company;

import com.company.contracts.ConsoleLogger;
import com.company.models.Board;
import com.company.models.BoardItem;
import com.company.models.Issue;
import com.company.models.Task;

import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {
        LocalDate tomorrow = LocalDate.now().plusDays(1);
        Task task = new Task("Write unit tests", "Pesho", tomorrow);
        Issue issue = new Issue("Review tests", "Someone must review Pesho's tests.", tomorrow);

        Board.addItem(task);
        Board.addItem(issue);

        ConsoleLogger logger = new ConsoleLogger();
        Board.displayHistory(logger);


    }
}
