package com.onlinestore;

import java.time.LocalDate;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
    Order order1=new Order();
    order1.recipient="Pesho";
    order1.total=200;
    order1.currency="BGN";
    order1.deliveryOn= LocalDate.now();
    order1.items=new String[]{"Some stuff ordered"};

    Order order2=new Order();
    order2.recipient="Ivo";
    order2.total=300;
    order2.currency="COP";
    order2.deliveryOn= LocalDate.now();
    order2.items=new String[]{"Ordered thing 1","Ordered thing 2"};
    //save the details in a Order string array then loop through the array to show what each order contains.
        Order[] orders=new Order[]{order1,order2};
        for(Order order:orders){
            System.out.printf("To be delivered on: %s %n",order.deliveryOn.toString());
            System.out.printf("To be delivered to: %s %n",order.recipient.toString());
            System.out.printf("Total order amount: %f %n",order.total);
            System.out.printf("Order currency: %s %n",order.currency.toString());
            for(String item: order.items) {
                System.out.printf("Ordered items: %s %n", item);
            }
            System.out.println("n---------------------n");
        }
        //Super



    }
}
