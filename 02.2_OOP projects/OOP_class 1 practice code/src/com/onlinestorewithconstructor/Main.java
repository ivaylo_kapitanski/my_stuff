package com.onlinestorewithconstructor;

import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {
        Order order1 = new Order("Ivo", 200, "COP", LocalDate.now(), new String[]{"Obushta", "Gashta"});
        Order order2 = new Order("Gosho", 300, "BGN", LocalDate.now(), new String[]{"Martenica", "Bira"});
        Order[] orders = new Order[]{order1, order2};
        for (Order order : orders) {
            System.out.printf("To be delivered on: %s %n", order.deliveryOn.toString());
            System.out.printf("To be delivered to: %s %n", order.recipient.toString());
            System.out.printf("Total order amount: %f %n", order.total);
            System.out.printf("Order currency: %s %n", order.currency.toString());
            for (String item : order.items) {
                System.out.printf("Ordered items: %s %n", item);
            }
            System.out.println("n---------------------n");
        }
        //Super


    }
}
