package com.storewithmethods;

import com.onlinestorewithconstructor.Order;

import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {
       OrderNew order1 = new OrderNew("Ivo", Currency.BGN, LocalDate.now());
        OrderNew order2 = new OrderNew("Gosho", Currency.EUR, LocalDate.now());
       order1.getItems().add(new Product("Gashta",120));
       order2.getItems().add(new Product("Bira",50));

        OrderNew[] orders = new OrderNew[]{order1, order2};

        for (OrderNew order : orders) {
            order.displayGeneralInfor();
            System.out.println();
            order.displayOrderDetails();
            System.out.println("n---------------------n");
        }
        //Super


    }
}
