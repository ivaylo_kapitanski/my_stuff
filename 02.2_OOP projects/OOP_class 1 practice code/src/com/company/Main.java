package com.company;

public class Main {

    public static void main(String[] args) {
        //create a new object-person1
	Person person1=new Person();
	//access the object state variable-name and instantiate it.
	person1.name="Ivo";
	//access the object method and print the name
        person1.sayName();

        Person person2=new Person();
        person2.name="Pesho";
        person2.sayName();

    }
}
