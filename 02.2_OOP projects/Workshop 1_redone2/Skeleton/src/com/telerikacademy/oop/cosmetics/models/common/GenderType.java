package com.telerikacademy.oop.cosmetics.models.common;

public enum GenderType {
    WOMAN,
    UNISEX,
    MEN;


    @Override
    public String toString() {
        switch (this){
            case MEN:
                return "Men";
            case WOMAN:
                return "Woman";
            case UNISEX:
                return "Unisex";
            default:
                return "N/a";
        }
    }
}
