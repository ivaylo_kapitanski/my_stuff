package com.telerikacademy.oop.cosmetics.models;

import com.telerikacademy.oop.cosmetics.models.products.Product;

import java.util.ArrayList;
import java.util.List;

public class Category {


    private static final int CATEGORY_NAME_MIN_LENGTH = 2;
    private static final int CATEGORY_NAME_MAX_LENGTH = 15;
    private String name;
    private List<Product> products;

    public Category(String name) {
        setName(name);
        products = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        Validator.checkInputLength(name, CATEGORY_NAME_MIN_LENGTH, CATEGORY_NAME_MAX_LENGTH);
        this.name = name;
    }

    public List<Product> getProducts() {
        return new ArrayList<>(products);
    }

    public void addProduct(Product product) {
        Validator.isProductNull(product);
        products.add(product);
    }

    public void removeProduct(Product product) {
        if (!products.contains(product)) {
            throw new IllegalArgumentException("A product with this name is not found!");
        }
        products.remove(product);
    }

    public String print() {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("#Category: %s%n", getName()));
        if (products.isEmpty()) {
            sb.append(" #No product in this category");
        }
        for (Product product : products) {
            sb.append(product.print());
        }
        return sb.toString();

    }

}
