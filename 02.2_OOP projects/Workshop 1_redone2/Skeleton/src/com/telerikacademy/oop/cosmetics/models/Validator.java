package com.telerikacademy.oop.cosmetics.models;


public class Validator {

    public static void checkInputLength(String input, int min, int max) {
        if (input == null) {
            throw new IllegalArgumentException("Input cannot be null");
        }
        if (input.trim().length() < min || input.trim().length() > max) {
            throw new IllegalArgumentException(String.format("Input cannot have less than %d and more than %d characters.", min, max));
        }
    }

    public static void checkPrice(double price) {
        if (price < 0) {
            throw new IllegalArgumentException("Price cannot be negative.");
        }
    }

    public static void isProductNull(Object product) {
        if (product == null) {
            throw new IllegalArgumentException("Product cannot be null");
        }
    }

}
