package com.telerikacademy.oop.cosmetics.models.products;

import com.telerikacademy.oop.cosmetics.models.Validator;
import com.telerikacademy.oop.cosmetics.models.common.GenderType;

public class Product {

    private static final int NAME_MIN_LENGTH = 3;
    private static final int BRAND_MAX_LENGTH = 10;
    private static final int NAME_MAX_LENGTH = BRAND_MAX_LENGTH;
    private static final int BRAND_MIN_LENGTH = 2;
    private double price;
    private String name;
    private String brand;
    private GenderType gender;

    public Product(String name, String brand, double price, GenderType gender) {
        setPrice(price);
        setName(name);
        setBrand(brand);
        setGender(gender);
    }

    public double getPrice() {
        return price;
    }

    private void setPrice(double price) {
        Validator.checkPrice(price);
        this.price = price;
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        Validator.checkInputLength(name, NAME_MIN_LENGTH, NAME_MAX_LENGTH);
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    private void setBrand(String brand) {
        Validator.checkInputLength(brand, BRAND_MIN_LENGTH, BRAND_MAX_LENGTH);
        this.brand = brand;
    }

    public GenderType getGender() {
        return gender;
    }

    private void setGender(GenderType gender) {
        this.gender = gender;
    }

    public String print() {
        return String.format("#%s %s%n" +
                "#Price: $%.2f%n" +
                "#Gender: %s%n" +
                "===", getName(), getBrand(), getPrice(), getGender().toString().toUpperCase());
    }

}
