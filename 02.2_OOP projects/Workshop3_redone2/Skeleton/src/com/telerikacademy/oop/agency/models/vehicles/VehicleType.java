package com.telerikacademy.oop.agency.models.vehicles;

public enum VehicleType {
    LAND,
    AIR,
    SEA;


    @Override
    public String toString() {
        switch(this){
            case AIR:
                return "AIR";
            case SEA:
                return "SEA";
            case LAND:
                return "LAND";
            default:
                return "N/A";
        }
    }
}
