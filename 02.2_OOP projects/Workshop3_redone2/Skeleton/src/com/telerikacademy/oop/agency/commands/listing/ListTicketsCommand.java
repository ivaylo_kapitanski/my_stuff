package com.telerikacademy.oop.agency.commands.listing;

import com.telerikacademy.oop.agency.commands.contracts.Command;
import com.telerikacademy.oop.agency.core.contracts.AgencyRepository;
import com.telerikacademy.oop.agency.models.contracts.Ticket;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.oop.agency.commands.CommandsConstants.JOIN_DELIMITER;

public class ListTicketsCommand implements Command {

    private final List<Ticket> tickets;

    public ListTicketsCommand(AgencyRepository agencyRepository) {
        tickets = agencyRepository.getTickets();
    }

    public String execute(List<String> parameters) {
        if (tickets.size() == 0) {
            return "There are no registered tickets.";
        }

        List<String> listTickets = ticketsToString();

        return String.join(JOIN_DELIMITER + System.lineSeparator(), listTickets).trim();
    }

    private List<String> ticketsToString() {
        List<String> stringifiedTickets = new ArrayList<>();
        for (Ticket ticket : tickets) {
            stringifiedTickets.add(ticket.toString());
        }
        return stringifiedTickets;
    }

}