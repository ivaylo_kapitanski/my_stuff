package com.telerikacademy.oop.agency.models.vehicles.contracts;

import com.telerikacademy.oop.agency.models.vehicles.VehicleType;

public interface Vehicle {
    
    int getPassengerCapacity();
    
    double getPricePerKilometer();
    
    VehicleType getType();
    
}