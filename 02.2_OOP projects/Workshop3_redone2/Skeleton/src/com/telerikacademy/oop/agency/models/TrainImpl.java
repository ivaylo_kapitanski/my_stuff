package com.telerikacademy.oop.agency.models;

import com.telerikacademy.oop.agency.models.vehicles.VehicleType;
import com.telerikacademy.oop.agency.models.vehicles.contracts.Train;

public class TrainImpl extends VehicleImpl implements Train {


    private static final int TRAIN_CARTS_MIN = 1;
    private static final int TRAIN_CARTS_MAX = 15;
    private static final int TRAIN_PASSENGER_MAX_CAPACITY = 150;
    private static final int TRAIN_PASSENGER_MIN_CAPACITY = 30;
    private static final String TRAIN_PASSENGER_CAPACITY_ERROR_MESSAGE = "A train cannot have less than 30 passengers or more than 150 passengers.";
    private static final String TRAIN_CART_ERROR_MESSAGE = "A train cannot have less than 1 cart or more than 15 carts.";
    private int carts;

    public TrainImpl(int passengerCapacity, double pricePerKilometer, int carts) {
        super(passengerCapacity, pricePerKilometer, VehicleType.LAND);
        setCarts(carts);
    }


    private void setCarts(int carts) {
        if (carts < TRAIN_CARTS_MIN || carts > TRAIN_CARTS_MAX) {
            throw new IllegalArgumentException(TRAIN_CART_ERROR_MESSAGE);
        }
        this.carts = carts;
    }

    @Override
    public int getCarts() {
        return this.carts;
    }

    @Override
    protected int getPassengerMaxCapacity() {
        return TRAIN_PASSENGER_MAX_CAPACITY;
    }

    @Override
    protected int getPassengerMinCapacity() {
        return TRAIN_PASSENGER_MIN_CAPACITY;
    }

    @Override
    protected String getPassengerCapacityErrorMessage() {
        return TRAIN_PASSENGER_CAPACITY_ERROR_MESSAGE;
    }

    @Override
    public String toString() {
        return String.format("Train ----%n" +
                "%s" +
                "Carts amount: %s%n", super.toString(), this.carts);
    }
}
