package com.telerikacademy.oop.agency.models;

import com.telerikacademy.oop.agency.models.contracts.Journey;
import com.telerikacademy.oop.agency.models.contracts.Ticket;

public class TicketImpl implements Ticket {

    private Journey journey;
    private double administrativeCosts;


    public TicketImpl(Journey journey, double administrativeCosts) {
        setJourney(journey);
        setAdministrativeCosts(administrativeCosts);
    }

    private void setJourney(Journey journey) {
        this.journey = journey;
    }

    private void setAdministrativeCosts(double administrativeCosts) {
        this.administrativeCosts = administrativeCosts;
    }

    @Override
    public double getAdministrativeCosts() {
        return this.administrativeCosts;
    }

    @Override
    public Journey getJourney() {
        return this.journey;
    }

    @Override
    public double calculatePrice() {
        return this.journey.calculateTravelCosts() * getAdministrativeCosts();
    }

    @Override
    public String toString() {
        return String.format("Ticket ----%n" +
                "Destination: %s%n" +
                "Price: %.2f%n", journey.getDestination(), calculatePrice());
    }
}
