package com.telerikacademy.oop.agency.models;

import com.telerikacademy.oop.agency.models.vehicles.VehicleType;
import com.telerikacademy.oop.agency.models.vehicles.contracts.Airplane;

public class AirplaneImpl extends VehicleImpl implements Airplane {


    private boolean hasFreeFood;

    public AirplaneImpl(int passengerCapacity, double pricePerKilometer, boolean hasFreeFood) {
        super(passengerCapacity, pricePerKilometer, VehicleType.AIR);
        setHasFreeFood(hasFreeFood);
    }

    private void setHasFreeFood(boolean hasFreeFood) {
        this.hasFreeFood = hasFreeFood;
    }


    @Override
    public boolean hasFreeFood() {
        return this.hasFreeFood;
    }

    @Override
    public String toString() {
        return String.format("Airplane ----%n" +
                "%s" +
                "Has free food: %b%n", super.toString(), this.hasFreeFood);
    }
}
