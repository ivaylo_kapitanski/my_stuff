package com.telerikacademy.oop.agency.models;

import com.telerikacademy.oop.agency.models.vehicles.VehicleType;
import com.telerikacademy.oop.agency.models.vehicles.contracts.Vehicle;

public abstract class VehicleImpl implements Vehicle {

    private static final int PASSENGER_DEFAULT_MIN_CAP = 1;
    private static final int PASSENGER_DEFAULT_MAX_CAP = 800;
    private static final String DEFAULT_PASSENGER_CAP_ERROR_MSG = "A vehicle with less than 1 passengers or more than 800 passengers cannot exist!";
    private static final double PRICE_PER_KM_DEFAULT_MIN = 0.1;
    private static final double PRICE_PER_KM_DEFAULT_MAX = 2.5;
    private static final String PRICE_PER_KM_DEFAULT_ERROR_MSG = "A vehicle with a price per kilometer lower than $0.10 or higher than $2.50 cannot exist!";
    private int passengerCapacity;
    private VehicleType vehicleType;
    private double pricePerKilometer;

    public VehicleImpl(int passengerCapacity, double pricePerKilometer, VehicleType vehicleType) {
        setPassengerCapacity(passengerCapacity);
        setPricePerKilometer(pricePerKilometer);
        this.vehicleType = vehicleType;

    }

    protected void validatePricerPerKilometer(double pricePerKilometer, double min, double max, String errorMessage) {
        if (pricePerKilometer < min || pricePerKilometer > max) {
            throw new IllegalArgumentException(errorMessage);
        }
    }

    protected void validatePassengerCapacity(int passengerCapacity, int min, int max, String errorMessage) {
        if (passengerCapacity < min || passengerCapacity > max) {
            throw new IllegalArgumentException(errorMessage);
        }
    }

    private void setPassengerCapacity(int passengerCapacity) {
        validatePassengerCapacity(passengerCapacity,
                getPassengerMinCapacity(), getPassengerMaxCapacity(), getPassengerCapacityErrorMessage());
        this.passengerCapacity = passengerCapacity;
    }

    private void setPricePerKilometer(double pricePerKilometer) {
        validatePricerPerKilometer(pricePerKilometer, getPricePerKilometerMin(), getPricePerKilometerMax(), getPricePerKilometerErrorMessage());
        this.pricePerKilometer = pricePerKilometer;
    }

    protected int getPassengerMaxCapacity() {
        return PASSENGER_DEFAULT_MAX_CAP;

    }

    protected int getPassengerMinCapacity() {
        return PASSENGER_DEFAULT_MIN_CAP;
    }

    protected String getPassengerCapacityErrorMessage() {
        return DEFAULT_PASSENGER_CAP_ERROR_MSG;
    }

    protected double getPricePerKilometerMin() {
        return PRICE_PER_KM_DEFAULT_MIN;
    }

    protected double getPricePerKilometerMax() {
        return PRICE_PER_KM_DEFAULT_MAX;
    }

    protected String getPricePerKilometerErrorMessage() {
        return PRICE_PER_KM_DEFAULT_ERROR_MSG;
    }

    @Override
    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    @Override
    public double getPricePerKilometer() {
        return pricePerKilometer;
    }

    @Override
    public VehicleType getType() {
        return vehicleType;
    }

    @Override
    public String toString() {
        return String.format(
                "Passenger capacity: %d%n" +
                        "Price per kilometer: %.2f%n" +
                        "Vehicle type: %s%n", this.passengerCapacity, this.pricePerKilometer, this.vehicleType);
    }
}
