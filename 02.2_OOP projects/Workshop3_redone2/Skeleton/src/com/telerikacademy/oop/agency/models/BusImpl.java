package com.telerikacademy.oop.agency.models;

import com.telerikacademy.oop.agency.models.vehicles.VehicleType;
import com.telerikacademy.oop.agency.models.vehicles.contracts.Bus;

public class BusImpl extends VehicleImpl implements Bus {


    private static final int BUS_PASSENGER_MAX_CAPACITY = 50;
    private static final int BUS_PASSENGER_MIN_CAPACITY = 10;
    private static final String BUS_PASSENGER_CAPACITY_ERROR_MESSAGE =
            "A bus cannot have less than 10 passengers or more than 50 passengers.";

    public BusImpl(int passengerCapacity, double pricePerKilometer) {
        super(passengerCapacity, pricePerKilometer, VehicleType.LAND);
    }

    @Override
    protected int getPassengerMaxCapacity() {
        return BUS_PASSENGER_MAX_CAPACITY;
    }

    @Override
    protected int getPassengerMinCapacity() {
        return BUS_PASSENGER_MIN_CAPACITY;
    }

    @Override
    protected String getPassengerCapacityErrorMessage() {
        return BUS_PASSENGER_CAPACITY_ERROR_MESSAGE;
    }

    @Override
    public String toString() {
        return String.format("Bus ----%n"+"%s",super.toString());
    }
}
