package com.telerikacademy.oop.agency.models;

import com.telerikacademy.oop.agency.models.contracts.Journey;
import com.telerikacademy.oop.agency.models.vehicles.contracts.Vehicle;

public class JourneyImpl implements Journey {

    private static final int DISTANCE_MIN_VALUE = 5;
    private static final int DESTINATION_MIN_LENGTH = DISTANCE_MIN_VALUE;
    private static final int STARTLOCATION_MIN_LENGTH = DESTINATION_MIN_LENGTH;
    private static final int DESTINATION_MAX_LENGTH = 25;
    private static final int STARTLOCATION_MAX_LENGTH = DESTINATION_MAX_LENGTH;
    private static final String STARTLOCATION_ERROR_MESSAGE = "The StartingLocation's length cannot be less than 5 or more than 25 symbols long.";
    private static final String DESTINATION_ERROR_MESSAGE = "The Destination's length cannot be less than 5 or more than 25 symbols long.";
    private static final int DISTANCE_MAX_VALUE = 5000;
    private static final String DISTANCE_ERROR_MESSAGE = "The Distance cannot be less than 5 or more than 5000 kilometers.";
    private String startLocation;
    private String destination;
    private int distance;
    private Vehicle vehicle;

    public JourneyImpl(String startLocation, String destination, int distance, Vehicle vehicle) {
        setStartLocation(startLocation);
        setDestination(destination);
        setDistance(distance);
        setVehicle(vehicle);
    }


    private void setStartLocation(String startLocation) {
        if (startLocation == null) {
            throw new IllegalArgumentException("Startlocation cannot be null.");
        }
        if (startLocation.length() < STARTLOCATION_MIN_LENGTH || startLocation.length() > STARTLOCATION_MAX_LENGTH) {
            throw new IllegalArgumentException(STARTLOCATION_ERROR_MESSAGE);
        }
        this.startLocation = startLocation;
    }

    private void setDestination(String destination) {
        if (destination == null) {
            throw new IllegalArgumentException("Destination cannot be null.");
        }
        if (destination.length() < DESTINATION_MIN_LENGTH || destination.length() > DESTINATION_MAX_LENGTH) {
            throw new IllegalArgumentException(DESTINATION_ERROR_MESSAGE);
        }
        this.destination = destination;
    }

    private void setDistance(int distance) {
        if (distance < DISTANCE_MIN_VALUE || distance > DISTANCE_MAX_VALUE) {
            throw new IllegalArgumentException(DISTANCE_ERROR_MESSAGE);
        }
        this.distance = distance;
    }

    private void setVehicle(Vehicle vehicle) {
        if(vehicle==null){
            throw new IllegalArgumentException("Vehicle cannot be null.");
        }
        this.vehicle = vehicle;
    }

    @Override
    public String getDestination() {
        return this.destination;
    }

    @Override
    public int getDistance() {
        return this.distance;
    }

    @Override
    public String getStartLocation() {
        return this.startLocation;
    }

    @Override
    public Vehicle getVehicle() {
        return this.vehicle;
    }

    @Override
    public double calculateTravelCosts() {
        return getDistance() * vehicle.getPricePerKilometer();
    }

    @Override
    public String toString() {
        return String.format("Journey ----%n" +
                        "Start location: %s%n" +
                        "Destination: %s%n" +
                        "Distance: %d%n" +
                        "Vehicle type: %s%n" +
                        "Travel costs: %.2f%n",
                this.startLocation, this.destination, this.distance, this.vehicle.getType(), calculateTravelCosts());
    }
}
