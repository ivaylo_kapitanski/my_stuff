package com.telerikacademy.oop.agency.tests.models.vehicles;

import com.telerikacademy.oop.agency.models.BusImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BusImpl_Tests {
    
    @Test
    public void constructor_should_throw_when_passengerCapacityLessThanMinValue() {
        // Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new BusImpl(9, 2));
    }
    
    @Test
    public void constructor_should_throw_when_passengerCapacityMoreThanMaxValue() {
        // Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new BusImpl(51, 2));
    }
    
    @Test
    public void constructor_should_throw_when_pricePerKmLessThanMinimum() {
        // Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new BusImpl(25, 0));
    }
    
    @Test
    public void constructor_should_throw_when_pricePerKmMoreThanMaximum() {
        // Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new BusImpl(25, 3));
    }
    
}
