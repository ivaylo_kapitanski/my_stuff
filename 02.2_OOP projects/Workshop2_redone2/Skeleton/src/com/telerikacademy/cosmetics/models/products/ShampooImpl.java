package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.utils.Validator;
import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.UsageType;
import com.telerikacademy.cosmetics.models.contracts.Shampoo;

public class ShampooImpl extends ProductBase implements Shampoo {

    private static final int SHAMPOO_NAME_MIN_LENGTH = 3;
    private static final int SHAMPOO_BRAND_MAX_LENGTH = 10;
    private static final int SHAMPOO_NAME_MAX_LENGTH = SHAMPOO_BRAND_MAX_LENGTH;
    private static final int SHAMPOO_BRAND_MIN_LENGTH = 2;
    private int milliliters;


    private UsageType usageType;

    public ShampooImpl(String name, String brand, double price, GenderType genderType, int milliliters, UsageType usageType) {
        super(name, brand, price, genderType);
        setUsageType(usageType);
        setMilliliters(milliliters);
    }

    public int getMilliliters() {
        return milliliters;
    }

    private void setMilliliters(int milliliters) {
        if (milliliters < 0) {
            throw new IllegalArgumentException("Milliliters cannot be negative.");
        }
        this.milliliters = milliliters;
    }

    public UsageType getUsageType() {
        return usageType;
    }

    private void setUsageType(UsageType usageType) {
        this.usageType = usageType;
    }

    @Override
    protected void validateName(String name) {
        Validator.checkInputLength(name, SHAMPOO_NAME_MIN_LENGTH, SHAMPOO_NAME_MAX_LENGTH, "Shampoo");
    }

    @Override
    protected void validateBrand(String brand) {
        Validator.checkInputLength(brand, SHAMPOO_BRAND_MIN_LENGTH, SHAMPOO_BRAND_MAX_LENGTH, "Brand");
    }

    @Override
    protected void validatePrice(double price) {
        Validator.checkIfPriceIsNegative(price);
    }

    @Override
    public String print() {
        return String.format("%s" +
                " #Milliliters: %d%n" +
                " #Usage: %s%n", super.print(), this.getMilliliters(), this.getUsageType());
    }
}
