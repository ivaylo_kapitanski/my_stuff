package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.utils.Validator;
import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Toothpaste;

import java.util.ArrayList;
import java.util.List;

public class ToothpasteImpl extends ProductBase implements Toothpaste {
    private static final int TOOTPHASTE_NAME_MIN_LENGTH = 3;
    private static final int TOOTHPASTE_BRAND_MAX_LENGTH = 10;
    private static final int TOOTHPASTE_NAME_MAX_LENGTH = TOOTHPASTE_BRAND_MAX_LENGTH;
    private static final int TOOTHPASTE_BRAND_MIN_LENGTH = 2;
    private List<String> ingredients;

    public ToothpasteImpl(String name, String brand, double price, GenderType genderType, List<String> ingredients) {
        super(name, brand, price, genderType);
        setIngredients(ingredients);
    }

    private void setIngredients(List<String> ingredients) {
        if (ingredients == null) {
            throw new IllegalArgumentException("Ingredients cannot be null.");
        }
        this.ingredients = new ArrayList<>(ingredients);
    }

    @Override
    public List<String> getIngredients() {
        return new ArrayList<>(ingredients);
    }

    @Override
    protected void validateName(String name) {
        Validator.checkInputLength(name,
                TOOTPHASTE_NAME_MIN_LENGTH, TOOTHPASTE_NAME_MAX_LENGTH, "Name");
    }

    @Override
    protected void validateBrand(String brand) {
        Validator.checkInputLength(brand,
                TOOTHPASTE_BRAND_MIN_LENGTH, TOOTHPASTE_BRAND_MAX_LENGTH, "Brand");

    }

    //needed here?
    //Seems like it
    @Override
    protected void validatePrice(double price) {
        Validator.checkIfPriceIsNegative(price);
    }

    @Override
    public String print() {
        return String.format("%s" +
                "#Ingredients: %s%n", super.print(), this.getIngredients().toString());
    }
}
