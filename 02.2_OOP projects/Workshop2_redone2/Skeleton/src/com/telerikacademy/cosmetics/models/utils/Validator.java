package com.telerikacademy.cosmetics.models.utils;

public class Validator {



    public static void checkInputLength(String input, int min, int max, String nameOfInput){
        if(input==null){
            throw new IllegalArgumentException(String.format("%s cannot be null!",nameOfInput));
        }
        if(input.trim().length()<min||input.trim().length()>max){
            throw new IllegalArgumentException(String.format("%s cannot be less than %d and more than %d characters",
                    nameOfInput,min,max));
        }
    }
    public static void checkIfPriceIsNegative(double price){
        if(price<0){
            throw new IllegalArgumentException("Price cannot be less than 0.");
        }
    }

}
