package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.ScentType;
import com.telerikacademy.cosmetics.models.contracts.Cream;
import com.telerikacademy.cosmetics.models.utils.Validator;

public class CreamImpl extends ProductBase implements Cream {


    private static final int CREAM_BRAND_MIN_LENGTH = 3;
    private static final int CREAM_NAME_MIN_LENGTH = CREAM_BRAND_MIN_LENGTH;
    private static final int CREAM_BRAND_MAX_LENGTH = 15;
    private static final int CREAM_NAME_MAX_LENGTH = CREAM_BRAND_MAX_LENGTH;
    private ScentType scentType;

    public CreamImpl(String name, String brand, double price, GenderType genderType, ScentType scentType) {
        super(name, brand, price, genderType);
        setScentType(scentType);
    }

    public ScentType getScentType() {
        return scentType;
    }

    private void setScentType(ScentType scentType) {
        this.scentType = scentType;
    }

    @Override
    protected void validateName(String name) {
        Validator.checkInputLength(name, CREAM_NAME_MIN_LENGTH, CREAM_NAME_MAX_LENGTH, "Name");
    }

    @Override
    protected void validateBrand(String brand) {
        Validator.checkInputLength(brand, CREAM_BRAND_MIN_LENGTH, CREAM_BRAND_MAX_LENGTH, "Brand");
    }

    @Override
    protected void validatePrice(double price) {
        Validator.checkIfPriceIsNegative(price);
    }

    @Override
    public String print() {
        return String.format("%s" +
                "#Scent: %s%n", super.print(), this.getScentType());
    }
}
