package com.telerikacademy.oop.furniture.models.enums;

public enum FurntitureType {
    TABLE,CHAIR,ADJUSTABLECHAIR,CONVERTIBLECHAIR;


    @Override
    public String toString() {
        switch (this){
            case CHAIR:
                return "Chair";
            case TABLE:
                return "Table";
            case ADJUSTABLECHAIR:
                return "AdjustableChair";
            case CONVERTIBLECHAIR:
                return "ConvertibleChair";
            default:
                return "N/A";
        }
    }
}
