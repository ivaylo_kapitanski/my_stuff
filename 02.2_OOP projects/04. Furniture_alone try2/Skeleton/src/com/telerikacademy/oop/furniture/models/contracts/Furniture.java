package com.telerikacademy.oop.furniture.models.contracts;

import com.telerikacademy.oop.furniture.models.enums.FurntitureType;
import com.telerikacademy.oop.furniture.models.enums.MaterialType;

public interface Furniture {
    
    String getModel();
    
    MaterialType getMaterialType();
    
    double getPrice();
    
    double getHeight();
    FurntitureType getType();
    
}
