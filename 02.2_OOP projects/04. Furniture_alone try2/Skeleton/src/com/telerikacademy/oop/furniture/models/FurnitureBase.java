package com.telerikacademy.oop.furniture.models;

import com.telerikacademy.oop.furniture.models.contracts.Furniture;
import com.telerikacademy.oop.furniture.models.enums.MaterialType;
import com.telerikacademy.oop.furniture.models.utils.Validator;

public abstract class FurnitureBase implements Furniture {

    private static final String MODEL_NULL_ERROR_MESSAGE = "Model name cannot be null or empty.";
    private static final int MODEL_LENGTH_MIN = 3;
    private static final String PRICE_BELOW_ZERO_ERROR_MESSAGE = "Price cannot be equal to or below 0";
    private static final String HEIGHT_MIN_ERROR_MESSAGE = "Height must be greater than 0.";
    private String model;
    private MaterialType material;
    private double price;
    private double height;

    public FurnitureBase(String model, MaterialType material, double price, double height) {
        setModel(model);
        setMaterial(material);
        setPrice(price);
        setHeight(height);
    }

    private void setModel(String model) {
        Validator.checkIfNull(model, MODEL_NULL_ERROR_MESSAGE);
        validateModelLen(model, MODEL_LENGTH_MIN);
        setHeight(height);
        this.model=model;
    }

    private void setMaterial(MaterialType material) {
        this.material = material;
    }

    private void setPrice(double price) {
        validatePrice(price);
        this.price = price;
    }

    private void setHeight(double height) {
        validateHeight(height);
        this.height = height;
    }


    protected void validateModelLen(String model, int min) {
        if (model.length() < min) {
            throw new IllegalArgumentException("Model name length must be more than 3 symbols.");
        }
    }
    protected void validatePrice(double input){
        Validator.checkIfBelowZero(input, PRICE_BELOW_ZERO_ERROR_MESSAGE);
    }
    protected void validateHeight(double height){
        Validator.checkIfBelowZero(height, HEIGHT_MIN_ERROR_MESSAGE);
    }

    @Override
    public String getModel() {
        return model;
    }
    @Override
    public MaterialType getMaterialType() {
        return material;
    }
    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public double getHeight() {
        return height;
    }

    @Override
    public String toString() {
        return String.format("Type: %s, Model: %s, Material: %s, Price: %.2f, Height: %.2f",getType(),
                this.model,this.material,this.price,this.height);
    }
}
