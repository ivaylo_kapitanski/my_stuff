package com.telerikacademy.oop.furniture.models;

import com.telerikacademy.oop.furniture.models.contracts.ConvertibleChair;
import com.telerikacademy.oop.furniture.models.enums.FurntitureType;
import com.telerikacademy.oop.furniture.models.enums.MaterialType;

public class ConvertibleChairImpl extends ChairImpl implements ConvertibleChair {
    private static final double CONVERTED_NEW_HEIGHT = 0.1;
    private boolean isConverted;
    private String state="Normal";

    public ConvertibleChairImpl(String model, MaterialType material, double price, double height, int legs) {
        super(model, material, price, height, legs);
        this.isConverted = false;
    }
    //returns the status to the ConvertChair command
    @Override
    public boolean getConverted() {
        return isConverted;
    }
    //checks if already converted
    @Override
    public void convert() {
        if(isConverted){
            isConverted=false;
            state="Converted";
        }else{
            isConverted=true;
            state="Normal";
        }
    }

    @Override
    public double getHeight() {
        if(isConverted){
            return CONVERTED_NEW_HEIGHT;
        }else{
            return getHeight();
        }
    }

    @Override
    public FurntitureType getType() {
        return FurntitureType.CONVERTIBLECHAIR;
    }

    @Override
    public String toString() {
        return String.format("%s, State: %s",super.toString(),state);
    }
}
