package com.telerikacademy.oop.furniture.models;

import com.telerikacademy.oop.furniture.models.contracts.Chair;
import com.telerikacademy.oop.furniture.models.enums.FurntitureType;
import com.telerikacademy.oop.furniture.models.enums.MaterialType;

public class ChairImpl extends FurnitureBase implements Chair {
    private int legs;

    public ChairImpl(String model, MaterialType material, double price, double height, int legs) {
        super(model, material, price, height);
        setLegs(legs);
    }

    protected void setLegs(int legs) {
        checkLegs(legs);
        this.legs = legs;
    }
    protected void checkLegs(int legs){
        if(legs<=0){
            throw new IllegalArgumentException("A chair has legs!");
        }
    }

    @Override
    public int getNumberOfLegs() {
        return legs;
    }

    @Override
    public FurntitureType getType() {
        return FurntitureType.CHAIR;
    }

    @Override
    public String toString() {
        return String.format("%s, Legs: %d",super.toString(),legs);
    }
}
