package com.telerikacademy.oop.furniture.core.factories;

import com.telerikacademy.oop.furniture.core.contracts.FurnitureFactory;
import com.telerikacademy.oop.furniture.models.AdjustableChairImpl;
import com.telerikacademy.oop.furniture.models.ChairImpl;
import com.telerikacademy.oop.furniture.models.ConvertibleChairImpl;
import com.telerikacademy.oop.furniture.models.TableImpl;
import com.telerikacademy.oop.furniture.models.contracts.Chair;
import com.telerikacademy.oop.furniture.models.contracts.Company;
import com.telerikacademy.oop.furniture.models.contracts.Table;
import com.telerikacademy.oop.furniture.models.enums.MaterialType;

public class FurnitureFactoryImpl implements FurnitureFactory {

    @Override
    public Company createCompany(String name, String registrationNumber) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    @Override
    public Table createTable(String model, String materialType, double price, double height, double length, double width) {
        return new TableImpl(model,getMaterialType(materialType),price,height,length,width);
    }

    @Override
    public Chair createChair(String type, String model, String material, double price, double height, int numberOfLegs) {
        switch (type){
            case "Normal":
                return new ChairImpl(model,getMaterialType(material),price, height,numberOfLegs);
            case "Adjustable":
                return new AdjustableChairImpl(model,getMaterialType(material),price, height,numberOfLegs);
            case "Convertible":
                return new ConvertibleChairImpl(model,getMaterialType(material),price, height,numberOfLegs);
            default:
                return null;
        }
    }

    private MaterialType getMaterialType(String material) {
        return MaterialType.valueOf(material.toUpperCase());
    }

}
