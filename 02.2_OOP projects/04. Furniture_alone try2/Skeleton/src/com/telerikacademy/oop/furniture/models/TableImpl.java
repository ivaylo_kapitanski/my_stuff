package com.telerikacademy.oop.furniture.models;

import com.telerikacademy.oop.furniture.models.contracts.Table;
import com.telerikacademy.oop.furniture.models.enums.FurntitureType;
import com.telerikacademy.oop.furniture.models.enums.MaterialType;
import com.telerikacademy.oop.furniture.models.utils.Validator;

public class TableImpl extends FurnitureBase implements Table {
    private static final String LENGTH_MIN_ERROR_MESSAGE = "Length must be above zero.";
    private static final String WIDTH_MIN_ERROR_MESSAGE = "Width must be above zero.";
    private double length;
    private double width;

    public TableImpl(String model, MaterialType material, double price, double height, double length, double width) {
        super(model, material, price, height);
        setLength(length);
        setWidth(width);
    }

    private void setLength(double length) {
        Validator.checkIfBelowZero(length, LENGTH_MIN_ERROR_MESSAGE);
        this.length = length;
    }

    private void setWidth(double width) {
        Validator.checkIfBelowZero(width, WIDTH_MIN_ERROR_MESSAGE);
        this.width = width;
    }

    @Override
    public FurntitureType getType() {
        return FurntitureType.TABLE;
    }

    @Override
    public double getLength() {
        return length;
    }

    @Override
    public double getWidth() {
        return width;
    }

    @Override
    public double getArea() {
        return width*length;
    }

    @Override
    public String toString() {
        return String.format("%s, Length: %.2f, Width: %.2f, Area: %.4f",super.toString(),length,width,getArea());
    }
}
