package com.telerikacademy.oop.furniture.models.contracts;

public interface ConvertibleChair extends Chair {
    
    boolean getConverted();
    
    void convert();
    
}
