package com.telerikacademy.oop.furniture.models;

import com.telerikacademy.oop.furniture.models.contracts.AdjustableChair;
import com.telerikacademy.oop.furniture.models.enums.FurntitureType;
import com.telerikacademy.oop.furniture.models.enums.MaterialType;

public class AdjustableChairImpl extends ChairImpl implements AdjustableChair {
    private double newHeight;

    public AdjustableChairImpl(String model, MaterialType material, double price, double height, int legs) {
        super(model, material, price, height, legs);
    }

    @Override
    public void setHeight(double adjHeight) {
        validateHeight(adjHeight);
        newHeight=adjHeight;
    }

    @Override
    protected void validateHeight(double adjHeight) {
        super.validateHeight(adjHeight);
    }

    @Override
    public double getHeight() {
        return newHeight;
    }

    @Override
    public FurntitureType getType() {
        return FurntitureType.ADJUSTABLECHAIR;
    }
}
