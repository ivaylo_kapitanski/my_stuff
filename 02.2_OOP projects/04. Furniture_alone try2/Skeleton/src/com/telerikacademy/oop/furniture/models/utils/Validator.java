package com.telerikacademy.oop.furniture.models.utils;

public class Validator {
    public static void checkIfNull(String input, String errorMessage){
        if(input==null||input.isEmpty()){
            throw new IllegalArgumentException(errorMessage);
        }
    }
    public static void checkIfBelowZero(double input, String errorMessage){
        if(input<=0){
            throw new IllegalArgumentException(errorMessage);
        }
    }
}
