package com.telerikacademy.core.strings;

@SuppressWarnings("StringConcatenationInLoop")
public class StringHelpers {
    private StringHelpers(){
        throw new UnsupportedOperationException();
    }

    /**
     * Abbreviates the source string up maxLength symbols ending with ellipses.
     *
     * The method returns an empty string when the source is either null or emppty,
     * or maxLen is negative or equal to 0.
     * The method will return the unchanged string when the source is longer than maxLength.
     *
     * Example
     * abbreviate("Telerik Academy", 13)
     * returns: Telerik Acade...
     *
     * @param source String - The string to modify
     * @param maxLength int - Maximum length of the resulting string
     * @return the abbreviated string
     *
     * @author Krasimir Malchev
     *
     */
    public static String abbreviate(String source, int maxLength) {
        String ellipses = "...";
        //throw new IllegalArgumentException();
        if (source == null || maxLength <= 0) return "";

        if (source.length() == 0) return "";

        if (maxLength > source.length()) return source;

        return source.substring(0, maxLength) + ellipses;

    }

    /**
     * Capitalizes the first character of the provided string input.
     * The first character of the input is extracted to a temporary String and capitalized.
     * The temporary string then replaces the first character of the source string and the result is returned.
     *
     * @param source string
     * @return the source string with the first character changed to uppercase.
     * @author Ivaylo Kapitanski
     */
    public static String capitalize(String source) {
        if (source.equals("")) {
            return "";
        }
        String firstLetter = String.valueOf(source.charAt(0)).toUpperCase();
        String capitalized = firstLetter + source.substring(1);

        return capitalized;
    }

    /**
     * Concatenates the first inputted string to the beginning of the string2.
     *
     * @param string1 The left part of the new string
     * @param string2 The right part of the new string
     * @return A string that represents the concatenation of string1 and string2 characters.
     * @author Ivaylo Kapitanski
     */
    public static String concat(String string1, String string2) {
        return (string1 + string2);
    }

    /**
     * public static boolean contains(String source, char symbol)
     * Returns:
     * true if and only if this string contains the specified char value.
     * Parameters:
     * @param source - the string itself
     * @param symbol - the symbol, which is checked, if he is part of source
     * Returns:
     * @return true if this string contains s, false otherwise
     * Throws:
     * @throws NullPointerException - if s is null
     * @author Kiril Georgiev
     */
    public static boolean contains(String source, char symbol) {
        if (source == null)
            throw new NullPointerException();
        for (int i = 0; i < source.length(); i++) {
            if (source.charAt(i) == symbol)
                return true;
        }
        return false;
    }

    /**
     * public static boolean endsWith(String source, char target)
     * <p>
     * This method checks if the string source ends with the given character.
     *
     * @param source - This is the submitted string.
     * @param target - This is the symbol for comparison.
     * @return - The method return true or false.
     * @author Todor Kunev
     */
    public static boolean endsWith(String source, char target) {

        boolean isEndWith = false;
        if (!source.equals("")) {
            char lastElement = source.charAt(source.length() - 1);
            if (lastElement == target) {
                isEndWith = true;
            }
        }
        return isEndWith;
    }

    /**
     * Find the first index of targeted "target" character within "source" String
     *
     * @param source String - The string to check
     * @param target char - The character to check for
     * @return int - The last index symbol within source or -1 if no match
     * @author Plamen Chipev
     */
    public static int firstIndexOf(String source, char target) {
        int index = -1;
        for (int i = 0; i < source.length(); i++) {
            if (target == source.charAt(i)) {
                index = i;
                break;
            }
        }
        return index;
    }

    /**
     * Finds the last index of target "symbol" within "source".
     *
     * @param source String - The string to check
     * @param symbol char - The character to check for
     * @return int - The last index symbol within source or -1 if no match
     * @author Alexandar Terziyski
     */
    public static int lastIndexOf(String source, char symbol) {
        int index = -1;
        for (int i = 0; i < source.length(); i++) {
            if (symbol == source.charAt(i)) {
                index = i;
            }
        }
        return index;
    }

    /**
     * public static String pad(String source, int length, char paddingSymbol)
     * <p>
     * This method checks if a string is shorter than the specified length,
     * if it is shorter, add pad symbol before and after the submitted string.
     *
     * @param source- Тhis is the name of the string submission.
     * @param length- Тhis is the length with which we compare the submitted string
     * @param paddingSymbol - This is the pad symbol we add before and after the submitted string
     * @return - This method return concatenate String.
     * @author Todor Kunev
     */
    public static String pad(String source, int length, char paddingSymbol) {

        String pad = source;
        int sourceLength = source.length();
        int num = Math.abs(length - sourceLength);
        String symbol = "";

        for (int i = 0; i < num / 2; i++) {
            symbol += paddingSymbol;
        }

        if (sourceLength < length) {
            pad = symbol + source + symbol;
        }
        return pad;
    }

    /**
     * Pads "source" on the right side with "paddingSymbol" enough times to reach length "length".
     *
     * @param source String - The string to pad
     * @param length int - The length of the string to achieve
     * @param paddingSymbol char - The character used as padding
     * @return String - The padded string
     * @author Alexandar Terziyski
     */
    public static String padEnd(String source, int length, char paddingSymbol) {
        for (int i = source.length(); i < length; i++) {
            source += paddingSymbol;
        }
        return source;
    }
    /**
     * public static String padStart (String source, int length, char paddingSymbol)
     * Returns:
     * String source after the adding of the block of paddingSymbols.
     * Parameters:
     * @param source - the string itself
     * @param length - the desired length of the string
     * @param paddingSymbol - the symbol, which should be added at the beginning
     * Returns:
     * @return the padded String source
     * Throws:
     * @throws NullPointerException - if s is null
     * @author Kiril Georgiev
     */
    public static String padStart(String source, int length, char paddingSymbol) {
        if (source == null)
            throw new NullPointerException();
        for (int i = 0; ; i++) {
            if (source.length() + 1 > length)
                break;
            source = paddingSymbol + source;
        }
        return source;

    }

    /**
     * Repeats the given string times times.
     *
     * @param source String - The string to repeat
     * @param times  int - The number of times to repeat the string
     * @return The repeated string
     * @author Irena Slavova
     */
    public static String repeat(String source, int times) {
        String result = source;
        for (int i = 0; i < times - 1; i++) {
            result = concat(result, source);
        }
        return result;
    }

    /**
     * Reverses a "source" string.
     * We take the source and we split it. After the split we change the position of the first index with the last one.
     * We do that until we are at half of the "source" length.
     * Then we create an empty string and add all the index from the Array "nameReverse"
     * Then we return the new "reverse" string.
     *
     * @param source String - The string to reverse.
     * @return The reversed string
     * @author Plamen Chipev
     */
    public static String reverse(String source) {
        String[] nameReverse = source.split("");
        for (int i = 0; i < source.length() / 2; i++) {
            String temp = nameReverse[i];
            nameReverse[i] = nameReverse[source.length() - 1 - i];
            nameReverse[source.length() - 1 - i] = temp;
        }
        String reverse = "";
        for (int i = 0; i < source.length(); i++) {
            reverse += nameReverse[i];
        }

        return reverse;
    }

    /**
     * Returns a new string, starting from start and ending at end.
     *
     * @param source String - The string to reverse
     * @param start int - The starting position in source (inclusive)
     * @param end int - The end position in source (inclusive)
     * @return String - A new string, formed by the characters in source, starting from start to end
     * @author Irena Slavova
     */
    public static String section(String source, int start, int end) {
        String result = "";
        for (int i = 0; i < source.length(); i++) {
            if (start <= i && i <= end) {
                result += source.charAt(i);
            }
        }
        return result;
    }

    /**
     * Returns true if the source starts with the given character.
     * Otherwise it returns false
     * The method will return false if the given source is null or empty
     *
     * Example
     * reverse("Java", 'J');
     * Returns: true
     *
     * reverse("Java", 'j');
     * Returns: false
     *
     * The returns false when given empty string
     * @param source String - The string to inspect
     * @param target char - The character to search for
     * @return boolean - true if string starts with target, otherwise false.
     *
     * @author Krasimir Malchev
     */
    public static boolean startsWith(String source, char target) {
        if (source == null || source.length() == 0)
            return false;

        return source.charAt(0) == target;
    }
}


