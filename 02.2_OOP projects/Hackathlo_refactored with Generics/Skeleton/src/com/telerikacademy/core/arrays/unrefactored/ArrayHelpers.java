package com.telerikacademy.core.arrays.unrefactored;

import java.util.ArrayList;

public class ArrayHelpers {
    private ArrayHelpers(){
        throw new UnsupportedOperationException();
    }

    /**
     * public static int[] add(int[] source, int element)
     * Returns:
     * newSource if and only if the source exists
     * Parameters:
     * @param source - array of integers
     * @param element - the element, which is added at source.length+1 position
     * Returns:
     * @return new array with last value element, which is the parameter, if source is null, then makes new array and adds element as
     * the only element in the new array
     * @author Kiril Georgiev
     */
    public static int[] add(int[] source, int element) {
        if (source == null)
            return new int[]{element};
        int[] newSource = new int[source.length + 1];
        for (int i = 0; i < newSource.length; i++) {
            if (i == newSource.length - 1) {
                newSource[i] = element;
                break;
            }
            newSource[i] = source[i];
        }
        return newSource;
    }

    /**
     * Adds a given element at the start of source array.
     * <p>
     * Example
     * add(new int[]{1, 2, 3}, 4);
     * Returns: {4, 1, 2, 3}
     *
     * @param source  int[] - The array to add to
     * @param element int - The element to add
     * @return int[] - A new array containing the given element, followed by
     * all the elements in the source array.
     * @throws IllegalArgumentException if source array is null.
     * @author Krasimir Malchev
     */
    public static int[] addFirst(int[] source, int element) {

        if (source == null)
            throw new IllegalArgumentException("The source array must not be null");

        int[] arr = new int[source.length + 1];
        arr[0] = element;
        //copy(int[] source, int[] destinationArray, int element);
        for (int i = 0; i < source.length; i++)
            arr[i + 1] = source[i];

        return arr;
    }

    /**
     * Adds all "elements" to the end of "source".
     *
     * @param source   int[] - The array to add to.
     * @param elements int[] - The elements to add.
     * @return int[] - A new array, the original array with all elements at the end.
     * @author Alexandar Terziyski
     */
    public static int[] addAll(int[] source, int... elements) {
        int[] result = new int[source.length + elements.length];
        int elementsCounter = 0;
        for (int i = 0; i < result.length; i++) {
            if (i < source.length) {
                result[i] = source[i];
            } else {
                result[i] = elements[elementsCounter];
                elementsCounter++;
            }
        }
        return result;
    }

    /**
     * Checks if the element is contained in the source array.
     *
     * @param source-the  int array which we go over and check.
     * @param element-the int element which is to be checked.
     * @return true if the element is found in the array.
     * @author Ivaylo Kapitanski
     */

    public static boolean contains(int[] source, int element) {
        for (int j : source) {
            if (j == element) {
                return true;
            }
        }
        return false;
    }

    /**
     * Copies the count number of elements from sourceArray to destinationArray.
     * If the number of elements to be copied is less than the source elements,
     * all elements from the source are copied to the destination array.
     * If the destination array length is larger than the elements to be copied, the remaining elements in the destination array are filled with 0-s.
     *
     * @param sourceArray-the      array from which we copy the elements.
     * @param destinationArray-the array to which we copy the elements.
     * @param count-the number of elements to be copied.
     * @author Ivaylo Kapitanski
     */

    public static void copy(int[] sourceArray, int[] destinationArray, int count) {

        if (count < sourceArray.length) {
            for (int i = 0; i < count; i++) {
                destinationArray[i] = sourceArray[i];
            }
        } else {
            for (int i = 0; i < sourceArray.length; i++) {
                destinationArray[i] = sourceArray[i];
            }
        }
        if (destinationArray.length > count) {
            for (int i = sourceArray.length - 1; i < destinationArray.length; i++) {
                destinationArray[i] = 0;
            }
        }
    }

    /**
     * Copies elements from sourceArray, starting from sourceStartIndex into destinationArray, starting from destStartIndex, taking count elements.
     * take part of source array by start point
     * the final point is count, which is parameter in the method
     * put the part in destination array
     * all other positions are 0
     * @param sourceArray int[] - The array to copy from
     * @param sourceStartIndex  int[] - The starting index in sourceArray
     * @param destinationArray int[] - The array to copy to
     * @param destStartIndex int[] - The starting index in destinationArray
     * @param count int - The number of elements to copy
     *
     * @author Irena Slavova
     */
    public static void copyFrom(int[] sourceArray, int sourceStartIndex,
                                int[] destinationArray, int destStartIndex, int count) {
        int index = 0;
        for (int i = destStartIndex; i < destStartIndex + count; i++) {
            destinationArray[i] = sourceArray[sourceStartIndex + index];
            index++;

        }
    }

    /**
     * public static void fill(int[] source, int element)
     * Returns:
     * type void
     * Parameters:
     * @param source - a array, which should be filled with an specific value of type int
     * @param element - the value, with which the array should be filled
     * Throws:
     * @throws NullPointerException - if array source is null
     * @author Kiril Georgiev
     */
    public static void fill(int[] source, int element) {
        if (source == null)
            throw new NullPointerException();
        for (int i = 0; i < source.length; i++)
            source[i] = element;
    }

    /**
     * Finds the first index of target within source.
     *
     * @param source int[] - The array to check in.
     * @param target int - The element to check for.
     * @return int - The first index of "target" within "source", otherwise returns -1.
     * @author Alexandar Terziyski
     */
    public static int firstIndexOf(int[] source, int target) {
        int index = -1;
        for (int i = 0; i < source.length; i++) {
            if (target == source[i]) {
                index = i;
                break;
            }
        }
        return index;
    }

    /**
     * public static int[] insert(int[] source, int index, int element)
     * <p>
     * This method adds a new element to the specified position in the matrix.
     *
     * @param source  The submitted matrix.
     * @param index   The index where the new element should be placed.
     * @param element The element who need to be insert.
     * @return Returns a matrix with a new element added.
     * @author Todor Kunev
     */
    public static int[] insert(int[] source, int index, int element) {

        ArrayList<Integer> list = new ArrayList<>();

        int[] matrix = new int[source.length + 1];

        for (int i = 0; i < source.length; i++) {
            list.add(source[i]);
        }
        list.add(index, element);

        for (int i = 0; i < matrix.length; i++) {
            matrix[i] = list.get(i);

        }
        return matrix;
    }

    /**
     * Checks if index is a valid index in source.
     * @param source int[] - The array to check against
     * @param index int - The index to check for
     * @return boolean - true if index is valid, otherwise, false
     * @author Irena Slavova
     */
    public static boolean isValidIndex(int[] source, int index) {

        return (0 <= index) && (index < source.length);
    }

    /**
     * We add an array of number "source",also we tell what Integer number "target" we are looking for and
     * the method is giving us the last position of the "target" number
     *
     * @param source int[] - The array to check in.
     * @param target int - The element to check for.
     * @return int - The last index of target within source, otherwise, -1
     * @author Plamen Chipev
     */

    public static int lastIndexOf(int[] source, int target) {
        int index = -1;
        for (int i = 0; i < source.length; i++) {
            if (source[i] == target) {
                index = i;
            }
        }
        return index;
    }

    /**
     * We add an array of number "source",also we tell what Integer number "Element" we are looking for and
     * then we change the "element" with "0" and for every "element" we add to a "count" how much elements we must
     * delete,aka how much we must trim from the length of the old Array "source" to the new one, that is
     * "noTarget" array. When we have the length we must add the elements that are not "0".
     * We create a new int that is "position" , and this is a counter that is the last position that we
     * put in a value different from "0". Then we  return the new array, that is "noTarget".
     *
     * @param source  int[] - The array to check in.
     * @param element int - The element to check for.
     * @return int[] - A new array with all occurences of element removed
     * @author Plamen Chipev
     */

    public static int[] removeAllOccurrences(int[] source, int element) {
        int count = 0;

        for (int i = 0; i < source.length; i++) {
            if (source[i] == element) {
                source[i] = 0;
                count++;
            }

        }
        int position = 0;
        int[] noTarget = new int[source.length - count];
        for (int value : source) {
            if (value != 0) {
                noTarget[position] = value;
                position++;
            }
        }
        return noTarget;
    }

    /**
     * public static void reverse(int[] arrayToReverse)
     * <p>
     * This method returns the reverse order of the matrix
     *
     * @param arrayToReverse The array to invert.
     * @author Todor Kanev
     */
    public static void reverse(int[] arrayToReverse) {
        int temp = 0;
        for (int i = 0; i < arrayToReverse.length / 2; i++) {
            temp = arrayToReverse[i];
            arrayToReverse[i] = arrayToReverse[arrayToReverse.length - 1 - i];
            arrayToReverse[arrayToReverse.length - 1 - i] = temp;
        }
    }

    /**
     * Returns a new array, from source, starting from
     * startIndex and until endIndex.
     * If the source array is empty, then a new empty array is returned.
     * If startIndex and endIndex are out of bounds then the source array
     * is returned.
     * If the end index is greater than the array size then a new sub-array
     * from the start index up to the end of the array is returned.
     * <p>
     * Example:
     * section(new int[]{1, 2, 3, 4, 2}, 0, 3)
     * Returns {1, 2, 3}
     *
     * @param source     int[] - The array to create the new array from
     * @param startIndex int - The starting index
     * @param endIndex   int - The end index
     * @return int[] - A new array starting from startIndex and until endIndex
     * @author Krasimir Malchev
     */
    public static int[] section(int[] source, int startIndex, int endIndex) {
        if (source == null || startIndex < 0 || startIndex > endIndex) {
            return source;
        }
        if (source.length == 0) return new int[]{};

        endIndex = Math.min(source.length - 1, endIndex);

        int[] arr = new int[endIndex - startIndex + 1];
        int n = 0;
        for (int i = startIndex; i <= endIndex; i++) {
            arr[n++] = source[i];
        }

        return arr;
    }

}
