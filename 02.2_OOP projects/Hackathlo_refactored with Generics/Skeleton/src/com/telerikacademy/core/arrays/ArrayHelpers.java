package com.telerikacademy.core.arrays;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class ArrayHelpers {
    private ArrayHelpers() {
        throw new UnsupportedOperationException();
    }


    public static int[] add(int[] source, int element) {

    }


    public static int[] addFirst(int[] source, int element) {

    }

    public <E> E[] addAll(E[] source, E... elements) {
        E[] temp = (E[]) new Object[source.length];
        for (int i = 0; i < source.length; i++) {
            temp[i] = source[i];
        }
        return temp;
    }


    public <E> boolean contains(E[] source, E element) {
        return firstIndexOf(source, element) != -1;
    }


    public static void copy(int[] sourceArray, int[] destinationArray, int count) {

    }

    public static void copyFrom(int[] sourceArray, int sourceStartIndex,
                                int[] destinationArray, int destStartIndex, int count) {

    }

    public static void fill(int[] source, int element) {
    }

    public <E> int firstIndexOf(E[] source, E target) {
        for (int i = 0; i < source.length; i++) {
            if (source[i] == target) {
                return i;
            }
        }
        return -1;
    }

    public static int[] insert(int[] source, int index, int element) {

    }

    public <E> boolean isValidIndex(E[] source, int index) {
        if (source.length > index && index > 0) {
            return true;
        }

        return false;
    }


    public static int lastIndexOf(int[] source, int target) {
    }

    public <E> E[] removeAllOccurrences(E[] source, E element) {

        int length= source.length;;
        int count=0;
        for (int i = 0; i <length ; i++) {
            if(source[i]!=element){
                source[count]=source[i];
                count++;
            }
        }
        return source;
    }

    public <E> void reverse(E[] arrayToReverse) {
        for (int i = 0; i < arrayToReverse.length/2; i++) {
            E temp=arrayToReverse[i];
            arrayToReverse[i]=arrayToReverse[arrayToReverse.length-1-i];
            arrayToReverse[arrayToReverse.length-1-i]=temp;
        }

    }


    public <E> E[] section(E[] source, int startIndex, int endIndex) {


    }

}
