package com.company;

public class Editor extends Staff {
    private int numberOfArticles;

    public Editor(String name, int staffId, int numberOfArticles) {
        //в конструктора винаги първо трябва да извикаме суперконструктора.
        // За да имаме готов обект за да можем да надграждаме.
        super(name, staffId);
        this.numberOfArticles = numberOfArticles;
        System.out.println("Editor constructor-ready");

    }

    public int getNumberOfArticles() {
        return numberOfArticles;
    }
    //no need to add the identify method here because it already does what we want in the base Staff method.
    // We would add it here only if it needs to be updated.


    @Override
    public String toString() {
        return String.format("%s, %nPublished %d articles", super.toString(), getNumberOfArticles());
    }
}
