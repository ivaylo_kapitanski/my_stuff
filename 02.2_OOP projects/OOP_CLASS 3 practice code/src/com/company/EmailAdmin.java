package com.company;

import java.util.ArrayList;
import java.util.List;

public class EmailAdmin extends Staff{
    private List<String>emails;

    public EmailAdmin(String name, int staffId, List<String> emails) {
        super(name, staffId);
        emails=new ArrayList<>();
        System.out.println("EmailAdmin constructor-ready");

    }

    public List<String> getEmails() {
        return new ArrayList<>(emails);
    }
    public void addEmails(String email){
        emails.add(email);
    }
}
