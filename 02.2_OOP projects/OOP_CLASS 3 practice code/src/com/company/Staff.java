package com.company;

public class Staff extends User {

    private int staffId;

    public Staff(String name, int staffId) {
        //този ред извиква суперконструктора(конструктора на базовият клас в Юзър) от клас юзър.
        super(name);
        this.staffId = staffId;
        System.out.println("Staff constructor-ready");

    }

    public int getStaffId() {
        return staffId;
    }


    //тук променяме функционолността на метода identify.
    @Override
    public String identify() {
        //със командата .super.identify() извикваме резултата от изпълнението на базовият метод
        return String.format("%s #%d",super.identify(),getStaffId());

    }
    @Override
    public String toString() {
        return String.format("%s, %n StaffID: %d",super.toString(),getStaffId());
    }

}
