package onlinestore;

import java.time.LocalDate;
import java.time.Month;

public class Main {
    public static void main(String[] args) {
        OrderNew order1 = new OrderNew("Ivo", Currency.BGN, LocalDate.now());

        OrderNew order2 = new OrderNew("Gosho", Currency.EUR, LocalDate.now());

        order1.addItem(new Product("Gashta",120));
        order2.addItem(new Product("Bira",50));



        InternationalOrder order3=new InternationalOrder(
                "Pesho2",Currency.USD,LocalDate.of(2021, Month.MAY,28),
                "DHL",20);

        order3.addItem(new Product("Ranitsa",150));
        order3.addItem(new Product("books",50));
        order3.addItem(new Product("speakers",90));


        OrderNew[] orders = new OrderNew[]{order1, order2,order3};

        for (OrderNew order : orders) {
            System.out.println(order.displayGeneralInfor());
            System.out.println(order.displayOrderDetails());
            System.out.println("n---------------------n");
        }
        //Super
    }
}
