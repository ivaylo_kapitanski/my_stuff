package onlinestore;

import java.time.LocalDate;

public class InternationalOrder extends OrderNew {
    private static final int MAX_PRODUCTS_LIMIT = 5;
    private String carrier;
    private int customsPercentage;

    public InternationalOrder(String recipient, Currency currency, LocalDate deliveryOn,
                              String carrier, int customsPercentage) {
        super(recipient, currency, deliveryOn);
        setCarrier(carrier);
        setCustomsPercentage(customsPercentage);
    }



    public String getCarrier() {
        return carrier;
    }

    private void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public int getCustomsPercentage() {
        return customsPercentage;
    }

    private void setCustomsPercentage(int customsPercentage) {
        this.customsPercentage = customsPercentage;
    }

    @Override
    public String displayGeneralInfor() {
        return String.format("%s | Carrier: %s| *Note*-customs fees of %d expected to" +
                " be calculated on arrival", super.displayGeneralInfor(), getCarrier(), getCustomsPercentage());


    }

    @Override
    public void addItem(Product item) {
        if (getItems().size() >= MAX_PRODUCTS_LIMIT) {
            throw new IllegalArgumentException("International orders are limited to 5 items");
        }
        super.addItem(item);
    }

}
