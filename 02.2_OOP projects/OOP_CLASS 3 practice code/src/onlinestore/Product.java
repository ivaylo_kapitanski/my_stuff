package onlinestore;

public class Product {
    private static final int NAME_MIN_LENGTH = 2;
    private static final int NAME_MAX_LENGTH = 15;
    private String name;
    private double price;
    private Currency currency;

    public Product(String name, double price, Currency currency){
        setName(name);
        setPrice(price);
        setCurrency(currency);
    }
    public String getName() {
        return name;
    }

    private void setName(String name) {
        if(name==null){
            throw new IllegalArgumentException("Product name cannot be empty");
        }
        if(name.length()< NAME_MIN_LENGTH ||name.length()> NAME_MAX_LENGTH){
            throw new IllegalArgumentException("Name length must be between 2 and 15 chars");
        }
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    private void setPrice(double price) {
        if(price<=0.00){
            throw new IllegalArgumentException("Price must be greater than 0.00");
        }
        this.price = price;
    }

    public Currency getCurrency(){
        return currency;
    }

    private void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Product(String name, double price){
        this(name,price,Currency.BGN);
    }
    public String getDisplayInfo(){
        return String.format("%s |%.2f |%s",name,price,currency);
    }

}
