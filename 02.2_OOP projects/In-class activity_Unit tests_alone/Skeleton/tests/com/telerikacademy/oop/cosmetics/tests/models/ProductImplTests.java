package com.telerikacademy.oop.cosmetics.tests.models;

import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.cosmetics.models.CategoryImpl;
import com.telerikacademy.oop.cosmetics.models.GenderType;
import com.telerikacademy.oop.cosmetics.models.ProductImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ProductImplTests {
    //Which are the test cases?
    @Test
    public void constructor_Should_CreateProduct_When_ArgumentsAreValid() {
        //Arrange, Act
        ProductImpl product=new ProductImpl("Product","Brand",3, GenderType.MEN);
        //Assert
        Assertions.assertEquals(product.getName(),"Product");

    }
    @Test
    public void constructor_Should_ThrowException_When_brandLengthIsLessThanMin(){
        // Arrange, Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class,
                ()->new ProductImpl("Product", new String(new char[1]),3,GenderType.MEN));

    }
    @Test
    public void constructor_Should_ThrowException_when_brandLengthIsMoreThanMax() {
        // Arrange, Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class,
                ()->new ProductImpl("Product", new String(new char[11]),3,GenderType.MEN));

    }
    @Test
    public void constructor_Should_ThrowException_when_brandIsNull() {
        // Arrange, Act, Assert
        Assertions.assertThrows(NullPointerException.class,
                ()->new ProductImpl("Product", null,3,GenderType.MEN));
    }
    @Test
    public void constructor_Should_ThrowException_When_nameLengthIsLessThanMIn(){
        // Arrange, Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class,
                ()->new ProductImpl(new String(new char[1]),"Brand",3,GenderType.MEN));

    }
    @Test
    public void constructor_Should_ThrowException_when_nameLengthIsMoreThanMin() {
        // Arrange, Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class,
                ()->new ProductImpl(new String(new char[11]),"Brand",3,GenderType.MEN));

    }
    @Test
    public void constructor_Should_ThrowException_when_nameIsNull() {
        // Arrange, Act, Assert
        Assertions.assertThrows(NullPointerException.class,
                ()->new ProductImpl(null,"Brand",3,GenderType.MEN));
    }
    @Test
    public void constructor_Should_ThrowException_when_priceIsLessThanZero() {
        // Arrange, Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class,
                ()->new ProductImpl("Product","Brand",-1,GenderType.MEN));
    }


}
