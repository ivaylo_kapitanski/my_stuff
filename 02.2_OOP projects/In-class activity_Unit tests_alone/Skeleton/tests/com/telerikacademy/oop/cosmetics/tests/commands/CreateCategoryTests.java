package com.telerikacademy.oop.cosmetics.tests.commands;

import com.telerikacademy.oop.cosmetics.core.ProductFactoryImpl;
import com.telerikacademy.oop.cosmetics.core.ProductRepositoryImpl;
import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductFactory;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class CreateCategoryTests {
    private Command createCategory;
    private ProductRepository productRepository;
    private ProductFactory productFactory;
    private List<String> parameters;

    @BeforeEach
    public void before() {
        productFactory = new ProductFactoryImpl();
        productRepository = new ProductRepositoryImpl();
        parameters = new ArrayList<>();
        parameters.add("Name");
    }

    @Test
    public void execute_Should_AddNewCategoryToRepository_When_ValidParameters() {
        // Arrange, Act & Assert
        createCategory.

        Assertions.assertThrows(InvalidUserInputException.class,()->createCategory.execute())


    }

    @Test
    public void execute_Should_ThrowException_When_MissingParameters() {

    }

    @Test
    public void execute_Should_ThrowException_When_DuplicateCategoryName() {

    }
}
