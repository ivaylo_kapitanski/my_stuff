package com.telerikacademy.oop.cosmetics.tests.models;

import com.telerikacademy.oop.cosmetics.core.contracts.Category;
import com.telerikacademy.oop.cosmetics.core.contracts.Product;
import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.cosmetics.models.CategoryImpl;
import com.telerikacademy.oop.cosmetics.models.GenderType;
import com.telerikacademy.oop.cosmetics.models.ProductImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class CategoryImplTests {
    private Product testProduct;
    private Product testProduct2;

    @BeforeEach
    public void before() {
        testProduct = new ProductImpl("testName","brand",3, GenderType.MEN);

        testProduct2 = new ProductImpl("testName2","brand2",3, GenderType.MEN);
    }


    @Test
    public void constructor_Should_CreateCategory_When_ArgumentsAreValid() {
        //Arrange, Act
        CategoryImpl catergory=new CategoryImpl("Name");
        //Assert
        Assertions.assertEquals(catergory.getName(),"Name");


    }

    @Test
    public void constructor_Should_ThrowException_When_NameIsInvalid() {
        //Arrange, Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class,()->new CategoryImpl("ZZ"));
      //  Assertions.assertThrows(IllegalArgumentException.class,()->new CategoryImpl("MoreThan10W"));
    }

    @Test
    public void addProduct_Should_AddProductToList() {
        //Arrange
       Category category=new CategoryImpl("test");
       //Act
       category.addProduct(testProduct);
       //Assert
        Assertions.assertEquals(1,category.getProducts().size());
    }

    @Test
    public void removeProduct_Should_RemoveProductFromList_When_ProductExist() {
        //Arrange
        Category category=new CategoryImpl("test");
        category.addProduct(testProduct);
        //Act
        category.removeProduct(testProduct);
        //Assert
        Assertions.assertEquals(0,category.getProducts().size());

    }

    @Test
    public void removeProduct_Should_NotRemoveProductFromList_When_ProductNotExist() {
        //Arrange
        Category category=new CategoryImpl("test");
        category.addProduct(testProduct);
        //Act
        category.removeProduct(testProduct2);
        //Assert
        Assertions.assertEquals(1,category.getProducts().size());


    }
}
