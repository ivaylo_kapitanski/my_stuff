package com.telerikacademy.oop.cosmetics.core.contracts;

import java.util.List;

public interface Command {
    void execute(List<String> parameters);
    String getResult();
}
