package com.telerikacademy.oop.cosmetics.core.contracts;

public interface Reader {
    String readLine();
}
