package com.telerikacademy.oop.cosmetics.core;

import com.telerikacademy.oop.cosmetics.commands.*;
import com.telerikacademy.oop.cosmetics.commands.CommandType;
import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.CommandFactory;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductFactory;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;

public class CommandFactoryImpl implements CommandFactory {
    @Override
    public Command createCommand(String commandTypeValue, ProductFactory productFactory, ProductRepository productRepository) {
        //TODO Validate command format
        CommandType commandType;
        try {
            commandType = CommandType.valueOf(commandTypeValue.toUpperCase());
            switch (commandType) {
                case CREATECATEGORY:
                    return new CreateCategory(productRepository, productFactory);
                case CREATEPRODUCT:
                    return new CreateProduct(productRepository, productFactory);
                case ADDPRODUCTTOCATEGORY:
                    return new AddProductToCategory(productRepository);
                case SHOWCATEGORY:
                    return new ShowCategory(productRepository);
                default:
                    return null;
            }
        } catch (Exception e) {
            throw new IllegalArgumentException("Command SomeCommand is not supported.");
        }
    }
}
