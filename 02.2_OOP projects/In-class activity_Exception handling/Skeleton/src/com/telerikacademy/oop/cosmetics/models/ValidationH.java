package com.telerikacademy.oop.cosmetics.models;


public class ValidationH {


    public static void checkNameLength(String name, int min, int max, String str) {
        if (name.length() < min || name.length() > max) {
            throw new IllegalArgumentException(String.format("%s name should be between %d and %d symbols.", str, min, max));
        }
    }

    public static void checkProdNameLength(String name, int min, int max) {
        if (name.length() < min || name.length() > max) {
            throw new IllegalArgumentException(String.format("Product brand should be between %d and %d symbols.", min, max));
        }
    }

    public static void checkIfBelowZero(double input, String str) {
        if (input < 0) {
            throw new IllegalArgumentException(String.format("%s can't be negative.", str));
        }
    }


}
