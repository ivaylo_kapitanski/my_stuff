package com.telerikacademy.oop.cosmetics.models;

import com.telerikacademy.oop.cosmetics.core.contracts.Product;

public class ProductImpl implements Product {
    private String name;
    private String brand;
    private double price;
    private final GenderType gender;
    private static final int PROD_NAME_MIN_LENGTH = 3;
    private static final int PROD_NAME_MAX_LENGTH = 10;
    private static final int BRAND_NAME_MIN_LENGTH = 2;
    private static final int BRAND_NAME_MAX_LENGTH = 10;

    public ProductImpl(String name, String brand, double price, GenderType gender) {
        setName(name);
        setBrand(brand);
        setPrice(price);
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        ValidationH.checkNameLength(name, PROD_NAME_MIN_LENGTH, PROD_NAME_MAX_LENGTH, "Product");
        //TODO Validate name
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    private void setBrand(String brand) {
        ValidationH.checkProdNameLength(brand, BRAND_NAME_MIN_LENGTH, BRAND_NAME_MAX_LENGTH);
        //TODO Validate brand
        this.brand = brand;
    }

    public double getPrice() {
        return price;
    }

    private void setPrice(double price) {
        //TODO Validate price
        ValidationH.checkIfBelowZero(price, "Price");

        this.price = price;
    }

    public GenderType getGender() {
        return gender;
    }

    @Override
    public String print() {
        return String.format(
                "#%s %s%n" +
                        " #Price: $%.2f%n" +
                        " #Gender: %s%n",
                name,
                brand,
                price,
                gender);
    }
}
