package com.telerikacademy.oop.cosmetics.commands;

import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.Product;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductFactory;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.models.GenderType;
import com.telerikacademy.oop.cosmetics.models.ValidationH;

import java.util.List;

public class CreateProduct implements Command {
    private static final String PRODUCT_CREATED = "Product with name %s was created!";

    private final ProductRepository productRepository;
    private final ProductFactory productFactory;
    private String result;

    public CreateProduct(ProductRepository productRepository, ProductFactory productFactory) {
        this.productRepository = productRepository;
        this.productFactory = productFactory;
    }

    @Override
    public void execute(List<String> parameters) {
        //TODO Validate parameters count
        if (parameters.size() != 4) {
            throw new IllegalArgumentException("CreateProduct command expects 4 parameters.");
        }
        String name = parameters.get(0);
        String brand = parameters.get(1);

        //TODO Validate price format
        double price;
        try {
            price = Double.parseDouble(parameters.get(2));
        } catch (NumberFormatException e) {
            throw new NumberFormatException("Third parameter should be price (real number).");
        }

        //TODO Validate gender format
        GenderType gender;
        try {
            gender = GenderType.valueOf(parameters.get(3).toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Forth parameter should be one of Men, Women or Unisex.");
        }
        result = createProduct(name, brand, price, gender);
    }

    @Override
    public String getResult() {
        return result;
    }

    private String createProduct(String name, String brand, double price, GenderType gender) {
        //TODO Ensure category name is unique
        if (productRepository.getProducts().containsKey(name)) {
            throw new IllegalArgumentException(String.format("Product %s already exist.", name));
        }
        Product product = productFactory.createProduct(name, brand, price, gender);
        productRepository.getProducts().put(name, product);
        return String.format(PRODUCT_CREATED, name);

    }
}
