package com.telerikacademy.oop.furniture.models.contracts;

public interface ConvertibleChair {
    
    boolean getConverted();
    
    void convert();

    double getHeight();
}
