package com.telerikacademy.oop.furniture.models;

import com.telerikacademy.oop.furniture.models.contracts.Furniture;
import com.telerikacademy.oop.furniture.models.enums.MaterialType;

public abstract class FurnitureBase implements Furniture {

    private static final String MODEL_NULL_ERROR_MESSAGE = "Model input cannot be null or empty!";
    private static final int MODEL_NAME_MIN_LENGTH = 3;
    private static final String MODEL_LENGTH_ERROR_MESSAGE = "Model name must be at least 3 characters!";
    private static final String PRICE_TOO_LOW_ERROR_MESSAGE = "Price cannot be less or equal to 0!";
    private static final String HEIGHT_TOO_LOW_ERROR_MESSAGE = "Height cannot be less or equal to 0!";
    private static final String MATERIAL_NULL_ERROR_MESSAGE = "Material cannot be null!";
    private String model;
    private MaterialType material;
    private double priceInDollars;
    private double height;


    public FurnitureBase(String model, MaterialType material, double priceInDollars, double height) {
        setModel(model);
        setMaterial(material);
        setPriceInDollars(priceInDollars);
        setHeight(height);
    }

    private void setMaterial(MaterialType material) {
        this.material = material;
    }

    private void setModel(String model) {
        validateModel(model);
        validateModel(model);
        this.model = model;
    }

    private void setPriceInDollars(double priceInDollars) {
        validatedPrice(priceInDollars);
        this.priceInDollars = priceInDollars;
    }

    protected void setHeight(double heightInMeters) {
        validateHeight(heightInMeters);
        this.height = heightInMeters;
    }

    protected void validatedMaterialNotNull(String material) {
        Validator.validateStringNotNull(material, MATERIAL_NULL_ERROR_MESSAGE);
    }

    protected void validateModel(String model) {
        Validator.validateStringNotNull(model, MODEL_NULL_ERROR_MESSAGE);
        Validator.validateStringLength(model, MODEL_NAME_MIN_LENGTH, MODEL_LENGTH_ERROR_MESSAGE);
    }

    protected void validatedPrice(double priceInDollars) {
        Validator.checkInputValue(priceInDollars, PRICE_TOO_LOW_ERROR_MESSAGE);
    }

    protected void validateHeight(double heightInMeters) {
        Validator.checkInputValue(heightInMeters, HEIGHT_TOO_LOW_ERROR_MESSAGE);
    }

    @Override
    public MaterialType getMaterialType() {
        return this.material;
    }

    @Override
    public double getPrice() {
        return this.priceInDollars;
    }


    public double getHeight() {
        return this.height;
    }

    @Override
    public String getModel() {
        return this.model;
    }

    @Override
    public String toString() {
        return String.format(" Model: %s, Material: %s, Price: %.2f, Height: %.2f",
                this.model, this.material, this.priceInDollars, this.height);
    }
}
