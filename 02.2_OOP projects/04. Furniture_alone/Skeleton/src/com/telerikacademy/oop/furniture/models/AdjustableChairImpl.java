package com.telerikacademy.oop.furniture.models;

import com.telerikacademy.oop.furniture.models.contracts.AdjustableChair;
import com.telerikacademy.oop.furniture.models.enums.MaterialType;

public class AdjustableChairImpl extends ChairImpl implements AdjustableChair {
    private double newHeight;

    public AdjustableChairImpl(String model, MaterialType material, double priceInDollars, double height, int numberOfLegs) {
        super(model, material, priceInDollars, height, numberOfLegs);

    }

    @Override
    public void setHeight(double adjHeight) {
        if (adjHeight < 0) {
            throw new IllegalArgumentException("New height cannot be negative!");
        }
        newHeight = adjHeight;
    }

    @Override
    public double getHeight() {
        return newHeight;
    }

    public String toString() {
        return String.format("Type: AdjustableChair, Model: %s, Material: %s, Price: %.2f, Height: %.2f, Legs: %d",
                getModel(), getMaterialType(), getPrice(), getHeight(), getNumberOfLegs());
    }
}
