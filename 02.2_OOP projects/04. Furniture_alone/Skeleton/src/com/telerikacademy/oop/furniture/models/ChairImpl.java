package com.telerikacademy.oop.furniture.models;

import com.telerikacademy.oop.furniture.models.contracts.Chair;

import com.telerikacademy.oop.furniture.models.enums.MaterialType;

public class ChairImpl extends FurnitureBase implements Chair {

    private int numberOfLegs;

    public ChairImpl(String model, MaterialType material, double priceInDollars, double height, int numberOfLegs) {
        super(model, material, priceInDollars, height);
        setNumberOfLegs(numberOfLegs);

    }

    private void setNumberOfLegs(int numberOfLegs) {
        if (numberOfLegs < 0) {
            throw new IllegalArgumentException("Number of legs cannot be less than 0!");
        }
        this.numberOfLegs = numberOfLegs;
    }

    @Override
    public int getNumberOfLegs() {
        return this.numberOfLegs;
    }

    @Override
    public String toString() {
        return String.format("Type: Chair," + "%s" + ", Legs: %d",
                super.toString(), getNumberOfLegs());
    }
}
