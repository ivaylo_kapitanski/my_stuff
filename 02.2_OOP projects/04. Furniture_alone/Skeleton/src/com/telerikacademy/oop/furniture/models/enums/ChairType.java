package com.telerikacademy.oop.furniture.models.enums;

public enum ChairType {
    NORMAL,
    ADJUSTABLE,
    CONVERTIBLE;

    @Override
    public String toString() {
        switch (this){
            case NORMAL:
                return "Normal";
            case ADJUSTABLE:
                return "Adjustable";
            case CONVERTIBLE:
                return "Convertible";
            default:
                throw new IllegalArgumentException("Not a valid entry!");
        }
    }
}
