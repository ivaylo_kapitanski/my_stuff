package com.telerikacademy.oop.furniture.models.contracts;

public interface Chair extends Furniture {
    
    int getNumberOfLegs();
    
}
