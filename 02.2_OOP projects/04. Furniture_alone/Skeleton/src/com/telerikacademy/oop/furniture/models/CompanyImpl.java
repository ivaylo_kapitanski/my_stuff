package com.telerikacademy.oop.furniture.models;

import com.telerikacademy.oop.furniture.models.contracts.Company;
import com.telerikacademy.oop.furniture.models.contracts.Furniture;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CompanyImpl implements Company {
    private static final double EXACT_REGISTRATION_NUMBER_LENGTH = 10;
    private static final int NAME_MIN_LENGTH = 5;
    // Finish the class


    private String name;
    private String registrationNumber;
    private List<Furniture> furnitures;

    public CompanyImpl(String name, String registrationNumber) {
        setName(name);
        setRegistrationNumber(registrationNumber);
        furnitures = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public List<Furniture> getFurnitures() {
        return new ArrayList<>(furnitures);
    }

    public void add(Furniture furniture) {
        if (furniture == null) {
            throw new IllegalArgumentException("Furniture cannot be null.");
        }
        if (furnitures.contains(furniture)) {
            throw new IllegalArgumentException("This furniture already exists.");
        }
        furnitures.add(furniture);
    }

    private void setName(String name) {
        Validator.validateStringNotNull(name, "Name cannot be null.");
        Validator.validateStringLength(name, NAME_MIN_LENGTH, "Company name cannot be less than 5 characters long.");
        this.name = name;
    }

    private void setRegistrationNumber(String registrationNumber) {
        if (registrationNumber.length() != EXACT_REGISTRATION_NUMBER_LENGTH) {
            throw new IllegalArgumentException("Registration number is not valid");
        }
        if (!registrationNumber.chars().allMatch(Character::isDigit)) {
            throw new IllegalArgumentException("Registration number is not valid");
        }

        this.registrationNumber = registrationNumber;

    }

    private void setFurnitures(List<Furniture> furnitures) {
        this.furnitures = furnitures;
    }

    public String catalog() {
        StringBuilder strBuilder = new StringBuilder();
        // Finish it
        if (furnitures.size() == 0) {
            return String.format("%s - %s - no furnitures", name, registrationNumber);
        }
        furnitures.sort(Comparator.comparing(Furniture::getPrice));
        if (furnitures.size() == 1) {
            strBuilder.append(String.format("%s - %s - %d furniture%n", name, registrationNumber, furnitures.size()));
        }
        if (furnitures.size() > 1) {
            strBuilder.append(String.format("%s - %s - %d furnitures%n", name, registrationNumber, furnitures.size()));
        }

        for (Furniture furniture : furnitures) {
            strBuilder.append(furniture.toString());
            strBuilder.append(String.format("%n"));

        }
        return strBuilder.toString().trim();
    }

    public Furniture find(String model) {
        if (model == null) {
            throw new IllegalArgumentException("Model cannot be null.");
        }

        for (Furniture furniture : furnitures) {
            if (furniture.getModel().equals(model)) {
                return furniture;
            }
        }
        return null;

    }
    public void remove(Furniture furniture) {
        if (furnitures.contains(furniture)) {
            furnitures.remove(furniture);
        }
    }

}
