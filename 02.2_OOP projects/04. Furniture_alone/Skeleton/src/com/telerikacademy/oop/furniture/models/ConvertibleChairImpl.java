package com.telerikacademy.oop.furniture.models;

import com.telerikacademy.oop.furniture.models.contracts.ConvertibleChair;
import com.telerikacademy.oop.furniture.models.enums.MaterialType;

public class ConvertibleChairImpl extends ChairImpl implements ConvertibleChair {

    private static final double CONVERTED_NEW_HEIGHT = 0.1;

    private boolean isConverted;
    private String state = "Normal";

    public ConvertibleChairImpl(String model, MaterialType material, double priceInDollars, double height, int numberOfLegs) {
        super(model, material, priceInDollars, height, numberOfLegs);
        isConverted = false;
    }

    @Override
    public boolean getConverted() {
        return isConverted;
    }

    @Override
    public void convert() {
        if (isConverted) {
            isConverted = false;
            state = "Converted";
        } else {
            isConverted = true;
            state = "Normal";
        }
    }

    @Override
    public double getHeight() {
        if (getConverted()) {
            return CONVERTED_NEW_HEIGHT;
        }
        return super.getHeight();
    }

    public String toString() {
        return String.format("Type: ConvertibleChair, Model: %s, Material: %s, Price: %.2f, Height: %.2f, Legs: %d, State: %s",
                getModel(), getMaterialType(), getPrice(), getHeight(), getNumberOfLegs(), state);
    }
}
