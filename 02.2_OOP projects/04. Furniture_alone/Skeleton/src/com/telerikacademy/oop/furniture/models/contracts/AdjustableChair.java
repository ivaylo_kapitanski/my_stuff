package com.telerikacademy.oop.furniture.models.contracts;

public interface AdjustableChair {
    
    void setHeight(double height);
    double getHeight();
}
