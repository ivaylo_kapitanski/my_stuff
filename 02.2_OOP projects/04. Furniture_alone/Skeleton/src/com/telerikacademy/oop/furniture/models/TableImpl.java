package com.telerikacademy.oop.furniture.models;

import com.telerikacademy.oop.furniture.models.contracts.Table;
import com.telerikacademy.oop.furniture.models.enums.MaterialType;

public class TableImpl extends FurnitureBase implements Table {
    private static final String LENGTH_MIN_ERROR_MESSAGE = "Length cannot be less or equal ot 0!";
    private static final String WIDTH_MIN_ERROR_MESSAGE = "Width cannot be less or equal ot 0!";
    private double length;
    private double width;

    public TableImpl(String model, MaterialType material, double priceInDollars, double height, double length, double width) {
        super(model, material, priceInDollars, height);
        setLength(length);
        setWidth(width);
    }

    private void setLength(double length) {
        Validator.checkInputValue(length, LENGTH_MIN_ERROR_MESSAGE);
        this.length = length;
    }

    private void setWidth(double width) {
        Validator.checkInputValue(width, WIDTH_MIN_ERROR_MESSAGE);
        this.width = width;
    }

    @Override
    public MaterialType getMaterialType() {
        return super.getMaterialType();
    }

    @Override
    public double getLength() {
        return this.length;
    }

    @Override
    public double getWidth() {
        return this.width;
    }

    @Override
    public double getArea() {
        return this.length * this.width;
    }

    @Override
    public String toString() {
        return String.format("Type: Table," + "%s" + ", Length: %.2f, Width: %.2f, Area: %.4f",
                super.toString(), this.length, this.width, getArea());
    }
}
