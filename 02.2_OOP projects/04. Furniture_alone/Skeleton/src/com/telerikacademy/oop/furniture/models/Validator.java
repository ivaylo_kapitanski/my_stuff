package com.telerikacademy.oop.furniture.models;

public class Validator {

    public static void validateStringNotNull(String input,String errorMessage) {
        if (input == null|| input.isEmpty()) {
            throw new IllegalArgumentException(errorMessage);
        }
    }
    public static void validateStringLength(String input,int min,String errorMessage) {
        if (input.length()<min) {
            throw new IllegalArgumentException(errorMessage);
        }
    }
    public static void checkInputValue(double input, String errorMessage){
        if(input<=0){
            throw new IllegalArgumentException(errorMessage);
        }
    }

}

