package com.telerikacademy.oop.furniture.models.contracts;

import com.telerikacademy.oop.furniture.models.enums.MaterialType;

public interface Table {
    
    double getLength();
    
    double getWidth();
    
    double getArea();

    MaterialType getMaterialType();
}
