package com.telerikacademy.oop.furniture.commands;

import com.telerikacademy.oop.furniture.commands.contracts.Command;
import com.telerikacademy.oop.furniture.core.FurnitureRepositoryImpl;
import com.telerikacademy.oop.furniture.core.contracts.FurnitureFactory;
import com.telerikacademy.oop.furniture.core.contracts.FurnitureRepository;
import com.telerikacademy.oop.furniture.core.factories.FurnitureFactoryImpl;
import com.telerikacademy.oop.furniture.models.ChairImpl;
import com.telerikacademy.oop.furniture.models.CompanyImpl;
import com.telerikacademy.oop.furniture.models.contracts.Company;
import com.telerikacademy.oop.furniture.models.contracts.Furniture;
import com.telerikacademy.oop.furniture.models.enums.MaterialType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

public class AddFurnitureToCompany_Tests {
    
    private Command testCommand;
    private Company testCompany;
    private Furniture testFurniture;
    private FurnitureRepository furnitureRepository;
    private FurnitureFactory furnitureFactory;
    
    @BeforeEach
    public void before() {
        furnitureFactory = new FurnitureFactoryImpl();
        furnitureRepository = new FurnitureRepositoryImpl();
        testCommand = new AddFurnitureToCompany(furnitureRepository, furnitureFactory);
        testCompany = new CompanyImpl("companyName", "1234567890");
        testFurniture = new ChairImpl("model", MaterialType.LEATHER, 4, 4, 4);
    }
    
    @Test
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange, Act & Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(asList(new String[1])));
    }
    
    @Test
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange, Act & Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(asList(new String[3])));
    }
    
    @Test
    public void execute_should_throwException_when_companyDoesNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("companyName");
        testList.add("model");
        furnitureRepository.addFurniture("model", testFurniture);
        
        // Act & Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(testList));
    }
    
    @Test
    public void execute_should_throwException_when_furnitureDoesNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("companyName");
        testList.add("model");
        furnitureRepository.addCompany("companyName", testCompany);
        
        // Act & Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(testList));
    }
    
    
    @Test
    public void execute_should_addFurnitureToCompany_whenInputCorrect() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("companyName");
        testList.add("model");
        furnitureRepository.addCompany("companyName", testCompany);
        furnitureRepository.addFurniture("model", testFurniture);
        
        // Act
        testCommand.execute(testList);
        
        // Assert
        Assertions.assertEquals(1, testCompany.getFurnitures().size());
        
    }
    
}
