package com.telerikacademy.oop.furniture.factories;

import com.telerikacademy.oop.furniture.core.contracts.FurnitureFactory;
import com.telerikacademy.oop.furniture.core.factories.FurnitureFactoryImpl;
import com.telerikacademy.oop.furniture.models.ChairImpl;
import com.telerikacademy.oop.furniture.models.contracts.AdjustableChair;
import com.telerikacademy.oop.furniture.models.contracts.Chair;
import com.telerikacademy.oop.furniture.models.contracts.ConvertibleChair;
import com.telerikacademy.oop.furniture.models.contracts.Table;
import com.telerikacademy.oop.furniture.models.enums.MaterialType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FurnitureFactoryImpl_Tests {
    
    @Test
    public void createTable_should_createNewTable_when_withValidMaterialType() {
        // Arrange & Act
        FurnitureFactory factory = new FurnitureFactoryImpl();
        Table table = factory.createTable("model", "leather", 4, 4, 4, 4);
        
        // Assert
        Assertions.assertEquals(MaterialType.LEATHER, table.getMaterialType());
    }
    
    @Test
    public void createChair_should_CreateConvertibleChairWhenTypeIsConvertible() {
        
        //Arrange
        FurnitureFactory furnitureFactory = new FurnitureFactoryImpl();
        Chair chair = furnitureFactory.createChair("convertible", "Test", "leather", 1.2, 11.3, 3);
        
        //Assert
        Assertions.assertTrue(chair instanceof ConvertibleChair);
    }
    
    @Test
    public void createChair_should_CreateChairWhenTypeIsNormal() {
        //Arrange
        FurnitureFactory furnitureFactory = new FurnitureFactoryImpl();
        Chair chair = furnitureFactory.createChair("normal", "Test", "leather", 1.2, 11.3, 3);
        
        //Assert
        Assertions.assertTrue(chair instanceof ChairImpl);
    }
    
    @Test
    public void createChair_should_CreateAdjustableChairWhenTypeIsAdjustable() {
        //Arrange
        FurnitureFactory furnitureFactory = new FurnitureFactoryImpl();
        Chair chair = furnitureFactory.createChair("adjustable", "Test", "leather", 1.2, 11.3, 3);
        
        //Assert
        Assertions.assertTrue(chair instanceof AdjustableChair);
    }
    
}
