package com.company;

import com.company.contracts.Movable;
import com.company.oop.enums.Direction;
import com.company.shapesexample.enums.Color;

public class Rectangle extends Shape implements Movable {
    private final double width;
    private final double height;
    private Point center;

    public Rectangle(Color color, double width, double height,Point center) {
        super(color);
        this.width = width;
        this.height = height;
        this.center=center;

    }

    public Point getCenter() {
        return center;
    }

    @Override
    public String toString() {
        return String.format("Rectangle with center: %s, width %.2f and height %.2f",
                getCenter(),getWidth(),getHeight());
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    @Override
    protected double getArea() {
        return getHeight()*getWidth()    ;
    }

    @Override
    public void move(double distance, Direction dir) {
    center.move(distance,dir);
    }
}
