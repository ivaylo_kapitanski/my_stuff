package com.company.tryone;

import com.company.shapesexample.enums.Color;

public class Rectangle extends Shape {
    private final double width;
    private final double hight;

    public Rectangle(Color color, double width, double hight) {
        super(color);
        this.width = width;
        this.hight = hight;
    }

    public double getWidth() {
        return width;
    }

    public double getHight() {
        return hight;
    }

    @Override
    protected double getArea() {
        return hight*width;
    }
}
