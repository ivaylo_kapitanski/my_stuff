package com.company.tryone;

import com.company.shapesexample.enums.Color;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Shape> shapes=generateListOfShapes();

        for (Shape shape : shapes) {
            System.out.printf("This %s %s has area: %.2f%n",shape.getColor().toString().toLowerCase(),shape.getClass().getSimpleName(),shape.getArea());
        }


    }



    private static List<Shape> generateListOfShapes() {
        //Shape shape=new Shape(..) -error
        Shape shape1=new Circle(Color.RED,4.5);
        Shape shape2=new Rectangle(Color.GREEN,4,5);
        List<Shape>shapes=new ArrayList<>();
        shapes.add(shape1);
        shapes.add(shape2);
        return shapes;
    }
}
