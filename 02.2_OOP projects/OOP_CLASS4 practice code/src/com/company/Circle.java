package com.company;

import com.company.contracts.Movable;
import com.company.oop.enums.Direction;
import com.company.shapesexample.enums.Color;

public class Circle extends Shape implements Movable {

    private final double radius;
    private Point center;

    public Circle(Color color, double radius,Point center) {
        super(color);
        this.radius = radius;
        this.center=center;
    }

    public Point getCenter() {
        return center;
    }

    public double getRadius() {
        return radius;
    }

    @Override
    public String toString() {
        return String.format("Circle with center: %s and radius: %.2f",getCenter(),getRadius());
    }

    @Override
    protected double getArea() {
        return Math.PI*getRadius()*getRadius();
    }

    @Override
    public void move(double distance, Direction dir) {
    center.move(distance,dir);
    }
}
