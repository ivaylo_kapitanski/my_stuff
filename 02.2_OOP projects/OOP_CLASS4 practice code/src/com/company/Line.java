package com.company;

import com.company.contracts.Movable;
import com.company.oop.enums.Direction;
import com.company.shapesexample.enums.Color;

public class Line implements Movable {
    private Point firstEnd;
    private Point secondEnd;
    private Color color;

    public Line(Point firstEnd, Point secondEnd, Color color) {
        this.firstEnd = firstEnd;
        this.secondEnd = secondEnd;
        this.color = color;
    }

    @Override
    public String toString() {
        return String.format("Line between points %s and %s.",getFirstEnd(),getSecondEnd());
    }

    public Point getFirstEnd() {
        return firstEnd;
    }

    public Point getSecondEnd() {
        return secondEnd;
    }

    public Color getColor() {
        return color;
    }

    @Override
    public void move(double distance, Direction dir) {
    firstEnd.move(distance,dir);
    secondEnd.move(distance,dir);
    }
}
