package com.company.oop.enums;

public enum Direction {
    UP, DOWN, RIGHT, LEFT;
}
