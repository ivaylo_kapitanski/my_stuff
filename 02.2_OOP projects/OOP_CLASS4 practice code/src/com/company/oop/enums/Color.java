package com.company.shapesexample.enums;

public enum Color {
    GREEN, YELLOW, RED, BLUE;


    @Override
    public String toString() {
        switch (this) {
            case RED:
                return "Red";
            case BLUE:
                return "Blue";
            case GREEN:
                return "Green";
            case YELLOW:
                return "Yellow";
            default:
                return "N/A";
        }
    }

}
