package com.company.onlinestore.models;

import com.company.onlinestore.enums.Currency;

import java.time.LocalDate;

public class DomesticOrder extends BaseOrder{

    public DomesticOrder(String recipient, Currency currency, LocalDate deliveryOn) {
        super(recipient, currency, deliveryOn);
    }

    @Override
    protected String getOrderType() {
        return "Domestic";
    }
}
