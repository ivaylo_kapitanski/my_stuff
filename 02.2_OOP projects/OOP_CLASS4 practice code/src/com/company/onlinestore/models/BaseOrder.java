package com.company.onlinestore.models;

import com.company.onlinestore.contracts.Order;
import com.company.onlinestore.enums.Currency;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseOrder implements Order {
    private static final int RECIPIENT_MIN_LENGTH = 3;
    private static final int RECIPIENT_MAX_LENGTH = 35;
    private static final double BGN_EUR_CONVERSION_RATE = 1.95583;
    private static final double BGN_USD_CONVERSION_RATE = 1.75;
    private String recipient;
    private Currency currency;
    private LocalDate deliveryOn;
    private ArrayList<Product> items;


    public BaseOrder(String recipient,
                     Currency currency, LocalDate deliveryOn) {
        setRecipient(recipient);
        setCurrency(currency);
        setDeliveryOn(deliveryOn);
        items = new ArrayList<>();

    }
    //Setter for arraylist
    @Override
    public void addItem(Product item){
        if(getItems().contains(item)){
            throw new IllegalArgumentException("This item is already in the order.");
        }
        items.add(item);
    }
    //getter for arraylist
    public List<Product> getItems(){
        return new ArrayList<>(items);
    }
    @Override
    public String getRecipient() {
        return recipient;
    }

    private void setRecipient(String recipient) {
        if (recipient == null) {
            throw new IllegalArgumentException("Recipient cannot be empty");
        }
        if (recipient.length() < RECIPIENT_MIN_LENGTH || recipient.length() > RECIPIENT_MAX_LENGTH) {
            throw new IllegalArgumentException("Recipient must be between 3 and 35 characters.");
        }
        this.recipient = recipient;
    }
    @Override
    public LocalDate getDeliveryOn() {
        return deliveryOn;
    }

    private void setDeliveryOn(LocalDate deliveryOn) {
        this.deliveryOn = deliveryOn;
    }
    @Override
    public Currency getCurrency() {
        return currency;
    }

    private void setCurrency(Currency currency) {
        this.currency = currency;
    }
    @Override
    public String displayOrderDetails() {
        if (items.size() == 0) {
            System.out.println("No items");
        }
        StringBuilder sb = new StringBuilder("Items: ");
        for (Product item : items) {
            sb.append(String.format(" %s|", item.getDisplayInfo()));
        }
        return (sb.toString());
    }
    @Override
    public String displayGeneralInfor() {
        return String.format("%s Order recipient: %s | To be delivered on: %s | " +
                "Order total %.2f | Order currency: %s",getOrderType(), recipient, deliveryOn, calculateTotalPrice(), currency);
    }

    public double calculateTotalPrice() {
        double total = 0;
        for (Product product : items) {
            total += product.getPrice();
        }
        if (currency == Currency.EUR) {
            total = total * BGN_EUR_CONVERSION_RATE;
        }
        if (currency == Currency.USD) {
            total = total * BGN_USD_CONVERSION_RATE;
        }
        return total;
    }
    protected abstract String getOrderType();
}
