package com.company.onlinestore;

import com.company.onlinestore.contracts.Order;
import com.company.onlinestore.enums.Currency;
import com.company.onlinestore.models.BaseOrder;
import com.company.onlinestore.models.DomesticOrder;
import com.company.onlinestore.models.InternationalOrder;
import com.company.onlinestore.models.Product;

import java.time.LocalDate;
import java.time.Month;

public class Main {
    public static void main(String[] args) {
        Order order1 = new DomesticOrder("Ivo", Currency.BGN, LocalDate.now());
        order1.addItem(new Product("Keyboard",120));
        order1.addItem(new Product("Bira",50));

        Order order2 = new DomesticOrder("Gosho", Currency.EUR, LocalDate.now());
        order2.addItem(new Product("Mouse",20));
        order2.addItem(new Product("Bira",15));


        Order order3=new InternationalOrder(
                "Pesho2",Currency.USD,LocalDate.of(2021, Month.MAY,28),
                "DHL",20);

        order3.addItem(new Product("Backpack",150));
        order3.addItem(new Product("books",50));
        order3.addItem(new Product("speakers",90));


        Order[] orders = new Order[]{order1, order2,order3};
        displayOrders(orders);

//        Order[] internationalOrders = new Order[]{order3};
//        displayOrders(internationalOrders);

    }

    private static void displayOrders(Order[] orders) {
        for (Order order : orders) {
            System.out.println(order.displayGeneralInfor());
            System.out.println(order.displayOrderDetails());
            System.out.println("n---------------------n");
        }
    }
}
//Super