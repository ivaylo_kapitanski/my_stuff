package com.company.onlinestore.contracts;

import com.company.onlinestore.models.Product;
import com.company.onlinestore.enums.Currency;

import java.time.LocalDate;
import java.util.List;

public interface Order {
    String getRecipient();
    Currency getCurrency();
    LocalDate getDeliveryOn();
    List<Product> getItems();
    void addItem(Product item);
    String displayOrderDetails();
    String displayGeneralInfor();



}
