package com.company;

import com.company.contracts.Movable;
import com.company.oop.enums.Direction;

public class Point implements Movable {
     private double x;
     private double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return String.format("{x:%.2f, y:%.2f}",x,y);
    }

    @Override
    public void move(double distance, Direction dir) {
    switch (dir){
        case UP:
            y-=distance;
            break;
        case DOWN:
            y+=distance;
            break;
        case RIGHT:
            x+=distance;
            break;
        case LEFT:
            x-=distance;
            break;
        default:
            throw new IllegalArgumentException("No such direction!");
    }
    }
}
