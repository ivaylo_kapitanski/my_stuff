package com.company;

import com.company.contracts.Movable;
import com.company.oop.enums.Direction;
import com.company.shapesexample.enums.Color;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Shape> shapes = generateListOfShapes();
        List<Movable> movables=generateListOfMovable();

        for (Shape shape : shapes) {
            System.out.printf("This %s %s has area: %.2f %n",shape.getColor().toString(),shape.getClass().getSimpleName(),shape.getArea());
        }
        for (Movable movable : movables) {
            System.out.println("Before:");
            System.out.println(movable);
            movable.move(1.5, Direction.UP);
            System.out.println("After");
            System.out.println(movable);
        }
    }
    private static List<Movable> generateListOfMovable(){
        Point center=new Point(4.5,5.0);
        Movable shape1 = new Circle(Color.BLUE, 3,center);
        Movable shape2 = new Rectangle(Color.GREEN, 22, 14,center);
        Point a=new Point(4,6);
        Point b=new Point(6,7);
        Movable line=new Line(a,b,Color.GREEN);

        List<Movable> movables = new ArrayList<>();
        movables.add(shape1);
        movables.add(shape2);
        movables.add(line);
        return movables;
    }

        private static List<Shape> generateListOfShapes () {

            //Shape shape=new Shape();-нямаме право да създаваме инстанция от абстрактен клас-дава ни грешка
            Point center=new Point(4.5,5.0);
            Shape shape1 = new Circle(Color.BLUE, 3,center);
            Shape shape2 = new Rectangle(Color.GREEN, 22, 14,center);
            List<Shape> tempList = new ArrayList<>();
            tempList.add(shape1);
            tempList.add(shape2);
            return tempList;
        }
    }
