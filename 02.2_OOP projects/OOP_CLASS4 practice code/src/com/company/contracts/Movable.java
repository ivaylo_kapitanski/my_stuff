package com.company.contracts;

import com.company.oop.enums.Direction;

public interface Movable {
    void move(double distance, Direction dir);
}
