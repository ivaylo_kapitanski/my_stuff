package com.company.customexceptions.exceptions;

public class InvalidUserOperationException extends RuntimeException{
    public InvalidUserOperationException(String message){
        super(message);
    }
}
