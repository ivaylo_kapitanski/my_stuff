package com.company.throwcheckedexception;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number,divisor,result;
        try {
            System.out.println("Enter a number to divide:");
             number = scanner.nextInt();
            System.out.println("Enter a divisor:");
             divisor = scanner.nextInt();
        }
        catch(InputMismatchException e) {
            System.out.println("User input should be an integer number greater than 0!");
            return;
        }
        finally {
            System.out.println("User input is done!");
        }
        try {
             result =divide(number,divisor) ;

            System.out.printf("The result from the division of %d and %d is %d", number, divisor, result);
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    private static int divide(int x, int y)throws Exception{
        if(y<=0 ||x<=0){
            throw new Exception("Input must be greater than 0!");
        }
        return x/y;
    }
}
