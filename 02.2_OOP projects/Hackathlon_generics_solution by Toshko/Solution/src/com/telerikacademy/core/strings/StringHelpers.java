package com.telerikacademy.core.strings;

/**
 * A utility class for operations on strings.
 */
@SuppressWarnings("StringConcatenationInLoop")
public class StringHelpers {

    /**
     * Abbreviates a string using ellipses.<br>
     * <p>
     * This will turn "Telerik Academy Alpha Java" into "Telerik Academy Alpha...".<br><br>
     * <p>
     * If the number of characters in <code>source</code> is less than or equal to <code>maxLength</code>, returns <code>source</code>.
     *
     * @param source    The string to modify
     * @param maxLength Maximum length of the resulting string
     * @return The abbreviated String
     */
    public static String abbreviate(String source, int maxLength) {
        if (source.length() <= maxLength) return source;
        return concat(source.substring(0, maxLength), "...");
    }

    /**
     * Capitalizes a string changing the first character to title case. No other characters are changed.
     * <p>
     * If <code>source</code> is empty returns empty string.
     *
     * @param source The string to capitalize
     * @return The capitalized string or empty string if <code>source</code> is empty
     */
    public static String capitalize(String source) {
        if (isEmpty(source)) return source;
        return Character.toUpperCase(source.charAt(0)) + source.substring(1);
    }

    /**
     * Concatenates <code>string1</code> to the end of <code>string2</code>.
     *
     * @param string1 The left part of the new string
     * @param string2 The right part of the new string
     * @return A string that represents the concatenation of string1 followed by string2's characters
     */
    public static String concat(String string1, String string2) {
        return string1 + string2;
    }

    /**
     * Checks if <code>source</code> contains a character.
     *
     * @param source The string to check
     * @param symbol The character to check for
     * @return true if <code>symbol</code> is within <code>source</code> or false if not
     */
    public static boolean contains(String source, char symbol) {
        return firstIndexOf(source, symbol) != -1;
    }

    /**
     * Checks if the string <code>source</code> ends with the given character.
     *
     * @param source The string to inspect
     * @param target The character to search for
     * @return true if the string ends with <code>target</code>, else false
     */
    public static boolean endsWith(String source, char target) {
        return source.length() != 0 && source.charAt(source.length() - 1) == target;
    }

    /**
     * Finds the first index of <code>target</code> within <code>source</code>.
     *
     * @param source The string to check
     * @param target The character to check for
     * @return The first index of <code>target</code> within <code>source</code>, otherwise, -1
     */
    public static int firstIndexOf(String source, char target) {
        var chars = source.toCharArray();

        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == target) {
                return i;
            }
        }

        return -1;
    }

    /**
     * Finds the last index of <code>symbol</code> within <code>source</code>.
     *
     * @param source The string to check
     * @param symbol The character to check for
     * @return The last index <code>symbol</code> within <code>source</code> or -1 if no match
     */
    public static int lastIndexOf(String source, char symbol) {
        var index = -1;
        var array = source.toCharArray();

        for (int i = 0; i < array.length; i++) {
            if (array[i] == symbol) {
                index = i;
            }
        }

        return index;
    }

    /**
     * Pads string on the left and right sides if it's shorter than length.<br> <br>
     * <p>
     * For example <code>pad("Java", 8, '*')</code> returns <code>**Java**</code>
     * <p>
     * If <code>source</code> is empty or longer than <code>length</code>, returns <code>source</code>
     *
     * @param source        The string to pad
     * @param length        The length of the string to achieve
     * @param paddingSymbol The character used as padding
     * @return The padded string
     */
    public static String pad(String source, int length, char paddingSymbol) {
        if (isEmpty(source) || source.length() >= length) return source;
        var result = "";

        var symbolRepeatTimes = (length - source.length()) / 2;

        var leftPadding = repeat(String.valueOf(paddingSymbol), symbolRepeatTimes);
        var rightPadding = padEnd(source, symbolRepeatTimes + source.length(), paddingSymbol);

        result += concat(leftPadding, rightPadding);

        return result;

    }

    /**
     * Pads <code>source</code> on the right side with <code>paddingSymbol</code>
     * enough times to reach length <code>length</code>. <br> <br>
     * <p>
     * For example <code>padEnd("Java", 6, '*')</code> returns <code>Java**</code>
     *
     * @param source        The string to pad
     * @param length        The desired length to achieve
     * @param paddingSymbol The character to pad with
     * @return The padded string
     */
    public static String padEnd(String source, int length, char paddingSymbol) {
        if (isEmpty(source) || source.length() >= length) return source;
        var result = "";

        result += source;

        while (result.length() < length) {
            result += paddingSymbol;
        }


        return result;
    }

    /**
     * Pads <code>source</code> on the left side with <code>paddingSymbol</code>
     * enough times to reach length <code>length</code>. <br> <br>
     * <p>
     * For example <code>padStart("Java", 6, '*')</code> returns <code>**Java</code>
     *
     * @param source        The string to pad
     * @param length        The desired length to achieve
     * @param paddingSymbol The character to pad with
     * @return The padded string
     */
    public static String padStart(String source, int length, char paddingSymbol) {
        if (isEmpty(source) || source.length() >= length) return source;
        var result = "";

        while (result.length() < length - source.length()) {
            result += paddingSymbol;
        }

        result += source;

        return result;

    }

    /**
     * Repeats the given string <code>times</code> times.
     *
     * @param source The string to repeat
     * @param times  The number of times to repeat the string
     * @return The repeated string
     */
    public static String repeat(String source, int times) {
        if (times == 0) return source;

        var result = "";

        for (int i = 0; i < times; i++) {
            result += source;
        }

        return result;
    }

    /**
     * Reverses <code>source</code> so that the first element becomes the last, the second element becomes the second to last, and so on.
     *
     * @param source The string to modify
     * @return The reversed string
     */
    public static String reverse(String source) {
        var chars = source.toCharArray();

        var begin = 0;
        var end = chars.length - 1;
        char temp;

        while (end > begin) {
            temp = chars[begin];
            chars[begin] = chars[end];
            chars[end] = temp;
            end--;
            begin++;
        }

        return new String(chars);
    }

    /**
     * Returns a new string, starting from <code>start</code> and ending at <code>end</code>.
     *
     * @param source The string to get the new string from
     * @param start  The starting position in <code>source</code> (inclusive)
     * @param end    The end position in <code>source</code> (inclusive)
     * @return A new string, formed by the characters in <code>source</code>,
     * starting from <code>start</code> to <code>end</code>
     */
    public static String section(String source, int start, int end) {
        var result = "";

        for (int i = start; i <= end; i++) {
            result += source.charAt(i);
        }

        return result;
    }

    /**
     * Checks if the string <code>source</code> starts with the given character.
     *
     * @param source The string to inspect
     * @param target The character to search for
     * @return true if string starts with target, else false
     */
    public static boolean startsWith(String source, char target) {
        return source.length() != 0 && source.charAt(0) == target;
    }

    private static boolean isEmpty(String string) {
        return string.length() == 0;
    }

}
