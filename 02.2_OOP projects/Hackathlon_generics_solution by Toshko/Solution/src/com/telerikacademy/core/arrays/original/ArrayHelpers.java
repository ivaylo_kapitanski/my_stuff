package com.telerikacademy.core.arrays.original;

@SuppressWarnings({"ManualArrayCopy", "ExplicitArrayFilling"})
public class ArrayHelpers {

    /**
     * Adds <code>element</code> to the end of <code>source</code>.
     *
     * @param source  The array to add to
     * @param element The element to add
     * @return A new array, the original array with <code>element</code> at the end
     */
    public static int[] add(int[] source, int element) {
        var newLength = source.length + 1;
        var newArray = new int[newLength];

        copy(source, newArray, newLength);

        newArray[newArray.length - 1] = element;
        return newArray;
    }

    /**
     * Adds <code>element</code> at the start of <code>source</code>.
     *
     * @param source  The array to add to
     * @param element The element to add
     * @return A new array, the original array with <code>element</code> at head
     * position
     */
    public static int[] addFirst(int[] source, int element) {
        var newArray = new int[source.length + 1];

        copyFrom(source, 0, newArray, 1, source.length);
        newArray[0] = element;
        return newArray;
    }

    /**
     * Adds all <code>elements</code> to the end of <code>source</code>.
     *
     * @param source   The array to add to
     * @param elements The elements to add
     * @return A new array, the original array with all <code>elements</code> at the
     * end
     */
    public static int[] addAll(int[] source, int... elements) {
        var newElementsCount = elements.length;
        if (newElementsCount == 0)
            return source;

        var newArray = new int[source.length + newElementsCount];

        copy(source, newArray, source.length);

        for (int i = source.length, j = 0; i < newArray.length; i++, j++) {
            newArray[i] = elements[j];
        }
        return newArray;
    }

    /**
     * Checks if <code>source</code> contains <code>element</code>.
     *
     * @param source  The array to check in
     * @param element The element to check for
     * @return true if <code>source</code> contains <code>element</code>, otherwise,
     * false
     */
    public static boolean contains(int[] source, int element) {
        return firstIndexOf(source, element) != -1;
    }

    /**
     * Copies <code>count</code> elements from <code>sourceArray</code> into
     * <code>destinationArray</code>.
     *
     * @param sourceArray      The array to copy from
     * @param destinationArray The array to copy to
     * @param count            The number of elements to copy
     */
    public static void copy(int[] sourceArray, int[] destinationArray, int count) {
        var minLength = Math.min(sourceArray.length, count);

        for (int i = 0; i < minLength; i++) {
            destinationArray[i] = sourceArray[i];
        }
    }

    /**
     * Copies elements from <code>sourceArray</code>, starting from
     * <code>sourceStartIndex</code> into <code>destinationArray</code>, starting
     * from <code>destStartIndex</code>, taking <code>count</code> elements. <br>
     * <br>
     * For example:<br>
     * <code>
     * int[] array = {1, 2, 3, 4, 5};<br>
     * sourceStartIndex = 0;<br>
     * int[] destinationArray = new int[4];<br>
     * destStartIndex = 1;<br>
     * count = 2;<br>
     * copyFrom(array, sourceStartIndex, destinationArray, destStartIndex, count);
     * </code><br>
     * <br>
     * <code>destinationArray == {0, 1, 2, 0}</code><br>
     *
     * @param sourceArray      The array to copy from
     * @param sourceStartIndex The starting index in sourceArray
     * @param destinationArray The array to copy from
     * @param destStartIndex   The starting index in destinationArray
     * @param count            The number of elements to copy
     */
    public static void copyFrom(int[] sourceArray, int sourceStartIndex, int[] destinationArray, int destStartIndex,
                                int count) {
        if (sourceArray.length == 0)
            return;
        if (count > sourceArray.length)
            return;

        var minLength = Math.min(sourceArray.length, count) + destStartIndex;

        for (int i = destStartIndex, j = sourceStartIndex; i < minLength; i++, j++) {
            destinationArray[i] = sourceArray[j];
        }
    }

    /**
     * Fills <code>source</code> with <code>element</code>.
     *
     * @param source  The array to fill
     * @param element The element to fill with
     */
    public static void fill(int[] source, int element) {
        for (int i = 0; i < source.length; i++) {
            source[i] = element;
        }
    }

    /**
     * Finds the first index of <code>target</code> within <code>source</code>.
     *
     * @param source The array to check in
     * @param target The element to check for
     * @return The first index of <code>target</code> within <code>source</code>,
     * otherwise, -1
     */
    public static int firstIndexOf(int[] source, int target) {
        for (int i = 0; i < source.length; i++) {
            if (source[i] == target) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Inserts <code>element</code> at index <code>index</code> in
     * <code>source</code>
     *
     * @param source  The array to insert in
     * @param index   The index to insert at
     * @param element The element to insert
     * @return A new array with <code>element</code> in it
     */
    public static int[] insert(int[] source, int index, int element) {
        if (index == 0)
            return addFirst(source, element);
        if (index == source.length)
            return add(source, element);

        var newArray = new int[source.length + 1];

        copyFrom(source, 0, newArray, 0, index);

        newArray[index] = element;

        var newStartPosition = source.length - index - 1;
        copyFrom(source, newStartPosition, newArray, index + 1, source.length - 1);

        return newArray;
    }

    /**
     * Checks if <code>index</code> is a valid index in <code>source</code>
     *
     * @param source The array to check against
     * @param index  The index to check for
     * @return true if <code>index</code> is valid, otherwise, false
     */
    public static boolean isValidIndex(int[] source, int index) {
        return index >= 0 && index < source.length;
    }

    /**
     * Finds the last index of <code>target</code> within <code>source</code>.
     *
     * @param source The array to check in
     * @param target The element to check for
     * @return The last index of <code>target</code> within <code>source</code>,
     * otherwise, -1
     */
    public static int lastIndexOf(int[] source, int target) {
        var lastIndex = -1;

        for (int i = 0; i < source.length; i++) {
            if (source[i] == target)
                lastIndex = i;
        }

        return lastIndex;
    }

    /**
     * Removes all occurrences of <code>element</code> within <code>source</code>
     *
     * @param source  The array to remove from
     * @param element The element to remove
     * @return A new array with all occurences of <code>element</code> removed
     */
    public static int[] removeAllOccurrences(int[] source, int element) {
        var length = source.length;
        var i = 0;

        for (var j = 0; j < length; j++) {
            if (source[j] != element) {
                source[i] = source[j];
                i++;
            }
        }

        return section(source, 0, i - 1);
    }

    /**
     * Reverses <code>arrayToReverse</code>
     *
     * @param arrayToReverse The array to reverse
     */
    public static void reverse(int[] arrayToReverse) {
        for (int i = 0; i < arrayToReverse.length / 2; i++) {
            int temp = arrayToReverse[i];
            arrayToReverse[i] = arrayToReverse[arrayToReverse.length - i - 1];
            arrayToReverse[arrayToReverse.length - i - 1] = temp;
        }
    }

    /**
     * Returns a new array, from <code>source</code>, starting from
     * <code>startIndex</code> and until <code>endIndex</code>
     *
     * @param source     The array to create the new array from
     * @param startIndex The starting index
     * @param endIndex   The end index
     * @return A new array starting from <code>startIndex</code> and until
     * <code>endIndex</code>
     */
    public static int[] section(int[] source, int startIndex, int endIndex) {
        if (source.length == 0)
            return source;
        if (startIndex > source.length)
            return source;
        if (endIndex > source.length)
            endIndex = source.length - 1;

        var newArrayLength = endIndex - startIndex + 1;
        var newArray = new int[newArrayLength];

        copyFrom(source, startIndex, newArray, 0, newArrayLength);

        return newArray;
    }

}
