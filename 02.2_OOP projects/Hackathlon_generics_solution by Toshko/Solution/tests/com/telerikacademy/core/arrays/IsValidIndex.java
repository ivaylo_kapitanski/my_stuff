package com.telerikacademy.core.arrays;

import com.telerikacademy.core.arrays.refactored.ArrayHelpers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class IsValidIndex {

    private static final ArrayHelpers helpers = new ArrayHelpers();

    @Test
    @DisplayName("isValidIndex() returns true if the index is valid")
    public void isValidIndex_returns_true() {
        //Arrange
        var source = new Integer[]{1, 2, 3, 4};

        //Act
        var result = helpers.isValidIndex(source, 2);

        //Assert
        Assertions.assertTrue(result);
    }

    @Test
    @DisplayName("isValidIndex() returns false if the index is not valid")
    public void isValidIndex_returns_false() {
        //Arrange
        var source = new Integer[]{1, 2, 3, 4};

        //Act
        var result = helpers.isValidIndex(source, 7);

        //Assert
        Assertions.assertFalse(result);
    }


    @Test
    @DisplayName("isValidIndex() returns false if given an empty array")
    public void isValidIndex_empty() {
        //Arrange
        var source = new Integer[]{};

        //Act
        var result = helpers.isValidIndex(source, 0);

        //Assert
        Assertions.assertFalse(result);
    }

}
