package com.telerikacademy.core.arrays;

import com.telerikacademy.core.arrays.refactored.ArrayHelpers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class Reverse {

    private static final ArrayHelpers helpers = new ArrayHelpers();


    @Test
    @DisplayName("reverse() reverses the given array")
    public void reverse() {
        //Arrange
        var source = new Integer[]{1, 2, 3, 4, 5};
        var expected = new Integer[]{5, 4, 3, 2, 1};

        //Act
        helpers.reverse(source);

        //Assert
        Assertions.assertArrayEquals(source, expected);
    }

}
