package com.telerikacademy.core.arrays;

import com.telerikacademy.core.arrays.refactored.ArrayHelpers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class Add {

    private static final ArrayHelpers helpers = new ArrayHelpers();

    @Test
    @DisplayName("add() adds at the end")
    public void addTest() {
        //Arrange
        var source = new Integer[]{1, 2, 3};
        var expected = new Integer[]{1, 2, 3, 4};

        //Act
        var result = helpers.add(source, 4);

        //Assert
        Assertions.assertArrayEquals(result, expected);
    }

    @Test
    @DisplayName("add() returns an array with one element when given empty initial array")
    public void addTest_EmptyArray() {
        //Arrange
        Integer[] source = {};
        Integer[] expected = {1};

        //Act
        var result = helpers.add(source, 1);

        //Assert
        Assertions.assertArrayEquals(result, expected);
    }

}
