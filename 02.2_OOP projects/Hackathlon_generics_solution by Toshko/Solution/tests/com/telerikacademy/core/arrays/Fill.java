package com.telerikacademy.core.arrays;

import com.telerikacademy.core.arrays.refactored.ArrayHelpers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class Fill {

    private static final ArrayHelpers helpers = new ArrayHelpers();

    @Test
    @DisplayName("fill() fills the array with the given element")
    public void fill_test() {
        //Arrange
        var source = new Integer[4];
        var expected = new Integer[]{1, 1, 1, 1};

        //Act
        helpers.fill(source, 1);

        //Assert
        Assertions.assertArrayEquals(expected, source);
    }

}
