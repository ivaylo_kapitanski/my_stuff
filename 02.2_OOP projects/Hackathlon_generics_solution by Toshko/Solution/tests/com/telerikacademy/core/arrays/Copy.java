package com.telerikacademy.core.arrays;

import com.telerikacademy.core.arrays.refactored.ArrayHelpers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class Copy {

    private static final ArrayHelpers helpers = new ArrayHelpers();

    @Test
    @DisplayName("copy() copies enough elements from source array")
    public void copy_sourceArray_bigger() {
        //Arrange
        var source = new Integer[]{1, 2, 3, 4, 5};
        var destination = new Integer[4];
        var expected = new Integer[]{1, 2, 3, 4};

        //Act
        helpers.copy(source, destination, 4);

        //Assert
        Assertions.assertArrayEquals(expected, destination);
    }


    @Test
    @DisplayName("copy() fills the empty positions with default values")
    public void copy_destinationArray_bigger() {
        //Arrange
        var source = new Integer[]{1, 2, 3};
        var destination = new Integer[]{0, 0, 0, 0, 0, 0};
        var expected = new Integer[]{1, 2, 3, 0, 0, 0};

        //Act
        helpers.copy(source, destination, 6);

        //Assert
        Assertions.assertArrayEquals(expected, destination);
    }

}
