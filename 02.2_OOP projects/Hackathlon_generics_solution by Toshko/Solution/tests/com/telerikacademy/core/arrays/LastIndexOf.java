package com.telerikacademy.core.arrays;

import com.telerikacademy.core.arrays.refactored.ArrayHelpers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class LastIndexOf {

    private static final ArrayHelpers helpers = new ArrayHelpers();


    @Test
    @DisplayName("lastIndexOf() should return -1 when the element is not in the array")
    public void lastIndexOf_element_not_found() {
        //Arrange
        var source = new Integer[]{1, 2, 3, 2, 1};
        var expected = -1;

        //Act
        var result = helpers.lastIndexOf(source, 7);

        //Assert
        Assertions.assertEquals(result, expected);

    }

    @Test
    @DisplayName("lastIndexOf() should return the index when the element is in the array")
    public void lastIndexOf_element_found() {
        //Arrange
        var source = new Integer[]{1, 2, 3, 2, 1};
        var expected = 4;

        //Act
        var result = helpers.lastIndexOf(source, 1);

        //Assert
        Assertions.assertEquals(result, expected);

    }

    @Test
    @DisplayName("lastIndexOf() should return -1 when the array is empty")
    public void lastIndexOf_empty_array() {
        //Arrange
        var source = new Integer[]{};
        var expected = -1;

        //Act
        var result = helpers.lastIndexOf(source, 1);

        //Assert
        Assertions.assertEquals(result, expected);

    }

}
