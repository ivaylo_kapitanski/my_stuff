package com.telerikacademy.core.arrays;

import com.telerikacademy.core.arrays.refactored.ArrayHelpers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class AddFirst {

    private static final ArrayHelpers helpers = new ArrayHelpers();

    @Test
    @DisplayName("addFirst() adds a new element at the begging")
    public void addFirstTest() {
        //Arrange
        Integer[] source = {1, 2, 3};
        Integer[] expected = {4, 1, 2, 3};

        //Act
        var result = helpers.addFirst(source, 4);

        //Assert
        Assertions.assertArrayEquals(result, expected);
    }

    @Test
    @DisplayName("addFirst() returns an array with one element when given empty initial array")
    public void addFirstTest_EmptyArray() {
        //Arrange
        Integer[] source = {};
        Integer[] expected = {1};

        //Act
        var result = helpers.addFirst(source, 1);

        //Assert
        Assertions.assertArrayEquals(result, expected);
    }

}
