package com.telerikacademy.core.arrays;

import com.telerikacademy.core.arrays.refactored.ArrayHelpers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class RemoveAllOccurrences {

    private static final ArrayHelpers helpers = new ArrayHelpers();


    @Test
    @DisplayName("removeAllOccurrences() removes when element exists")
    public void removeAllOccurrences_removes_when_elementExists() {
        //Arrange
        var source = new Integer[]{1, 2, 3, 2, 4, 2, 5, 6};
        var expected = new Integer[]{1, 3, 4, 5, 6};

        //Act
        var result = helpers.removeAllOccurrences(source, 2);

        //Assert
        Assertions.assertArrayEquals(expected, result);
    }

    @Test
    @DisplayName("removeAllOccurrences() returns unchanged array when element doesn't exist")
    public void removeAllOccurrences_returnsUnchangedArray_when_elementDoesntExist() {
        //Arrange
        var source = new Integer[]{1, 2, 3, 2, 4, 2, 5, 6};
        var expected = new Integer[]{1, 2, 3, 2, 4, 2, 5, 6};

        //Act
        var result = helpers.removeAllOccurrences(source, 7);

        //Assert
        Assertions.assertArrayEquals(expected, result);
    }

}
