package com.telerikacademy.core.arrays;

import com.telerikacademy.core.arrays.refactored.ArrayHelpers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class Contains {

    private static final ArrayHelpers helpers = new ArrayHelpers();

    @Test
    @DisplayName("contains() returns true when the array contains the element")
    public void contains_true() {
        //Arrange
        Integer[] source = {1, 2, 3};

        //Act
        var result = helpers.contains(source, 1);

        //Assert
        Assertions.assertTrue(result);
    }

    @Test
    @DisplayName("contains() returns false when the array doesn't contain the element")
    public void contains_false() {
        //Arrange
        Integer[] source = {1, 2, 3};

        //Act, Assert
        Assertions.assertFalse(helpers.contains(source, 4));
    }

}
