package com.telerikacademy.core.arrays;

import com.telerikacademy.core.arrays.refactored.ArrayHelpers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class CopyFrom {

    private static final ArrayHelpers helpers = new ArrayHelpers();

    @Test
    @DisplayName("copyFrom() copies enough elements from source array")
    public void copy_sourceArray_bigger() {
        //Arrange
        var source = new Integer[]{1, 2, 3, 4, 5};
        var destination = new Integer[]{0, 0, 0, 0};
        var expected = new Integer[]{0, 1, 2, 0};

        //Act
        helpers.copyFrom(source, 0, destination, 1, 2);

        //Assert
        Assertions.assertArrayEquals(expected, destination);
    }

    @Test
    @DisplayName("copyFrom() fills the empty positions with default values")
    public void copy_destinationArray_bigger() {
        //Arrange
        var source = new Integer[]{1, 2, 3};
        var destination = new Integer[]{0, 0, 0, 0, 0};
        var expected = new Integer[]{0, 1, 2, 0, 0};

        //Act
        helpers.copyFrom(source, 0, destination, 1, 2);

        //Assert
        Assertions.assertArrayEquals(expected, destination);
    }

    @Test
    @DisplayName("copyFrom() takes all values from source array")
    public void copy_take_all() {
        //Arrange
        var source = new Integer[]{1, 2, 3};
        var destination = new Integer[]{0, 0, 0, 0};

        var expected = new Integer[]{0, 1, 2, 3};

        //Act
        helpers.copyFrom(source, 0, destination, 1, 3);

        //Assert
        Assertions.assertArrayEquals(expected, destination);
    }

}
