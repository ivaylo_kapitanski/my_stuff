package com.telerikacademy.core.arrays;

import com.telerikacademy.core.arrays.refactored.ArrayHelpers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class AddAll {

    private static final ArrayHelpers helpers = new ArrayHelpers();

    @Test
    @DisplayName("addAll() appends new elements at the end")
    public void addAllTest() {
        //Arrange
        Integer[] source = {1, 2, 3};
        Integer[] expected = {1, 2, 3, 4, 5, 6};

        //Act
        var result = helpers.addAll(source, 4, 5, 6);

        //Assert
        Assertions.assertArrayEquals(result, expected);
    }

    @Test
    @DisplayName("addAll() returns array of the new elements if source is empty")
    public void addAllTest_EmptyArray() {
        //Arrange
        Integer[] source = {};
        Integer[] expected = {1, 2, 3};

        //Act
        var result = helpers.addAll(source, 1, 2, 3);

        //Assert
        Assertions.assertArrayEquals(result, expected);
    }


    @Test
    @DisplayName("addAll() returns unchanged array if no new elements provided")
    public void addAllTest_noNewElements() {
        //Arrange
        Integer[] source = {1, 2, 3};
        Integer[] expected = {1, 2, 3};

        //Act
        var result = helpers.addAll(source);

        //Assert
        Assertions.assertArrayEquals(result, expected);
    }

}
