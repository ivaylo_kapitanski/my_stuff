package com.company.models;

public class ValidateHelper {

    public static void validateInput(String input){
        if(input==null){
            throw new IllegalArgumentException("Input cannot be empty");
        }
    }
    public static void validateLength(String input, int min, int max){
        if(input.length()<min||input.length()>max){
            throw new IllegalArgumentException(String.format("Input must be between %d and %d characters",min,max));
        }
    }


}
