package com.company.models;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class EventLog {


    final private String description;
    final private String localDateTimeConverted;

    public EventLog(String description) {
        //check if description is null
        ValidateHelper.validateInput(description);
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        localDateTimeConverted = now.format(formatter);
        this.description = description;
    }
    public String getDescription() {
        return description;
    }
    public String viewInfo(){
        return String.format("[%s] %s", localDateTimeConverted, description);

    }
}
