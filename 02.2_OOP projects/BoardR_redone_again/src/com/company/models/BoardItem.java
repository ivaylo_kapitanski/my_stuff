package com.company.models;

import com.company.enums.Status;
import jdk.jfr.Event;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class BoardItem {
    private static final int TITLE_MIN_LENGTH = 5;
    private static final int TITLE_MAX_LENGTH = 30;
    private String title;
    private LocalDate dueDate;
    private Status status = Status.OPEN;
    private ArrayList<EventLog> logs;

    public BoardItem(String title, LocalDate dueDate) {
        validateTitle(title);
        validateDueDate(dueDate);
        setStatus(status);
        logs = new ArrayList<>();
        logs.add(new EventLog(String.format("Item created: %s, [%s | %s]", title, status, dueDate)));
    }

    void addLog(EventLog log) {
        logs.add(log);
    }

    public ArrayList<EventLog> getLog() {
        return new ArrayList<>(logs);
    }

    public String getTitle() {
        return title;
    }

    public void validateTitle(String title) {
        //check if input is null
        ValidateHelper.validateInput(title);
        //check if title is of the correct length;
        ValidateHelper.validateLength(title, TITLE_MIN_LENGTH, TITLE_MAX_LENGTH);
        this.title = title;
    }

    public void setTitle(String title) {
        logs.add(new EventLog(String.format("Title changed from %s to %s", this.title, title)));
        this.title = title;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    void validateDueDate(LocalDate dueDate) {
        if (dueDate.isAfter(LocalDate.now())) {
            this.dueDate = dueDate;
        } else {
            throw new IllegalArgumentException("Please enter a valid date");
        }
    }

    public void setDueDate(LocalDate dueDate) {
        logs.add(new EventLog(String.format("DueDate changed from %s to %s", this.dueDate, dueDate)));
        this.dueDate = dueDate;
    }

    public Status getStatus() {
        return status;
    }

    private void setStatus(Status status) {
        this.status = status;
    }

    public void revertStatus() {
        if (getStatus() == Status.OPEN) {
            System.out.println("Can't revert, already at Open");
        } else {
            logs.add(new EventLog(String.format("Status changed from %s to %s", this.status, Status.values()[getStatus().ordinal() - 1])));
        }
        if (getStatus() != Status.OPEN) {
            setStatus(Status.values()[getStatus().ordinal() - 1]);
        }

    }

    public void advanceStatus() {
        if (getStatus() == Status.VERIFIED) {
            System.out.println("Can't advance, already at Verified");
        } else {
            logs.add(new EventLog(String.format("Status changed from %s to %s", this, Status.values()[getStatus().ordinal() + 1])));
        }

        if (getStatus() != Status.VERIFIED) {
            setStatus(Status.values()[getStatus().ordinal() + 1]);
        }
    }

    public String viewInfo() {
        return String.format("'%s', [%s | %s]", getTitle(), getStatus(), getDueDate());
    }

    public void displayHistory() {

    }
}
