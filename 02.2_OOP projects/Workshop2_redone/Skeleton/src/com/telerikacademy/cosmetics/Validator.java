package com.telerikacademy.cosmetics;

import java.util.Objects;

public class Validator {

    public static void checkIfNull(Object object){
        if(Objects.isNull(object)){
            throw new IllegalArgumentException("Input cannot be null!");
        }
    }

    public static void checkLength(String string,  int min, int max){
        if(string.length()<min || string.length()>max){
            throw new IllegalArgumentException(String.format("The %s cannot must be between %d and %d characters long!",
                    string,min,max));
        }
    }

    public static void checkIfBelowZero(double input,String str){
        if(input<0){
            throw new IllegalArgumentException(String.format("The %s cannot be below 0!",str));
        }
    }

}
