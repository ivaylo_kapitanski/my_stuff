package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.Validator;
import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.ScentType;
import com.telerikacademy.cosmetics.models.contracts.Cream;

public class CreamImpl extends ProductImpl implements Cream {

    private static final int CREAM_BRAND_MIN_LENGTH = 3;
    private static final int CREAM_NAME_MIN_LENGTH = 3;
    private static final int CREAM_BRAND_MAX_LENGTH = 15;
    private static final int CREAM_NAME_MAX_LENGTH = 15;

    private ScentType scent;

    public CreamImpl(String name, String brand, double price, GenderType gender, ScentType scent) {
        super(name, brand, price, gender);
        setScent(scent);
    }

    public ScentType getScent() {
        return scent;
    }

    protected void setScent(ScentType scent) {
        this.scent = scent;
    }

    @Override
    protected void validateName(String name) {
        Validator.checkIfNull(name);
        Validator.checkLength(name, CREAM_NAME_MIN_LENGTH, CREAM_NAME_MAX_LENGTH);

    }

    @Override
    protected void validateBrand(String brand) {
        Validator.checkIfNull(brand);
        Validator.checkLength(brand, CREAM_BRAND_MIN_LENGTH, CREAM_BRAND_MAX_LENGTH);

    }

    @Override
    public String print() {
        return String.format("%s" +
                " #Scent: %s\n", super.print(), getScent());
    }
}
