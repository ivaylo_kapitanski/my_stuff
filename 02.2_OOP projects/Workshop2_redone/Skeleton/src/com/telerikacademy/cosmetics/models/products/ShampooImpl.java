package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.Validator;
import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.UsageType;
import com.telerikacademy.cosmetics.models.contracts.Product;
import com.telerikacademy.cosmetics.models.contracts.Shampoo;
import org.junit.jupiter.params.shadow.com.univocity.parsers.annotations.Validate;

public class ShampooImpl extends ProductImpl implements Shampoo {
    private int milliliters;
    private UsageType usage;

    public ShampooImpl(String name, String brand, double price, GenderType gender, int milliliters, UsageType usage) {
        super(name, brand, price, gender);
        setMilliliters(milliliters);
        setUsage(usage);
    }


    public int getMilliliters() {
        return milliliters;
    }

    void setMilliliters(int milliliters) {
        Validator.checkIfBelowZero(milliliters, "Milliliters");
        this.milliliters = milliliters;
    }

    public UsageType getUsage() {
        return usage;
    }

    public void setUsage(UsageType usage) {
        this.usage = usage;
    }

    @Override
    public String print() {
        return String.format("%s" +
                " #Milliliters: %d\n" +
                " #Usage: %s\n", super.print(), getMilliliters(), getUsage());
    }
}
