package com.telerikacademy.cosmetics.models.common;

public enum ScentType {
    CHERRYVANILLA, STRAWBERRY,FRESHANDCLEAN,ORANGEULTRA;


    @Override
    public String toString() {
        switch (this){
            case STRAWBERRY:
                return "Strawberry";
            case ORANGEULTRA:
                return "Orange Ultra";
            case FRESHANDCLEAN:
                return "Fresh and Clean";
            case CHERRYVANILLA:
                return "Cherry Vanilla";
            default:
                return "Not specified";
        }
    }
}
