package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.Validator;
import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Product;

public abstract class ProductImpl implements Product {
    private static final int NAME_MIN_LENGTH = 3;
    private static final int NAME_MAX_LENGTH = 10;
    private static final int BRAND_MIN_LENGTH = 2;
    private static final int BRAND_MAX_LENGTH = 10;
    private String name;
    private String brand;
    private double price;
    private GenderType genderType;

    public ProductImpl(String name, String brand, double price, GenderType genderType) {
        validateName(name);
        validateBrand(brand);
        setPrice(price);
        setGenderType(genderType);

        this.name=name;
        this.brand=brand;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getBrand() {
        return brand;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public GenderType getGender() {
        return genderType;
    }

    @Override
    public String print() {
        return String.format("#%s %s\n" +
                " #Price: $%.2f\n" +
                " #Gender: %s\n", getName(), getBrand(), getPrice(), getGender());
    }

    protected void validateName(String name) {
        Validator.checkIfNull(name);
        Validator.checkLength(name, NAME_MIN_LENGTH, NAME_MAX_LENGTH);

    }

    protected void validateBrand(String brand) {
        Validator.checkIfNull(brand);
        Validator.checkLength(brand, BRAND_MIN_LENGTH, BRAND_MAX_LENGTH);
    }

    protected void setPrice(double price) {
        Validator.checkIfBelowZero(price, "Price");
        this.price =  price;
    }

    protected void setGenderType(GenderType genderType) {
        this.genderType = genderType;
    }
}
