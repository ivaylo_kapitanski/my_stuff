package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.Validator;
import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Product;
import com.telerikacademy.cosmetics.models.contracts.Toothpaste;
import org.junit.jupiter.params.shadow.com.univocity.parsers.annotations.Validate;

import java.util.ArrayList;
import java.util.List;

public class ToothpasteImpl extends ProductImpl implements Toothpaste {

    private List<String> ingredients = new ArrayList<>();

    public ToothpasteImpl(String name, String brand, double price, GenderType gender, List<String> ingredients) {
        super(name, brand, price, gender);
        setIngredients(ingredients);
    }

    protected void setIngredients(List<String> ingredients) {
        Validator.checkIfNull(ingredients);
        this.ingredients = ingredients;
    }

    @Override
    public List<String> getIngredients() {
        return new ArrayList<>(ingredients);
    }

    @Override
    public String print() {
        return String.format("%s" +
                " #Ingredients: %s\n", super.print(), getIngredients().toString());
    }

}
