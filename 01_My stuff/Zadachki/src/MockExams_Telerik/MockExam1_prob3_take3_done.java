package MockExams_Telerik;

import java.util.Scanner;

public class MockExam1_prob3_take3_done {
    public static void main(String[] args) {
        //scanner for the number
        //boolean to check if the condition is met
        //while loop to test each number
        //split the number into 3 digits, check if first+last==middle.
        //if yes-sum
        //if no-boolean is false and print the sum

        Scanner scanner=new Scanner(System.in);

        boolean isBalanced=true;
        int sum=0;
        while(isBalanced){
            int num=Integer.parseInt(scanner.nextLine());
            int digit1=(String.valueOf(num).charAt(0))-'0';
            int digit2=(String.valueOf(num).charAt(1))-'0';
            int digit3=(String.valueOf(num).charAt(2))-'0';
            if(digit1+digit3==digit2){
                sum+=num;
            }else{
                System.out.println(sum);
                isBalanced=false;
            }
        }

    }
}
