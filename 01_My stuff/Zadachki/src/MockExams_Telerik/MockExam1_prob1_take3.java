package MockExams_Telerik;

import java.util.Scanner;

public class MockExam1_prob1_take3 {
    public static void main(String[] args) {
        //scanner for the number-OK
        //enter the number to a string-OK
        //replace "-" and "." with ""-OK
        //split the digits of the number and sum to a new string
        //check if the result is less than 10-if so-print it
        //create a while loop to run until the result is less 10
        //in the loop-split the string into digits-sum them and store them to the first string-then check

        Scanner scanner=new Scanner(System.in);

        String input=scanner.nextLine();

        String inputClean=input.replaceAll("-","");
        inputClean=inputClean.replaceAll("\\.","");



        long num1=0;
        for (int i = 0; i <inputClean.length() ; i++) {
            num1+=inputClean.charAt(i)-'0';

        }
        boolean isReady=false;
        if(num1<10){
            System.out.println(num1);
           isReady=true;

        }

        long sum=num1;
        while(!isReady){
            long temp=0;
            for (int i = 0; i <String.valueOf(sum).length() ; i++) {
                temp+=(String.valueOf(sum).charAt(i))-'0';
            }
            sum=temp;
            if(sum<10){
                System.out.println(sum);
                isReady=true;
                break;
            }
        }
    }
}
