package MockExams_Telerik;

import java.util.Arrays;
import java.util.Scanner;

public class MockExam2_prob3_take3_done {
    public static void main(String[] args) {


        //scanner for the word to check-ok
        //scanner for the number of words to follow-ok
        //check if word.length==word1.length-if no-print no-OK
        //If yes-check if word1 contains the same characters-if no-print no
        //if yes-check if they are on a different place compared to word-if yes-print no
        //if yes-print yes
        //....this should be it

        Scanner scanner = new Scanner(System.in);
        String word = scanner.nextLine();
        int n = Integer.parseInt(scanner.nextLine());


        for (int i = 0; i <n ; i++) {
            String word2 = scanner.nextLine();

            if(word.equals(word2)|| word.length()!=word2.length()){
            System.out.println("No");
            continue;
            }
            char[] wordChar=word.toCharArray();
            char[] word2Char=word2.toCharArray();
            int count=0;

            Arrays.sort(wordChar);
            Arrays.sort(word2Char);

            for (int j = 0; j < wordChar.length ; j++) {

                if(wordChar[j]==word2Char[j]){
                    count++;
                }
            }
            if(count!=wordChar.length){
                System.out.println("No");
                count=0;
                continue;
            }else {
                System.out.println("Yes");
            }
            count=0;
        }
    }
}
