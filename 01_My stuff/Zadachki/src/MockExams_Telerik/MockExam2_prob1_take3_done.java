package MockExams_Telerik;

import java.util.Scanner;

public class MockExam2_prob1_take3_done {
    public static void main(String[] args) {
        //scanner for the input
        //create a max value of 0
        //split the num into separate digits;
        //compare the max with all possible combinations of*/+ with the numbers
        //print the new max

        Scanner scanner=new Scanner(System.in);

        int num=scanner.nextInt();
        int max=0;
        int digit1=(String.valueOf(num).charAt(0))-'0';
        int digit2=(String.valueOf(num).charAt(1))-'0';
        int digit3=(String.valueOf(num).charAt(2))-'0';


        max=Math.max(max,(digit1+digit2+digit3));
        max=Math.max(max,(digit1*digit2*digit3));
        max=Math.max(max,(digit1+digit2*digit3));
        max=Math.max(max,(digit1*digit2+digit3));

        System.out.println(max);

    }
}
