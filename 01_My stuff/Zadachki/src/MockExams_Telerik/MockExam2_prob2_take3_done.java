package MockExams_Telerik;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MockExam2_prob2_take3_done {
    public static void main(String[] args) {
        //scanner for the number of digits-OK
        //create na list or an array for the following entries-OK
        //populate the list with a loop-OK
        //enter one list of the merged digits
        //enter 2nd list for the squashed digits
        //print both lists with a space

        Scanner scanner=new Scanner(System.in);
        int n=Integer.parseInt(scanner.nextLine());

        List<Integer> list1=new ArrayList<>();

        for (int i = 0; i <n ; i++) {
            list1.add(Integer.parseInt(scanner.nextLine()));
        }

        List<String> merged=new ArrayList<>();
        List<Integer> squashed=new ArrayList<>();

        for (int k = 1; k < list1.size() ; k++) {
            char[] num1=(String.valueOf(list1.get(k-1))).toCharArray();
            char[] num2=(String.valueOf(list1.get(k))).toCharArray();
            merged.add(String.valueOf(num1[1])+String.valueOf(num2[0]));
            int digit=(num1[1]-'0')+(num2[0]-'0');
            if(digit>9){
                digit=digit%10;
            }
            String digit2= String.valueOf(num1[0])+String.valueOf(digit)+String.valueOf(num2[1]);
            squashed.add(Integer.parseInt(digit2));
        }
        for (int i = 0; i < merged.size() ; i++) {
            System.out.print(merged.get(i)+" ");
        }
        System.out.println();
        for (int i = 0; i < squashed.size() ; i++) {
            System.out.print(squashed.get(i)+" ");
        }


    }
}
