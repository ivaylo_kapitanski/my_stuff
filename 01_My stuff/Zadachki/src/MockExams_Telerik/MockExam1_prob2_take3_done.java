package MockExams_Telerik;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class MockExam1_prob2_take3_done {
    public static void main(String[] args) {
        //scanner for the num
        //loop from 0 to num and check if each int is a prime number.
        //if prime-add to list1
        //create a triangle with list1.size rows
        //each row is from 1 to list.get(i)
        //if the num is contained in list1-print 1(is prime). Else-print 0;

        Scanner scanner=new Scanner(System.in);

        int num=scanner.nextInt();

        List<Integer> primes=new ArrayList<>();
        for (int i = 1; i <= num; i++) {
            int flag=1;
            for (int k = 2; k <= i/2; ++k) {
                if (i % k == 0) {
                    flag = 0;
                    break;
                }
            }
            if(flag==1){
                primes.add(i);
            }}
        System.out.println(Arrays.toString(new List[]{primes}));

        for (int i = 0; i <primes.size() ; i++) {
            for (int z = 1; z <= primes.get(i) ; z++) {
                if(primes.contains(z)){
                    System.out.print(1);
                }else{
                    System.out.print(0);
                }
            }
            System.out.println();
        }
    }
}
