package MockExams_Telerik;

import java.util.Scanner;

public class MockExam2Problem1_done {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String[] input = scanner.nextLine().split("");
        int first = Integer.parseInt(input[0]);
        int second = Integer.parseInt(input[1]);
        int third = Integer.parseInt(input[2]);
        int max=0;
        max=Math.max(max,first+second+third);
        max=Math.max(max,first*second+third);
        max=Math.max(max,first+second*third);
        max=Math.max(max,first*second*third);

        System.out.println(max);
    }
}
