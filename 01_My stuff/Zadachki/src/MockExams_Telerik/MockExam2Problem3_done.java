package MockExams_Telerik;

import java.util.Arrays;

import java.util.Scanner;

public class MockExam2Problem3_done {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String word = scanner.nextLine();
        String n = scanner.nextLine();

        int nInt = Integer.parseInt(n);
        String[] wordArray = word.split("");
        boolean isAnagram=true;

        for (int i = 1; i <= nInt; i++) {
            String word2 = scanner.nextLine();
            String[] word2Array = word2.split("");

            if (word.equals(word2)) {
                System.out.println("No");

            } else if (word2Array.length == wordArray.length) {
                char[] char1 = word.toCharArray();
                Arrays.sort(char1);
                char[] char2 = word2.toCharArray();
                Arrays.sort(char2);

                for (int j = 0; j < char1.length; j++) {
                    if (!(char1[j] == (char2[j]))) {
                        isAnagram=false;
                        break;
                    }
                }
                if(isAnagram){
                System.out.println("Yes");}
                else{
                    System.out.println("No");
                    isAnagram=true;
                }
            }else{
                System.out.println("No");
            }
        }
    }
}
