package Telerik_exam_191020;

import java.util.Scanner;

public class _problem_1_done {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String[] input1=scanner.nextLine().split(", ");
        int wordsPage=Integer.parseInt(scanner.nextLine());
        String target=scanner.nextLine();

        int count=0;
        int totalCount=0;
        int page=1;

        for (int i = 0; i <input1.length ; i++) {
            //count++;
            if (i!=0&&(i)%(wordsPage)==0){
                page++;
                //count=1;
            }

            if(input1[i].equals(target)){

                totalCount=page;
            }

        }

        if(totalCount>0){
            System.out.println(totalCount);
        }else{
            System.out.println(-1);
        }

    }
}
