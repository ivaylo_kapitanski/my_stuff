package Telerik_exam_191020;

import java.util.Scanner;

public class _problem_3_done {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String[] input=scanner.nextLine().split("");

        int sumE=0;
        int sumO=0;

        for (int i = 0; i <input.length ; i++) {
            if(Integer.parseInt(input[i])%2==0){
                sumE+=Integer.parseInt(input[i]);
            }else{
                sumO+=Integer.parseInt(input[i]);
            }
        }
        if(sumE>sumO){
            System.out.println(sumE+" "+"energy drinks");
        }else if(sumE<sumO){
            System.out.println(sumO+" "+"cups of coffee");
        }else{
            System.out.println(sumO+" "+"of both");
        }

    }
}
