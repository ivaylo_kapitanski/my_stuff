package telerik.zadachi;

import java.util.Stack;

public class Valid_Parentheses_20 {

    public static void main(String[] args) {
        String s = "([{)]";
        System.out.println(isValid(s));
    }


    public static boolean isValid(String s) {

        if (s.isEmpty() || s.length() % 2 != 0) {
            return false;
        }

        Stack<Character> newStack = new Stack<>();

        for (int i = 0; i < s.length(); i++) {
            char sChar = s.charAt(i);
            if (sChar == '(' || sChar == '{' || sChar == '[') {
                newStack.push(sChar);
            }

            if (!newStack.isEmpty()) {
                switch (sChar) {
                    case ')':
                        if (newStack.peek() == '(') {
                            newStack.pop();
                        } else {
                            return false;
                        }
                        break;
                    case '}':
                        if (newStack.peek() == '{') {
                            newStack.pop();
                        } else {
                            return false;
                        }
                        break;
                    case ']':
                        if (newStack.peek() == '[') {
                            newStack.pop();
                        } else {
                            return false;
                        }
                        break;
                }

            } else return false;
        }
        return newStack.isEmpty();
    }
}

