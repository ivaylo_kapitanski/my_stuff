package telerik.zadachi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Jumps {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int inputLength = Integer.parseInt(br.readLine());
        String lineStr = br.readLine();

        int[] input = new int[inputLength];
        Queue<Integer> myQueue = new ArrayDeque<>();
        int counter=0;
        int max = 0;

        for (String entry : lineStr.split(" ")) {
            int entryInt = Integer.parseInt(entry);
            myQueue.offer(entryInt);
            input[counter]=entryInt;
            counter++;
            max = (max > entryInt) ? max : entryInt;
        }

        int maxMax = 0;
        int loopCounter = 0;
        StringBuilder stringBuilder = new StringBuilder();

        while (loopCounter < inputLength) {
            int jumpCount = 0;
            int current = (input[loopCounter]);
            if (current == max) {
                stringBuilder.append(0).append(" ");
                loopCounter++;
                myQueue.poll();
                continue;
            }
            myQueue.poll();
            for (Integer integer : myQueue) {
                if (current < integer) {
                    jumpCount++;
                    current = integer;
                    maxMax = Math.max(maxMax, jumpCount);
                }
                if(integer==max){
                    break;
                }
            }
            loopCounter++;
            stringBuilder.append(jumpCount).append(" ");
        }
        System.out.println(maxMax);
        System.out.println(stringBuilder.toString());
    }
}
