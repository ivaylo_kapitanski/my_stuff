package telerik.zadachi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Stack;

public class HDNL_TOY {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int inputLength = Integer.parseInt(br.readLine());
        Stack<String> inputStack = new Stack<>();
        Stack<String> tempStack = new Stack<>();

        for (int i = 0; i < inputLength; i++) {
            String newInput = br.readLine();
            if (inputStack.isEmpty()) {
                inputStack.push(String.format("<%s>", newInput));
                tempStack.push(String.format("</%s>", newInput));

            } else if (intConverter(inputStack.peek()) < intConverter(newInput)) {
                inputStack.push(String.format("<%s>", newInput));
                tempStack.push(String.format("</%s>", newInput));

            } else if (intConverter(inputStack.peek()) == intConverter(newInput)) {
                popUntilPriorNestedIsReached(inputStack, tempStack, newInput);
                inputStack.push(String.format("<%s>", newInput));
                tempStack.push(String.format("</%s>", newInput));

            } else {
                popUntilPriorNestedIsReached(inputStack, tempStack, newInput);
                inputStack.push(String.format("<%s>", newInput));
                tempStack.push(String.format("</%s>", newInput));
            }
            if (i == inputLength - 1 && !tempStack.isEmpty()) {
                while (!tempStack.isEmpty())
                inputStack.push(String.format("%s", tempStack.pop()));
            }
        }
        for (String s : inputStack) {
            System.out.println(s);
        }
    }

    private static int intConverter(String string) {
        return Integer.parseInt(string.replaceAll("[^0-9]", ""));
    }

    private static void popUntilPriorNestedIsReached(Stack<String> inputStack,
                                                     Stack<String> closingStack, String temp) {
        int newDigit = intConverter(temp);
        int peekDigit = intConverter(closingStack.peek());
        while (peekDigit >= newDigit) {
            inputStack.push(String.format("%s", closingStack.pop()));
            if (closingStack.isEmpty()) {
                break;
            }
            peekDigit = intConverter(closingStack.peek());
        }
    }
}
