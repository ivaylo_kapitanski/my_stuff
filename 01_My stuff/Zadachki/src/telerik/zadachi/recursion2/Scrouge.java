package telerik.zadachi.recursion2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Scrouge {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int[] mazeSize = Arrays.stream(br.readLine().split(" ")).mapToInt(Integer::parseInt).toArray();
        int[][] maze = new int[mazeSize[0]][mazeSize[1]];
        int startRow = 0;
        int startCol = 0;


        for (int row = 0; row < mazeSize[0]; row++) {
            int[] temp = Arrays.stream(br.readLine().split(" ")).mapToInt(Integer::parseInt).toArray();
            for (int col = 0; col < temp.length; col++) {
                maze[row][col] = temp[col];
                if (maze[row][col] == 0) {
                    startRow = row;
                    startCol = col;
                }

            }
        }
        int maxStart = checkMax(maze, startRow, startCol);
        int[] result=new int[1];
        int counter=0;

        solve(maze, startRow,startCol,maxStart, result,counter);

    }
    private static boolean isFinished=true;

    private static void solve(int[][] maze, int startRow, int startCol, int max, int[] result,int counter) {
        if(isOutOfMaze(maze,startRow,startCol)){
            return;
        }
        if(max==0 &&isFinished){
            System.out.println(result[0]);
            isFinished=false;
            return;
        }
        if(maze[startRow][startCol]<max&&counter>0){
            return;
        }
        if(maze[startRow][startCol]==max){
            maze[startRow][startCol]-=1;
            result[0]+=1;
            max=checkMax(maze,startRow,startCol);
            counter=0;
        }
        counter++;
        solve(maze,startRow,startCol-1,max,result,counter);
        solve(maze,startRow,startCol+1,max,result,counter);
        solve(maze,startRow-1,startCol,max,result,counter);
        solve(maze,startRow+1,startCol,max,result,counter);

    }

    private static boolean isOutOfMaze(int[][] maze, int startRow, int startCol) {
        return startRow < 0 || startCol  <0
                || startRow >= maze.length || startCol >= maze[0].length;
    }


    public static int checkMax(int[][] maze, int startRow, int startCol){
        int startNum=0;
        if(startCol-1>=0){
            startNum=Math.max(startNum,maze[startRow][startCol-1]);
        }
        if(startCol+1<maze[0].length){
            startNum=Math.max(startNum,maze[startRow][startCol+1]);
        }
        if(startRow-1>=0){
            startNum=Math.max(startNum,maze[startRow-1][startCol]);
        }
        if(startRow+1<maze.length){
            startNum=Math.max(startNum,maze[startRow+1][startCol]);
        }
        return startNum;

    }
}
