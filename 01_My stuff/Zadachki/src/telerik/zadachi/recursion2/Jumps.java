package telerik.zadachi.recursion2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Jumps {

    public static void main(String[] args) throws IOException {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        int inputSize=Integer.parseInt(br.readLine());
        int[] intArray = Arrays.stream(br.readLine().split(" ")).mapToInt(Integer::parseInt).toArray();

        int max=Integer.MIN_VALUE;
        for (int i = 0; i <intArray.length ; i++) {
            max=Math.max(max,intArray[i]);
        }

        int counter=0;
        StringBuilder sb=new StringBuilder();
        int maxJumps=Integer.MIN_VALUE;
        while(counter<inputSize){
            int jumpCounter=0;

            if(intArray[counter]==max){
                sb.append("0");
                sb.append(" ");
                counter++;
                maxJumps=Math.max(maxJumps,0);
                continue;
            }
            int tempMax=intArray[counter];
            for (int i = counter; i < inputSize; i++) {
                if(tempMax<intArray[i] ){
                    tempMax=intArray[i];
                    jumpCounter++;
                    maxJumps=Math.max(maxJumps,jumpCounter);
                }
                if(intArray[i]==max){
                    sb.append(jumpCounter);
                    sb.append(" ");
                    break;
                }
                if(i==inputSize-1){
                    sb.append(jumpCounter);
                    sb.append(" ");
                }

            }
            counter++;

        }
        System.out.println(maxJumps);
        System.out.println( sb.substring(0,sb.length()));

    }
}
