package telerik.zadachi.recursion2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Students_order {
    public static void main(String[] args) throws IOException {
        BufferedReader br =new BufferedReader(new InputStreamReader(System.in));
        int[] intArray = Arrays.stream(br.readLine().split(" ")).mapToInt(Integer::parseInt).toArray();
        List<String> students=new ArrayList<>(Arrays.asList(br.readLine().split(" ")));


        for (int i = 0; i <intArray[1] ; i++) {
            String[] changes=br.readLine().split(" ");
            students.remove(changes[0]);
            students.add(students.indexOf(changes[1]),changes[0]);
        }

        for (String student : students) {
            System.out.print(student+" ");

        }
    }
}
