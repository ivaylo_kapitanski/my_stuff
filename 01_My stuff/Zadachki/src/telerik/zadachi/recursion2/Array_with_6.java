package telerik.zadachi.recursion2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Array_with_6 {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int[] intArray = Arrays.stream(br.readLine().split(",")).mapToInt(Integer::parseInt).toArray();
        int index=Integer.parseInt(br.readLine());

        System.out.println(solve(intArray,index));

    }
    private static boolean solve(int[] intArray,int index) {
        if(intArray.length==0||index>intArray.length-1){
            return false;
        }
        if(intArray[index]==6){
            return true;
        }
        return solve(intArray,index+1);

    }
}
