package telerik.zadachi.recursion2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Factorial {
    public static void main(String[] args) throws IOException {
        BufferedReader br =new BufferedReader(new InputStreamReader( System.in));
       int input=Integer.parseInt(br.readLine());
        System.out.println(solve(input));
    }
    private static int solve(int input){
        if(input==1||input==0){
            return 1;
        }

        input*=solve(input-1);
        return input;
    }

}
