package telerik.zadachi.recursion2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Power_N {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int input = Integer.parseInt(br.readLine());
        int power = Integer.parseInt(br.readLine());
        System.out.println(solve(input, power));

    }

    private static int solve(int input, int power) {
        int result = input;

        if (input == 0) {
            return 0;
        }
        if (power == 0) {
            return 1;
        }
            if (power == 1) {
                return input;
            }


            result *= solve(input, power - 1);
            return result;


    }
}
