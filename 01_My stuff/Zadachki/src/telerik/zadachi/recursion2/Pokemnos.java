package telerik.zadachi.recursion2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Pokemnos {

    private static class Pokemon implements Comparable<Pokemon>{
        String name;
        String type;
        int power;
        int rank;

        public Pokemon(String name, String type, int power,int rank) {
            this.name = name;
            this.type = type;
            this.power = power;
            this.rank=rank;

        }

        public String getName() {
            return name;
        }

        public String getType() {
            return type;
        }

        public int getPower() {
            return power;
        }

        @Override
        public int compareTo(Pokemon o) {
            return Comparator
                    .comparing(Pokemon::getName)
                    .thenComparing(Pokemon::getPower,Comparator.reverseOrder())
                    .compare(this,o);
        }

        @Override
        public String toString() {
            return String.format("%s(%d)",getName(),getPower());
        }
    }
    private static HashMap<String, TreeSet<Pokemon>> pokemonsByType = new HashMap<>();
    private static List<Pokemon> rankList=new ArrayList<>();


    public static void main(String[] args) throws IOException {
        BufferedReader br =new BufferedReader(new InputStreamReader(System.in));

        while (true) {
            String[] commandLine=br.readLine().split(" ");

            switch (commandLine[0]){
                case "add":
                    addPokemon(commandLine[1],commandLine[2],Integer.parseInt(commandLine[3]),
                            Integer.parseInt(commandLine[4]));
                    break;
                case "find":
                    findByType(commandLine[1]);
                    break;
                case "ranklist":
                    getRanklist(Integer.parseInt(commandLine[1]),Integer.parseInt(commandLine[2]));
                    break;
                case "end":
                return;
            }

        }

    }

    private static void getRanklist(int start,int end) {
        int endCorrect=Math.min(end,rankList.size());
        int startCorrect = Math.max(0, start - 1);
        int counter=1;
        if(!rankList.isEmpty()){
            for (int i = startCorrect; i <endCorrect ; i++) {
                System.out.printf("%d. %s", counter++, rankList.get(i));
                if (i < endCorrect - 1) {
                    System.out.print("; ");
                }
            }
            System.out.println();
        }
    }

    private static void findByType(String type) {
        if (!pokemonsByType.containsKey(type)) {
            System.out.printf("Type %s: %n", type);
            return;
        }

        StringJoiner joiner = new StringJoiner("; ");
        int limit=5;
        for(Pokemon pokemon:pokemonsByType.get(type)){
            if(limit--==0){
                break;
            }
            joiner.add(pokemon.toString());
        }
        System.out.printf("Type %s: %s %n",type,joiner.toString());
    }

    private static void addPokemon(String name, String type, int power, int rank) {

        Pokemon newPokemon=new Pokemon(name,type,power,rank);
        if(!pokemonsByType.containsKey(type)){
            pokemonsByType.put(type,new TreeSet<>());
        }
        TreeSet<Pokemon>newBucket=pokemonsByType.get(type);
        newBucket.add(newPokemon);

        rankList.add(rank-1,newPokemon);
        System.out.printf("Added pokemon %s to position %d%n",name,rank);


    }
}
