package telerik.zadachi.recursion2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Bunny_ears {
    public static void main(String[] args) throws IOException {
        BufferedReader br =new BufferedReader(new InputStreamReader( System.in));
        int input=Integer.parseInt(br.readLine());
        System.out.println(solve(input));
    }
    private static int solve(int input){
        int countears=0;
        if(input==0){
            return 0;
        }
        if(input>0){
            countears+=2;
        }

         countears+=solve(input-1);
        return countears;
    }
}
