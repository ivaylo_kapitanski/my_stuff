package telerik.zadachi.recursion2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Count_Hi {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String input = (br.readLine());
        System.out.println(solve(input));

    }

    private static int solve(String input) {
        int count=0;
        int subHi=0;
        if (input.length() == 0||!input.contains("hi")) {
            return 0;
        }

        subHi=input.indexOf("hi");
            count++;


        count+= solve(input.substring(subHi+2));
        return count;


    }
}
