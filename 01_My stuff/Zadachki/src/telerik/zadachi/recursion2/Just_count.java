package telerik.zadachi.recursion2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Just_count {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        char[] input = br.readLine().toCharArray();
        Map<Character, Integer> characters = new TreeMap<>();
        Map<Character, Integer> upperCase = new TreeMap<>();
        Map<Character, Integer> lowerCase = new TreeMap<>();

        for (char c : input) {
            if (Character.isUpperCase(c)) {
                upperCase.put(c, upperCase.getOrDefault(c,0) + 1);
            } else if (Character.isLowerCase(c)) {
                lowerCase.put(c, lowerCase.getOrDefault(c,0) + 1);
            } else {
                characters.put(c, characters.getOrDefault(c,0) + 1);
            }
        }
        pritnMax(characters);
        pritnMax(lowerCase);
        pritnMax(upperCase);

    }

    private static void pritnMax(Map<Character, Integer> input ){
        if(input.isEmpty()) {
            System.out.println("-");
        }else {
          int maxValue=0;
          char maxChar=0;
            for (Map.Entry<Character, Integer> chars : input.entrySet()) {
                if(chars.getValue()>maxValue){
                    maxChar=chars.getKey();
                    maxValue=chars.getValue();
                }
            }

            System.out.println(maxChar + " " + maxValue);
        }
    }
}
