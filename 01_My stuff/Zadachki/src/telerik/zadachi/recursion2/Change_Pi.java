package telerik.zadachi.recursion2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Change_Pi {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String input = (br.readLine());
        solve(input);
    }

    private static void solve(String input) {
        int subHi=0;
        if (input.length() == 0||!input.contains("pi")) {
            System.out.println(input);
            return;
        }

        subHi=input.indexOf("pi");
        input=input.substring(0,subHi)+"3.14"+input.substring(subHi+2);

        solve(input);

    }
}
