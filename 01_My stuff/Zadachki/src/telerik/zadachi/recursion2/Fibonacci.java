package telerik.zadachi.recursion2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Fibonacci {
    public static void main(String[] args) throws IOException {
        BufferedReader br =new BufferedReader(new InputStreamReader( System.in));
        int input=Integer.parseInt(br.readLine());
        long[] fibArray = new long[input + 2];
        System.out.println(solve(input,fibArray));
    }
    private static long solve(int input, long[] fibArray){
        if(fibArray[input]==0){
            if(input==0){
                fibArray[input]=0;
            }else if(input==1) {
                fibArray[input] = 1;
            }else {
                fibArray[input] = solve(input - 1, fibArray) + solve(input - 2, fibArray);
            }
        }
        return fibArray[input];
    }
}
