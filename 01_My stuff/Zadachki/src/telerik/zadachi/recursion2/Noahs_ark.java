package telerik.zadachi.recursion2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.TreeMap;

public class Noahs_ark {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int inputSize = Integer.parseInt(br.readLine());

        Map<String, Integer> arkMap=new TreeMap<>();

        for (int counter = 0; counter <inputSize ; counter++) {
            String input= br.readLine();

            if(!arkMap.containsKey(input)){
                arkMap.put(input,1);
            }else{
                arkMap.put(input, arkMap.get(input)+1);
            }
        }
        for (Map.Entry<String, Integer> stringIntegerEntry : arkMap.entrySet()) {
            if(stringIntegerEntry.getValue()%2==0) {
                System.out.printf("%s %d %s%n", stringIntegerEntry.getKey(), stringIntegerEntry.getValue(), "Yes");
            }else{
                System.out.printf("%s %d %s%n", stringIntegerEntry.getKey(), stringIntegerEntry.getValue(), "No");
            }

        }
    }
}
