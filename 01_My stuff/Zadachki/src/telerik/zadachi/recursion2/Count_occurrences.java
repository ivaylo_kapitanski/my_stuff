package telerik.zadachi.recursion2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Count_occurrences {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int input = Integer.parseInt(br.readLine());

        System.out.println(solve(input));
    }
    private static int solve(int input){
        int count=0;
        if(input==0){
            return 0;
        }
        if(input%10==7){
            count++;
        }

        count+=solve(input/10);
        return count;
    }
}
