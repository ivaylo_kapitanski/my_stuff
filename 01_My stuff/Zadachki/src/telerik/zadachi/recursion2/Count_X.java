package telerik.zadachi.recursion2;

    import java.io.BufferedReader;
    import java.io.IOException;
    import java.io.InputStreamReader;

    public class Count_X {
        public static void main(String[] args) throws IOException {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String input = (br.readLine());
            System.out.println(solve(input));

        }

        private static int solve(String input) {
            int count=0;

            if (input.length() == 0||!input.contains("x")) {
                return 0;
            }
            if(input.charAt(input.length()-1)=='x'){
                count++;
            }

            count+= solve(input.substring(0,input.length()-1));
            return count;


        }
    }
