package telerik.zadachi.recursion2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Count_occurrences_2 {

        public static void main(String[] args) throws IOException {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            int input = Integer.parseInt(br.readLine());
            int old=0;
            System.out.println(solve(input,old));

        }
        private static int solve(int input,int old){
            int count=0;

            if(input==0){
                return 0;
            }
            if(input%10==8) {
                if (old == 8) {
                    count += 2;
                } else {
                    count++;
                }
            }
            old = input % 10;

            count+=solve(input/10,old);
            return count;
        }
    }