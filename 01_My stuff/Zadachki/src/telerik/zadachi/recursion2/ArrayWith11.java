package telerik.zadachi.recursion2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class ArrayWith11 {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int[] intArray = Arrays.stream(br.readLine().split(",")).mapToInt(Integer::parseInt).toArray();
        int index=Integer.parseInt(br.readLine());

        System.out.println(solve(intArray,index));

    }
    private static int solve(int[] intArray,int index) {
        int count=0;
        if(intArray.length==0||index>intArray.length-1){
            return 0;
        }
        if(intArray[index]==11){
            count++;

        }
        count+= solve(intArray,index+1);
        return count;
    }
}
