package telerik.zadachi.recursion2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Stack;

public class HDNL_Toy {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int inputSize = Integer.parseInt(br.readLine());

        Stack<String> storage=new Stack<>();
        Stack<String> finalStack=new Stack<>();

        for (int i = 0; i <inputSize ; i++) {
            String input=br.readLine();
            if(finalStack.isEmpty()){
                finalStack.push(String.format("<%s>",input));
                storage.push(String.format("</%s>",input));
            }
            else if(getInputDigit(input)>getInputDigit(storage.peek())){
                finalStack.push(String.format("<%s>",input));
                storage.push(String.format("</%s>",input));
            }else{
                while(getInputDigit(input)<=getInputDigit(storage.peek())) {
                    finalStack.push(storage.pop());
                    if(storage.isEmpty()){
                        break;
                    }
                }
                finalStack.push(String.format("<%s>",input));
                storage.push(String.format("</%s>",input));
            }
        }
        while (!storage.isEmpty()){
            finalStack.push(storage.pop());
        }

        for (String s : finalStack) {
            System.out.println(s);

        }
    }

    private static int getInputDigit(String input){
        return Integer.parseInt(input.replaceAll("\\D+",""));
    }
}
