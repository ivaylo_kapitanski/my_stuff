package telerik.zadachi.recursion2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.*;
import java.util.stream.Collectors;

public class Inventory_Manager_DAAAMN {

    private static class Items implements Comparable<Items> {
        public String name;
        public String type;
        public double price;

        public Items(String name, double price, String type) {
            this.name = name;
            this.type = type;
            this.price = price;
        }

        public String getName() {
            return name;
        }

        public String getType() {
            return type;
        }

        public double getPrice() {
            return price;
        }

        @Override
        public int compareTo(Items o) {
            return Comparator.comparing(Items::getPrice)
                    .thenComparing(Items::getName)
                    .thenComparing(Items::getType)
                    .compare(this, o);
        }
    }

    private static HashMap<String, Items> items = new HashMap<>();
    private static HashMap<String, TreeSet<Items>> itemsByType = new HashMap<>();
    private static TreeSet<Items> itemsSorted = new TreeSet<>();
    private static StringBuilder output = new StringBuilder();


    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));


        while (true) {
            String[] nextCommand = br.readLine().split(" ");

            switch (nextCommand[0]) {

                case "add":
                    addProduct(nextCommand[1], Double.parseDouble(nextCommand[2]), nextCommand[3]);
                    break;
                case "filter":
                    if (nextCommand[2].equals("type")) {
                        filterByType(nextCommand[3]);

                    } else if (nextCommand[2].equals("price")) {
                        if (nextCommand.length == 7) {
                            filterFromTo(Double.parseDouble(nextCommand[4]), Double.parseDouble(nextCommand[6]));
                        }else if(nextCommand[3].equals("from")){
                            filterFrom(Double.parseDouble(nextCommand[4]));
                        }else if(nextCommand[3].equals("to")){
                            filterTo(Double.parseDouble(nextCommand[4]));
                        }
                    }
                    break;
                case "end":
                    System.out.println(output);
                    return;
            }


        }
    }


    private static void addProduct(String name, double price, String type) {
        if (items.containsKey(name)) {
            output.append(String.format("Error: Item %s already exists%n", name));
            return;
        }
        Items newItem = new Items(name, price, type);
        items.put(name, newItem);

        if (!itemsByType.containsKey(type)) {
            itemsByType.put(type, new TreeSet<>());
        }
        TreeSet<Items> getItemBucket = itemsByType.get(type);
        getItemBucket.add(newItem);

        itemsSorted.add(newItem);
        output.append(String.format("Ok: Item %s added successfully%n",name));

    }

    private static void filterByType(String type) {
        if (!itemsByType.containsKey(type)) {
            output.append(String.format("Error: Type %s does not exists%n", type));
            return;
        }
        List<Items> filteredItems = itemsByType.get(type)
                .stream()
                .limit(10)
                .collect(Collectors.toList());

        print(filteredItems);

    }

    private static void filterFromTo(double from, double to) {
        List<Items> filteredItems = new ArrayList<>(itemsSorted);
        List<Items> result = new ArrayList<>();

        int limit = 10;
        for (Items filteredItem : filteredItems) {
            if (filteredItem.getPrice() > from && filteredItem.getPrice() < to) {
                if(result.size()<=limit) {
                    result.add(filteredItem);
                }
            }
        }
        print(result);

    }
    private static void filterFrom(double from) {
        List<Items> filteredItems = new ArrayList<>(itemsSorted);
        List<Items> result = new ArrayList<>();

        int limit = 10;
        for (Items filteredItem : filteredItems) {
            if (filteredItem.getPrice() > from) {
                if(result.size()<limit) {
                    result.add(filteredItem);
                }
            }
        }
        print(result);

    }

    private static void filterTo(double to) {
        List<Items> filteredItems = new ArrayList<>(itemsSorted);
        List<Items> result = new ArrayList<>();

        int limit = 10;
        for (Items filteredItem : filteredItems) {
            if (filteredItem.getPrice() < to) {
                if(result.size()<limit) {
                    result.add(filteredItem);
                }
            }
        }
        print(result);

    }

    private static void print(List<Items> filteredItems) {
        DecimalFormat newFormat=new DecimalFormat("#.#######");

       output.append("Ok: ");
       if(filteredItems.size()>0) {
           Items temp = filteredItems.get(0);
           output.append(String.format("%s(%s)", temp.name, newFormat.format(temp.price)));
           for (int i = 1; i < filteredItems.size(); i++) {
              temp=filteredItems.get(i);
               output.append(String.format(", %s(%s)", temp.name, newFormat.format(temp.price)));
           }
           output.append(System.lineSeparator());
       }else{
           output.append(System.lineSeparator());
       }

    }
}
