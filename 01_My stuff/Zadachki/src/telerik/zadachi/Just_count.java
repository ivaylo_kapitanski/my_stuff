package telerik.zadachi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.TreeMap;

public class Just_count {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        char[] input = (br.readLine()).toCharArray();

        Map<Character,Integer> lowerCase=new TreeMap<>();
        Map<Character,Integer> upperCase=new TreeMap<>();
        Map<Character,Integer> symbols=new TreeMap<>();

        for (Character c : input) {
            if(Character.isLowerCase(c)){
                lowerCase.put(c, lowerCase.getOrDefault(c,0)+1);
            } else if(Character.isUpperCase(c)){
                upperCase.put(c, upperCase.getOrDefault(c,0)+1);
            }else{
                symbols.put(c,symbols.getOrDefault(c,0)+1);
            }
        }
        print(symbols);
        print(lowerCase);
        print(upperCase);

    }
    public static void print(Map<Character,Integer> map){
        int max = 0;
        char maxChar = 0;
        for (Map.Entry<Character, Integer> characterIntegerEntry : map.entrySet()) {
            if(characterIntegerEntry.getValue() > max){
                max = characterIntegerEntry.getValue();
                maxChar = characterIntegerEntry.getKey();
            }
        }

        if(map.size() == 0){
            System.out.println("-");
        }else{
            System.out.printf("%s %d %n",maxChar,max);
        }
    }
}
