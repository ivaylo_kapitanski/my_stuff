package telerik.zadachi;

public class group_sum_rec {
    public static void main(String[] args) {
    int[] nums={2,4,8};
    int start=0;
    int target=10;
        System.out.println(groupSumClump(start,nums,target));
    }

    private static int lastAdded = 0;
    private static int sum=0;


    private static boolean groupSumClump(int start, int[] nums, int target) {
        if (sum == target) {
            return true;
        }
        if(start==nums.length-1){
            start=0;
            sum=0;
            start++;
        }

        if (nums[start] == lastAdded) {
            sum += nums[start++];
        }else{
            sum+=nums[start];
            lastAdded = nums[start];
        }
        groupSumClump(start+1, nums, target);

        return false;


    }


}
