package telerik.zadachi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import static java.util.Collections.reverseOrder;

public class PokemonApp {

    static class Pokemon implements Comparable<Pokemon> {
        private String name;
        private String type;
        private int power;
        private int position;

        public Pokemon(String name, String type, int power, int position) {
            this.name = name;
            this.power = power;
            this.position = position;
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public int getPower() {
            return power;
        }

        public int getPosition() {
            return position;
        }

        public String getType() {
            return type;
        }

        @Override
        public int compareTo(Pokemon o) {
            return Comparator
                    .comparing(Pokemon::getName)
                    .thenComparing(Pokemon::getPower, reverseOrder())
                    .compare(this, o);
        }

        @Override
        public String toString() {
            return String.format("%s(%d)", name, power);
        }
    }

    static Map<String, Set<Pokemon>> pokemonByType = new TreeMap<>();
    static List<Pokemon> rankList = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fullCommand;
        String[] input;
        while (true) {
            fullCommand = reader.readLine();
            input = fullCommand.split("\\s+");
            switch (input[0].toUpperCase(Locale.ROOT)) {
                case "ADD":
                    add(input);
                    break;
                case "FIND":
                    find(input[1], pokemonByType);
                    break;
                case "RANKLIST":
                    printRankList(Integer.parseInt(input[1]), Integer.parseInt(input[2]));
                    break;
                case "END":
                    return;
            }
        }
    }

    public static void add(String[] input) {
        Pokemon pokemon = new Pokemon(input[1], input[2], Integer.parseInt(input[3]), Integer.parseInt(input[4]));
        if (!pokemonByType.containsKey(input[2])) {
            pokemonByType.put(input[2], new TreeSet<>());
        }
        pokemonByType.get(input[2]).add(pokemon);
        rankList.add(Integer.parseInt(input[4]) - 1, pokemon);
        System.out.printf("Added pokemon %s to position %d%n", pokemon.getName(), pokemon.getPosition());
    }

    public static void find(String type, Map<String, Set<Pokemon>> pokemonByType) {
        if (!pokemonByType.containsKey(type)) {
            System.out.printf("Type %s: %n", type);
            return;
        }
        StringJoiner joiner = new StringJoiner("; ");
        long limit = 5;
        for (Pokemon pokemon : pokemonByType
                .get(type)) {
            if (limit-- == 0) break;
            String toString = pokemon.toString();
            joiner.add(toString);
        }
        String result = joiner.toString();
        System.out.printf("Type %s: %s%n", type, result);
    }

    public static void printRankList(int start, int end) {
        int endCorrect = Math.min(end, rankList.size());
        int counter = start;
        int startCorrect = Math.max(0, start - 1);
        for (int i = startCorrect; i < endCorrect; i++) {
            System.out.printf("%d. %s", counter++, rankList.get(i));
            if (i < endCorrect - 1) {
                System.out.print("; ");
            }
        }
        System.out.println();
    }
}
