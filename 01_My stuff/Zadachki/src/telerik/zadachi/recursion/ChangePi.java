package telerik.zadachi.recursion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ChangePi {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String n = (br.readLine());

        System.out.println(getNewString(n));

    }
    private static String getNewString(String x){

        int indexOfPi;

        if(!x.contains("pi")) {
            return x;
        }else {
            indexOfPi= x.indexOf("pi");
        }

        return getNewString(x.substring(0,indexOfPi)+"3.14"+x.substring(indexOfPi+2));

    }

}
