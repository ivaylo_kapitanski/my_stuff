package telerik.zadachi.recursion;

import java.util.*;

public class Variations {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = Integer.parseInt(sc.nextLine());
        String[] input = sc.nextLine().split("\\s+");

       // int counter = ((int) Math.pow(input.length, n));

        List<String> strings = new ArrayList<>();

        generateVariations(input, 0, n, strings);

        Set<String> set = new TreeSet<>(strings);

        for (String s : set) {
            System.out.println(s);
        }
    }

    public static void generateVariations(String[] input, int currDepth, int lengthVariation, List<String> currentList) {
        if (currDepth == lengthVariation) {
            return;
        }

        List<String> newSet = new ArrayList<>();

        if (currentList.isEmpty()) {
            currentList.addAll(Arrays.asList(input));
        } else {
            for (String s : currentList) {
                for (String i : input) {
                    newSet.add(i + s);
                }
            }
            currentList.clear();
            currentList.addAll(newSet);
        }

        generateVariations(input, currDepth + 1, lengthVariation, currentList);

    }
}
