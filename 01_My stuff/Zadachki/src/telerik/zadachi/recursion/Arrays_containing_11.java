package telerik.zadachi.recursion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Arrays_containing_11 {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int[] intArray = Arrays.stream(br.readLine().split(",")).mapToInt(Integer::parseInt).toArray();
        int index=Integer.parseInt(br.readLine());

        System.out.println(countOccurances(intArray,index));

    }
    private static int countOccurances(int[] intArray, int index){
        int count=0;
        if(intArray.length-1<index) {
            return 0;
        }

        if(intArray[index]==11) {
            count++;
        }
        count+= countOccurances(intArray,index+1);
        return count;
    }
}

