package telerik.zadachi.recursion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class bunny_ears2 {

        public static void main(String[] args) throws IOException {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            int n = Integer.parseInt(br.readLine());

            System.out.println(gerBunnyEars(n));

        }
        private static int gerBunnyEars(int n){
            int countEars=0;
            if(n==0) {
                return 0;
            }
            else if ((n%2==0)) {
                countEars+=3;

            }else if((n%2!=0)){
                countEars+=2;
            }

            countEars+=gerBunnyEars(n-1);
            return countEars;
        }
    }

