package telerik.zadachi.recursion;

public class main3 {
    public static void main(String[] args) {
        String s= "1122";
        ListLetterCombinationUtil(s, 0, "");
    }


    public static void ListLetterCombinationUtil(String s, int start, String preprocess) {
        if (start >= s.length()) {
            System.out.println(preprocess);
            return;
        }
        for (int i = start; i < s.length(); i++) {

            preprocess = preprocess + s.charAt(i);
            preprocess = preprocess + " ";
            ListLetterCombinationUtil(s, i + 1, preprocess);
            preprocess = preprocess.substring(0, preprocess.length() - 1);
        }
    }
}
