package telerik.zadachi.recursion;


    import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

    public class Cipher3 {
        public static void main(String[] args) throws IOException, IOException {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String message = br.readLine();
            String cipher = br.readLine();
            String[] code = cipher.split("[^A-Z0-9]+|(?<=[A-Z])(?=[0-9])|(?<=[0-9])(?=[A-Z])");
            Map<String,String> storage = new HashMap<>();
            Set<String> results = new TreeSet<>();

            String[] letters = new String[code.length/2];
            String[] ciphers = new String[code.length/2];

            for (int i = 0; i < code.length; i+=2) {
                letters[i/2] = code[i];
                ciphers[i/2] = code[i+1];
            }

            for (int i = 0; i < code.length/2; i++) {
                storage.put(ciphers[i], letters[i]);
            }

            codesCheck(message, storage, results, new StringBuilder());

            System.out.println(results.size());
            for (String s : results) {
                System.out.println(s);
            }
        }

        static void codesCheck(String message, Map<String,String> storage, Set<String> gList, StringBuilder sb) {
            if (message.equals("")) {
                gList.add(sb.toString());
                return;
            }

            for (String value : storage.keySet()) {
                if (message.startsWith(value)) {
                    String newMsg = message.substring(value.length());
                    sb.append(storage.get(value));
                    codesCheck(newMsg, storage, gList, sb);
                    sb.setLength(sb.length() - storage.get(value).length());
                }
            }
        }

    }

