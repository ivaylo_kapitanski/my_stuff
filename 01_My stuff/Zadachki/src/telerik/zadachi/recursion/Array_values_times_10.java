package telerik.zadachi.recursion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Array_values_times_10 {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int[] intArray = Arrays.stream(br.readLine().split(",")).mapToInt(Integer::parseInt).toArray();
        int index = Integer.parseInt(br.readLine());

        System.out.println(countOccurances(intArray, index));

    }

    private static boolean countOccurances(int[] intArray, int index) {

        if (intArray.length - 1 < index) {
            return false;
        }

        if (index > 0 && intArray[index - 1] * 10 == intArray[index]) {
            return true;
        }
        return countOccurances(intArray, index + 1);
    }
}
