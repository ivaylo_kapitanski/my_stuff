package telerik.zadachi.recursion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class countX {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String n = (br.readLine());

        System.out.println(getCount(n));

    }
    private static int getCount(String x){
        int countXs =0;

        if(x.length()==0) {
            return 0;
        }
        if(x.charAt(x.length()-1)=='x') {
            countXs++;
        }

        countXs +=getCount(x.substring(0,x.length()-1));
        return countXs;
    }

}
