package telerik.zadachi.recursion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class countHi {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String n = (br.readLine());

        System.out.println(getCount(n));

    }
    private static int getCount(String x){
        int count =0;
        int indexOfHi=0;
        if(!x.contains("hi")) {
            return 0;
        }else if(x.length()>2){
            count++;
           indexOfHi= x.indexOf("hi");

        }else {
            return 1;
        }

        count +=getCount(x.substring(indexOfHi+2));
        return count;
    }

}
