package telerik.zadachi.recursion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Largest_Surface {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int[] mazeSize = Arrays.stream(br.readLine().split(" ")).mapToInt(Integer::parseInt).toArray();
        int[][] maze = new int[mazeSize[0]][mazeSize[1]];

        for (int row = 0; row < mazeSize[0]; row++) {
            int[] temp = Arrays.stream(br.readLine().split(" ")).mapToInt(Integer::parseInt).toArray();
            for (int col = 0; col < temp.length; col++) {
                maze[row][col] = temp[col];
            }
        }
        boolean[][] memoMaze=new boolean[mazeSize[0]][mazeSize[1]];
        int currentDigit=maze[0][0];

        int maxRegion = 0;

        for (int row = 0; row < maze.length; row++) {
            for (int col = 0; col < maze[row].length; col++) {
                int size = checkRegion(maze, row, col, currentDigit, memoMaze);
                maxRegion = Math.max(maxRegion, size);
                currentDigit = maze[row][col];
            }
        }
        
        System.out.println(maxRegion);

    }

    private static int checkRegion(int[][] maze, int rowInput, int colInput, int currentDigit,boolean[][] memoMaze) {

        if (isOutOfMaze(maze, rowInput, colInput)) {
            return 0;
        }

        if (isVisited(rowInput, colInput,memoMaze)) {
            return 0;
        }

        if (isDifferentDigit(maze, rowInput, colInput, currentDigit)) {
            return 0;
        }

        memoMaze[rowInput][colInput] = true;
        int size = 1;


       size+= checkRegion(maze, rowInput, colInput-1,currentDigit,memoMaze);
        size+= checkRegion(maze, rowInput, colInput+1,currentDigit,memoMaze);
        size+= checkRegion(maze, rowInput+1, colInput,currentDigit,memoMaze);
        size+= checkRegion(maze, rowInput-1, colInput,currentDigit,memoMaze);

//        for (int currentRow = rowInput - 1; currentRow <= rowInput + 1; currentRow++) {
//            for (int currentCol = colInput - 1; currentCol <= colInput + 1; currentCol++) {
//                if (currentRow != rowInput || currentCol != colInput) {
//                    size += checkRegion(maze, currentRow, currentCol,currentDigit,memoMaze);
//                }
//
//            }
//        }
        return size;


    }

//    public static int getRegionSize(int[][] maze, int currentDigit,boolean[][] memoMaze) {
//        int maxRegion = 0;
//
//        for (int row = 0; row < maze.length; row++) {
//            for (int col = 0; col < maze[row].length; col++) {
//                    int size = checkRegion(maze, row, col,currentDigit,memoMaze);
//                    maxRegion = Math.max(maxRegion, size);
//                currentDigit=maze[row][col];
//            }
//
//        }
//        return maxRegion;
//    }


    private static boolean isOutOfMaze(int[][] maze, int row, int col) {
        return (row<0||col<0||row>=maze.length||col>=maze[0].length);

    }

    private static boolean isDifferentDigit(int[][] maze, int row, int col, int currentDigit) {
        return (maze[row][col] != currentDigit);

    }

    private static boolean isVisited(int row, int col,boolean[][] memoMaze) {
        return memoMaze[row][col] == true;
    }


}
