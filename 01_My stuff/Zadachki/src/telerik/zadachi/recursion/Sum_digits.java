package telerik.zadachi.recursion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Sum_digits {

        public static void main(String[] args) throws IOException {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            int n = Integer.parseInt(br.readLine());

            System.out.println(sumDigits(n));

        }
        private static int sumDigits(int n){
            int counter=0;
            if(n<1){
                return 0;
            }else {
                counter+=n%10;
            }

            counter+= sumDigits(n/10);
            return counter;
        }
    }


