package telerik.zadachi.recursion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Scrooge_McDuck {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int[] mazeSize = Arrays.stream(br.readLine().split(" ")).mapToInt(Integer::parseInt).toArray();
        int[][] maze = new int[mazeSize[0]][mazeSize[1]];
        int startRow = 0;
        int startCol = 0;


        for (int row = 0; row < mazeSize[0]; row++) {
            int[] temp = Arrays.stream(br.readLine().split(" ")).mapToInt(Integer::parseInt).toArray();
            for (int col = 0; col < temp.length; col++) {
                maze[row][col] = temp[col];
                if (maze[row][col] == 0) {
                    startRow = row;
                    startCol = col;
                }
            }
        }
         int maxStart = checkMax(maze, startRow, startCol);

            solveMaze(maze, startRow, startCol, maxStart);
    }

    private static int coinsCollected = 0;
    private static int counter=0;
    private static boolean isFinished=true;


    private static void solveMaze(int[][] maze, int startRow, int startCol, int max) {


        if (isOutOfMaze(maze, startRow, startCol)) {
            return;
        }

        if (max == 0&&isFinished) {
            System.out.println(coinsCollected);
            isFinished=false;
            return;
        }

        if (ifBelowMax(maze, startRow, startCol, max)&&counter>0) {
            return;
        }
        if (maze[startRow][startCol] == max) {
            maze[startRow][startCol] = maze[startRow][startCol] - 1;
            coinsCollected++;
            max=checkMax(maze,startRow,startCol);
            counter=0;

        }
        counter++;
            solveMaze(maze, startRow, startCol - 1, max);
            solveMaze(maze, startRow, startCol + 1, max);
            solveMaze(maze, startRow - 1, startCol, max);
            solveMaze(maze, startRow + 1, startCol, max);
    }

    private static boolean isOutOfMaze(int[][] maze, int startRow, int startCol) {
        return (startRow >= maze.length || startRow < 0 ||
                startCol >= maze[0].length || startCol < 0);
    }

    private static boolean ifBelowMax(int[][] maze, int startRow, int startCol, int max) {
        if (maze[startRow][startCol] < max) {
            return true;
        }
        return false;

    }

    private static int checkMax(int[][] maze, int startRow, int startCol) {
        int max = 0;
        if (startCol - 1 >= 0) {
            max = Math.max(max, maze[startRow][startCol - 1]);
        }
        if (startCol + 1 < maze[0].length) {
            max = Math.max(max, maze[startRow][startCol + 1]);
        }
        if (startRow - 1 >= 0) {
            max = Math.max(max, maze[startRow - 1][startCol]);
        }
        if (startRow + 1 < maze.length) {
            max = Math.max(max, maze[startRow + 1][startCol]);
        }

        return max;

    }

}
