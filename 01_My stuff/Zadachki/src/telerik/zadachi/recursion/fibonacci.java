package telerik.zadachi.recursion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class fibonacci {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(br.readLine());
        long[] fibArray = new long[n + 2];

        System.out.println(getFibonacciNum(n, fibArray));

    }

    private static long getFibonacciNum(int n, long[] fibArray) {
        if (fibArray[n] == 0) {
            if (n <= 0) {
                fibArray[n] = 0;
            } else if (n == 1) {
                fibArray[n] = 1;
            } else {
                fibArray[n] = getFibonacciNum(n - 1, fibArray) + getFibonacciNum(n - 2, fibArray);
            }
        }
        return fibArray[n];

    }
}