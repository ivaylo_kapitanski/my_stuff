package telerik.zadachi.recursion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Power_N {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int num = Integer.parseInt(br.readLine());
        int pow = Integer.parseInt(br.readLine());

        System.out.println(getThePowerN(num,pow));

    }

    private static long getThePowerN(int num, int pow) {
        if(pow<1){
            return 1;
        }
        return getThePowerN(num,pow-1)*num;

    }
}
