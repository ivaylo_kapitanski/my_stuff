package telerik.zadachi.recursion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;


public class Cipher {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String[] scrtMsg = (br.readLine().split(""));

        String[] cipher = br.readLine().trim().split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");

        Map<String, String> cipherMap = new HashMap<>();

        for (int i = cipher.length - 1; i > 0; i--) {
            cipherMap.put(cipher[i], cipher[i - 1]);
            i--;
        }
        Set<String> decoded = new TreeSet<>();
        StringBuilder stringBuilder = new StringBuilder();
        decodeCipher(scrtMsg, 0, stringBuilder, decoded, cipherMap);

        if (decoded.size() == 0) {
            System.out.println(0);
            return;
        }

        System.out.println(decoded.size());
        for (String s : decoded) {
            System.out.println(s);
        }

    }

    public static void decodeCipher(String[] scrtMsg, int start, StringBuilder codeToCheck,
                                    Set<String> decoded, Map<String, String> cipherMap) {
        if (start >= scrtMsg.length) {
            if (decodeHelper(codeToCheck.toString(), cipherMap).length() > 0) {
                decoded.add(decodeHelper(codeToCheck.toString(), cipherMap));
            }
            return;
        }

        for (int i = start; i < scrtMsg.length; i++) {


            codeToCheck.append(scrtMsg[i]);
            codeToCheck.append(" ");
            decodeCipher(scrtMsg, i + 1, codeToCheck, decoded, cipherMap);

//            if (codeToCheck.lastIndexOf(" ") == codeToCheck.length() - 1) {
//                codeToCheck.deleteCharAt(codeToCheck.length() - 1);
//            }
            codeToCheck.deleteCharAt(codeToCheck.length() -1);

        }
    }

    private static String decodeHelper(String input, Map<String, String> cipherMap) {
        String[] splitterator = input.split(" ");
        StringBuilder stringBuilder = new StringBuilder();

        for (String s : splitterator) {
            if (cipherMap.containsKey(s)) {
                stringBuilder.append(cipherMap.get(s));
            } else {
                stringBuilder.setLength(0);
                break;
            }
        }
        return stringBuilder.toString();
    }
}


