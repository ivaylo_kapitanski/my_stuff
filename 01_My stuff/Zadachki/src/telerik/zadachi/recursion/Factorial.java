package telerik.zadachi.recursion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Factorial {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(br.readLine());

        System.out.println(getFactorial(n));

    }
    private static int getFactorial(int n){

        if(n<=1){
            return 1;
        }
        return getFactorial(n-1)*n;
    }
}
