package telerik.zadachi.recursion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Count_occurances {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(br.readLine());

        System.out.println(countSpecificDigit(n));

    }
    private static int countSpecificDigit(int n){
        int counter=0;
        if(n<1){
            return 0;
        }else if(n%10==7) {
            counter++;
        }

        counter+= countSpecificDigit(n/10);
        return counter;
    }

}
