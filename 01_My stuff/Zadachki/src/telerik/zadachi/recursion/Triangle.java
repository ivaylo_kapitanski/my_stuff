package telerik.zadachi.recursion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Triangle {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(br.readLine());

        System.out.println(getTotalCubes(n));

    }
    private static int getTotalCubes(int n){
        int counter=0;
        if(n<1){
            return 0;
        }else {
            counter+=n;
        }
        counter+=getTotalCubes(n-1);
        return counter;
    }
}
