package telerik.zadachi.recursion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Variations2 {
    public static void main(String[] args) throws IOException {


        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        int variationLenght = Integer.parseInt(bufferedReader.readLine());
        String[] input = bufferedReader.readLine().split(" ");

        List<String> currentResult = new ArrayList<>();
        int depthCounter = 0;

        variationLooper(input, depthCounter, currentResult, variationLenght);

        Set<String> mySet = new TreeSet<>(currentResult);


        for (String s : mySet) {
            System.out.println(s);
        }
    }

    private static void variationLooper(String[] intput, int depthCounter,
                                        List<String> currentResult, int variationLength) {

        if (depthCounter == variationLength) {
            return;
        }
        List<String> tempList = new ArrayList<>();

        if (currentResult.isEmpty()) {
            currentResult.addAll(Arrays.asList(intput));
        } else {

            for (int listIndex = 0; listIndex < currentResult.size(); listIndex++) {
                for (int inputIndex = 0; inputIndex < intput.length; inputIndex++) {
                    tempList.add(currentResult.get(listIndex) + intput[inputIndex]);
                }
            }

            currentResult.clear();
            currentResult.addAll(tempList);
        }
        variationLooper(intput, depthCounter + 1, currentResult, variationLength);
    }

}
