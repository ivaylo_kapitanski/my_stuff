package telerik.zadachi.recursion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class ArrayWith6 {
    public static void main(String[] args) throws IOException {

    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    int[] intArray =Arrays.stream(br.readLine().split(",")).mapToInt(Integer::parseInt).toArray();
        int index=Integer.parseInt(br.readLine());

        System.out.println(checkIfDigitIsThere(intArray,index));

}
    private static boolean checkIfDigitIsThere(int[] intArray, int index){

        if(intArray.length-1<index) {
            return false;
        }

        if(intArray[index]==6) {
            return true;
        }
        return checkIfDigitIsThere(intArray,index+1);
    }
}
