package telerik.zadachi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.*;
import java.util.stream.Collectors;

public class Inventory_Manager {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Map<String, Item> itemsMap = new HashMap<>();
        Map<String, ArrayList<Item>> arrayListItems = new HashMap<>();

        String[] input;

        do {
            input = br.readLine().split(" ");

            switch (input[0].toLowerCase()) {

                case "add":
                    if (itemsMap.containsKey(input[1])) {
                        System.out.printf("Error: Item %s already exists%n", input[1]);
                        break;

                    } else {
                        Item item = new Item(input[1], Double.parseDouble(input[2]), input[3]);

                        itemsMap.put(input[1], item);
                        System.out.printf("Ok: Item %s added successfully%n", input[1]);
                        ArrayList<Item> newItemList = arrayListItems.get(input[3]);
                        if (newItemList == null) {
                            newItemList = new ArrayList<>();
                            newItemList.add(item);
                            itemsMap.put(input[1], item);
                            arrayListItems.put(input[3], newItemList);

                        } else {
                            newItemList.add(item);
                        }
                    }
                    break;

                case "filter":
                    switch (input[2]) {

                        case "type":
                            String typeFromList = input[3];

                            ArrayList<Item> newList = arrayListItems.get(typeFromList);

                            if (newList == null) {
                                System.out.printf("Error: Type %s does not exists%n", input[3]);
                            } else {

                                Collections.sort(newList);
                                System.out.print("Ok: ");
                                int limit = Math.min(newList.size(), 10);
                                for (int i = 0; i < limit; i++) {
                                    System.out.print(newList.get(i));
                                    if (i < limit - 1) {
                                        System.out.print(", ");

                                    } else {
                                        System.out.println();
                                    }
                                }
                            }
                            break;

                        case "price":
                            if (input.length > 5) {
                                double from = Double.parseDouble(input[4]);
                                double to = Double.parseDouble(input[6]);
                                printSortedMap(getSortedMapByPriceWithFromAndTO(itemsMap, from, to));
                                break;

                            } else if (input[3].equalsIgnoreCase("from")) {
                                double from = Double.parseDouble(input[4]);
                                printSortedMap(getSortedMapByPriceWithFrom(itemsMap, from));
                                break;

                            } else if (input[3].equalsIgnoreCase("to")) {
                                double to = Double.parseDouble(input[4]);
                                printSortedMap(getSortedMapByPriceWithTo(itemsMap, to));
                                break;
                            }
                    }
            }
        } while (!input[0].equalsIgnoreCase("end"));

    }

    private static class Item implements Comparable<Item> {
        private final String name;
        private final double price;
        private final String type;

        public Item(String name, double price, String type) {
            this.name = name;
            this.price = price;
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public double getPrice() {
            return price;
        }

        public String getType() {
            return type;
        }

        @Override
        public String toString() {
            DecimalFormat decimalFormat = new DecimalFormat("###.####");
            return String.format("%s(%s)", getName(), decimalFormat.format(getPrice()));
        }

        @Override
        public int compareTo(Item o) {
            return Comparator.comparing(Item::getPrice)
                    .thenComparing(Item::getName)
                    .thenComparing(Item::getType).compare(this, o);
        }
    }


    public static Map<String, Item> getSortedMapByPriceWithFromAndTO(Map<String, Item> itemsMap2, double from, double to) {

        return itemsMap2.entrySet()
                .stream().filter(e -> e.getValue().getPrice() >= from && e.getValue().getPrice() <= to)
                .sorted(Map.Entry.comparingByValue(
                        Comparator.comparing(Item::getPrice)
                                .thenComparing(Item::getName).thenComparing(Item::getType)))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (e1, e2) -> e1, LinkedHashMap::new));
    }

    public static Map<String, Item> getSortedMapByPriceWithFrom(Map<String, Item> itemsMap2, double from) {

        return itemsMap2.entrySet()
                .stream().filter(e -> e.getValue().getPrice() >= from)
                .sorted(Map.Entry.comparingByValue(
                        Comparator.comparing(Item::getPrice)
                                .thenComparing(Item::getName)
                                .thenComparing(Item::getType)))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (e1, e2) -> e1, LinkedHashMap::new));
    }

    public static Map<String, Item> getSortedMapByPriceWithTo(Map<String, Item> itemsMap2, double to) {

        return itemsMap2.entrySet()
                .stream().filter(e -> e.getValue().getPrice() <= to)
                .sorted(Map.Entry.comparingByValue(
                        Comparator.comparing(Item::getPrice)
                                .thenComparing(Item::getName).thenComparing(Item::getType)))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (e1, e2) -> e1, LinkedHashMap::new));
    }

    private static void printSortedMap(Map<String, Item> itemsMap2) {
        StringBuilder stringBuilder = new StringBuilder();

        int counter = 0;
        for (Map.Entry<String, Item> entry : itemsMap2.entrySet()) {
            stringBuilder.append(entry.getValue().toString()).append(", ");
            counter++;
            if (counter == 10) {
                break;
            }
        }
        if (stringBuilder.length() > 0) {
            System.out.println("Ok: " + stringBuilder.substring(0, stringBuilder.length() - 2));
        } else {
            System.out.println("Ok: ");
        }
    }
}
