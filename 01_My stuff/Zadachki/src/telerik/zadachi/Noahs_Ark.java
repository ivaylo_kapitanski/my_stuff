package telerik.zadachi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.TreeMap;

public class Noahs_Ark {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int inputLength = Integer.parseInt(br.readLine());
        Map<String,Integer> arkInput=new TreeMap<>();


        for (int i = 0; i <inputLength ; i++) {
            String input= br.readLine();
            if(arkInput.containsKey(input)){
                arkInput.put(input,arkInput.get(input)+1);
            }else{
                arkInput.put(input,1);
            }
        }

        for (Map.Entry<String, Integer> stringIntegerEntry : arkInput.entrySet()) {

            System.out.println(stringIntegerEntry.getKey()+" "
                    + stringIntegerEntry.getValue()+" "+checkIfEven(stringIntegerEntry.getValue()));
        }
    }


    private static String checkIfEven(int num){
        if(num%2==0){
            return "Yes";
        }
        return "No";
    }
}
