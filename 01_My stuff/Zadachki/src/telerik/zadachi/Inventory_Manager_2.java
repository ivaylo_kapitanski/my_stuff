package telerik.zadachi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.*;
import java.lang.*;
import java.util.stream.Collectors;

public class Inventory_Manager_2 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Set<Item> treeSet = new TreeSet<>();
        Map<String, ArrayList<Item>> typeToItems = new HashMap<>();
        String[] input;

       // long startTime = System.currentTimeMillis();


        do {
            input = br.readLine().split(" ");

            switch (input[0].toLowerCase()) {
                case "add":
                    String name = input[1];
                    ArrayList<Item> items = typeToItems.get(input[3]);
                    Item item = new Item(input[1], Double.parseDouble(input[2]), input[3]);

                    if (items == null) {
                        items = new ArrayList<>();

                    }
                    items.add(item);
                    typeToItems.put(input[3], items);

                    if (treeSet.stream().anyMatch(tree -> tree.getName().equals(name))) {
                        System.out.printf("Error: Item %s already exists%n", input[1]);
                        break;

                    } else {
                       treeSet.add(new Item(input[1], Double.parseDouble(input[2]), input[3]));
                        System.out.printf("Ok: Item %s added successfully%n", input[1]);
                        break;

                    }

                case "filter":
                    switch (input[2]) {

                        case "type":
                            String typeFromList = input[3];

                            if(treeSet.stream().noneMatch(item1 -> item1.getType().equals(typeFromList))) {
                                System.out.printf("Error: Type %s does not exists%n", input[3]);
                                break;
                            }
                             printSortedMap(getSortedMapByType(treeSet, typeFromList));

                            break;

                        case "price":
                            if (input.length > 5) {
                                double from = Double.parseDouble(input[4]);
                                double to = Double.parseDouble(input[6]);
                                printSortedMap(getSortedMapByPriceWithFromAndTO(treeSet, from, to));
                                break;

                            } else if (input[3].equalsIgnoreCase("from")) {
                                double from = Double.parseDouble(input[4]);
                                printSortedMap(getSortedMapByPriceWithFrom(treeSet, from));
                                break;

                            } else if (input[3].equalsIgnoreCase("to")) {
                                double to = Double.parseDouble(input[4]);
                                printSortedMap(getSortedMapByPriceWithTo(treeSet, to));
                                break;
                            }
                    }
            }
        } while (!input[0].equalsIgnoreCase("end"));
//        System.out.println(System.currentTimeMillis()-startTime);

    }

    private static class Item implements Comparable<Item> {
        private final String name;
        private final double price;
        private final String type;

        public Item(String name, double price, String type) {
            this.name = name;
            this.price = price;
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public double getPrice() {
            return price;
        }

        public String getType() {
            return type;
        }

        @Override
        public String toString() {
            DecimalFormat decimalFormat = new DecimalFormat("###.####");
            return String.format("%s(%s)", getName(), decimalFormat.format(getPrice()));
        }

        @Override
        public int compareTo(Item o) {
            return Comparator.comparing(Item::getPrice)
                    .thenComparing(Item::getName)
                    .thenComparing(Item::getType).compare(this, o);
        }
    }

    public static List<Item> getSortedMapByType(Set<Item> treeSet, String typeFromList) {

        return treeSet.stream().filter(item -> item.getType().equals(typeFromList))
                .sorted(Comparator.comparing(Item::getPrice)
                        .thenComparing(Item::getName)
                        .thenComparing(Item::getType)).collect(Collectors.toList());
    }

    public static List<Item> getSortedMapByPriceWithFromAndTO(Set<Item> treeSet, double from, double to) {

        return treeSet
                .stream().filter(item -> item.getPrice() >= from && item.getPrice() <= to)
                .sorted(Comparator.comparing(Item::getPrice)
                        .thenComparing(Item::getName)
                        .thenComparing(Item::getType))
                .collect(Collectors.toList());
    }

    public static List<Item> getSortedMapByPriceWithFrom(Set<Item> treeSet, double from) {
        return treeSet
                .stream().filter(item -> item.getPrice() >= from)
                .sorted(Comparator.comparing(Item::getPrice)
                        .thenComparing(Item::getName)
                        .thenComparing(Item::getType))
                .collect(Collectors.toList());
    }

    public static List<Item> getSortedMapByPriceWithTo(Set<Item> treeSet, double to) {

        return treeSet
                .stream().filter(item -> item.getPrice() <= to)
                .sorted(Comparator.comparing(Item::getPrice)
                        .thenComparing(Item::getName)
                        .thenComparing(Item::getType))
                .collect(Collectors.toList());
    }


    private static void printSortedMap(List<Item> newSet) {

        //StringJoiner stringJoiner = new StringJoiner(", ");
        StringBuilder stringBuilder = new StringBuilder();

        int counter = 0;
        for (Item item : newSet) {
            stringBuilder.append(item.toString()).append(", ");
            counter++;
            if (counter == 10) {
                break;
            }
        }

        if (stringBuilder.length() > 0) {
            System.out.println("Ok: " + stringBuilder.substring(0, stringBuilder.length() - 2));
        } else {
            System.out.println("Ok: ");
        }
    }
}

