package telerik.zadachi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Students_order {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String[] inputNums = br.readLine().split(" ");
        List<String> studentsAll = new ArrayList<>(Arrays.asList(br.readLine().split(" ")));

        for (int i = 0; i < Integer.parseInt(inputNums[1]); i++) {
            String[] temp = br.readLine().split(" ");

            studentsAll.remove(temp[0]);
            studentsAll.add(studentsAll.indexOf(temp[1]), temp[0]);
        }

        for (String s : studentsAll) {
            System.out.print(s + " ");

        }
    }
}
