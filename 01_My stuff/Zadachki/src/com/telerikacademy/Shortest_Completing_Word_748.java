package com.telerikacademy;

import java.util.Arrays;
import java.util.Scanner;

public class Shortest_Completing_Word_748 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String licensePlate = scanner.nextLine();
        String[] words = scanner.nextLine().split(",");

        licensePlate=licensePlate.toLowerCase();
        licensePlate = licensePlate.replaceAll("[0-9]", "");
        licensePlate = licensePlate.replaceAll(" ", "");

        String shortestWord = Arrays.toString(words);
        boolean doesContain = true;
        for (String word : words) {
            String wordTemp = word;
            char[] temp = word.toCharArray();
            if (licensePlate.length() > word.length()) {
                continue;
            }
            for (int z = 0; z < licensePlate.length(); z++) {
                String licensePlateTemp = String.valueOf(licensePlate.charAt(z));
                if (wordTemp.contains(licensePlateTemp)) {
                    int index = wordTemp.indexOf(licensePlateTemp);
                    temp[index] = '1';
                    wordTemp = String.valueOf(temp);
                } else {
                    doesContain = false;
                    break;
                }
            }
            if (doesContain && shortestWord.length() > word.length()) {
                shortestWord = word;
            }
            doesContain=true;

        }
        System.out.println(shortestWord);

    }
}
