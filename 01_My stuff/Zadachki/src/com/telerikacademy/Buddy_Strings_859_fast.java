package com.telerikacademy;
import java.util.HashSet;
import java.util.Scanner;

public class Buddy_Strings_859_fast {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String A = scanner.nextLine();
        String B = scanner.nextLine();


        //strategy: find differences
        //know for sure would return false
        if (A.length() != B.length() || A.length() < 2) {
            System.out.println(false);
            return;
        }
        //count how many differences
        int count = 0;
        int[] data = new int[26];
        HashSet<Integer> h = new HashSet<>();
        for (int i = 0; i < A.length(); i++) {
            h.add(A.charAt(i) - 'a');
            if (A.charAt(i) == B.charAt(i)) {
                continue;
            }
            count++;
            //too many differences
            if (count > 2) {
                System.out.println(false);
                return;
            }
            //update the individual chars
            data[A.charAt(i)-'a']++;
            data[B.charAt(i)-'a']++;
        }
        int diff=0;
        //count how many are different
        for (int i = 0; i < data.length ; i++) {
            if(i!=0){
                diff+=1;}
            else{
                diff+=0;}
        }
        //two cases
        //1. same amount of each letter in both Strings and differences are 2
        //2. same String and there is a duplicate for switching
        System.out.println((diff==0&&count==2)||(A.equals(B)&&h.size()<A.length()));
    }
}