package com.telerikacademy;

import java.util.Scanner;

public class Reverse_Integer_7 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        long x = scanner.nextLong();
        String xStr = String.valueOf(x);
        String x2 = "";


        if (x > Integer.MAX_VALUE || x < Integer.MIN_VALUE) {
            System.out.println(0);
            //return (0);
            return;
        }

        if (xStr.contains("-")) {
            x2 = "-";

            for (int i = xStr.length() - 1; i > 0; i--) {
                x2 += Integer.parseInt(String.valueOf(xStr.charAt(i)));
            }
        } else {

            for (int i = xStr.length() - 1; i >= 0; i--) {
                x2 += Integer.parseInt(String.valueOf(xStr.charAt(i)));
            }
        }
        long x3=Long.parseLong(x2);

        if (x3 > Integer.MAX_VALUE || x3 < Integer.MIN_VALUE) {
            System.out.println(0);
            //return (0);
            return;
        }
        System.out.println(Integer.parseInt(x2));
        //return (Integer.parseInt(x2));
        //1,534,236,469

    }
}
