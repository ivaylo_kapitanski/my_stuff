package com.telerikacademy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Build_an_Array_With_Stack_Operations_1441 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String[]input=scanner.nextLine().split(",");
        int n= scanner.nextInt();

        int[]target=new int[input.length];

        for (int i = 0; i <input.length ; i++) {
            target[i]=Integer.parseInt(input[i]);
        }

        List<Integer>example=new ArrayList<>();
        for (int i = 1; i <=n ; i++) {
            example.add(i);
        }
        List<String>output=new ArrayList<>();
            int pos=0;
        for (int i = 0; i < target.length ; i++) {
            if(target[i]== example.get(pos)){
                output.add("Push");
                pos++;
            }else{
                output.add("Push");
                output.add("Pop");
                i--;
                pos++;
            }
        }

        System.out.println(Arrays.toString(new List[]{output}));


    }
}
