package com.telerikacademy;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class KidsWithTheGreatestNumberOf_Candies_1431 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String[] input=scanner.nextLine().split(",");
        int extraCandies=Integer.parseInt(scanner.nextLine());
        int[] candies=new int[input.length];
        List<Boolean> kidsWithCandies=new ArrayList<>();


        for (int i = 0; i < input.length ; i++) {
            candies[i]=Integer.parseInt(input[i]);
        }

        int max=0;
        for (int i = 0; i <candies.length ; i++) {
            max=Math.max(max,candies[i]);
        }
        for (int i = 0; i <candies.length ; i++) {
            if(candies[i]+extraCandies>=max){
                kidsWithCandies.add(true);
            }else{
                kidsWithCandies.add(false);
            }
        }
        for (int i = 0; i < kidsWithCandies.size() ; i++) {
            System.out.print(kidsWithCandies.get(i));
        }
















    }
}
