package com.telerikacademy;

import java.util.*;

public class Majority_Element_II_229 {
    public static void main(String[] args) {

        Scanner scanner=new Scanner(System.in);
        String[] input=scanner.nextLine().split(",");
        int[]nums=new int[input.length];
        for (int i = 0; i < input.length ; i++) {
            nums[i]=Integer.parseInt(input[i]);
        }
        HashMap<Integer,Integer> hashMap=new HashMap<>();
        for (int num : nums) {
            if (hashMap.containsKey(num)) {
                hashMap.put(num, hashMap.get(num) + 1);
            } else {
                hashMap.put(num, 1);
            }
        }
        List<Integer>list1=new ArrayList<>();

        for (int num: hashMap.keySet()) {
            if(hashMap.get(num)> nums.length/3){
                list1.add(num);
            }
        }
        System.out.println(Arrays.toString(list1.toArray()));

    }
}
