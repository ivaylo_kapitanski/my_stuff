package com.telerikacademy;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class Squares_of_a_Sorted_Array_977 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String[]input1=scanner.nextLine().split(",");
        int[]nums=new int[input1.length];


        for (int i = 0; i < input1.length ; i++) {
            nums[i]=Integer.parseInt(input1[i]);
        }

        for (int i = 0; i <nums.length ; i++) {
            nums[i]=nums[i]*nums[i];
        }
        Arrays.sort(nums);

        System.out.println(Arrays.toString(nums));



    }
}
