package com.telerikacademy;

import java.util.Scanner;

public class Generate_a_String_With_Characters_That_Have_Odd_Counts_1374 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);

        int n=scanner.nextInt();
//        char[] alphabet=new char[26];
//        for (char i = 'a'; i <='z' ; i++) {
//            alphabet[i-'a']=i;
//        }
            StringBuilder word=new StringBuilder();
        if(n%2!=0){
            for (int i = 0; i <n-1 ; i++) {
                word.append("h");
            }
        }else{
            word.append("q");
            for (int i = 0; i <n ; i++) {
                word.append('r');

            }
        }
        String result=String.valueOf(word);
        System.out.println(result);

    }
}
