package com.telerikacademy;

import java.util.Arrays;

import java.util.Scanner;

public class Make_Two_Arrays_Equal_by_Reversing_SubArrays_1460 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String[] input=scanner.nextLine().split(",");
        String[] input2=scanner.nextLine().split(",");
        int[] target=new int[input.length];
        int[] arr=new int[input2.length];

        for (int i = 0; i < input.length ; i++) {
            target[i]=Integer.parseInt(input[i]);
        }
        for (int i = 0; i < input2.length ; i++) {
            arr[i]=Integer.parseInt(input[i]);
        }
        Arrays.sort(target);
        Arrays.sort(arr);
        boolean isTrue=true;
        for (int i = 0; i < target.length ; i++) {
            if(target[i]!=arr[i]){
            isTrue=false;
            break;
        }}
        System.out.println(isTrue);

    }
}
