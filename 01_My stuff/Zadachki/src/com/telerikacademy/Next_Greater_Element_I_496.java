package com.telerikacademy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Next_Greater_Element_I_496 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] input1 = scanner.nextLine().split(",");
        String[] input2 = scanner.nextLine().split(",");
        int[] nums1 = new int[input1.length];
        int[] nums2 = new int[input2.length];

        for (int i = 0; i < input1.length; i++) {
            nums1[i] = Integer.parseInt(input1[i]);
        }
        for (int i = 0; i < input2.length; i++) {
            nums2[i] = Integer.parseInt(input2[i]);
        }

        int[]nums3=new int[nums1.length];
        for (int i = 0; i < nums1.length; i++) {
            for (int z = 0; z < nums2.length; z++) {
                if (nums1[i] == nums2[z]) {
                    try {
                        for (int p = z; p < nums2.length ; p++) {
                        if (nums2[p] > nums1[i]) {
                            nums3[i]=(nums2[p]);
                            break;
                        } else {
                            nums3[i]=(-1);
                        }}
                    } catch (ArrayIndexOutOfBoundsException exception) {
                        nums3[i]=(-1);
                    }
                }
            }
        }
        System.out.println(Arrays.toString(nums3));
    }
}
