package com.telerikacademy;

import java.util.*;

public class Unique_Number_of_Occurrences_1207 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String[] input=scanner.nextLine().split(",");

        int[] arr=new int[input.length];

        for (int i = 0; i < arr.length ; i++) {
            arr[i]=Integer.parseInt(input[i]);
        }
        Arrays.sort(arr);
        boolean isUnique=true;
        HashMap<Integer,Integer> hash=new HashMap<>();
        HashSet<Integer> hashSet=new HashSet<>();

        for (int uniqEl:arr) {
           if(hash.containsKey(uniqEl)){
               hash.put(uniqEl,hash.get(uniqEl)+1);
           }else{
               hash.put(uniqEl,1 );
           }

        }

        for (int ele:hash.values()) {
            hashSet.add(ele);
        }
        if(hashSet.size()!=hash.size()){
            isUnique=false;
        }

        System.out.println(isUnique);

    }
}
