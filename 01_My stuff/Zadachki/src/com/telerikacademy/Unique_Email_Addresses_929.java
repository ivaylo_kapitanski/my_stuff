package com.telerikacademy;

import java.util.Arrays;
import java.util.Scanner;

public class Unique_Email_Addresses_929 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] emails = scanner.nextLine().split(",");

        for (int i = 0; i < emails.length; i++) {
            String[] temp = emails[i].split("");
            boolean plusExists = false;
            int countS = 0;
            int countE = 0;
            innerLoop:
            for (int k = 0; k < temp.length; k++) {
                if (temp[k].equals("@")) {
                    countE = k;
                    break innerLoop;
                }
                if (temp[k].equals(".")) {
                    temp[k] = "";
                }
                if ((temp[k].equals("+") && !plusExists)) {
                    plusExists = true;
                    countS = k;
                }
            }
            if (plusExists) {
                for (int p = countS; p < countE; p++) {
                    temp[p] = "";
                }
            }
            String str = String.join("", temp);

            emails[i] = str;

        }
        int countReceived=0;
        if(emails.length>0){
         countReceived=1;}

        Arrays.sort(emails);
        for (int i = 0; i <emails.length-1 ; i++) {
            if(!emails[i].equals(emails[i+1])){
                countReceived++;
            }
        }


        System.out.println(Arrays.toString(emails));
        System.out.println(countReceived);
    }
}
