package com.telerikacademy;

import java.util.Arrays;
import java.util.Scanner;

public class Last_Stone_Weight_1046 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] input1 = scanner.nextLine().split(",");
        int[] stones = new int[input1.length];
        for (int i = 0; i < input1.length; i++) {
            stones[i] = Integer.parseInt(input1[i]);
        }

        Arrays.sort(stones);
        int finalStone=0;
        int stonesLeft = stones.length;
        if (stones.length > 1) {
            while (stonesLeft > 1) {
                int temp=0;
                int count=0;
                if (stones[stones.length - 1] == stones[stones.length - 2]) {
                    stones[stones.length - 1] = 0;
                    stones[stones.length - 2] = 0;
                } else {
                    stones[stones.length - 1] = stones[stones.length - 1] - stones[stones.length - 2];
                    stones[stones.length - 2] = 0;
                }
                for (int i = 0; i < stones.length; i++) {
                    if(stones[i]==0) {
                        count++;
                    }
                    temp += stones[i];
                }
                if(count==(stones.length-1)){
                    finalStone=temp;
                    break;
                }else{
                    stonesLeft=stones.length-count;
                }
                Arrays.sort(stones);
            }
        } else {
            finalStone = stones[0];
        }

        System.out.println(finalStone);


    }
}
