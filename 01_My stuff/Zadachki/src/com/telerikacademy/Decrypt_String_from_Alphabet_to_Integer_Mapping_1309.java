package com.telerikacademy;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Decrypt_String_from_Alphabet_to_Integer_Mapping_1309 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String s=scanner.nextLine();

        List<Character> alphabet=Arrays.asList('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');

      char[] sChar=s.toCharArray();
        String word="";

        for (int i =sChar.length-1; i>=0; i--) {
            if(sChar[i]=='#'){
                String index=(sChar[i-2])+(String.valueOf(sChar[i-1]));
                word+=String.valueOf(alphabet.get(Integer.parseInt(index)-1));
                i-=2;
            }else{
                word+=String.valueOf(alphabet.get(sChar[i]-'0'-1));
            }
            
        }
        String[] word2=word.split("");
        String word3="";
        for (int i = word2.length-1; i >=0 ; i--) {
            word3+=word2[i];
        }
        System.out.println(word3);

    }
}
