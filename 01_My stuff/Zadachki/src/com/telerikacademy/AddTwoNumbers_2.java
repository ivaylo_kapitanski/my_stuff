package com.telerikacademy;

import java.util.*;

public class AddTwoNumbers_2 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String[] input1=scanner.nextLine().split(",");
        String[] input2=scanner.nextLine().split(",");
        List<Integer> l1=new ArrayList<>();
        List<Integer> l2=new ArrayList<>();

        String digit1="", digit2="";
        int sum;

        for (int i = 0; i <input1.length ; i++) {
            l1.add(Integer.parseInt(input1[i]));
        }

        for (int i = 0; i <input2.length ; i++) {
            l2.add(Integer.parseInt(input2[i]));
        }


        for (int i = 0; i <l1.size() ; i++) {
            digit1=digit1+String.valueOf(l1.get(input1.length-1-i));
        }
        for (int i = 0; i <l2.size() ; i++) {
            digit2=digit2+String.valueOf(l2.get(input2.length-i-1));
        }

        sum=Integer.parseInt(digit1)+Integer.parseInt(digit2);

        String[] sum1=String.valueOf(sum).split("");
        List<String> result=new ArrayList<>();

        for (int i =sum1.length-1; i >=0 ; i--) {
            result.add(sum1[i]);
        }

        for (int i = 0; i <result.size() ; i++) {
            System.out.print(result.get(i));
        }

    }

}
