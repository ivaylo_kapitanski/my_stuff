package com.telerikacademy;


import java.util.Scanner;

public class Increasing_Decreasing_String_1370 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String s=scanner.nextLine();
        int[] frequency=new int[26];

        for (char x:s.toCharArray()) {
            frequency[x-'a']+=1;
        }
        StringBuilder sb=new StringBuilder();
        while (sb.length()<s.length()){
            for (int i = 0; i <26 ; i++) {
                if(frequency[i]>0){
                    sb.append((char)(i+'a'));
                    frequency[i]-=1;
                }
            }
            for (int k = 25; k >=0 ; k--) {
                if(frequency[k]>0){
                    sb.append((char)(k+'a'));
                    frequency[k]-=1;
                }
            }
        }
        String s2=sb.toString();
        System.out.println(s2);









    }
}
