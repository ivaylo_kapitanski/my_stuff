package com.telerikacademy;

import java.util.Arrays;
import java.util.Scanner;

public class Average_Salary_Excluding_the_Minimum_and_Maximum_Salary_1491 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);

        String[]input=scanner.nextLine().split(",");
        int[]salary=new int[input.length];

        for (int i = 0; i < salary.length ; i++) {
            salary[i]=Integer.parseInt(input[i]);
        }
        Arrays.sort(salary);
        double sum=0;
        for (int k = 1; k < salary.length-1 ; k++) {
            sum+=salary[k];

        }
        double result=sum/(salary.length-2);
        System.out.println(result);


    }
}
