package com.telerikacademy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Fizz_Buzz_412 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n= scanner.nextInt();
        List<String> list1=new ArrayList<>();
        for (int i = 1; i <=n ; i++) {

            if((i%3==0)&&(i%5==0)){
                list1.add("FizzBuzz");
            }
            else if(i%3==0){
                list1.add("Fizz");
            }else if(i%5==0){
                list1.add("Buzz");
            }else{
                list1.add(String.valueOf(i));
            }
        }
        System.out.println(Arrays.toString(list1.toArray()));

    }
}
