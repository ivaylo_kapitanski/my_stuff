package com.telerikacademy;

import java.util.Scanner;

public class Matrix_Diagonal_Sum_1572 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int rows = scanner.nextInt();
        int columns = scanner.nextInt();
        int[][] mat = new int[rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                mat[i][j] = scanner.nextInt();
            }
        }

        int primary = 0;
        int secondary = 0;
        int noNo = (int) mat.length / 2;

        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat.length; j++) {
                if (i == j) {
                    primary += mat[i][j];
                }

                if ((i + j) == (mat.length - 1)) {
                    if (mat.length % 2 != 0) {
                        if (i == noNo && j == noNo) {
                            continue;
                        } else {
                            secondary += mat[i][j];
                        }
                    } else {
                        secondary += mat[i][j];
                    }
                }
            }
        }
        //return(primary+secondary);
        System.out.println(primary + secondary);


//        for (int i = 0; i < rows; i++) {
//            for (int j = 0; j < columns; j++) {
//                System.out.print(mat[i][j]);
//            }
//            System.out.println();
//        }
    }
}
