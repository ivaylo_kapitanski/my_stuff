package com.telerikacademy;

import java.util.Arrays;
import java.util.Scanner;

public class Height_Checker_1051 {
    public static void main(String[] args) {

        Scanner scanner=new Scanner(System.in);

        String[] input1=scanner.nextLine().split(",");

        int[] nums=new int[input1.length];

        for (int i = 0; i <input1.length ; i++) {
            nums[i]=Integer.parseInt(input1[i]);
        }

        int[]numClone=nums.clone();
        Arrays.sort(numClone);
        int count=0;
        for (int i = 0; i <nums.length ; i++) {
            if(numClone[i]!=nums[i]){
                count++;
            }
        }

        System.out.println(count);






    }
}
