package com.telerikacademy;
import java.util.Scanner;

public class Maximum_Product_of_Two_Elements_in_an_Array_1464 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String[] input1 = scanner.nextLine().split(",");

        int[] nums = new int[input1.length];


        for (int i = 0; i < input1.length ; i++) {
            nums[i]=Integer.parseInt(input1[i]);
        }

        int index1=0;
        int index2=0;

            for (int k = 0; k < nums.length ; k++) {
                if(nums[k]>index1){
                    index2=index1;
                    index1=nums[k];
                }else if(nums[k]>index2){
                    index2=nums[k];
                }
            }

        System.out.println((index1-1)*(index2-1));



    }
}
