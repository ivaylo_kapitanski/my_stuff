package com.telerikacademy;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Count_Good_Triplets_1534 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] input = scanner.nextLine().split(",");
        int a = Integer.parseInt(scanner.nextLine());
        int b = Integer.parseInt(scanner.nextLine());
        int c = Integer.parseInt(scanner.nextLine());

        int[] arr = new int[input.length];
        int count = 0;

        for (int i = 0; i < input.length; i++) {
            arr[i] = Integer.parseInt(input[i]);
        }


        for (int i = 0; i < arr.length-2; i++) {
            for (int j = i+1; j < arr.length-1 ; j++) {
                if((Math.abs(arr[i]-arr[j]))<=a){
                    for (int k = j+1; k < arr.length ; k++) {
                        if((Math.abs(arr[j]-arr[k]))<=b){
                            if((Math.abs(arr[i]-arr[k]))<=c){
                                count++;
                            }
                        }

                    }
                }

            }


        }
        System.out.println(count);

    }
}
