package com.telerikacademy;

import java.util.Scanner;

public class Complement_of_Base_10_Integer_1009 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n= scanner.nextInt();
        String newN=Integer.toBinaryString(n);
        String temp="";
        for (int i = 0; i <newN.length() ; i++) {
            if(newN.charAt(i)=='0'){
                temp+="1";
            }else{
                temp+="0";
            }
        }
      n=Integer.parseInt(temp,2);
        System.out.println(n);
    }
}
