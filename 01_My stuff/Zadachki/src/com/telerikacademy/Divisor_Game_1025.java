package com.telerikacademy;

import java.util.Scanner;

public class Divisor_Game_1025 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();

        boolean sheWins = false;
        int count = 0;
        while (num > 0) {

                num -= 1;
                count++;
                if (num == 1 && count % 2 != 0) {
                    sheWins = true;
                    break;
                }

        }
        System.out.println(sheWins);

    }
}
