package com.telerikacademy;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Baseball_Game_682 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);

        String[]ops=scanner.nextLine().split(",");
        List<Integer>list1=new ArrayList<>();
        int count=0;
        for (String op : ops) {
            switch (op) {
                case "D":
                    list1.add((list1.get(count - 1) * 2));
                    count++;
                    break;
                case "C":
                    list1.remove(count - 1);
                    count--;
                    break;
                case "+":
                    list1.add((list1.get(count - 1) + list1.get(count - 2)));
                    count++;
                    break;
                default:
                    list1.add(Integer.parseInt(op));
                    count++;
            }
        }
        int sum=0;
        for (int i = 0; i < list1.size() ; i++) {
            sum+= list1.get(i);
        }
        System.out.println(sum);

    }
}
