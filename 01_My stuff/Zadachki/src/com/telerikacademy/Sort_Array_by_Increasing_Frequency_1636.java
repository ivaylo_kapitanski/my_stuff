package com.telerikacademy;

import java.util.*;

public class Sort_Array_by_Increasing_Frequency_1636 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String[]input=scanner.nextLine().split(",");
        int[]nums=new int[input.length];

        for (int i = 0; i <input.length; i++) {
            nums[i]=Integer.parseInt(input[i]);
        }


        List<Integer>list1=new ArrayList<>();
        HashMap<Integer,Integer>hMap=new HashMap<>();
        for (int i = 0; i < nums.length ; i++) {
            list1.add(nums[i] );
            if(hMap.containsKey(nums[i])){
                hMap.put(nums[i],hMap.get(nums[i])+1 );
            }else{
                hMap.put(nums[i],1 );
            }
        }
       list1.sort((a, b) -> {
           if (hMap.get(a).equals(hMap.get(b)))
               return b - a;
           else return hMap.get(a) - hMap.get(b);
       });
        for (int i = 0; i < nums.length ; i++) {
            nums[i]= list1.get(i);
        }

        System.out.println(Arrays.toString(nums));
    }
}
