package com.telerikacademy;

import java.util.HashMap;
import java.util.Scanner;

public class Find_Lucky_Integer_in_an_Array_1394 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] input = scanner.nextLine().split(",");
        int[] arr = new int[input.length];

        for (int i = 0; i < input.length; i++) {
            arr[i] = Integer.parseInt(input[i]);
        }

        HashMap<Integer, Integer> hashMap = new HashMap<>();
        for (int j : arr) {
            if (hashMap.containsKey(j)) {
                hashMap.put(j, hashMap.get(j) + 1);
            } else {
                hashMap.put(j, 1);
            }
        }
        int luckyNum = Integer.MIN_VALUE;

        for (int z : hashMap.keySet()) {
            if (z == hashMap.get(z) && z > luckyNum) {
                luckyNum = z;
            }
        }
        if(luckyNum==Integer.MIN_VALUE){
            luckyNum=-1;
        }
        System.out.println(luckyNum);


    }
}
