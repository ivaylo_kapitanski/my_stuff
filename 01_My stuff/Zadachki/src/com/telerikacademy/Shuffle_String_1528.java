package com.telerikacademy;

import java.util.Arrays;
import java.util.Scanner;


public class Shuffle_String_1528 {
    public static void main(String[] args) {





        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        String[] input = scanner.nextLine().split(",");
        int[] indices = new int[input.length];

        for (int i = 0; i < input.length; i++) {
            indices[i] = Integer.parseInt(input[i]);
        }

        char[] charS = s.toCharArray();

        for (int i = 0; i < indices.length; i++) {
            for (int k = 1; k < indices.length-i; k++) {


                if (indices[k-1] > indices[k]) {
                    int temp = indices[k-1];
                    indices[k-1 ] = indices[k];
                    indices[k] = temp;

                    char temp2 = charS[k-1];
                    charS[k-1 ] = charS[k];
                    charS[k] = temp2;

                }

            }
        }
        s=String.valueOf(charS);


        System.out.println(Arrays.toString(indices));
        System.out.println(s);

    }



}
