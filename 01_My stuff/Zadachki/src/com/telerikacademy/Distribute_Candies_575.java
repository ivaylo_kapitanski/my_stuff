package com.telerikacademy;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;

public class Distribute_Candies_575 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);

        String[]input=scanner.nextLine().split(",");
        int[]candies=new int[input.length];

        for (int i = 0; i < input.length ; i++) {
            candies[i]=Integer.parseInt(input[i]);
        }
        HashSet<Integer>hashSet=new HashSet<>();

            for (int i = 0; i < candies.length; i++) {
                if (hashSet.size() < candies.length / 2) {
                    hashSet.add(candies[i]);
                }
            }

            System.out.println(Arrays.toString(hashSet.toArray()));

    }
}
