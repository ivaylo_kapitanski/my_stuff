package com.telerikacademy;

import java.util.Arrays;
import java.util.Scanner;

public class Mean_of_Array_After_Removing_Some_Elements_1619 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String[] input=scanner.nextLine().split(",");
        int[] arr=new int[input.length];
        for (int i = 0; i < input.length ; i++) {
            arr[i]=Integer.parseInt(input[i]);
        }

        Arrays.sort(arr);
        int remove=(int) ((arr.length)*0.05);
        int[] newArray=Arrays.copyOfRange(arr,remove,arr.length-remove);

        int sum=0;
        for (int i = 0; i <newArray.length ; i++) {
            sum+=newArray[i];
        }
        double result=(double)sum/ newArray.length;
        System.out.println(result);

    }
}
