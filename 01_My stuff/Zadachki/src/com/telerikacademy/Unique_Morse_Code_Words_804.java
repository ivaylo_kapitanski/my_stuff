package com.telerikacademy;

import java.util.*;

public class Unique_Morse_Code_Words_804 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);

        String[] words=scanner.nextLine().split(",");

        List<String> code= Arrays.asList(".-","-...","-.-.","-..",".","..-.","--.","....","..",".---","-.-",".-..","--","-.","---",".--.","--.-",".-.",
                "...","-","..-","...-",".--","-..-","-.--","--..");
       List<Character> alphabet=Arrays.asList('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');

        HashSet<String> countThem=new HashSet<>();
        //gin,zen,gig,msg
        for (int i = 0; i < words.length ; i++) {
            String temp="";
            for (int k = 0; k <words[i].length() ; k++) {
                temp+=code.get(alphabet.indexOf((words[i]).charAt(k)));
            }
            countThem.add(temp);
        }

        System.out.println(countThem.size());
    }
}
