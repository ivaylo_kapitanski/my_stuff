package com.telerikacademy;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Water_Bottles_1518 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numBottles = Integer.parseInt(scanner.nextLine());
        int numExchange = Integer.parseInt(scanner.nextLine());


        List<Integer> list1 = new ArrayList<>();
        int residual = numBottles % numExchange;
        list1.add(numBottles - residual);

        int sum = numBottles;

        while (list1.get(list1.size() - 1) / numExchange >= 1) {
            sum += list1.get(list1.size() - 1) / numExchange;
            int temp = residual + list1.get(list1.size() - 1) / numExchange;
            if (temp % numExchange == 0) {
                list1.add(temp);
                residual = 0;
            } else if (temp - numExchange < 0) {
                break;
            } else {
                residual = temp % numExchange;
                list1.add(temp - residual);
            }
        }
        System.out.println(sum);
    }
}
