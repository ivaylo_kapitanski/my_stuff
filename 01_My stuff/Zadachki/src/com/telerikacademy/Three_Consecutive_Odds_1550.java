package com.telerikacademy;

import java.util.Scanner;

public class Three_Consecutive_Odds_1550 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String[]input=scanner.nextLine().split(",");
        int[]arr=new int[input.length];
        for (int i = 0; i < input.length ; i++) {
            arr[i]=Integer.parseInt(input[i]);
        }

        boolean oddsExist=false;
        int count=0;
        for (int i = 0; i < arr.length ; i++) {
            if(arr[i]%2!=0){
                count++;
            }else{
                count=0;
            }
            if (count==3){
                oddsExist=true;
                break;
            }
        }
        System.out.println(oddsExist);






    }
}
