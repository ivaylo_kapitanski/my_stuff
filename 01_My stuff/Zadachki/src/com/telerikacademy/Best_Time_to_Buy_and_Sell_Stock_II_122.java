package com.telerikacademy;


import java.util.Scanner;

public class Best_Time_to_Buy_and_Sell_Stock_II_122 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String[]input=scanner.nextLine().split(",");
        int[] prices=new int[input.length];
        for (int i = 0; i < input.length ; i++) {
            prices[i]=Integer.parseInt(input[i]);
        }
        int maxProfit=0;
        int min=prices[0];
            for (int i = 1; i < prices.length; i++) {
                if (min > prices[i]) {
                min=prices[i];
                }else if(min<prices[i]){
                    maxProfit+=prices[i]-min;
                    min=prices[i];
            }
        }
        System.out.println(maxProfit);

    }
}
