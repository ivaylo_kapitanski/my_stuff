package com.telerikacademy;

import java.util.Scanner;

public class Binary_Gap_868 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n= scanner.nextInt();
        String nStr=Integer.toBinaryString(n);
        int maxDistance=0;
        int count=1;
        int temp=1;
        for (int i = 0; i <nStr.length() ; i++) {
            if(nStr.charAt(i)=='1'&&(count%2==0)){
                maxDistance=Math.max(maxDistance,temp);
                temp=1;

            }else if(nStr.charAt(i)=='1') {
                count++;
            }
            if(nStr.charAt(i)=='0'&&(count%2==0)){
                temp++;
            }
        }

        System.out.println(maxDistance);


    }
}
