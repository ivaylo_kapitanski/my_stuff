package com.telerikacademy;

import java.util.Arrays;
import java.util.Scanner;

public class Can_Make_Arithmetic_Progression_From_Sequence_1502 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);

        String[] input1=scanner.nextLine().split(",");

        int[] nums=new int[input1.length];

        for (int i = 0; i <input1.length ; i++) {
            nums[i]=Integer.parseInt(input1[i]);

        }
        Arrays.sort(nums);
int diff=nums[1]-nums[0];
boolean isArithmetic=true;
        for (int i = 0; i < nums.length-1 ; i++) {
            int temp=nums[i+1]-nums[i];

            if(temp!=diff){
                isArithmetic=false;
                break;
            }

        }
        System.out.println(isArithmetic);

    }
}
