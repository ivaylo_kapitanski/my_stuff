package com.telerikacademy;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class NonDecreasing_Array_665_longer {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] input = scanner.nextLine().split(",");

        int[] nums = new int[input.length];

        for (int i = 0; i < input.length; i++) {
            nums[i] = Integer.parseInt(input[i]);
        }


        int count = 0;
        int count2 = 0;
        int max = 0;
        int min = 0;
        List<Integer> numsClone = new ArrayList<>();
        List<Integer> numsClone2 = new ArrayList<>();
        for (int i = 1; i < nums.length; i++) {
            if (nums[i - 1] <= nums[i]) {
                count++;
            } else {
                max = i - 1;
                break;
            }
        }

        for (int i = 1; i < nums.length; i++) {
            if (nums[i - 1] <= nums[i]) {
                count2++;
            } else {
                min = i;
                break;
            }
        }
        if (count == nums.length - 1) {
            System.out.println(true);
            return;
        }
        count = 0;

        for (int i = 0; i < nums.length; i++) {
            numsClone.add(nums[i]);
        }
        numsClone.remove(max);
        for (int i = 0; i < nums.length; i++) {
            numsClone2.add(nums[i]);
        }
        numsClone2.remove(min);

        for (int i = 0; i < numsClone.size() - 1; i++) {
            if (numsClone.get(i) <= numsClone.get(i + 1)) {
                count++;

            }
        }
        count2 = 0;
        for (int i = 0; i < numsClone2.size() - 1; i++) {
            if (numsClone2.get(i) <= numsClone2.get(i + 1)) {
                count2++;

            }
        }
        if (count == numsClone.size() - 1 || count2 == numsClone2.size() - 1) {
            System.out.println(true);
        } else {
            System.out.println(false);
        }
    }
}
