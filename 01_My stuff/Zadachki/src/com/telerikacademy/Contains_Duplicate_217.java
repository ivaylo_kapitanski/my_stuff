package com.telerikacademy;

import java.util.Arrays;
import java.util.Scanner;

public class Contains_Duplicate_217 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String[]input=scanner.nextLine().split(",");
        int[]nums=new int[input.length];
        for (int i = 0; i < input.length ; i++) {
            nums[i]=Integer.parseInt(input[i]);
        }
        Arrays.sort(nums);
        boolean isDuplicate=false;
        for (int i = 1; i <nums.length ; i++) {
            if(nums[i-1]==nums[i]){
                isDuplicate=true;
                break;
            }
        }
        System.out.println(isDuplicate);

    }
}
