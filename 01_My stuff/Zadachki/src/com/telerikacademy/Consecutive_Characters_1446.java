package com.telerikacademy;


import java.util.Scanner;

public class Consecutive_Characters_1446 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();

        int count = 1;
        int maxCount=0;
        for (int i = 1; i <s.length() ; i++) {
            if(s.charAt(i-1)==s.charAt(i)){
                count++;
                maxCount=Math.max(maxCount,count);
            }else{
                count=1;
            }
        }
        System.out.println(maxCount);


    }
}
