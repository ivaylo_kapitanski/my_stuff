package com.telerikacademy;

import java.time.LocalDate;

import java.util.Scanner;

public class Day_of_the_Week_1185 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int day=Integer.parseInt(scanner.nextLine());
        int month=Integer.parseInt(scanner.nextLine());
        int year=Integer.parseInt(scanner.nextLine());


        LocalDate localDate=LocalDate.of(year,month,day);
        java.time.DayOfWeek dayOfWeek=localDate.getDayOfWeek();
        String dayOfString=String.valueOf(dayOfWeek).toLowerCase();
        System.out.println((char)(dayOfString.charAt(0)-32)+(dayOfString.substring(1)));

    }
}
