package com.telerikacademy;

import java.util.Scanner;

public class Cells_with_Odd_Values_in_a_Matrix_1252 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int m = scanner.nextInt();

        int[][] indices = {{0, 1}, {1, 1}};

        int[][] matrix = new int[n][m];

        int total = 0;

        for (int i = 0; i < indices.length; i++) {
            for (int j = 0; j < m; j++) {
                matrix[indices[i][0]][j] += 1;
            }
            for (int p = 0; p < n; p++) {
                matrix[p][indices[i][1]] += 1;
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (matrix[i][j] % 2 != 0)
                    total++;
            }
        }
        System.out.println(total);

    }
}






