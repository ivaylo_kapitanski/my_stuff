package com.telerikacademy;

import java.util.Arrays;
import java.util.Scanner;

public class RunningSumOf1dArray_1480 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String[] input=scanner.nextLine().split(",");
        int[]nums=new int[input.length];

        for (int i = 0; i <input.length ; i++) {
            nums[i]=Integer.parseInt(input[i]);
        }
        int[] runningSum=new int[nums.length];

        int run=0;
        for (int i = 0; i < nums.length ; i++) {
            for (int j = 0; j <=i ; j++) {
                run+=nums[j];
            }
            runningSum[i]=run;
            run=0;

        }
        System.out.println(Arrays.toString(runningSum));








    }





}
