package com.telerikacademy;

import java.util.Arrays;
import java.util.Scanner;

public class The_K_Weakest_Rows_in_a_Matrix_1337 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int row = Integer.parseInt(scanner.nextLine());
        int col = Integer.parseInt(scanner.nextLine());
        int k = Integer.parseInt(scanner.nextLine());

        int[][] mat = new int[row][col];

        for (int i = 0; i < row; i++) {
            for (int p = 0; p < col; p++) {
                mat[i][p] = Integer.parseInt(scanner.nextLine());
            }
        }
//        for (int i = 0; i < mat.length; i++) {
//            for (int p = 0; p < mat[0].length; p++) {
//                System.out.print(mat[i][p] + " ");
//            }
//            System.out.println();
//        }
        int count = 0;
        int[] soldiers = new int[mat.length];
        int[] rows = new int[mat.length];


        for (int i = 0; i < mat.length; i++) {
            for (int p = 0; p < mat[0].length; p++) {
                if (mat[i][p] == 1) {
                    count++;
                }
            }
            soldiers[i] = (count);
            rows[i] = (i);
            count = 0;
        }
//        System.out.println(Arrays.toString(soldiers));
//        System.out.println(Arrays.toString(rows));


        for (int i = 0; i < soldiers.length - 1; i++) {
            for (int j = 0; j < soldiers.length - 1; j++) {
                if (soldiers[j] > soldiers[j + 1]) {
                    int tempS = soldiers[j];
                    soldiers[j] = soldiers[j + 1];
                    soldiers[j + 1] = tempS;

                    int tempR = rows[j];
                    rows[j] = rows[j + 1];
                    rows[j + 1] = tempR;

                }else if(soldiers[j] == soldiers[j + 1]){
                    continue;
                }
            }
        }
        int[]weaksest=new int[k];
        for (int i = 0; i <k ; i++) {
            weaksest[i]=(rows[i]);

        }
        System.out.println(Arrays.toString(weaksest));

    }
}
