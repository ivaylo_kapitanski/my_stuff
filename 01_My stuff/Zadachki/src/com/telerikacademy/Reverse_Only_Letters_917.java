package com.telerikacademy;


import java.util.Scanner;

public class Reverse_Only_Letters_917 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        char[] sChars = s.toCharArray();
        int countStart = 0;
        int countEnd = s.length() - 1;
        while (countStart < countEnd) {
            if (!Character.isLetter(sChars[countStart])) {
                countStart++;
            } else if (!Character.isLetter(sChars[countEnd])) {
                countEnd--;
            } else {
                char temp = sChars[countStart];
                sChars[countStart] = sChars[countEnd];
                sChars[countEnd] = temp;
                countStart++;
                countEnd--;
            }
        }
        System.out.println(new String(sChars));

    }
}
