package com.telerikacademy;


import java.util.Arrays;
import java.util.Scanner;

public class Valid_Anagram_242 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String s=scanner.nextLine();
        String t=scanner.nextLine();
        boolean isAnagram=true;

         if(s.length()!=t.length()){
            isAnagram=false;

        }
        char[]sChars=s.toCharArray();
        Arrays.sort(sChars);
        char[]tChars=t.toCharArray();
        Arrays.sort(tChars);
        for (int i = 0; i <sChars.length ; i++) {
            if(sChars[i]!=tChars[i]){
                isAnagram=false;
                break;
            }
        }
        System.out.println(isAnagram);

    }
}
