package com.telerikacademy;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Keyboard_Row_500 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] words = scanner.nextLine().split(",");
        List<String> list2 = new ArrayList<>();

        list2.add("qwertyuiop");
        list2.add("asdfghjkl");
        list2.add("zxcvbnm");

        List<String> list1 = new ArrayList<>();

        for (String word : words) {

            for (int p = 0; p < list2.size(); p++) {
                int count = 0;
                for (int j = 0; j < word.length(); j++) {
                    String temp = String.valueOf(word.charAt(j));
                    temp = temp.toLowerCase();
                    if (list2.get(p).contains(temp)) {
                        count++;
                    }
                }
                if (count == word.length()) {
                    list1.add(word);
                }
            }
        }
        String[] result = new String[list1.size()];
        result = list1.toArray(result);
        System.out.println(Arrays.toString(result));

    }
}
