package com.telerikacademy;

import java.util.Scanner;

public class Number_of_Students_Doing_Homework_at_a_Given_Time_1450 {
    public static void main(String[] args) {


        Scanner scanner = new Scanner(System.in);

        String[] input1 = scanner.nextLine().split(",");
        String[] input2 = scanner.nextLine().split(",");
        int queryTime=Integer.parseInt(scanner.nextLine());

        int[] startTime = new int[input1.length];
        int[] endTime = new int[input2.length];


        for (int i = 0; i < input1.length ; i++) {
            startTime[i]=Integer.parseInt(input1[i]);
        }
        for (int i = 0; i < input2.length ; i++) {
            endTime[i]=Integer.parseInt(input2[i]);
        }

        int count=0;

        for (int i = 0; i < startTime.length ; i++) {
            for (int k = i; k < endTime.length ; k++) {
                if(startTime[i]<=queryTime&&queryTime<=endTime[k]){
                    count++;
                }
                break;

            }

        }
        System.out.println(count);






    }
}
