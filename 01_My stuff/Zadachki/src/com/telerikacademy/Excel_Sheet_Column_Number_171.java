package com.telerikacademy;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Excel_Sheet_Column_Number_171 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();

        List<Character> alphaChars = new ArrayList<>();
        for (char i = 'A'; i <= 'Z'; i++) {
            alphaChars.add(i);
        }
        int colNum = 0;

        for (int i = 0; i < s.length(); i++) {
            int temp = 1;
            for (int z = 1; z < s.length() - i; z++) {
                temp *= 26;
            }
            temp *= alphaChars.indexOf(s.charAt(i)) + 1;
            colNum += temp;
        }
        System.out.println(colNum);
    }

}

