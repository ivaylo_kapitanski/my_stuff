package com.telerikacademy;

import java.util.Arrays;
import java.util.Scanner;

public class N_Repeated_Element_in_Size_2N_Array_961 {
    public static void main(String[] args) {
        //enter the array
        //create a counter
        //there is only one unique element with size array.length/2
        //sort the array and count the elements of the same size. If the following element is different-
        //compare the counter with the target number.
        Scanner scanner = new Scanner(System.in);
        String[] input = scanner.nextLine().split(",");

        int[] nums = new int[input.length];

        for (int i = 0; i < input.length; i++) {
            nums[i] = Integer.parseInt(input[i]);
        }

        Arrays.sort(nums);
        int target=0;
        int count=1;
        for (int i =0; i <nums.length-1 ; i++) {
            if(nums[i]==nums[i+1]){
                count++;
                if(count== (nums.length)/2){
                    target=nums[i];
                    break;}
            }else{
                count=1;
            }
        }
        System.out.println(target);

    }
}
