package com.telerikacademy;


import java.util.Scanner;

public class Find_Numbers_with_Even_Number_of_Digits_1295 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String[]input=scanner.nextLine().split(",");
        int count=0;


        int[]nums=new int[input.length];

        for (int i = 0; i <input.length ; i++) {
            nums[i]=Integer.parseInt(input[i]);
        }

        for (int i = 0; i < nums.length ; i++) {
            if(String.valueOf(nums[i]).length()%2==0){
                count++;
            }

        }
        System.out.println(count);





    }
}
