package com.telerikacademy;

import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class Maximum_Number_of_Balloons_1189_shortest {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();
      //  String toCompare = "balloon";
        
        int[] charsArray=new int[256];

        for (char c:text.toCharArray()) {
            charsArray[c]++;
            
        }
        int min= Collections.min(Arrays.asList(
                charsArray['b'],
                charsArray['a'],
                charsArray['l']/2,
                charsArray['o']/2,
                charsArray['n']
        ));

        System.out.println(min);
        
    }
}
