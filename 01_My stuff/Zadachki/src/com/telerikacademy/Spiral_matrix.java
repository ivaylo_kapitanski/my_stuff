package com.telerikacademy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Spiral_matrix {
    public static void main(String[] args) {

        Scanner scanner=new Scanner(System.in);

        int n=scanner.nextInt();

//        List<Integer> list1=new ArrayList<>();
//
//        for (int i = 1; i <=n*n ; i++) {
//            list1.add(i);
//
//        }
//        System.out.println(Arrays.toString(new List[]{list1}));


        int[][]matrix=new int[n][n];
        int count=1;
        int rowMin=0;
        int rowMax=n-1;
        int colMin=0;
        int colmax=(n-1);
        
       while(count<=n*n) {
           for (int i = colMin; i <= colmax; i++) {
               matrix[rowMin][i] = count;
               count++;
           }
           for (int i = rowMin + 1; i <= rowMax; i++) {
               matrix[i][colmax] = count;
               count++;
           }
           for (int i = colmax - 1; i >= colMin; i--) {
               matrix[rowMax][i] = count;
               count++;
           }
           for (int i = rowMax - 1; i >= rowMin + 1; i--) {
               matrix[i][colMin] = count;
               count++;
           }
           rowMin++;
           colMin++;
           colmax--;
           rowMax--;
       }
        for (int i = 0; i <matrix.length ; i++) {
            for (int j = 0; j <matrix[0].length ; j++) {
                System.out.print(matrix[i][j]+" ");
            }
            System.out.println();

        }

    }
}
