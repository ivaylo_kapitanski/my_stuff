package com.telerikacademy;

import java.util.Scanner;

public class Remove_Outermost_Parentheses_1021_faster {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String S = scanner.nextLine();

        int start = 0;
        StringBuilder sb = new StringBuilder();
        int total = 0;

        for (int i = 0; i < S.length(); i++) {
            char temp = S.charAt(i);
            if (temp == '(') {
                total++;
            } else if (temp == ')') {
                total--;
            }
            if (total == 0) {
                for (int j = start + 1; j < i; j++) {
                    sb.append(S.charAt(j));
                }
                start = i + 1;
            }
        }

        System.out.println(sb.toString());

    }
}
