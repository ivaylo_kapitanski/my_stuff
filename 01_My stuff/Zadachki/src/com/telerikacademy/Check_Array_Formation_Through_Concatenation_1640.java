package com.telerikacademy;

import java.util.Scanner;

public class Check_Array_Formation_Through_Concatenation_1640 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int rows = Integer.parseInt(scanner.nextLine());
        int cols = Integer.parseInt(scanner.nextLine());
        String[] input1 = scanner.nextLine().split(",");
        int[][] pieces = new int[rows][cols];
        int[] arr = new int[input1.length];

        for (int i = 0; i < input1.length; i++) {
            arr[i] = Integer.parseInt(input1[i]);
        }
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                pieces[i][j] = Integer.parseInt(scanner.nextLine());
            }
        }

        boolean canBeDone = true;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < arr.length; i++) {
            sb.append(String.valueOf(arr[i]));
        }

        for (int i = 0; i < pieces.length; i++) {
            String check = "";
            for (int z = 0; z < pieces[i].length; z++) {
                check += String.valueOf(pieces[i][z]);
            }
            if (!sb.toString().contains(check)) {
                canBeDone = false;
            }

        }


        System.out.println(canBeDone);


    }
}
