package com.telerikacademy;

import java.util.Scanner;

public class Maximum_Nesting_Depth_of_the_Parentheses_1614 {
    public static void main(String[] args) {

        Scanner scanner=new Scanner(System.in);
        String s=scanner.nextLine();

        char[] sChar=s.toCharArray();
        int countL=0;
        int countR=0;
        int max=0;

        for (int i = 0; i <sChar.length ; i++) {
            if(Character.isDigit(sChar[i])){
                for (int l = 0; l <i ; l++) {
                    if(sChar[l] == '('){
                        countL++;
                    }

                }

            for (int k = 0; k <i ; k++) {
                if(sChar[k] == ')'){
                    countR++;
                }

            }
            }
            max=Math.max(max,(countL-countR));
            countL=0;
            countR=0;

        }
        System.out.println(max);







    }



}
