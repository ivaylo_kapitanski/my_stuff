package com.telerikacademy;

import java.util.Scanner;

public class Check_If_a_Word_Occurs_As_a_Prefix_of_Any_Word_in_a_Sentence_1455 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String sentence= scanner.nextLine();
        String searchWord= scanner.nextLine();

        String[] sentenceArray=sentence.split(" ");
        int count=0;
        boolean isPresent=false;
        for (int i = 0; i <sentenceArray.length ; i++) {
            count++;
            if(sentenceArray[i].length()<searchWord.length()){
                continue;
            }else if(searchWord.equals(sentenceArray[i].substring(0,searchWord.length()))){
                isPresent=true;
                break;
            }
        }
        if(!isPresent){
            System.out.println(-1);
        }else{
            System.out.println(count);
        }

    }
}
