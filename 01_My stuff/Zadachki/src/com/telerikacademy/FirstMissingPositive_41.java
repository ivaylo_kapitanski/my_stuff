package com.telerikacademy;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class FirstMissingPositive_41 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] input = scanner.nextLine().split(",");
        List<Integer> digits = new ArrayList<>();
        List<Integer> copy = new ArrayList<>();
        List<Integer> copy2 = new ArrayList<>();
        List<Integer> copy3 = new ArrayList<>();

        for (int i = 0; i < input.length; i++) {
            digits.add(Integer.parseInt(input[i]));
        }
        digits.sort(Comparator.naturalOrder());
        for (int i = 0; i < digits.size(); i++) {
            if (digits.get(i) >= 0) {
                copy.add(digits.get(i));
            }
        }
        int copyLast = copy.get(copy.size() - 1);

        for (int i = 0; i <= copyLast; i++) {
            copy2.add(i);
        }
        // int size = copy2.size();
        for (int i = 0; i < copy2.size(); i++) {
            if (!(copy.contains(copy2.get(i)))) {
                copy3.add(copy2.get(i));
            }

        }
        if (copy3.size() == 0) {
            System.out.println(copyLast + 1);
        } else if (copy3.get(0) == 0) {
            System.out.println(copy3.get(1));
        } else {
            System.out.println(copy3.get(0));
        }
    }
}
