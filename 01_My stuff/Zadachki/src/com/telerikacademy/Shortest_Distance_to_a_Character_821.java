package com.telerikacademy;

import java.util.Arrays;
import java.util.Scanner;

public class Shortest_Distance_to_a_Character_821 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        char c = scanner.nextLine().charAt(0);
        int[] shortest = new int[input.length()];
        int length = input.length();
        int newC = -length;


        for (int i = 0; i < length; i++) {
            if (input.charAt(i) == c){
                newC = i;}
            else {
                shortest[i] = i - newC;
            }
        }

        newC = -length;

        for (int i = length - 1; i >= 0; i--) {
            if (input.charAt(i) == c){
                newC = i;}
            else {
                shortest[i] = Math.min(shortest[i], Math.abs(newC - i));
            }
        }

        System.out.println(Arrays.toString(shortest));


    }
}
