package com.telerikacademy;

import java.util.Arrays;
import java.util.Scanner;

public class Fair_Candy_Swap_888 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);

        String[] input1=scanner.nextLine().split(",");
        String[] input2=scanner.nextLine().split(",");

        int[] a=new int[input1.length];
        int[] b=new int[input2.length];



        for (int i = 0; i <input1.length ; i++) {
            a[i]=Integer.parseInt(input1[i]);
        } for (int i = 0; i <input2.length ; i++) {
            b[i]=Integer.parseInt(input2[i]);
        }

        int n1=0, n2=0;
        for (int i = 0; i < a.length ; i++) {
            n1+=a[i];
        } for (int i = 0; i <b.length; i++) {
            n2+=b[i];
        }
        int[] result=new int[2];

        for (int i = 0; i <a.length ; i++) {
            for (int k = 0; k < b.length ; k++) {
                int checkA=n1-a[i];
                int checkB=n2-b[k];
                if(checkA+b[k]==checkB+a[i]){
                    result[0]=a[i];
                    result[1]=b[k];
                    break;
                }
            }

        }



        System.out.println(Arrays.toString(result));




    }
}
