package com.telerikacademy;
import java.util.Scanner;

public class Minimum_Time_Visiting_All_Points_1266 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int rows=scanner.nextInt();
        int columns=scanner.nextInt();
        int[][] points=new int[rows][columns];
        for (int i = 0; i <rows ; i++) {
            for (int j = 0; j <columns ; j++) {
                points[i][j]= scanner.nextInt();
            }
        }
        int count=0;
        for(int i=1;i<points.length;i++){
            count += Math.max(Math.abs(points[i][0]-points[i-1][0]),Math.abs(points[i][1]-points[i-1][1]) );
        }


        System.out.println(count);



//        for (int i = 0; i <rows ; i++) {
//            for (int j = 0; j <columns ; j++) {
//                System.out.print(points[i][j]);
//            }
//            System.out.println();
//        }



    }



}
