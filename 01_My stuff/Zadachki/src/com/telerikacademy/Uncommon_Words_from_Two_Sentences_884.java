package com.telerikacademy;

import java.util.*;

public class Uncommon_Words_from_Two_Sentences_884 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String a = scanner.nextLine();
        String b = scanner.nextLine();

        String[] aStr = a.split(" ");
        String[] bStr = b.split(" ");
        List<String> list1 = new ArrayList<>();

        HashMap<String, Integer>hashMap=new HashMap<>();
        HashMap<String, Integer>hashMap2=new HashMap<>();

        for (int i = 0; i < aStr.length; i++) {
            if(hashMap.containsKey(aStr[i])){
                hashMap.put(aStr[i], hashMap.get(aStr[i])+1);
            }else{
                hashMap.put(aStr[i], 1);
            }
        }


        for (int i = 0; i < bStr.length; i++) {
            if(hashMap2.containsKey(bStr[i])){
                hashMap2.put(bStr[i], hashMap2.get(bStr[i])+1);
            }else{
                hashMap2.put(bStr[i], 1);
            }
        }
        for (String i:hashMap.keySet()) {
            if (hashMap.get(i) == 1 && !hashMap2.containsKey(i)) {
                if (!list1.contains(i)) {
                    list1.add(i);
                }
            }
        }
        for (String z:hashMap2.keySet()) {
            if (hashMap2.get(z) == 1 && !hashMap.containsKey(z)) {
                if (!list1.contains(z)) {
                    list1.add(z);
                }
            }
        }

        String[] really = list1.toArray(new String[0]);
        System.out.println(Arrays.toString(really));
    }
}

