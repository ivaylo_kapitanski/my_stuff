package com.telerikacademy;

import java.util.Scanner;

public class Special_Array_With_X_Elements_Greater_Than_or_Equal_X_1608 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String[] input=scanner.nextLine().split(",");
        int[]nums=new int[input.length];

        for (int i = 0; i <input.length ; i++) {
            nums[i]=Integer.parseInt(input[i]);
        }

        int count=0;
        int special=0;
        for (int i = 1; i <= nums.length ; i++) {
            for (int j = 0; j <nums.length ; j++) {
                if(nums[j]>=i){
                    count++;
                }
            }
            if(count==i){
                special=i;
                break;
            }else{
                count=0;
            }
        }
        if(special>0){
            System.out.println(special);
        }else{
            System.out.println(-1);
        }

    }
}
