package com.telerikacademy;

import java.util.Arrays;
import java.util.Scanner;

public class Final_Prices_With_a_Special_Discount_in_a_Shop_1475 {
    public static void main(String[] args) {

        Scanner scanner=new Scanner(System.in);
        
        String[]input1=scanner.nextLine().split(",");
        int[] prices=new int[input1.length];

        for (int i = 0; i < input1.length ; i++) {
            prices[i]=Integer.parseInt(input1[i]);
        }

        int[] finalPrices=new int[prices.length];

        for (int i = 0; i < prices.length ; i++) {
            for (int j = i; j <prices.length ; j++) {
                if(j>i&&prices[i]>=prices[j]){
                    finalPrices[i]=prices[i]-prices[j];
                    break;
                }else{
                    finalPrices[i]=prices[i];
                }
            }
        }
        System.out.println(Arrays.toString(finalPrices));


        
    }
}
