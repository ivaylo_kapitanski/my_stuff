package com.telerikacademy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class String_Matching_in_an_Array_1408 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] words = scanner.nextLine().split(",");

        List<String> list1 = new ArrayList<>();
        for (String word : words) {
            for (String s : words) {
                if (word.length() < s.length()) {
                    if (s.contains(word)) {
                        if (!list1.contains(word)) {
                            list1.add(word);
                        }
                    }
                }
            }
        }
        System.out.println(Arrays.toString(list1.toArray()));


    }
}
