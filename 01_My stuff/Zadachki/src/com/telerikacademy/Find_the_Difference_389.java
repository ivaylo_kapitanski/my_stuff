package com.telerikacademy;

import java.util.Arrays;
import java.util.Scanner;

public class Find_the_Difference_389 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String s=scanner.nextLine();
        String t=scanner.nextLine();
        char[]sChars=s.toCharArray();
        Arrays.sort(sChars);
        char[]tChars=t.toCharArray();
        Arrays.sort(tChars);
        char theDifference='a';
        boolean isFound=false;
        for (int i = 0; i <sChars.length ; i++) {
            if(sChars[i]!=tChars[i]){
                theDifference=tChars[i];
                isFound=true;
                break;
            }
        }
        if(!isFound){
            theDifference=tChars[tChars.length-1];
        }
        System.out.println(theDifference);

    }
}
