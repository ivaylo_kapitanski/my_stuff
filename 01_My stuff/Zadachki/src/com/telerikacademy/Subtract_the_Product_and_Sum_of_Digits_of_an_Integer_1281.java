package com.telerikacademy;

import java.util.Scanner;

public class Subtract_the_Product_and_Sum_of_Digits_of_an_Integer_1281 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);

        int n=scanner.nextInt();


        char[] charN=String.valueOf(n).toCharArray();
        int sum=0;
        int product=1;


        for (int i = 0; i < charN.length ; i++) {
            product*=(charN[i]-'0');
            sum+=(charN[i]-'0');
        }
        int difference=product-sum;
        System.out.println(difference);


    }



}
