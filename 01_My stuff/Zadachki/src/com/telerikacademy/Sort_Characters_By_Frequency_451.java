package com.telerikacademy;

import java.util.*;

public class Sort_Characters_By_Frequency_451 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String s=scanner.nextLine();

        char[] sArray=s.toCharArray();


        List<Character> list1=new ArrayList<>();
        HashMap<Character,Integer> hMap=new HashMap<>();
        for (char c : sArray) {
            list1.add(c);
            if (hMap.containsKey(c)) {
                hMap.put(c, hMap.get(c) + 1);
            } else {
                hMap.put(c, 1);
            }
        }
        list1.sort((b, a) -> {
            if (hMap.get(a).equals(hMap.get(b)))
                return b - a;
            else return hMap.get(a) - hMap.get(b);
        });
        for (int i = 0; i < sArray.length ; i++) {
            sArray[i]= list1.get(i);
        }
            s=String.valueOf(list1);
        System.out.println(s);


    }
}
