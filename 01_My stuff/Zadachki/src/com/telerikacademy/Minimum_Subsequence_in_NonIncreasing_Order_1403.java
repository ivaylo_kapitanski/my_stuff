package com.telerikacademy;

import java.util.*;

public class Minimum_Subsequence_in_NonIncreasing_Order_1403 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);

        String[] input1=scanner.nextLine().split(",");

        int[] nums=new int[input1.length];

        for (int i = 0; i <input1.length ; i++) {
            nums[i]=Integer.parseInt(input1[i]);
        }

        List<Integer> selectN=new ArrayList<>();
        
        Arrays.sort(nums);
        
        int sum=0;
        int sum2=0;
        for (int i = nums.length-1; i >=0 ; i--) {
            sum+=nums[i];
        }
        for (int i = nums.length-1; i >=0 ; i--) {
            sum2+=nums[i];
            selectN.add(nums[i]);
            if(sum2>(sum-sum2)){
                break;
            }
        }
        System.out.println(Arrays.toString(new List[]{selectN}));

    }
}
