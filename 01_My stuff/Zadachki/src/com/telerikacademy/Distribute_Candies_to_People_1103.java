package com.telerikacademy;

import java.util.Arrays;
import java.util.Scanner;

public class Distribute_Candies_to_People_1103 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int candies = Integer.parseInt(scanner.nextLine());
        int num_people = Integer.parseInt(scanner.nextLine());
        int[] distributed = new int[num_people];

        for (int i = 0; i < num_people; i++) {
            if (candies > (i + 1)) {
                distributed[i] += i + 1;
                candies -= i + 1;
            } else {
                distributed[i] += candies;
                candies -= candies;
            }
        }
        int counter = num_people;
        int people = 0;
        while (candies > 0) {
            counter++;
            if (candies <= counter) {
                distributed[people] += candies;
                candies -= candies;
            } else {
                distributed[people] += counter;
                candies -= counter;
            }
            if (people == (num_people - 1)) {
                people = 0;
            } else {
                people++;
            }
        }
        System.out.println(Arrays.toString(distributed));

    }
}
