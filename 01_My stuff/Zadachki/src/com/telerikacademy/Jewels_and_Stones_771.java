package com.telerikacademy;

import java.util.Scanner;

public class Jewels_and_Stones_771 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String j= scanner.nextLine();
        String s=scanner.nextLine();

        char[] jChar=j.toCharArray();
        char[] sChar=s.toCharArray();
        int count=0;

        for (int i = 0; i < jChar.length ; i++) {
            for (int k = 0; k < sChar.length ; k++) {
                if(jChar[i]==sChar[k]){
                    count++;
                }
            }
        }
        System.out.println(count);

    }
}
