package com.telerikacademy;

import java.util.Scanner;

public class Hamming_Distance_461 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int x=Integer.parseInt(scanner.nextLine());
        int y=Integer.parseInt(scanner.nextLine());

        String xBit=String.format("%32s", Integer.toBinaryString(x)).replace(' ', '0');
        String yBit=String.format("%32s", Integer.toBinaryString(y)).replace(' ', '0');


            int count=0;
        for (int i = 0; i <xBit.length() ; i++) {
            if(xBit.charAt(i)!=yBit.charAt(i)){
                count++;
            }
        }
        System.out.println(count);













    }
}
