package com.telerikacademy;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PascalsTriangle_119 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String rows1= scanner.nextLine();
        int rows=Integer.parseInt(rows1);

        String rowNumber= scanner.nextLine();
        int number=Integer.parseInt(rowNumber);
        List<Integer> list1=new ArrayList<>();

        int coef = 1;

        for(int i = 0; i < rows; i++) {
            for(int space = 1; space < rows - i; ++space) {
                System.out.print("  ");
            }

            for(int j = 0; j <= i; j++) {
                if (j == 0 || i == 0)
                    coef = 1;
                else
                    coef = coef * (i - j + 1) / j;

              //  System.out.printf("%4d", coef);
                list1.add(coef);
            }
           // System.out.println();
        }
        int count=0;
        for (int i = 1; i <=number  ; i++) {
            count+=i;}
        for (int i = count; i <count+number+1 ; i++) {
            System.out.printf("%d ",list1.get(i));
        }

    }
}
