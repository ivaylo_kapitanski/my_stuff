package com.telerikacademy;

import java.util.HashMap;
import java.util.Scanner;

public class Contains_Duplicate_II_219 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String[]input=scanner.nextLine().split(",");
        int k= scanner.nextInt();

        int[]nums=new int[input.length];
        for (int i = 0; i < input.length ; i++) {
            nums[i]=Integer.parseInt(input[i]);
        }
        boolean isTrue=false;
        HashMap<Integer,Integer> hashMap=new HashMap<>();
        for (int i=0;i<nums.length;i++) {
            int temp=nums[i];
            if(!hashMap.containsKey(temp)){
                hashMap.put(temp,i );
            }else if(Math.abs(hashMap.get(temp)-i)<=k){
                isTrue=true;
                break;}
            else {
                hashMap.put(temp,i);
            }
        }
        System.out.println(isTrue);
    }
}
