package com.telerikacademy;

import java.util.Scanner;

public class Transpose_Matrix_867 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int row = Integer.parseInt(scanner.nextLine());
        int col = Integer.parseInt(scanner.nextLine());


        int[][] mat = new int[row][col];

        for (int i = 0; i < row; i++) {
            for (int p = 0; p < col; p++) {
                mat[i][p] = Integer.parseInt(scanner.nextLine());
            }
        }
        for (int i = 0; i < mat.length; i++) {
            for (int p = 0; p < mat[0].length; p++) {
                System.out.print(mat[i][p] + " ");
            }
            System.out.println();
        }
//        for (int p = 0; p < mat[0].length; p++) {
//                for (int i = 0; i < mat.length; i++) {
//                System.out.print(mat[i][p] + " ");
//            }
//            System.out.println();
//        }
      
        int[][]matClone=new int[mat[0].length][mat.length];
     

        for (int i = 0; i <mat.length ; i++) {
            for (int j = 0; j <mat[0].length ; j++) {
            matClone[j][i]=mat[i][j];

            }
        }
        for (int i = 0; i < matClone.length; i++) {
            for (int p = 0; p < matClone[0].length; p++) {
                System.out.print(matClone[i][p] + " ");
            }
            System.out.println();
        }



    }
}
