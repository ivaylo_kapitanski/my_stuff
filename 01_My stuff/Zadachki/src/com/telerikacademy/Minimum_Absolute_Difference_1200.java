package com.telerikacademy;
import java.util.*;

public class Minimum_Absolute_Difference_1200 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] input1 = scanner.nextLine().split(",");

        int[] nums = new int[input1.length];


        for (int i = 0; i < input1.length; i++) {
            nums[i] = Integer.parseInt(input1[i]);
        }
        int min = Integer.MAX_VALUE;
        Arrays.sort(nums);

            for (int k = 0; k < nums.length-1; k++) {
                if (nums[k] == nums[k+1]) {
                    continue;
                } else {
                    min = (Math.min(min, Math.abs(nums[k+1] - nums[k])));

                }
            }
        List<List<Integer>> list2=new ArrayList<>();
            for (int p =0; p < nums.length-1; p++) {
                if (nums[p+1] - nums[p] == min) {

                    list2.add(Arrays.asList(nums[p],nums[p+1]));

                }

            }

        System.out.println(Arrays.toString(new List[]{list2}));


    }
}
