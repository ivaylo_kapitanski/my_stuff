package com.telerikacademy;

import java.util.HashMap;
import java.util.Scanner;

public class Maximum_Number_of_Balloons_1189_shorter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();
        String toCompare = "balloon";

        HashMap<Character,Integer>hashMapToCompare=new HashMap<>();

        for (int i = 0; i <toCompare.length() ; i++) {
            if(hashMapToCompare.containsKey(toCompare.charAt(i))){
                hashMapToCompare.put(toCompare.charAt(i),hashMapToCompare.get(toCompare.charAt(i))+1);
            }else {
                hashMapToCompare.put(toCompare.charAt(i),1);
            }
        }
        HashMap<Character,Integer>hashMap=new HashMap<>();
        for (int i = 0; i <text.length() ; i++) {
            if(hashMap.containsKey(text.charAt(i))){
                hashMap.put(text.charAt(i),hashMap.get(text.charAt(i))+1);
            }else {
                hashMap.put(text.charAt(i),1);
            }
        }
        int min=Integer.MAX_VALUE;
        for (Character c:hashMapToCompare.keySet()) {
            if(hashMap.containsKey(c)){
                int temp=hashMap.get(c)/hashMapToCompare.get(c);
                min=Math.min(min,temp);
            }else{
                min=0;
                break;
            }
        }
        System.out.println(min);


    }
}
