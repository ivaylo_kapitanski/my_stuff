package com.telerikacademy;

import java.util.Arrays;
import java.util.Scanner;

public class Sort_Integers_by_The_Number_of_1_Bits_1356 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String[]input=scanner.nextLine().split(",");

        int[]arr=new int[input.length];

        for (int i = 0; i <input.length ; i++) {
            arr[i]=Integer.parseInt(input[i]);
        }
        Arrays.sort(arr);
        for (int z = 0; z < arr.length ; z++) {
            for (int i = 0; i < arr.length-1 ; i++) {
                if(Integer.bitCount(arr[i])>Integer.bitCount(arr[i+1])){
                    int temp=arr[i];
                    arr[i]=arr[i+1];
                    arr[i+1]=temp;
                }
            }
        }
        System.out.println(Arrays.toString(arr));

    }
}
