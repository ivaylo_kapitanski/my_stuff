package com.telerikacademy;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Find_Words_That_Can_Be_Formed_by_Characters_1160 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String[]words=scanner.nextLine().split(",");
        String chars= scanner.nextLine();
        int checker=0;


        for (int i = 0; i < words.length ; i++) {
            int count=0;
            String[]charsArray=chars.split("");
            List<String> charsClone=Arrays.asList(charsArray);
            for (int k = 0; k <words[i].length() ; k++) {
                if(charsClone.contains(String.valueOf(words[i].charAt(k)))){
                    count++;
                    int index=Arrays.asList(charsArray).indexOf(String.valueOf(words[i].charAt(k)));
                charsArray[index]="";
                }else{
                    break;
                }
                charsClone=Arrays.asList(charsArray);
            }

            if(count==words[i].length()){
                checker+= words[i].length();
            }
        }
        System.out.println(checker);
    }
}
