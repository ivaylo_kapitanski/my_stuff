package com.telerikacademy;

import java.util.Scanner;

public class Number_of_Steps_to_Reduce_a_Number_to_Zero_1342 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int num=Integer.parseInt(scanner.nextLine());


        int count=0;

        while(num>0){

            if(num%2==0){
                num/=2;
                count++;
            }if(num%2!=0){
                num-=1;
                count++;
            }

        }
        System.out.println(count);








    }
}
