package com.telerikacademy;

import java.util.Arrays;
import java.util.Scanner;

public class DI_String_Match_942 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String s = scanner.nextLine();

        int[] nums = new int[s.length()+1];
        int max = s.length();
        int min = 0;


        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == 'I') {
                nums[i] = min;
                min++;
            } else if (s.charAt(i) == 'D') {
                nums[i] = max;
                max--;
            }
        }
        if(s.charAt(s.length()-1)=='I'){
            nums[s.length()]=min;
        }else{
            nums[s.length()]=max;
        }

        System.out.println(Arrays.toString(nums));


    }
}
