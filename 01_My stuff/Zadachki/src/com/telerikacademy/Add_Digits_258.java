package com.telerikacademy;

import java.util.Scanner;

public class Add_Digits_258 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);

        int num= scanner.nextInt();
        int digitalRoot=0;
        if(num<10){
            digitalRoot=num;
        }else{
            digitalRoot=(num-1)%9+1;
        }
        System.out.println(digitalRoot);

    }
}
