package com.telerikacademy;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Happy_Number_202 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n=scanner.nextInt();
        String nStr=String.valueOf(n);

        int sum=0;
        boolean isHappy=false;
        List<Integer> list1=new ArrayList<>();

       while(!isHappy){
           char[] nChar= nStr.toCharArray();
           for (int i =nChar.length-1; i >=0 ; i--) {
               int digit=Integer.parseInt(String.valueOf(nChar[i]));
               sum+=digit*digit;
               digit=0;
           }
           if(sum==1){
               isHappy=true;
           }else if(list1.contains(sum)){
               isHappy=false;
               break;
           }
           else{
               list1.add(sum);
               nStr=String.valueOf(sum);
               sum=0;
           }
       }


        System.out.println(isHappy);



    }
}
