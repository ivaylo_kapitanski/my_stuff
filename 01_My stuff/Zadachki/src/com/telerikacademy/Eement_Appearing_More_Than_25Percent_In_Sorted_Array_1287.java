package com.telerikacademy;

import java.util.Arrays;
import java.util.Scanner;

public class Eement_Appearing_More_Than_25Percent_In_Sorted_Array_1287 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] input1 = scanner.nextLine().split(",");

        int[] arr1 = new int[input1.length];


        for (int i = 0; i < input1.length; i++) {
            arr1[i] = Integer.parseInt(input1[i]);
        }

        Arrays.sort(arr1);

        double target = arr1.length / 4.0;
        int count = 1;
        for (int i = 0; i < arr1.length - 1; i++) {
            if (arr1[i] == arr1[i + 1]) {
                count++;
                if (count > target) {
                    target = arr1[i];
                    break;
                }
            }else {

                count=1;
            }

    }
        if(arr1.length<=1){
            target=arr1[0];
        }
        System.out.println((int)target);


}
}
