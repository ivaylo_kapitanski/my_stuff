package com.telerikacademy;

import java.util.Scanner;

public class Island_Perimeter_463 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int rows = scanner.nextInt();
        int columns = scanner.nextInt();
        int[][] mat = new int[rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                mat[i][j] = scanner.nextInt();
            }
        }
        int area = 0;

            int finalArea=0;
        for (int i = 0; i < mat.length; i++) {
            for (int k = 0; k < mat[0].length; k++) {

                if (mat[i][k] == 1) {

                    if (i - 1 >= 0 && mat[i - 1][k] == 1) {
                        area++;
                    }
                    if (k + 1 < mat[0].length && mat[i][k + 1] == 1) {
                        area++;
                    }
                    if (i + 1 < mat.length && mat[i + 1][k] == 1) {
                        area++;
                    }
                    if (k - 1 >= 0 && mat[i][k - 1] == 1) {
                        area++;
                    }
                    finalArea=finalArea+(4-area);
                    area=0;
                }
            }
        }
        System.out.println(finalArea);
    }
}
