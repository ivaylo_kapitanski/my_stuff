package com.telerikacademy;

import java.util.Scanner;

public class Monotonic_Array_896 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String[]input=scanner.nextLine().split(",");
        int[] arr=new int[input.length];
        for (int i = 0; i < input.length ; i++) {
            arr[i]=Integer.parseInt(input[i]);
        }
        int countUp=1;
        int countdown=1;
        for (int i = 0; i < arr.length-1 ; i++) {
            if(arr[i]>=arr[i+1]){
                countUp++;
            } if(arr[i]<=arr[i+1]){
                countdown++;
            }
        }
        System.out.println(countdown == arr.length || countUp == arr.length);
    }
}
