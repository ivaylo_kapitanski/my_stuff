package com.telerikacademy;

import java.util.*;

public class Find_N_Unique_Integers_Sum_up_to_Zero_1304 {

    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);

        int n=Integer.parseInt(scanner.nextLine());
        int[] sumZero= new int[n];

        if(n%2!=0){
            sumZero[0]=0;
            for (int i = 1; i <n ; i++) {
                sumZero[i]=i;
                sumZero[i+1]=-i;
                i++;

            }
        }else{
            for (int i = 1; i <n ; i++) {
                sumZero[i-1]=i;
                sumZero[i]=-i;
                i++;

            }
        }

        System.out.println(Arrays.toString(sumZero));


    }
}
