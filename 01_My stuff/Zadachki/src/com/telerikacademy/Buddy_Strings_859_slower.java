package com.telerikacademy;

import java.util.Scanner;

public class Buddy_Strings_859_slower {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String A = scanner.nextLine();
        String B = scanner.nextLine();

        char[] aStr=A.toCharArray();

        boolean isBuddy=false;

        if (A.length() != B.length() || A.length() < 2) {
            System.out.println(false);
            return;
        }

        for (int i = 1; i < aStr.length ; i++) {
            if (A.equals(B)) {
                if (aStr[i - 1]!=(aStr[i])) {
                    if(A.length()<=2)
                        isBuddy=false;
                    break;
                } else {
                    isBuddy=true;
                }
            }
        }
        firstloop:
        for (int i = 0; i < aStr.length ; i++) {
            for (int k = 0; k < aStr.length ; k++) {
                if(aStr[i]==aStr[k]){
                    if(i==k){
                        continue;
                    }}
                char temp= aStr[k];
                aStr[k]=aStr[i];
                aStr[i]=temp;
                if(B.equals(String.valueOf(aStr))){
                    isBuddy=true;
                    break firstloop;
                }else{
                    aStr=A.toCharArray();
                }
            }
        }

        if(isBuddy){
            System.out.println(true);
        }else{
            System.out.println(false);
        }

    }
}

