package com.telerikacademy;

import java.util.Arrays;
import java.util.Scanner;

public class Reverse_String_344 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);

        String [] input=scanner.nextLine().split(",");
        char[]s=new char[input.length];

        for (int i = 0; i < input.length ; i++) {
            s[i]=input[i].charAt(0);
        }
        int pos=s.length-1;
        for (int k = 0; k <s.length/2; k++) {
            char temp=s[pos];
            s[pos]=s[k];
            s[k]=temp;
            pos--;
        }
        String sS="";
        for (int i = 0; i <s.length ; i++) {
            sS+=String.valueOf(s[i]);
        }
        System.out.println(Arrays.toString(s));

    }
}
