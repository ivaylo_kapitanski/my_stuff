package com.telerikacademy;

import java.util.Scanner;

public class Slowest_Key_1629 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] input = scanner.nextLine().split(",");
        int[] releaseTimes = new int[input.length];
        String keysPressed = scanner.nextLine();

        for (int i = 0; i < input.length; i++) {
            releaseTimes[i] = Integer.parseInt(input[i]);
        }
        int maxTime = releaseTimes[0];
        char chosenOne = (keysPressed.charAt(0));
        for (int i = 1; i < releaseTimes.length; i++) {
            int temp = releaseTimes[i] - releaseTimes[i - 1];
            if (temp > maxTime) {
                maxTime = temp;
                chosenOne = (keysPressed.charAt(i));
            } else if (temp == maxTime) {
                chosenOne = (char) Math.max(chosenOne, keysPressed.charAt(i));
            }
        }

        System.out.println(String.valueOf(chosenOne));

    }
}
