package com.telerikacademy;


import java.util.Scanner;

public class NonDecreasing_Array_665_Shorter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] input = scanner.nextLine().split(",");

        int[] nums = new int[input.length];

        for (int i = 0; i < input.length; i++) {
            nums[i] = Integer.parseInt(input[i]);
        }




        int count = 0;
        for (int i = 1; i < nums.length&&count<2 ; i++) {
            if(nums[i-1]<=nums[i]){
                continue;
            }

            if(i-2>=0&&nums[i-2]>nums[i]){
                nums[i]=nums[i-1];
            }else{
                nums[i-1]=nums[i];
            }
            count++;
        }
        if(count<2){
            System.out.println(true);
        }else{
            System.out.println(false);
        }
          // return count<2;

           }
}
