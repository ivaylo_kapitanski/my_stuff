package com.telerikacademy;

import java.util.*;

public class Rank_Transform_of_an_Array_1331 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String[]input=scanner.nextLine().split(",");
        int[] arr=new int[input.length];
        for (int i = 0; i < input.length ; i++) {
            arr[i]=Integer.parseInt(input[i]);
        }

        Set<Integer>treeSet=new TreeSet<>();
        for (int i :arr) {
            treeSet.add(i);
        }
        TreeMap<Integer,Integer> treeMap=new TreeMap<>();
        int count=1;
        for (int i :treeSet) {
            treeMap.put(i,count++);
        }
        for (int i = 0; i < arr.length ; i++) {
            arr[i]=treeMap.get(arr[i]);
        }
        System.out.println(Arrays.toString(arr));


    }
}
