package com.telerikacademy;

import java.util.Scanner;

public class Peak_Index_in_a_Mountain_Array_852 {
    public static void main(String[] args) {


        Scanner scanner = new Scanner(System.in);
        String[] input1 = scanner.nextLine().split(",");
        int[] nums = new int[input1.length];


        for (int i = 0; i < input1.length; i++) {
            nums[i] = Integer.parseInt(input1[i]);
        }
        int top=0;
        for (int i = 0; i <nums.length-1 ; i++) {
            if(nums[i]>nums[i+1]){
                top=i;
                break;
            }

        }
        System.out.println(top);
    }
}
