package com.telerikacademy;

import java.util.Arrays;
import java.util.Scanner;

public class XOR_Operation_in_an_Array_1486 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n=Integer.parseInt(scanner.nextLine());
        int start=Integer.parseInt(scanner.nextLine());

        int[] nums=new int[n];
        int output=start;

        for (int i = 0; i <n ; i++) {
            nums[i]=start+(2*i);

        }
        for (int k = 1; k < nums.length ; k++) {
            output^=nums[k];

        }
        System.out.println(output);
        System.out.println(Arrays.toString(nums));










    }
}
