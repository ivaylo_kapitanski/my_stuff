package com.telerikacademy;

import java.util.Scanner;

public class Robot_Return_to_Origin_657 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String moves = scanner.nextLine();

        int x = 0;
        int y = 0;

        for (int i =0; i < moves.length(); i++) {
            if (moves.charAt(i) == 'U') {
                x += 1;
            } else if (moves.charAt(i) == 'D') {
                x -= 1;
            } else if (moves.charAt(i) == 'L') {
                y -= 1;
            } else if (moves.charAt(i) == 'R') {
                y += 1;
            }

        }
        if (x == 0 && y == 0) {
            System.out.println(true);
        } else {
            System.out.println(false);
        }


    }
}
