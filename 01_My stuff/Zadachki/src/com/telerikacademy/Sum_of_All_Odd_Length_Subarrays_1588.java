package com.telerikacademy;

import java.util.Scanner;

public class Sum_of_All_Odd_Length_Subarrays_1588 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String[] input=scanner.nextLine().split(",");
        int[]arr=new int[input.length];

        for (int i = 0; i < input.length ; i++) {
            arr[i]=Integer.parseInt(input[i]);
        }
        int total=0;

        for (int i = 0; i < arr.length ; i++) {

            for (int l = i; l <arr.length ; l++) {
                if((l-i)%2==0){
                    for (int j = i; j <=l ; j++) {
                        total+=arr[j];
                    }
                }

            }

        }
        System.out.println(total);




    }
}
