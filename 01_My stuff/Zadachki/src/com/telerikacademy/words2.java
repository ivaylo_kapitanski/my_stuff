package com.telerikacademy;

import java.util.Arrays;
import java.util.Scanner;

public class words2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] entry = scanner.nextLine().split("");
        String entry2= Arrays.toString(entry);
        //char[] charEntry = entry.toCharArray();
        boolean isUpper = false;
        boolean isLower = false;
        boolean isSnake = false;
        boolean isKebab = false;
        boolean isPascal = false;
        boolean isCamel = false;


        for (int i = 1; i < entry.length; i++) {
            if (entry[0].toUpperCase().equals(entry[0])) {
                if (entry[i].toLowerCase().toLowerCase().equals(entry[i])) {
                    isPascal = true;
                } else if (entry[i].toUpperCase().equals(entry[i])) {
                    isUpper = true;
                }
            } else if (entry[0].toLowerCase().equals(entry[0])) {
                if (entry[i].toLowerCase().equals(entry[i])) {
                    if (entry2.contains("-")) {
                        isKebab = true;
                    } else if (entry2.contains("_")) {
                        isSnake = true;
                    } else {
                        isCamel = true;
                    }
                } else {
                    isLower = true;
                }
            }
        }
        if (isPascal) {
            System.out.println("Pascal case");
        } else if (isCamel) {
            System.out.println("Camel case");
        } else if (isKebab) {
            System.out.println("Kebab case");
        } else if (isSnake) {
            System.out.println("Snake case");
        } else if (isUpper) {
            System.out.println("Upper case");
        } else if (isLower) {
            System.out.println("Lower case");
        }

    }


}
