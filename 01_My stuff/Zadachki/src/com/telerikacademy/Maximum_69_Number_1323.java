package com.telerikacademy;


import java.util.Arrays;
import java.util.Scanner;

public class Maximum_69_Number_1323 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int num=scanner.nextInt();
        char[] charNum=String.valueOf(num).toCharArray();

        int max=num;

        for (int i = 0; i < charNum.length ; i++) {
            if(charNum[i]=='6'){
                charNum[i]='9';
                max=Math.max(max,Integer.parseInt(new String(charNum)));
                break;
            }
        }
        System.out.println(max);

    }
}
