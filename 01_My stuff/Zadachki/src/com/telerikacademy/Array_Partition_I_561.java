package com.telerikacademy;


import java.util.Arrays;
import java.util.Scanner;

public class Array_Partition_I_561 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String[]input1=scanner.nextLine().split(",");
        int[]nums=new int[input1.length];

        for (int i = 0; i < input1.length ; i++) {
            nums[i]=Integer.parseInt(input1[i]);
        }
        Arrays.sort(nums);

        int sum=0;
        for (int i = 0; i < nums.length ; i++) {
           int min=Math.min(nums[i],nums[i+1]);
            i++;
            sum+=min;
        }

        System.out.println(sum);
    }
}
