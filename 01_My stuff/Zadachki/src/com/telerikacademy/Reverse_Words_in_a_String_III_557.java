package com.telerikacademy;

import java.util.Scanner;

public class Reverse_Words_in_a_String_III_557 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String[] input=scanner.nextLine().split(" ");


        for (int i = 0; i < input.length ; i++) {
            String temp="";
            for (int k = input[i].length()-1; k >=0 ; k--) {
                temp+= String.valueOf(input[i].charAt(k));
            }
            input[i]= temp;
        }

        String stringJoiner=String.join(" ",input);

        System.out.println(stringJoiner);

    }
}
