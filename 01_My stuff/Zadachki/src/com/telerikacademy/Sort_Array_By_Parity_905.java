package com.telerikacademy;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Sort_Array_By_Parity_905 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);

        String[] input=scanner.nextLine().split(",");
        int[]arr=new int[input.length];

        for (int i = 0; i < input.length ; i++) {
            arr[i]=Integer.parseInt(input[i]);
        }

        List<Integer> even=new ArrayList<>();
        List<Integer> odd=new ArrayList<>();

        for (int i = 0; i < arr.length ; i++) {
            if(arr[i]%2!=0){
                odd.add(arr[i]);
            }else{
                even.add(arr[i]);
            }
        }
        List<Integer> combined=new ArrayList<>(even);
        combined.addAll(odd);
      int[] result=new int[combined.size()];

        for (int i = 0; i <combined.size() ; i++) {
            result[i]= combined.get(i);
        }









    }
}
