package com.telerikacademy;

import java.util.Arrays;
import java.util.Scanner;

public class Convert_Integer_to_the_Sum_of_Two_NoZero_Integers_1317 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n=scanner.nextInt();
        int[]checked=new int[2];

        for (int i = 1; i <=n ; i++) {
            if(String.valueOf(i).contains("0")){
                continue;
            }
            if(String.valueOf(n-i).contains("0")){
                continue;
            }
            checked[0]=i;
            checked[1]=(n-i);
        }
        System.out.println(Arrays.toString(checked));


    }
}
