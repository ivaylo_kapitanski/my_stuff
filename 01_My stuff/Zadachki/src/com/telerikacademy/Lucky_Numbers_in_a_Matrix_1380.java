package com.telerikacademy;


import java.util.*;

public class Lucky_Numbers_in_a_Matrix_1380 {
    public static void main(String[] args) {
        //enter the matrix
        //check for the min of each row.
        //check for the max of each column
        //if min of row==max of col=>that is the lucky number
        Scanner scanner = new Scanner(System.in);

        int row = Integer.parseInt(scanner.nextLine());
        int col = Integer.parseInt(scanner.nextLine());
        int[][] matrix = new int[row][col];

        for (int i = 0; i < row; i++) {
            for (int k = 0; k < col; k++) {
                matrix[i][k] = Integer.parseInt(scanner.nextLine());
            }
        }
//        for (int i = 0; i < row; i++) {
//            for (int k = 0; k < col; k++) {
//                System.out.print(matrix[i][k] + " ");
//            }
//            System.out.println();
//        }
        List<Integer> minRow=new ArrayList<>();
        List<Integer> maxcol=new ArrayList<>();

        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;


        for (int i = 0; i < matrix.length; i++) {
            for (int k = 0; k <matrix[0].length; k++) {
            min=Math.min(min,matrix[i][k]);
            }
            minRow.add(min);
            min = Integer.MAX_VALUE;
        }

        for (int k = 0; k <matrix[0].length ; k++) {
            for (int i = 0; i <matrix.length ; i++) {
                max=Math.max(max,matrix[i][k]);
            }
            maxcol.add(max);
            max = Integer.MIN_VALUE;
        }

        minRow.sort(Comparator.reverseOrder());
        Arrays.sort(new List[]{maxcol});
        int max2=Integer.MIN_VALUE;
        int min2=Integer.MAX_VALUE;
        for (int i = 0; i <minRow.size() ; i++) {

            max2=Math.max(max2,minRow.get(i));
        }

        for (int i = 0; i <maxcol.size() ; i++) {
            min2=Math.min(min2,maxcol.get(i));
        }

        List<Integer> luckyNum=new ArrayList<>();

        for (int i = 0; i < matrix.length; i++) {
            for (int k = 0; k < matrix[0].length; k++) {
                if(matrix[i][k]== max2&&matrix[i][k]==min2){
                    luckyNum.add(matrix[i][k]);
                }
            }
        }

        System.out.println(luckyNum);




    }
}
