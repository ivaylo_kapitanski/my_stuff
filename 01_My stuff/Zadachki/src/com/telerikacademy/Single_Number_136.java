package com.telerikacademy;

import java.util.Arrays;
import java.util.Scanner;

public class Single_Number_136 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String[] input1=scanner.nextLine().split(",");
        int[] nums=new int[input1.length];

        for (int i = 0; i <input1.length ; i++) {
            nums[i]=Integer.parseInt(input1[i]);
        }
        Arrays.sort(nums);


        int alone=0;
        int count=0;
        if(nums.length<2){
            alone=nums[0];
        }

        for (int i = 0; i < nums.length-1 ; i++) {

            if(nums[i]==nums[i+1]){
                count+=2;
                if((i+1)==nums.length-2){
                    alone=nums[nums.length-1];
                    break;
                }

            }else if(count<2){
                alone=nums[i];
                break;
            }else if(nums[i]!=nums[i+1]){
                alone=nums[i];
                break;
            }

                i++;
        }

        System.out.println(alone);

    }
}
