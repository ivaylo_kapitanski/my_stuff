package com.telerikacademy;

import java.util.Scanner;

public class Special_Positions_in_a_Binary_Matrix_1582 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int row = Integer.parseInt(scanner.nextLine());
        int col = Integer.parseInt(scanner.nextLine());


        int[][] mat = new int[row][col];

        for (int i = 0; i < row; i++) {
            for (int p = 0; p < col; p++) {
                mat[i][p] = Integer.parseInt(scanner.nextLine());
            }
        }
        for (int i = 0; i < mat.length; i++) {
            for (int p = 0; p < mat[0].length; p++) {
                System.out.print(mat[i][p] + " ");
            }
            System.out.println();
        }
        int countdown=0;
        int count=0;
        for (int i = 0; i <mat.length ; i++) {
            for (int k = 0; k < mat[0].length; k++) {
                if (mat[i][k] == 1) {
                    count++;
                    int temp = 0;
                    for (int j = 0; j < mat[i].length; j++) {
                        if (mat[i][j] == 1) {
                            temp++;
                        }
                    }
                    for (int j = 0; j < mat.length; j++) {
                        int zorNaMax=mat[j][k];
                        if (zorNaMax == 1) {
                            temp++;
                        }
                    }
                    if (count + temp != 3) {
                        temp = 0;
                        count = 0;
                    } else {
                        countdown++;
                        temp = 0;
                        count = 0;
                    }
                }
            }
        }
        System.out.println(countdown);
    }
}
