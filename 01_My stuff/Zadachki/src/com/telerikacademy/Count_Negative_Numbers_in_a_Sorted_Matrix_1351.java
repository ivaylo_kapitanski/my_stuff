package com.telerikacademy;

import java.util.Scanner;

public class Count_Negative_Numbers_in_a_Sorted_Matrix_1351 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n=Integer.parseInt( scanner.nextLine());
        int m=Integer.parseInt( scanner.nextLine());
        int[][] grid=new int[n][m];

        for (int i = 0; i <n ; i++) {
            for (int k = 0; k <m ; k++) {
                grid[i][k]= scanner.nextInt();

            }
        }
        int negative=0;
        for (int i = 0; i < grid.length ; i++) {
            for (int k = 0; k < grid[0].length ; k++) {
                if(grid[i][k]<0){
               negative++;}

            }
        }

        System.out.println(negative);


    }
}
