package com.telerikacademy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Self_Dividing_Numbers_728 {
    public static void main(String[] args) {

        Scanner scanner=new Scanner(System.in);

        int left=Integer.parseInt(scanner.nextLine());
        int right=Integer.parseInt(scanner.nextLine());


        List<Integer> second=new ArrayList<>();


        for (int i =left; i <=right ; i++) {
            if(String.valueOf(i).contains("0")){
                continue;
            }
            String iStr=(String.valueOf(i));
            int count=0;
            for (int k = 0; k <iStr.length() ; k++) {

                int temp=iStr.charAt(k)-'0';
            if(i%temp==0){
                count++;
            }else {
                break;
            }

            }
            if(!second.contains(i)&&count==iStr.length()){
                second.add(i);
            }

        }
        int[] result=new int[second.size()];
        for (int i = 0; i <second.size() ; i++) {
            result[i]= second.get(i);
        }

        System.out.println(Arrays.toString(new List[]{second}));




    }
}
