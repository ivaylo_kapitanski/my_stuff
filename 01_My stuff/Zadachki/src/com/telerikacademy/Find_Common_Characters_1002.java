package com.telerikacademy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Find_Common_Characters_1002 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String[]arr=scanner.nextLine().split(",");

        List<String> list1 = new ArrayList<>();
        int[][] words = new int[arr.length][26];

        for(int i=0;i<arr.length;i++)
        {
            for(Character ch :arr[i].toCharArray())
            {
                words[i][ch-'a']++;
            }
        }

        for(int i=0;i<26;i++)
        {
            for(int j=1;j<arr.length;j++)
            {
                words[0][i]= Math.min(words[0][i],words[j][i]);
            }

            for(int k=0;k<words[0][i];k++)
            {
                list1.add(Character.toString((char)('a'+i)));
            }

        }
        System.out.println(Arrays.toString(new List[]{list1}));

    }
}
