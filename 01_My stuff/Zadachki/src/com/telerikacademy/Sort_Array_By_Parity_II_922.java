package com.telerikacademy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Sort_Array_By_Parity_II_922 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String[]input=scanner.nextLine().split(",");

        int[] nums=new int[input.length];

        List<Integer>even=new ArrayList<>();
        List<Integer>odd=new ArrayList<>();

        for (int i = 0; i <input.length ; i++) {
            nums[i]=Integer.parseInt(input[i]);
        }


        for (int i = 0; i < nums.length ; i++) {
            if(nums[i]%2!=0){
                odd.add(nums[i]);
            }else{
                even.add(nums[i] );
            }
        }
        int posE=0;
        int posO=0;
        for (int i = 0; i < nums.length ; i++) {
            if(i%2==0){
                nums[i]= even.get(posE);
                posE++;
            }else{
                nums[i]= odd.get(posO);
                posO++;
            }
        }
        System.out.println(Arrays.toString(nums));


    }

}
