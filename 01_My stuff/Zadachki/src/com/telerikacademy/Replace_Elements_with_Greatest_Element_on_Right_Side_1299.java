package com.telerikacademy;

import java.util.Arrays;
import java.util.Scanner;

public class Replace_Elements_with_Greatest_Element_on_Right_Side_1299 {
    public static void main(String[] args) {
        //scanner+String/int array for the entries
        //loop through the array. If i= the max value of the following digits.
        //Replace the last element with -1 to keep the same size

        Scanner scanner=new Scanner(System.in);

        String[]input1=scanner.nextLine().split(",");

        int[]nums=new int[input1.length];


        for (int i = 0; i <input1.length ; i++) {
            nums[i]=Integer.parseInt(input1[i]);
        }


        for (int i = 0; i < nums.length ; i++) {
            int max=0;
            for (int k = i+1; k < nums.length ; k++) {
                max=Math.max(max,nums[k]);

            }
            nums[i]=max;
            if(i==nums.length-1){
                nums[i]=-1;
            }
        }
        System.out.println(Arrays.toString(nums));

    }
}
