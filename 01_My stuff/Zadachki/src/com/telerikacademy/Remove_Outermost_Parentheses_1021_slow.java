package com.telerikacademy;

import java.util.Scanner;

public class Remove_Outermost_Parentheses_1021_slow {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String S = scanner.nextLine();


        String[] splitS = S.split("");
        int countR = 0;
        int countL = 0;
        String s2 = "";
        String s3 = "";
        if (splitS[0].equals("(")) {
            countL++;
        } else if (splitS[0].equals(")")) {
            countR++;
        }
        s2 += splitS[0];
        int i = 1;

        while (countL != countR || i < splitS.length) {
            s2 += splitS[i];
            if (splitS[i].equals("(")) {
                countL++;
            } else if (splitS[i].equals(")")) {
                countR++;
            }
            if (countL == countR) {
                char[] temp = s2.toCharArray();
                for (int j = 1; j < temp.length - 1; j++) {
                    s3 += (temp[j]);
                }
                s2 = "";
                countL = 0;
                countR = 0;
            }
            i++;
        }

        System.out.println(s3);


    }
}
