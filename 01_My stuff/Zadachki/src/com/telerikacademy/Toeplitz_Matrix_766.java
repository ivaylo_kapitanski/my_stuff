package com.telerikacademy;

import java.util.Scanner;

public class Toeplitz_Matrix_766 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int row = Integer.parseInt(scanner.nextLine());
        int col = Integer.parseInt(scanner.nextLine());


        int[][] mat = new int[row][col];

        for (int i = 0; i < row; i++) {
            for (int p = 0; p < col; p++) {
                mat[i][p] = Integer.parseInt(scanner.nextLine());
            }
        }
        for (int i = 0; i < mat.length; i++) {
            for (int p = 0; p < mat[0].length; p++) {
                System.out.print(mat[i][p] + " ");
            }
            System.out.println();
        }
        boolean isToeplitz=false;
        if(mat.length<2){
            isToeplitz=true;

        }else if(mat[0].length<2){
            isToeplitz=true;
        }


        outerloop:
        for (int i = 0; i < mat.length-1 ; i++) {
            for (int k = 0; k < mat[0].length-1; k++) {
                int diagonAli=mat[i+1][k+1];
                if(mat[i][k]==diagonAli){
                    isToeplitz=true;
                }else{
                    isToeplitz=false;
                    break outerloop;
                }
            }

        }

        System.out.println(isToeplitz);

















    }
}
