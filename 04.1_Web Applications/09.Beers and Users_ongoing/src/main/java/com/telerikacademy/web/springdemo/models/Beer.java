package com.telerikacademy.web.springdemo.models;

import javax.persistence.*;


@Entity
@Table(name="beers")
public class Beer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="beer_id")
    private int id;

    @Column(name="name")

    private String name;

    @Column(name="abv")

    private double abv;

    @ManyToOne
    @JoinColumn(name="style_id")
    private Style style_id;

    public Beer() {
    }

    public Beer(int id, String name, double abv) {
        this.id = id;
        this.name = name;
        this.abv = abv;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAbv() {
        return abv;
    }

    public void setAbv(double abv) {
        this.abv = abv;
    }

    public Style getStyle_id() {
        return style_id;
    }

    public void setStyle_id(Style style_id) {
        this.style_id = style_id;
    }
}