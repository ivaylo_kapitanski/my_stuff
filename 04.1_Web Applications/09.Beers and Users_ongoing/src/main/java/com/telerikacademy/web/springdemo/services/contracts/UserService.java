package com.telerikacademy.web.springdemo.services.contracts;

import com.telerikacademy.web.springdemo.models.User;


public interface UserService extends GenericService<User>{

    User getByEmail(String email);
}
