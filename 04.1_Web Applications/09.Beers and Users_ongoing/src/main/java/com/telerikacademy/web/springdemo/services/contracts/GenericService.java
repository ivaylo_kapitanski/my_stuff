package com.telerikacademy.web.springdemo.services.contracts;

import java.util.List;

public interface GenericService <T>{
    List<T> getAll();

    T getById(int id);

    void create(T input);

    void update(T input);

    void delete(int id);
}
