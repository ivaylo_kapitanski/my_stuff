package com.telerikacademy.web.springdemo.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserDto {
    @NotNull(message = "First name cannot be null.")
    @Size(min = 1, max = 20, message = "First name should be between 1 and 20 symbols")
    private String firstName;

    @Size(min = 1, max = 20, message = "Last name should be between 1 and 20 symbols")
    @NotNull(message = "Last name cannot be null")
    private String lastName;

    @Size(min = 2, max = 20, message = "Email should be between 2 and 20 symbols")
    @NotNull(message = "Email cannot be null")
    private String email;

    public UserDto() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
