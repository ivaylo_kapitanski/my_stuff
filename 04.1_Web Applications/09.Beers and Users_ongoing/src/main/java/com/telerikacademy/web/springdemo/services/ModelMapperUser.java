package com.telerikacademy.web.springdemo.services;

import com.telerikacademy.web.springdemo.models.User;
import com.telerikacademy.web.springdemo.models.UserDto;
import com.telerikacademy.web.springdemo.repositories.contracts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ModelMapperUser {
    private final UserRepository userRepository;

    @Autowired
    public ModelMapperUser(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User fromDto(UserDto userDto) {
        User user = new User();
        dtoToObject(userDto, user);
        return user;
    }

    public User fromDto(UserDto userDto, int id) {
        User user = userRepository.getById(id);
        dtoToObject(userDto, user);
        return user;
    }

    private void dtoToObject(UserDto userDto, User user) {
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setEmail(userDto.getEmail());
    }
}
