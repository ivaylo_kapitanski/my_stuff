package com.telerikacademy.web.springdemo.services;

import com.telerikacademy.web.springdemo.models.Beer;
import com.telerikacademy.web.springdemo.models.BeerDto;
import com.telerikacademy.web.springdemo.models.User;
import com.telerikacademy.web.springdemo.models.UserDto;
import com.telerikacademy.web.springdemo.repositories.contracts.BeerRepository;
import com.telerikacademy.web.springdemo.repositories.contracts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ModelMapper {
    private final BeerRepository beerRepository;

    @Autowired
    public ModelMapper(BeerRepository beerRepository) {
        this.beerRepository = beerRepository;
    }

    public Beer fromDto(BeerDto beerDto) {
        Beer beer = new Beer();
        dtoToObject(beerDto, beer);
        return beer;
    }

    public Beer fromDto(BeerDto beerDto, int id) {
        Beer beer = beerRepository.getById(id);
        dtoToObject(beerDto, beer);
        return beer;
    }

    private void dtoToObject(BeerDto beerDto, Beer beer) {
        beer.setName(beerDto.getName());
        beer.setAbv(beerDto.getAbv());
    }
}
