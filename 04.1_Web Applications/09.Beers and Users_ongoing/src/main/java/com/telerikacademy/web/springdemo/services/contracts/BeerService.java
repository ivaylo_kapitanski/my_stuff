package com.telerikacademy.web.springdemo.services.contracts;

import com.telerikacademy.web.springdemo.models.Beer;

public interface BeerService extends GenericService<Beer> {

    Beer getByName(String name);
}
