package com.telerikacademy.web.springdemo.repositories.contracts;

import com.telerikacademy.web.springdemo.models.Beer;

public interface BeerRepository extends GenericRepository<Beer>{
    Beer getByName(String name);

}
