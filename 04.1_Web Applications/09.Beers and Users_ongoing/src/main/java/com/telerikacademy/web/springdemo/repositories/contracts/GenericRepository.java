package com.telerikacademy.web.springdemo.repositories.contracts;



import java.util.List;

public interface GenericRepository<T> {
    List<T> getAll();

    T getById(int id);

    void create(T input);

    void update(T input);

    void delete(int id);
}
