package com.telerikacademy.web.springdemo.repositories.contracts;

import com.telerikacademy.web.springdemo.models.User;

public interface UserRepository extends GenericRepository<User>{
     User getByEmail(String email);


}
