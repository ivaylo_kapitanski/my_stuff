package com.telerikacademy.web.springdemo.services;

import com.telerikacademy.web.springdemo.exceptions.DuplicateEntityException;
import com.telerikacademy.web.springdemo.exceptions.EntityNotFoundException;
import com.telerikacademy.web.springdemo.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.springdemo.models.Beer;
import com.telerikacademy.web.springdemo.models.Role;
import com.telerikacademy.web.springdemo.models.User;
import com.telerikacademy.web.springdemo.repositories.BeerRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Set;

import static com.telerikacademy.web.springdemo.Helpers.createMockBeer;
import static com.telerikacademy.web.springdemo.Helpers.createMockUser;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class BeerServiceImplTests {

    @Mock
    BeerRepository beerRepository;

    @InjectMocks
    BeerServiceImpl beerService;
    @Test
    public void getById_Should_ReturnBeer_When_MatchExist() {
        // Arrange
        Mockito.when(beerRepository.getById(2))
                .thenReturn(new Beer(2, "Name2", 1.3));
        // Act
        Beer result = beerService.getById(2);

        // Assert
        Assertions.assertEquals(2, result.getId());
        Assertions.assertEquals("Name2", result.getName());
        Assertions.assertEquals(1.3, result.getAbv());
    }

    @Test
    public void create_Should_Throw_When_BeerWithSameNameExists() {
        // Arrange
        var mockBeer = createMockBeer();

        Mockito.when(beerRepository.getByName(mockBeer.getName()))
                .thenReturn(mockBeer);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> beerService.create(mockBeer));
    }

    @Test
    public void create_Should_CallRepository_When_BeerWithSameNameDoesNotExist() {
        // Arrange
        var mockBeer = createMockBeer();

        Mockito.when(beerRepository.getByName(mockBeer.getName()))
                .thenThrow(new EntityNotFoundException("Beer", "name", mockBeer.getName()));

        // Act, Assert
        Mockito.verify(beerRepository, Mockito.times(1))
                .create(Mockito.any(Beer.class));
    }

//    @Test
//    public void update_Should_Throw_When_UserIsNotCreatorOrAdmin() {
//        var mockBeer = createMockBeer();
//        mockBeer.setCreatedBy(new User("MockUser2"));
//
//        Mockito.when(mockRepository.getById(Mockito.anyInt()))
//                .thenReturn(mockBeer);
//
//        Assertions.assertThrows(UnauthorizedOperationException.class,
//                () -> service.update(mockBeer, createMockUser()));
//    }

    @Test
    public void update_Should_Throw_When_BeerNameIsTaken() {
        var mockBeer = createMockBeer();
        var anotherMockBeer = createMockBeer();
        anotherMockBeer.setId(2);

        Mockito.when(beerRepository.getById(Mockito.anyInt()))
                .thenReturn(mockBeer);

        Mockito.when(beerRepository.getByName(Mockito.anyString()))
                .thenReturn(anotherMockBeer);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> beerService.update(mockBeer, createMockUser()));
    }

    @Test
    public void update_Should_Call_Repository_WhenUserIsCreator() {
        // Arrange
        var mockCreator = createMockUser();
        var mockBeer = createMockBeer();
        mockBeer.setCreatedBy(mockCreator);

        // when(beerRepository.getByName(mockBeer.getName()))
        //      .thenReturn(mockBeer);

        when(beerRepository.getByName(mockBeer.getName()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        beerService.update(mockBeer, mockCreator);

        // Assert
        verify(beerRepository, times(1)).update(mockBeer);
    }

    @Test
    public void update_Should_Call_Repository_WhenUserIsAdmin() {
        // Arrange
        var mockAdmin = createMockUser();
        mockAdmin.setRoles(Set.of(new Role("Admin")));

        var mockBeer = createMockBeer();
        mockBeer.setCreatedBy(new User());

        when(beerRepository.getByName(mockBeer.getName()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        beerService.update(mockBeer, mockAdmin);

        // Assert
        verify(beerRepository, times(1)).update(mockBeer);
    }

    @Test
    public void update_Should_Throw_When_UserIsNotCreatorOrAdmin() {
        // Arrange
        var mockCaller = createMockUser();
        mockCaller.setRoles(Set.of(new Role("User")));

        var mockBeer = createMockBeer();
        mockBeer.setCreatedBy(new User());

        // Act, Assert
        assertThrows(UnauthorizedOperationException.class,
                () -> beerService.update(mockBeer, mockCaller));
    }

    @Test
    public void update_Throws_When_BeerWithSameNameExists() {
        // Arrange
        var beerToUpdate = createMockBeer();
        var existingBeer = createMockBeer();
        existingBeer.setId(beerToUpdate.getId() + 1);

        when(beerRepository.getByName(beerToUpdate.getName()))
                .thenReturn(existingBeer);

        // Act, Assert
        assertThrows(DuplicateEntityException.class,
                () -> beerService.update(beerToUpdate, beerToUpdate.getCreatedBy()));
    }

    @Test
    public void update_Should_Call_Repository_WhenNameIsUnique() {
        // Arrange
        var beerToUpdate = createMockBeer();

        when(beerRepository.getByName(beerToUpdate.getName()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        beerService.update(beerToUpdate, beerToUpdate.getCreatedBy());

        // Assert
        verify(beerRepository, times(1)).update(beerToUpdate);
    }

    @Test
    public void update_Should_Call_Repository_WhenUpdatingSameBeer() {
        // Arrange
        var beerToUpdate = createMockBeer();

        when(beerRepository.getByName(beerToUpdate.getName()))
                .thenReturn(beerToUpdate);

        // Act
        beerService.update(beerToUpdate, beerToUpdate.getCreatedBy());

        // Assert
        verify(beerRepository, times(1)).update(beerToUpdate);
    }
}
