package com.telerikacademy.web.springdemo.models;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class RatingDto {

    @Min(0)
    @Max(5)
    private double rating;

    public RatingDto() {
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }
}
