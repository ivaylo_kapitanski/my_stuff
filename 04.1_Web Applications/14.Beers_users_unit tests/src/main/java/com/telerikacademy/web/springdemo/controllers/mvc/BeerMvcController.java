package com.telerikacademy.web.springdemo.controllers.mvc;

import com.telerikacademy.web.springdemo.exceptions.DuplicateEntityException;
import com.telerikacademy.web.springdemo.exceptions.EntityNotFoundException;
import com.telerikacademy.web.springdemo.models.Beer;
import com.telerikacademy.web.springdemo.models.User;
import com.telerikacademy.web.springdemo.services.BeerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Controller
@RequestMapping("/beers")
public class BeerMvcController {
    private final BeerService beerService;


    @Autowired
    public BeerMvcController(BeerService beerService) {
        this.beerService = beerService;
    }


    @GetMapping
    public String showAllBeers(Model model) {
//        if(currentUser.isAdmin()){
//            model.addAttribute("beers",beerService.getAll());
//        }else{
//            var beersCreatedBy=beerService.getAll()
//                    .stream()
//                    .filter(b->b.getCreatedBy().getUsername().equals(currentUser.getUsername()))
//                    .collect(toList());
//            model.addAttribute("beers",beersCreatedBy);
//
//        }
        return "beers";
    }

    @GetMapping("/{id}")
    public String showSingleBeer(@PathVariable int id, Model model) {
        try {
            Beer beer = beerService.getById(id);
            model.addAttribute("beerModel", beer);
            return "beerView";
        } catch (EntityNotFoundException | DuplicateEntityException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }

    }
}
