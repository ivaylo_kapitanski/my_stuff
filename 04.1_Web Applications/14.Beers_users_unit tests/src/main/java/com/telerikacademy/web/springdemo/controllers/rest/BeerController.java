package com.telerikacademy.web.springdemo.controllers.rest;

import com.telerikacademy.web.springdemo.controllers.AuthenticationHelper;
import com.telerikacademy.web.springdemo.exceptions.DuplicateEntityException;
import com.telerikacademy.web.springdemo.exceptions.EntityNotFoundException;
import com.telerikacademy.web.springdemo.models.Beer;
import com.telerikacademy.web.springdemo.models.BeerDto;
import com.telerikacademy.web.springdemo.models.RatingDto;
import com.telerikacademy.web.springdemo.models.User;
import com.telerikacademy.web.springdemo.services.BeerService;
import com.telerikacademy.web.springdemo.services.ModelMapper;
import com.telerikacademy.web.springdemo.services.RatingService;
import com.telerikacademy.web.springdemo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/beers")
public class BeerController {

    private final String RATING_KEY = "rating";

    private final BeerService service;
    private final UserService userService;
    private final ModelMapper modelMapper;
    private final RatingService ratingService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public BeerController(BeerService service,
                          ModelMapper modelMapper,
                          UserService userService,
                          RatingService ratingService,
                          AuthenticationHelper authenticationHelper) {
        this.service = service;
        this.userService = userService;
        this.ratingService = ratingService;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Beer> getAll() {
        return service.getAll();
    }

    @GetMapping("/filter")
    public List<Beer> filter(@RequestParam(required = false) Optional<Integer> styleId) {
        return service.filter(styleId);
    }

    @GetMapping("/{id}")
    public Beer getById(@PathVariable int id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Beer create(@RequestHeader HttpHeaders headers,
                       @Valid @RequestBody BeerDto beerDto) {
        try {
            var user = authenticationHelper.tryGetUser(headers);
            var beer = modelMapper.fromDto(beerDto, user);
            service.create(beer);
            return beer;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Beer update(@PathVariable int id,
                       @RequestHeader HttpHeaders headers,
                       @Valid @RequestBody BeerDto beerDto) {
        try {
            var beer = modelMapper.fromDto(beerDto, id);
            var user = authenticationHelper.tryGetUser(headers);
            service.update(beer, user);
            return beer;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        try {
            var user = authenticationHelper.tryGetUser(headers);
            service.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/{id}/rating")
    public void rate(@PathVariable int id,
                     @RequestBody RatingDto dto,
                     @RequestHeader HttpHeaders headers) {
        User user;
        var beer = getById(id);
        double rating = dto.getRating();

        try {
            user = authenticationHelper.tryGetUser(headers);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

        ratingService.rate(beer, user, rating);
    }

    @GetMapping("/{id}/rating")
    public Double averageRating(@PathVariable int id) {
        var beer = getById(id);
        var rating = ratingService.getAverageRating(beer);

        if (rating == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "The given beer has no ratings yet!");
        }

        return rating;
    }

}
