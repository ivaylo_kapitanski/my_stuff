package com.telerikacademy.web.springdemo.services;

import com.telerikacademy.web.springdemo.models.Beer;
import com.telerikacademy.web.springdemo.models.User;

public interface RatingService {
    void rate(Beer beer, User user, double rating);

    Double getAverageRating(Beer beer);
}
