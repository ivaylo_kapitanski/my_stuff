package com.telerikacademy.web.springdemo.repositories;

import com.telerikacademy.web.springdemo.exceptions.EntityNotFoundException;
import com.telerikacademy.web.springdemo.models.Beer;
import com.telerikacademy.web.springdemo.models.Rating;
import com.telerikacademy.web.springdemo.models.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class RatingRepositoryImpl implements RatingRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public RatingRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(Rating rating) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(rating);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(Rating rating) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(rating);
            session.getTransaction().commit();
        }
    }

    @Override
    public Rating getByUserAndBeer(User user, Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            var query = session.createQuery("from Rating " +
                    "where user = :user and beer = :beer", Rating.class);

            query.setParameter("user", user);
            query.setParameter("beer", beer);

            var result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException(
                        String.format("Rating with user %s and beer %s not found",
                                user.getUsername(), beer.getName()));
            }

            return result.get(0);
        }
    }

    @Override
    public Double getAverage(Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            var query = session.createQuery("select round(avg(rating), 2) " +
                    "from Rating where beer = :beer", Double.class);
            query.setParameter("beer", beer);

            return query.uniqueResult();
        }
    }
}
