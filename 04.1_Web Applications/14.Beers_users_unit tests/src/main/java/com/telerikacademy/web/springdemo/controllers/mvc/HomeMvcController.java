package com.telerikacademy.web.springdemo.controllers.mvc;

import com.telerikacademy.web.springdemo.models.User;
import com.telerikacademy.web.springdemo.services.BeerService;
import com.telerikacademy.web.springdemo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class HomeMvcController {
  private final String currentUser;
  private final UserService userService;
  private final BeerService beerService;

  @Autowired
    public HomeMvcController(String currentUser,
                             UserService userService,
                             BeerService beerService) {
        this.currentUser = currentUser;
        this.userService = userService;
        this.beerService = beerService;
    }

    @GetMapping
    public String showHomePage(Model model) {
        model.addAttribute("topBeers",beerService.getTopBeers());
        if(currentUser.equals("anon")){
            model.addAttribute("currentUser",null);
        }else{
            model.addAttribute("currentUser",userService.getByUsername(currentUser));
        }
        return "index";
    }
}
