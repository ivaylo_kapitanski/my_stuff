package com.telerikacademy.web.springdemo.repositories;

import com.telerikacademy.web.springdemo.models.Beer;
import com.telerikacademy.web.springdemo.models.Rating;
import com.telerikacademy.web.springdemo.models.User;

public interface RatingRepository {

    void update(Rating rating);

    void create(Rating rating);

    Rating getByUserAndBeer(User user, Beer beer);

    Double getAverage(Beer beer);
}
