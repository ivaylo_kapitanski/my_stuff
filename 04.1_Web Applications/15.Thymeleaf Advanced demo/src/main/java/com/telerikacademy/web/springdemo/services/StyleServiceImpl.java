package com.telerikacademy.web.springdemo.services;

import com.telerikacademy.web.springdemo.models.Style;
import com.telerikacademy.web.springdemo.repositories.StyleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class StyleServiceImpl implements StyleService{
    private final StyleRepository styleRepo;

    @Autowired
    public StyleServiceImpl(StyleRepository styleRepo) {
        this.styleRepo = styleRepo;
    }




    @Override
    public List<Style> getAll() {
        return styleRepo.getAll();
    }
}
