package com.telerikacademy.web.springdemo.services;

import com.telerikacademy.web.springdemo.models.Style;

import java.util.List;

public interface StyleService {
    List<Style> getAll();
}
