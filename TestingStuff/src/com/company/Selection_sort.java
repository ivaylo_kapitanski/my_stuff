package com.company;

import java.util.Arrays;

public class Selection_sort {
    public static void main(String[] args) {

        //Selection sort
        int[] list = {1, 5, 3, 0, 4, 11, 555, 33, 55, 777};
       selectionSeort(list);
        System.out.println(Arrays.toString(list));
    }
private static void selectionSeort(int[] list){
    for (int i = 0; i <list.length ; i++) {
        for (int j = i+1; j < list.length ; j++) {
            if(list[j]<list[i]){
                int t=list[i];
                list[i]=list[j];
                list[j]=t;
            }
        }
    }
}
}
