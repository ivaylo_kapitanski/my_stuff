package com.company;

public class Recursion {
    public static void main(String[] args) {

        // printNumbers(10);
        //System.out.println(fact(5));
       // System.out.println(reverse("ivo"));
       // System.out.println(power(2,20));
        System.out.println(isPalindrome("madam"));
    }


    private static boolean isPalindrome(String str){
        if(str.length()<=1){
            return true;
        }
        if(str.charAt(0)!=str.charAt(str.length()-1)){
            return false;
        }
      return isPalindrome(str.substring(1,str.length()-1));

    }
    private static int power(int num, int pow){
        if(pow==0){
            return 1;
        }

        return num*power(num,pow-1);

    }

    private static String reverse(String input) {
        if (input.length() == 0) {
            return input;
        }

        return reverse(input.substring(1)) + input.charAt(0);

    }

    private static int fact(int num) {
        if (num <= 1) {
            return 1;
        }

        return num * fact(num - 1);
    }


    private static void printNumbers(int num) {
        if (num == 0) {
            return;
        }
        //down the stack recursion
        System.out.println(num);
        printNumbers(num - 1);

        //up the stack recursion
        printNumbers(num - 1);
        System.out.println(num);
    }


}
