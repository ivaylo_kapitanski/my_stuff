package com.company;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Searching {
    public static void main(String[] args) {
        List<Integer> list1 = new ArrayList<>(List.of(1, 15, 3, 22, 18, 20, 33, 100));
        list1.sort(Comparator.naturalOrder());
        System.out.println(biNarySearch(list1,33));


    }
    private static boolean biNarySearch(List<Integer> list, int target){
      return   binarySearchRec(list,0,target,list.size()-1);
    }

    private static boolean binarySearchRec(List<Integer> list, int start, int target, int end) {
        if (start > end) {
            return false;
        }
        int middle=(start+end)/2;
        if(list.get(middle)==target){
            return true;
        }
        if(target<list.get(middle)){
            return binarySearchRec(list, start,target,middle-1);
        }
        return binarySearchRec(list,middle+1,target,end);
    }

}
