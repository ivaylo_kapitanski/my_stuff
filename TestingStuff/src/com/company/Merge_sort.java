package com.company;

import java.util.Arrays;

public class Merge_sort {
    public static void main(String[] args) {
        int[] list = {1, 5, 3, 0, 4, 11, 555, 33, 55, 777};
        int size=list.length;
        sort(list,size);
        System.out.println(Arrays.toString(list));
    }
    private static void sort(int[] list, int size){
        if(size<2){
            return;
        }

        int middle=size/2;

        int[] left=new int[middle];
        int[]right=new int[size-middle];

        for (int i = 0; i <middle ; i++) {
            left[i]=list[i];
        }
        for (int i = middle; i < size ; i++) {
            right[i-middle]=list[i];
        }
        sort(left,middle);
        sort(right,size-middle);
        merge(list,left,right,middle,size-middle);

    }
    private static void merge(int[] list, int[] left,int[]right, int leftEnd, int rightEnd){
        int lCounter=0;
        int rCounter=0;
        int listCounter=0;

        while (lCounter<leftEnd&&rCounter<rightEnd){
            if(left[lCounter]<=right[rCounter]){
                list[lCounter++]=left[lCounter++];
            }else{
                list[lCounter++]=right[rCounter++];
            }
        }
        while (lCounter<leftEnd){
            list[listCounter++]=left[lCounter++];
        }
        while (rCounter<rightEnd){
            list[listCounter++]=right[rCounter++];
        }

    }
}
