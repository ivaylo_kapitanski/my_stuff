package com.company;

import java.util.concurrent.atomic.AtomicInteger;

public class CreatorOfIDs {

    private static AtomicInteger idCounter=new AtomicInteger();

    public static String createID(String itemType){
        return String.format("%s_%s",itemType, idCounter.getAndIncrement()+1);
    }
}
