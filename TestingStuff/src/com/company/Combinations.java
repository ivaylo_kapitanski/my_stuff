package com.company;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

public class Combinations {

        public static void main(String[] args) {
            ArrayList<Integer> list = new ArrayList<>(Arrays.asList(2,4,8)); // Create an ArrayList
            int start=0;
            int target=10;
            System.out.println(list);

            int[] currentSum=new int[1];
            System.out.println(combinations(list,target,currentSum));

        }

        public static boolean combinations(ArrayList<Integer> list,int target,int[] currentSum) {

            if(currentSum[0]==target){
                return true;
            }

            if (list.size() == 0) {
                currentSum[0]=0;
                return false;
            }

            ArrayList<ArrayList<String>> result = new ArrayList();
            int firstEl = list.get(0);
            ArrayList<Integer> rest = new ArrayList<>(list.subList(1, list.size()));

            ArrayList<Integer> withoutFirst = combinations(rest,target,currentSum);

            for (ArrayList<String> comb : withoutFirst) {
                result.add(comb);
                ArrayList<String> withFirst = (ArrayList<String>) comb.clone();
                withFirst.add(firstEl);
                result.add(withFirst);
            }

            return result;
        }
    }

