package com.company;

import java.util.Arrays;

public class Bubble_sort {
    public static void main(String[] args) {
        int[] list = {1, 5, 3, 0, 4, 11, 555, 33, 55, 777};
        bubbleSort(list);
        System.out.println(Arrays.toString(list));
    }
    private static void bubbleSort(int[] list){
        boolean inOrder=false;

        while(!inOrder){
            inOrder=true;
            for (int i = 0; i < list.length-1 ; i++) {
                if(list[i]>list[i+1]){
                    int t=list[i];
                    list[i]=list[i+1];
                    list[i+1]=t;
                    inOrder=false;
                }

            }
        }


    }

}
