package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        //SomeCommand Title {do something here} Description {steps to take}

        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        String commandName = input.split(" ")[0];
        List<String> parameters = new ArrayList<>();

        List<String> inputList = Arrays.asList(input.split(" "));

        for (int i = 1; i < inputList.size(); ) {
            int counter = i;
            if (!inputList.get(i).contains("{")) {
                parameters.add(inputList.get(i));
                i++;
            } else {
                StringBuilder stringBuilder = new StringBuilder();
                do {
                    stringBuilder.append(inputList.get(counter));
                    stringBuilder.append(" ");
                    counter++;
                }while (!inputList.get(counter-1).contains("}"));

                parameters.add(stringBuilder.substring(1,stringBuilder.length()-2));
                i = counter;
            }
        }
        System.out.println(parameters);
    }

}
