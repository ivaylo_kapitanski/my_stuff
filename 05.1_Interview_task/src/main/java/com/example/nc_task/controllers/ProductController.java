package com.example.nc_task.controllers;

import com.example.nc_task.dtos.ProductDto;
import com.example.nc_task.exceptions.DuplicateEntityException;
import com.example.nc_task.exceptions.EntityNotFoundException;
import com.example.nc_task.mappers.ProductModelMapper;
import com.example.nc_task.models.Product;
import com.example.nc_task.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;

@RestController
@RequestMapping("/api/v1/products")
public class ProductController {

    private final ProductModelMapper productModelMapper;
    private final ProductService productService;

    @Autowired
    public ProductController(ProductModelMapper productModelMapper, ProductService productService) {
        this.productModelMapper = productModelMapper;
        this.productService = productService;
    }

    @PostMapping
    public Product create(@RequestBody ProductDto productDto) throws IOException, ClassNotFoundException {

        try{
            Product product = productModelMapper.fromDto(productDto);
            return  productService.createProduct(product);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (DuplicateEntityException ex){
            throw new ResponseStatusException(HttpStatus.CONFLICT, ex.getMessage());
        }

    }
}
