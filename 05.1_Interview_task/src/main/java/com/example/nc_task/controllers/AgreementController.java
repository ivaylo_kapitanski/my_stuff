package com.example.nc_task.controllers;

import com.example.nc_task.dtos.AgreementDto;
import com.example.nc_task.exceptions.DuplicateEntityException;
import com.example.nc_task.exceptions.EntityNotFoundException;
import com.example.nc_task.mappers.AgreementModelMapper;
import com.example.nc_task.models.Agreement;
import com.example.nc_task.services.AgreementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;

@RestController
@RequestMapping("/api/v1/agreements")
public class AgreementController {

    private final AgreementModelMapper agreementModelMapper;
    private final AgreementService agreementService;

    @Autowired
    public AgreementController(AgreementModelMapper agreementModelMapper, AgreementService agreementService) {
        this.agreementModelMapper = agreementModelMapper;
        this.agreementService = agreementService;
    }

    @PostMapping
    public Agreement create(@RequestBody AgreementDto agreementDto) throws IOException {
        try {
            Agreement agreement = agreementModelMapper.fromDto(agreementDto);
            return agreementService.createAgreement(agreement, agreementDto.getFileName());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException ex) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, ex.getMessage());
        }

    }

    @GetMapping
    public Agreement findByPath(@RequestParam String path) {
        try {
            return agreementService.getAgreement(path);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
