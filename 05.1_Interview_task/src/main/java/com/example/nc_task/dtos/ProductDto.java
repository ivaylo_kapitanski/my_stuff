package com.example.nc_task.dtos;

public class ProductDto {

    private String parent;

    private String productName;

    private String childProduct;

    private Double price;

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getChildProduct() {
        return childProduct;
    }

    public void setChildProduct(String childProduct) {
        this.childProduct = childProduct;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
