package com.example.nc_task.models;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.io.Serializable;
import java.util.Set;


@XStreamAlias("agreement")
public class Agreement implements Serializable {

    @XStreamAlias("agreementName")
    private String name;

    @XStreamAlias("signedBy")
    private String signedBy;

    @XStreamImplicit(itemFieldName = "childProducts")
    private Set<Product> productSet;

    public Agreement() {
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSignedBy() {
        return signedBy;
    }

    public void setSignedBy(String signedBy) {
        this.signedBy = signedBy;
    }

    public Set<Product> getProductSet() {
        return productSet;
    }

    public void setProductSet(Set<Product> productSet) {
        this.productSet = productSet;
    }

    @Override
    public String toString() {
        return "Agreement{" +
                "name='" + name + '\'' +
                ", signedBy='" + signedBy + '\'' +
                ", productSet=" + productSet +
                '}';
    }
}
