package com.example.nc_task.models;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.io.Serializable;
import java.util.Set;

@XStreamAlias("product")
public class Product implements Serializable {

    @XStreamAlias("parent")
    private String parentObject;

    @XStreamAlias("productName")
    private String productName;

    @XStreamImplicit(itemFieldName="childProducts")
    private Set<Product> childrenProducts;
    @XStreamAlias("price")
    private double price;

    public String getParentObject() {
        return parentObject;
    }

    public void setParentObject(String parentObject) {
        this.parentObject = parentObject;
    }

    public Set<Product> getChildrenProducts() {
        return childrenProducts;
    }

    public void setChildrenProducts(Set<Product> childrenProducts) {
        this.childrenProducts = childrenProducts;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product{" +
                "parentObject=" + parentObject +
                ", productName='" + productName + '\'' +
                ", childrenProducts=" + childrenProducts +
                ", price=" + price +
                '}';
    }
}
