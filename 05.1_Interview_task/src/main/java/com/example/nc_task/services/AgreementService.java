package com.example.nc_task.services;

import com.example.nc_task.models.Agreement;
import com.example.nc_task.models.Product;

import java.io.IOException;
import java.util.Set;


public interface AgreementService {

    Agreement createAgreement(Agreement agreement,String fileName) throws IOException;

    Agreement getAgreement(String path);

    void checkAndAddProductsToSet(Set<Product> productsToAdd, String productList);
}
