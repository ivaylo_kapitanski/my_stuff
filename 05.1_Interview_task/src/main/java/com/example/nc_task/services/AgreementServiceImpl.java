package com.example.nc_task.services;

import com.example.nc_task.exceptions.DuplicateEntityException;
import com.example.nc_task.exceptions.EntityNotFoundException;
import com.example.nc_task.models.Agreement;
import com.example.nc_task.models.Product;
import com.thoughtworks.xstream.XStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.Set;

@Service
public class AgreementServiceImpl implements AgreementService {
    private final ProductService productService;

    @Autowired
    public AgreementServiceImpl(ProductService productService) {
        this.productService = productService;
    }

    @Override
    public Agreement createAgreement(Agreement agreement, String fileName) throws IOException {
        checkForDuplicates(fileName);
        XStream xstream = new XStream();
        xstream.autodetectAnnotations(true);
        xstream.alias("agreement", Agreement.class);
        String xml = xstream.toXML(agreement);
        BufferedReader reader = new BufferedReader(new StringReader(xml));
        BufferedWriter writer = new BufferedWriter(new FileWriter(fileName+ ".xml",
                true));

        while ((xml = reader.readLine()) != null) {
            writer.write(xml + System.getProperty("line.separator"));
        }
        writer.close();
        return agreement;
    }

    @Override
    public Agreement getAgreement(String path) {

        Agreement newAgreement;
        XStream xstream = new XStream();
        xstream.alias("agreement", Agreement.class);
        xstream.autodetectAnnotations(true);
        try {
            File xmlFile = new File(path + ".xml");
            newAgreement = (Agreement) xstream.fromXML(new FileInputStream(xmlFile));
        } catch (Exception e) {
            throw new EntityNotFoundException("File not found");
        }
        return newAgreement;

    }

    public void checkAndAddProductsToSet(Set<Product> productsToAdd, String productList) {

        String[] products = productList.split(", ");
        for (String product : products) {
            try {
                Product productToAdd = productService.getProduct(product);
                if (!productToAdd.getParentObject().contains("Agreement")) {
                    continue;
                }
                productsToAdd.add(productToAdd);
            } catch (IOException | ClassNotFoundException e) {
                System.out.println(e.getMessage());
            }catch (EntityNotFoundException e){
                throw new EntityNotFoundException(e.getMessage());
            }
        }
    }

    private void checkForDuplicates(String fileName) {
        XStream xstream = new XStream();
        xstream.alias("product", Product.class);
        boolean isAnotherPresent = true;
        try {
            getAgreement(fileName);
        } catch (EntityNotFoundException e) {
            isAnotherPresent = false;
        }
        if (isAnotherPresent) {
            throw new DuplicateEntityException("File with such name already exists");
        }
    }
}
