package com.example.nc_task.services;

import com.example.nc_task.exceptions.DuplicateEntityException;
import com.example.nc_task.exceptions.EntityNotFoundException;
import com.example.nc_task.models.Product;
import com.thoughtworks.xstream.XStream;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Set;

@Service
public class ProductServiceImpl implements ProductService {


    @Override
    public Product createProduct(Product product) throws IOException {
        checkForDuplicates(product.getProductName());
        if (product.getChildrenProducts().size() > 0 || product.getChildrenProducts() != null) {
            for (Product product1 : product.getChildrenProducts()) {
                try {
                    updateProduct(product1, product.getProductName());
                } catch (EntityNotFoundException e) {
                    throw new EntityNotFoundException(e.getMessage());
                }
            }
        }
        createFileFromObject(product);
        return product;
    }



    @Override
    public Product getProduct(String productName) {
        XStream xstream = new XStream();
        Product newProduct;
        xstream.alias("product", Product.class);

        try {
            File xmlFile = new File(productName + ".xml");
            newProduct = (Product) xstream.fromXML(xmlFile);
        } catch (Exception e) {
            throw new EntityNotFoundException("File not found");
        }
        return newProduct;

    }

    @Override
    public void addProductsToSet(Set<Product> productsToAdd, String productList) {
        if(!productList.isEmpty()){
        String[] products = productList.split(", ");
            for (String product : products) {
                productsToAdd.add(getProduct(product));
            }
        }
    }

    private void createFileFromObject(Product product) throws IOException {
        XStream xstream = new XStream();
        String xml = xstream.toXML(product);
        xstream.autodetectAnnotations(true);
        xstream.alias("product", Product.class);
        BufferedReader reader = new BufferedReader(new StringReader(xml));
        BufferedWriter writer = new BufferedWriter(new FileWriter(product.getProductName() + ".xml",
                true));

        while ((xml = reader.readLine()) != null) {
            writer.write(xml + System.getProperty("line.separator"));
        }
        writer.close();
    }


    private void updateProduct(Product product, String parent) throws IOException {

        product.setParentObject(parent);
        try {
            getProduct(product.getProductName());
            Files.deleteIfExists(Path.of(product.getProductName() + ".xml"));
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
        createFileFromObject(product);
    }
    private void checkForDuplicates(String fileName) {
        XStream xstream = new XStream();
        xstream.alias("product", Product.class);
        boolean isAnotherPresent = true;
        try {
            getProduct(fileName);
        } catch (EntityNotFoundException e) {
            isAnotherPresent = false;
        }
        if (isAnotherPresent) {
            throw new DuplicateEntityException("File with such name already exists");
        }
    }


}