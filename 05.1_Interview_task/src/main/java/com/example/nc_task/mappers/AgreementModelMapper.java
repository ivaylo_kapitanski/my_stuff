package com.example.nc_task.mappers;

import com.example.nc_task.dtos.AgreementDto;
import com.example.nc_task.models.Agreement;
import com.example.nc_task.models.Product;
import com.example.nc_task.services.AgreementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;

@Component
public class AgreementModelMapper {
    private final AgreementService agreementService;

    @Autowired
    public AgreementModelMapper(AgreementService agreementService) {
        this.agreementService = agreementService;
    }

    public Agreement fromDto(AgreementDto agreementDto) {
        Agreement agreement = new Agreement();
        dtoToObject(agreement, agreementDto);
        return agreement;
    }

    private void dtoToObject(Agreement agreement, AgreementDto agreementDto) {
        LocalDate now = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        String formatDateTime = now.format(formatter);
        agreement.setName("Agreement " + formatDateTime);
        agreement.setSignedBy(agreementDto.getSignedBy());
        Set<Product> productSet = new HashSet<>();
        agreementService.checkAndAddProductsToSet(productSet, agreementDto.getProducts());
        agreement.setProductSet(productSet);


    }
}
