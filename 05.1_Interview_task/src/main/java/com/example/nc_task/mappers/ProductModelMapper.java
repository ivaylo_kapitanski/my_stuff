package com.example.nc_task.mappers;

import com.example.nc_task.dtos.ProductDto;
import com.example.nc_task.models.Product;
import com.example.nc_task.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

@Component
public class ProductModelMapper {
    private final ProductService productService;

    @Autowired
    public ProductModelMapper(ProductService productService) {
        this.productService = productService;
    }

    public Product fromDto(ProductDto productDto) throws IOException, ClassNotFoundException {
        Product product = new Product();
        dtoToObject(product, productDto);
        return product;
    }

    private void dtoToObject(Product product, ProductDto productDto) throws IOException, ClassNotFoundException {
        product.setProductName(productDto.getProductName());
        product.setParentObject(productDto.getParent());
        product.setPrice(productDto.getPrice());
        Set<Product> productSet = new HashSet<>();
        productService.addProductsToSet(productSet, productDto.getChildProduct());
        product.setChildrenProducts(productSet);

    }
}
