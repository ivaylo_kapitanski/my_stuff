package com.example.nc_task;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NcTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(NcTaskApplication.class, args);
    }

}
