use
telerikacademy;

-- 1. Create query to update column transferred_to to ‘Invalid_destination’ in Storage table if
-- transferred_to column is equal 100.

update storage
set transfered_to='Invalid_destination'
where transfered_to = 100;

-- 2. Create query to select all products of Dell manufacturer.
select*
from storage s
         join product p on s.model = p.model
where p.maker = 'Dell';

-- 3. Create query to delete rows from Storage table that have value ‘Invalid_destination’ in
-- transferred_to column.

delete
from storage
where transfered_to = 'Invalid_destination';

-- 4. Create query to select most expensive product for every maker. Output should have maker,
-- product model and price.


select table1.maker, table1.model, table1.price as MaxPrice
from (
         select product.maker, printer.model, printer.price
         from printer
                  join product on printer.model = product.model
         union all
         select product.maker, laptop.model, laptop.price
         from laptop
                  join product on laptop.model = product.model
         union all
         select product.maker, pc.model, pc.price
         from pc
                  join product on pc.model = product.model
     ) table1
         inner join(select maker, max(price) as MaxValue
                    from (
                             select product.maker, printer.model, printer.price
                             from printer
                                      join product on printer.model = product.model
                             union all
                             select product.maker, laptop.model, laptop.price
                             from laptop
                                      join product on laptop.model = product.model
                             union all
                             select product.maker, pc.model, pc.price
                             from pc
                                      join product on pc.model = product.model
                         ) t1
                    group by maker) a on a.maker = table1.maker and a.MaxValue = table1.price;

-- 5. Create query to select overall amount of laptops that Hitachi manufactures.
select count(model)
from product
where maker = 'Hitachi'
  and type = 'laptop';
-- 6. Create query to select all devices ordered from most expensive ones to cheapest. Output should
-- have serial number, maker, price, type.

select*
from (select printer.serial_number, product.maker, printer.price, product.type
      from printer
               join product on printer.model = product.model

      union all

      select laptop.serial_number, product.maker, laptop.price, product.type
      from laptop
               join product on laptop.model = product.model

      union all

      select pc.serial_number, product.maker, pc.price, product.type
      from pc
               join product on pc.model = product.model
     )
order by price;

-- 7. Create query to select makers that make more printers than pc\laptops.
select maker
from product
group by maker
having count(Case type when 'printer' then 1 else null end) >
       count(Case type when 'pc' then 1 else null end) + count(Case type when 'laptop' then 1 else null end);
-- 8. Create query to select most expensive PC without CD drive for makers who manufacture only
-- PCs. Output should have serial_number, maker, price.
SELECT pc.serial_number, product.maker, pc.price
FROM pc
         JOIN product on product.model = pc.model
where pc.price = (
    select max(pc.price)
    from pc
             join product on product.model = pc.model
    WHERE maker in (SELECT maker
                    FROM product
                    WHERE type = 'pc'
                      AND maker NOT IN
                          (SELECT maker FROM product WHERE type = 'laptop')
                      AND maker NOT IN
                          (SELECT maker FROM product WHERE type = 'printer'))
      AND pc.cd is null);

-- 9. Create query to select what's cheaper pc or laptop (considering speed, ram, hd).
-- If maker doesn't manufacture pc then laptop should be shown as cheapest and vice versa. As input you have speed, ram, hd.
-- as output you should show at least maker, serial_number, speed, ram, hd, price.

-- I had trouble understanding what exactly should the query show
-- so given the description above I have created the query to search for the cheapest possible option from with
-- inputted parameters
-- and show all matching laptops/pc-s with the now determined min price.
select product.maker,
       product.type,
       newGroup.serial_number,
       newGroup.speed,
       newGroup.ram,
       newGroup.hd,
       newGroup.price
from (
         select*
         from laptop
         union all
         select*
         from pc) newGroup
         join product on newGroup.model = product.model

where newGroup.price = (select min(group2.price)
                        from (
                                 select*
                                 from laptop
                                 union all
                                 select*
                                 from pc) group2
                        where group2.speed = 800
                          and group2.ram = 4096
                          and group2.hd = 600);