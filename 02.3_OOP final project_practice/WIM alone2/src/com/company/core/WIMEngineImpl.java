package com.company.core;

import com.company.commands.Command;
import com.company.core.contracts.*;
import com.company.core.factories.CommandFactoryImpl;
import com.company.core.factories.WIMFactoryImpl;
import com.company.core.providers.CommandParserImpl;
import com.company.core.providers.ConsoleReader;
import com.company.core.providers.ConsoleWriter;

import java.util.List;

public class WIMEngineImpl implements Engine {
    private static final String TERMINATION_COMMAND = "Exit";
    private static final String ERROR_EMPTY_COMMAND = "Command cannot be null or empty.";

    private final WIMFactory wimFactory;
    private final CommandParser commandParser;
    private final WIMRepository wimRepository;
    private final Reader reader;
    private final Writer writer;
    private final CommandFactory commandFactory;

    public WIMEngineImpl() {
        wimFactory = new WIMFactoryImpl();
        commandParser = new CommandParserImpl();
        wimRepository = new WIMRepositoryImpl();
        reader = new ConsoleReader();
        writer = new ConsoleWriter();
        commandFactory = new CommandFactoryImpl();
    }

    @Override
    public void start() {
        while (true) {
            try{
                String commandAsString = reader.readLine();
                if(commandAsString.equalsIgnoreCase(TERMINATION_COMMAND))
                    break;
                processCommand(commandAsString);
            } catch (Exception ex){
                writer.writeLine(ex.getMessage() != null && !ex.getMessage().isEmpty() ? ex.getMessage() : ex.toString());
            }
        }
    }

    private void processCommand(String commandAsString) {
        if (commandAsString == null || commandAsString.trim().equals("")) {
            throw new IllegalArgumentException(ERROR_EMPTY_COMMAND);
        }
        String commandName = commandParser.parseCommand(commandAsString);
        Command command = commandFactory.createCommand(commandName, wimFactory, wimRepository);
        List<String> parameters = commandParser.parseParameters(commandAsString);
        String executionResult = command.execute(parameters);
        writer.writeLine(executionResult);
    }
}
