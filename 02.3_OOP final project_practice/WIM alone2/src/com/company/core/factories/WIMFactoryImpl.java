package com.company.core.factories;

import com.company.core.contracts.WIMFactory;
import com.company.enums.BugStatus;
import com.company.enums.Severity;
import com.company.models.Bug;
import com.company.models.BugImpl;
import com.company.models.Feedback;
import com.company.models.FeedbackImpl;
import com.company.teams.*;

import java.util.List;

public class WIMFactoryImpl implements WIMFactory {
    @Override
    public Team createTeam(String name) {
        return new TeamImpl(name);
    }

    @Override
    public Board createBoard(String name) {
        return new BoardImpl(name);
    }

    @Override
    public Member createMembers(String name) {
        return new MemberImpl(name);
    }

    @Override
    public Bug createBug(String title, String description,List<Member> assignee, Severity severity, BugStatus bugStatus) {
        return new BugImpl(title,description,assignee,severity,bugStatus);
    }

//    @Override
//    public Story createStory(String title, String description, String id) {
//        return new StoryImpl(title, description,id);
//    }
//
    @Override
    public Feedback createFeedback(String title, String description, int rating) {
        return new FeedbackImpl(title, description,rating);
    }
}
