package com.company.core.factories;

import com.company.commands.*;
import com.company.core.contracts.CommandFactory;
import com.company.core.contracts.WIMFactory;
import com.company.core.contracts.WIMRepository;

public class CommandFactoryImpl implements CommandFactory {
    private static final String INVALID_COMMAND = "Invalid command name: %s!";

    @Override
    public Command createCommand(String commandTypeAsString, WIMFactory factory, WIMRepository repository) {

        CommandType commandType = CommandType.valueOf(commandTypeAsString.toUpperCase());
        switch (commandType) {
//            case LISTBUGSONLY:
//                return new ListBugsOnly(repository);
//
//            case LISTSTORIESONLY:
//                return new ListStoriesOnly(repository);
//
//            case LISTALLWORKITEMS:
//                return new ListAllWorkItems(repository);
//
//            case LISTFEEDBACKONLY:
//                return new ListFeedbackOnly(repository);
//
            case CREATEBOARDINTEAM:
                return new CreateBoardInTeam(factory, repository);

//            case CREATEBUGINBOARD:
//                return new CreateBugInBoard(factory, repository);
//
            case CREATEFEEDBACKINBOARD:
                return new CreateFeedBackInBoard(factory, repository);

//            case CREATEPERSON:
//                return new CreatePerson(factory, repository);
//
            case CREATETEAM:
                return new CreateTeam(factory, repository);

//            case CREATESTORYINBOARD:
//                return new CreateStoryInBoard(factory, repository);
//
//            case ADDCOMMENTTOWORKITEM:
//                return new AddCommentToWorkItem(factory, repository);
//
            case ADDPERSONTOTEAM:
                return new AddPersonToTeam(factory, repository);

//            case ASSIGNITEMTOPERSON:
//                return new AssignItemToPerson(factory, repository);

//            case UNASSIGNITEMTOPERSON:
//                return new UnassignItemToPerson(factory, repository);
//
//            case CHANGEPRIORITYOFBUG:
//                return new ChangePriorityOfBug(factory, repository);
//
//            case CHANGEPRIORITYOFSTORY:
//                return new ChangePriorityOfStory(factory, repository);
//
//            case CHANGERATINGOFFEEDBACK:
//                return new ChangeRatingOfFeedback(factory, repository);
//
//            case CHANGESEVERITYOFBUG:
//                return new ChangeSevirityOfBug(factory, repository);
//
//            case CHANGESIZEOFSTORY:
//                return new ChangeSizeOfStory(factory, repository);
//
//            case CHANGESTATUSOFBUG:
//                return new ChangeStatusOfBug(factory, repository);
//
//            case CHANGESTATUSOFFEEBDACK:
//                return new ChangeStatusOfFeedback(factory, repository);
//
//            case CHANGESTATUSOFSTORY:
//                return new ChangeStatusOfStory(factory, repository);
//
//            case FILTERITEMSBYASSIGNEE:
//                return new FilterItemsByAssignee(repository);
//
//            case FILTERITEMSBYSTATUS:
//                return new FilterItemsByStatus(repository);
//
//            case FILTERITEMSBYSTATUSANDASSIGNEE:
//                return new FilterItemsByStatusAndAssignee(repository);
//
//            case SORTITEMSBYPRIORITY:
//                return new SortItemsByPriority(repository);
//
//            case SORTITEMSBYSIZE:
//                return new SortItemsBySize(repository);
//
//            case SORTITEMSBYTITLE:
//                return new SortItemsByTitle(repository);
//
//            case SORTITEMSBYSEVERITY:
//                return new SortItemsBySeverity(repository);
//
//            case SORTITEMSBYRATING:
//                return new SortItemsByRating(repository);
//
            case SHOWALLPEOPLE:
                return new ShowAllPeople(repository);

//            case SHOWALLTEAMBOARDS:
//                return new ShowAllTeamBoards(repository);
//
            case SHOWALLTEAMMEMBERS:
                return new ShowAllTeamMembers(repository);

            case SHOWALLTEAMS:
                return new ShowAllTeams(repository);

//            case SHOWPERSONACTIVITY:
//                return new ShowPersonActivity(repository);
//
//            case SHOWBOARDACTIVITY:
//                return new ShowBoardActivity(repository);
        }
        throw new IllegalArgumentException(String.format(INVALID_COMMAND, commandTypeAsString));
    }
}
