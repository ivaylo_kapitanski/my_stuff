package com.company.core;

import com.company.core.contracts.WIMRepository;
import com.company.models.*;
import com.company.teams.Board;
import com.company.teams.Member;
import com.company.teams.Team;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WIMRepositoryImpl implements WIMRepository {
    List<Team> teams;
    List<Board> boards;
    List<Member> members;
    List<Bug> bugs;
    List<Story>stories;
    List<Feedback>feedbacks;
    List<Workitem> workItems;
    List<Assignable> assignable;

    public WIMRepositoryImpl() {
        this.teams = new ArrayList<>();
        this.boards = new ArrayList<>();
        this.workItems = new ArrayList<>();
        this.assignable = new ArrayList<>();
        this.members=new ArrayList<>();
        this.bugs=new ArrayList<>();
        this.feedbacks=new ArrayList<>();
        this.stories=new ArrayList<>();
    }

    @Override
    public List<Team> getAllTeams() {
        return new ArrayList<>(teams);
    }

    @Override
    public List<Board> getAllBoards() {
        return new ArrayList<>(boards);
    }

    @Override
    public List<Member> getAllMembers() {
        return new ArrayList<>(members);
    }

    @Override
    public List<Workitem> getAllWorkItems() {
       // var result=new ArrayList<>(workItems);
     //   result.addAll(getAllFeedbacks())
        return new ArrayList<>(workItems);
    }

    @Override
    public List<Assignable> getAllAssignable() {
        var result=new ArrayList<>(assignable);
        result.addAll(getAllBugs());
        result.addAll(getAllStories());

        return result;
    }

    @Override
    public List<Bug> getAllBugs() {
        return new ArrayList<>(bugs);
    }

    @Override
    public List<Story> getAllStories() {
        return new ArrayList<>(stories);
    }

    @Override
    public List<Feedback> getAllFeedbacks() {
        return new ArrayList<>(feedbacks);
    }

    @Override
    public void addTeam(Team team) {
        this.teams.add(team);
    }

    @Override
    public void addBoard(Board board) {
    this.boards.add(board);
    }

    @Override
    public void addMember(Member member) {
    this.members.add(member);
    }

    @Override
    public void addFeedback(Feedback feedback) {
    feedbacks.add(feedback);
    }

    @Override
    public void addWorkItem(Workitem workItem) {
    workItems.add(workItem);
    }


}
