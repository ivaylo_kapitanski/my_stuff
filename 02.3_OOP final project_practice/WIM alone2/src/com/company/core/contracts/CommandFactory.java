package com.company.core.contracts;

import com.company.commands.Command;

public interface CommandFactory {
    Command createCommand(String commandTypeAsString, WIMFactory factory, WIMRepository repository);
}
