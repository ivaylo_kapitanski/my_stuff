package com.company.core.contracts;

import com.company.models.*;
import com.company.teams.Board;
import com.company.teams.Member;
import com.company.teams.Team;

import java.util.List;
import java.util.Map;

public interface WIMRepository {
    List<Team> getAllTeams();
    List<Board> getAllBoards();
    List<Member> getAllMembers();
    List<Workitem> getAllWorkItems();
    List<Assignable> getAllAssignable();
    List<Bug> getAllBugs();
    List<Story> getAllStories();
    List<Feedback> getAllFeedbacks();

    void addTeam(Team team);
    void addBoard(Board board);
    void addMember(Member member);
    void addWorkItem(Workitem workItem);
    void addFeedback(Feedback feedback);
   // void addAssignable(Assignable assignable);

}
