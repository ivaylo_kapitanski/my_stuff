package com.company.core.contracts;

import com.company.enums.BugStatus;
import com.company.enums.Severity;
import com.company.models.Bug;
import com.company.models.Feedback;
import com.company.models.Story;
import com.company.teams.Board;
import com.company.teams.Member;
import com.company.teams.Team;

import java.util.List;

public interface WIMFactory {
    Team createTeam(String name);

    Board createBoard(String name);

    Member createMembers(String name);


    Bug createBug(String title, String description, List<Member> assignee, Severity severity, BugStatus bugStatus);

//    Story createStory(String title, String description, String id);
//
  Feedback createFeedback(String title, String description, int rating);
}
