package com.company.enums;

public enum StoryStatus {
    NOTDONE,
    INPROGRESS,
    DONE;
    @Override
    public String toString() {
        switch (this){
            case INPROGRESS:
                return "InProgress";
            case NOTDONE:
                return "NotDone";
            case DONE:
                return "Done";
            default:
                throw new IllegalArgumentException("Invalid option");
        }
    }
}
