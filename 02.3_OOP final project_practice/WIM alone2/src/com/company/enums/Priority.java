package com.company.enums;

public enum Priority {
    HIGH,
    MEDIUM,
    LOW;

    @Override
    public String toString() {
        switch(this){
            case LOW:
                return "Low";
            case HIGH:
                return "High";
            case MEDIUM:
                return "Medium";
            default:
                throw new IllegalArgumentException("Invalid priority option.");
        }
    }
}
