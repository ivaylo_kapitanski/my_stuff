package com.company.enums;

public enum Severity {
    CRITICAL,
    MAJOR,
    MINOR;

    @Override
    public String toString() {
        switch(this){
            case MAJOR:
                return "Major";
            case MINOR:
                return "Minor";
            case CRITICAL:
                return "Critical";
            default:
                throw new IllegalArgumentException("Invalid severity option.");
        }
    }
}
