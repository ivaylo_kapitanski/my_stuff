package com.company.utils;

import java.util.concurrent.atomic.AtomicInteger;

public class CreatorOfIDs{
    private static AtomicInteger idCounter=new AtomicInteger();

    public static String createID(String itemType){
        return String.format("%s_%s",itemType, idCounter.getAndIncrement()+1);
    }
}
