package com.company.utils;

public class Validator {
    public static void checkIfNull(Object input){
        if(input==null)
            throw new IllegalArgumentException("Input cannot be null.");

    }
    public static void checkLength(String input, int min, int max, String errorMessage){
        if(input.trim().length()<min|| input.trim().length()>max )
            throw new IllegalArgumentException(errorMessage);
    }
}
