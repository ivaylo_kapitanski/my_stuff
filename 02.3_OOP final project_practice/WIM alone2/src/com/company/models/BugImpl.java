package com.company.models;

import com.company.enums.BugStatus;
import com.company.enums.Severity;
import com.company.teams.Member;

import java.util.List;

public class BugImpl extends AssignableBase implements Bug{
    private Severity severity;
    private BugStatus bugStatus;


    public BugImpl(String title, String description, List<Member> assignee, Severity severity, BugStatus bugStatus) {
        super(title, description, assignee);
        setSeverity(severity);
       setBugStatus(bugStatus);
    }

    public Severity getSeverity() {
        return severity;
    }

    public void setSeverity(Severity severity) {
        this.severity = severity;
    }

    public BugStatus getBugStatus() {
        return bugStatus;
    }

    public void setBugStatus(BugStatus bugStatus) {
        this.bugStatus = bugStatus;
    }
}
