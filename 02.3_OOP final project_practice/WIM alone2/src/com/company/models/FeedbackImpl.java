package com.company.models;


import com.company.enums.FeedbackStatus;

import java.util.List;

public class FeedbackImpl extends WorkitemBase implements Feedback{

    private int rating;
    private FeedbackStatus feedbackStatus=FeedbackStatus.NEW;

    public FeedbackImpl(String title, String description, int rating) {
        super(title, description);
        setRating(rating);
        setFeedbackStatus(feedbackStatus);
    }

    public FeedbackStatus getFeedbackStatus() {
        return feedbackStatus;
    }

    public void setFeedbackStatus(FeedbackStatus feedbackStatus) {
        this.feedbackStatus = feedbackStatus;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        if (rating < 0 || rating > 5)
            throw new IllegalArgumentException("Rating must between 0 and 5.");
        this.rating = rating;
    }

    @Override
    public String additionalInfo() {
        return null;
    }


}
