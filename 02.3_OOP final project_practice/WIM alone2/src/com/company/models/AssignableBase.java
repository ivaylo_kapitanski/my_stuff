package com.company.models;

import com.company.enums.Priority;
import com.company.teams.Member;
import com.company.utils.Validator;

import java.util.ArrayList;
import java.util.List;

public abstract class AssignableBase extends WorkitemBase implements Assignable {
    private boolean isAssigned;
    private Priority priority;
    private List<Member> assignee;

    public AssignableBase(String title, String description, List<Member> assignee) {
        super(title, description);
        setPriority(priority);
        assignee=new ArrayList<>();
        setAssigned(isAssigned);
    }

    public void setAssigned(boolean assigned) {
        isAssigned = assignee.size() > 0;
    }

    @Override
    public List<Member> getAssignee() {
        return new ArrayList<>(assignee);
    }

    @Override
    public void addAssignee(Member newAssignee) {
        Validator.checkIfNull(assignee);
        assignee.add(newAssignee);
    }

    @Override
    public void removeAssignee(Member newAssignee){
        Validator.checkIfNull(assignee);
        assignee.remove(newAssignee);
    }
    @Override
    public Priority getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    @Override
    public String additionalInfo() {
        return String.format("Priority %s, Assignee%s", getPriority(), getAssignee());
    }
}
