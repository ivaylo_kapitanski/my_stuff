package com.company.models;

import java.util.List;

public interface Comments {
    List<Comments> getComments();
    void addComment(Comments commentToAdd);
}
