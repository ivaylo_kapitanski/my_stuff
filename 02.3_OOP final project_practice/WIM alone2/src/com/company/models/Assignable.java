package com.company.models;

import com.company.enums.Priority;
import com.company.teams.Member;

import java.util.List;

public interface Assignable {
    void removeAssignee(Member newAssignee);
    void addAssignee(Member newAssignee);
    List<Member> getAssignee();
    Priority getPriority();
}
