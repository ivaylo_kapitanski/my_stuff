package com.company.models;

import java.util.List;

public interface History {
    List<History> getHistory();
    void addHistory(History historyToAdd);
}
