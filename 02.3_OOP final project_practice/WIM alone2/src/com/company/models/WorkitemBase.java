package com.company.models;

import com.company.utils.CreatorOfIDs;
import com.company.utils.Validator;

import java.util.ArrayList;
import java.util.List;

public abstract class WorkitemBase implements Workitem {

    private String title;
    private String description;
    private String itemID;
    List<Comments> comments;
    List<History> historyList;

    public WorkitemBase(String title, String description) {
        setTitle(title);
        setDescription(description);
        setItemID(itemID);
        comments = new ArrayList<>();
        historyList = new ArrayList<>();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        Validator.checkIfNull(title);
        Validator.checkLength(title, 10, 50, "Title must be between 10 and 50 symbols.");
        this.title = title;
    }

    @Override
    public List<Comments> getComments() {
        return new ArrayList<>(comments);
    }

    @Override
    public List<History> getHistory() {
        return new ArrayList<>(historyList);
    }

    @Override
    public void addComment(Comments commentToAdd) {
        Validator.checkIfNull(commentToAdd);
        comments.add(commentToAdd);

    }

    @Override
    public void addHistory(History historyToAdd) {
        Validator.checkIfNull(historyToAdd);
        historyList.add(historyToAdd);
    }

    @Override
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        Validator.checkIfNull(description);
        Validator.checkLength(description, 10, 500, "Description must be between 10 and 500 symbols.");
        this.description = description;
    }

    @Override
    public String getItemID() {
        return itemID;
    }

    public void setItemID(String newItemID) {
        newItemID = CreatorOfIDs.createID(this.getClass().getSimpleName().replace("Impl", ""));
        this.itemID = newItemID;
    }

    @Override
    public String toString() {
        return String.format("Title: %s, Description: %s, %s", getTitle(), getDescription(), additionalInfo());
    }

    public abstract String additionalInfo();
}
