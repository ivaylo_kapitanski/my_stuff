package com.company.models;

import com.company.enums.FeedbackStatus;

public interface Feedback extends Workitem{
    int getRating();
    void setRating(int rating);
    FeedbackStatus getFeedbackStatus();
    void setFeedbackStatus(FeedbackStatus feedbackStatus);
}
