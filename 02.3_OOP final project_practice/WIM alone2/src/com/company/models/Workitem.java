package com.company.models;

public interface Workitem extends History, Comments {
    String getItemID();
    String getDescription();
    String getTitle();

}
