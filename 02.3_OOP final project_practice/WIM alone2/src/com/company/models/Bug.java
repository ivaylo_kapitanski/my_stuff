package com.company.models;

import com.company.enums.BugStatus;
import com.company.enums.Severity;

public interface Bug extends Assignable{
    Severity getSeverity();
    BugStatus getBugStatus();
}
