package com.company;

import com.company.core.WIMEngineImpl;

public class Main {

    public static void main(String[] args) {
        WIMEngineImpl engine = new WIMEngineImpl();
        engine.start();
    }
}
