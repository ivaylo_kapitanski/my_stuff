package com.company.commands;

import com.company.core.contracts.WIMRepository;
import com.company.teams.Member;
import com.company.teams.Team;

import java.util.List;

public class ShowAllPeople implements Command{
    private final WIMRepository wimRepository;

    public ShowAllPeople(WIMRepository wimRepository) {
        this.wimRepository = wimRepository;
    }

    @Override
    public String execute(List<String> parameters) {

        return showAllPeople();
    }
    private String showAllPeople(){
        if(wimRepository.getAllMembers().size()<1){
            throw new IllegalArgumentException("No people available to show.");
        }
        StringBuilder sb = new StringBuilder();
        sb.append("--PEOPLE--").append(System.lineSeparator());

        int counter = 1;
        for (Member member : wimRepository.getAllMembers()) {
            sb.append(String.format("%d. %s", counter, member.toString()))
                    .append(System.lineSeparator());
            counter++;
        }
        return sb.toString().trim();
        //  wimRepository.getAllTeams().stream().forEach(System.out::println);
    }
}
