package com.company.commands;

import com.company.core.contracts.WIMRepository;
import com.company.teams.Member;
import com.company.teams.Team;

import java.util.List;

public class ShowAllTeamMembers implements Command{
    private final WIMRepository wimRepository;

    public ShowAllTeamMembers(WIMRepository wimRepository) {
        this.wimRepository = wimRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        String teamInput = parameters.get(0);
        return showAllTeamMembers(teamInput);
    }

    private String showAllTeamMembers(String teamInput){
        if(wimRepository.getAllTeams().size()<1){
            throw new IllegalArgumentException("No teams available to show.");
        }

        Team team = wimRepository.getAllTeams()
                .stream().filter(teamCheck -> teamCheck.getName()
                        .equals(teamInput)).findFirst().get();


        if(team.getMember().size()<1){
            throw new IllegalArgumentException("No members to show");
        }

        StringBuilder sb = new StringBuilder();
        sb.append("--MEMBERS--").append(System.lineSeparator());

        int counter = 1;
        for (Member member : team.getMember()) {
            sb.append(String.format("%d. %s", counter, member.toString()))
                    .append(System.lineSeparator());
            counter++;
        }
        return sb.toString().trim();
        //  wimRepository.getAllTeams().stream().forEach(System.out::println);
    }
}
