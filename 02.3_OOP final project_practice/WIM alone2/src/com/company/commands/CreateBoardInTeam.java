package com.company.commands;

import com.company.core.contracts.WIMFactory;
import com.company.core.contracts.WIMRepository;
import com.company.teams.Board;
import com.company.teams.Team;

import java.util.List;

public class CreateBoardInTeam implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;
    private static final String TEAM_EXISTS_ERROR_MESSAGE = "%s exist chose another name";
    private static final String TEAM_CREATED_SUCCESS_MESSAGE = "%s created!";
    private static final String INVALID_NUMBER_OF_ARGUMENTS = "Invalid numbers of arguments";

    private final WIMRepository wimRepository;
    private final WIMFactory wimFactory;

    public CreateBoardInTeam(WIMFactory wimFactory, WIMRepository wimRepository) {
        this.wimRepository = wimRepository;
        this.wimFactory = wimFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS)
            throw new IllegalArgumentException("Invalid number of arguments!");

        String teamInput = parameters.get(0);
        String boardInput = parameters.get(1);

        return createBoard(teamInput, boardInput);
    }

    private String createBoard(String teamInput, String boardInput) {
        if (wimRepository.getAllTeams()
                .stream().noneMatch(team -> teamInput.equals(team.getName()))) {

            return String.format("Team with name %s does not exist!", teamInput);
        }

        Team team = wimRepository.getAllTeams()
                .stream().filter(teamCheck -> teamCheck.getName()
                        .equals(teamInput)).findFirst().get();


        if (team.getBoard().stream().anyMatch(boardCheck -> boardInput.equals(boardCheck.getName()))) {
            throw new IllegalArgumentException(String.format("%s already exists in team: %s", boardInput, teamInput));
        }
        Board board = wimFactory.createBoard(boardInput);
        wimRepository.addBoard(board);
        team.boardsAdd(board);
        return String.format("Board with name %s added to team with name %s", boardInput, teamInput);

    }
}
