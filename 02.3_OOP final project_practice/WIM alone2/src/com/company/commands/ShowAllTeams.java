package com.company.commands;

import com.company.core.contracts.WIMRepository;
import com.company.teams.Team;

import java.util.List;

public class ShowAllTeams implements Command{
    private final WIMRepository wimRepository;

    public ShowAllTeams(WIMRepository wimRepository) {
        this.wimRepository = wimRepository;
    }

    @Override
    public String execute(List<String> parameters) {

        return showAllTeams();
    }
    private String showAllTeams(){
        if(wimRepository.getAllTeams().size()<1){
            throw new IllegalArgumentException("No teams available to show.");
        }
        StringBuilder sb = new StringBuilder();
        sb.append("--TEAMS--").append(System.lineSeparator());

        int counter = 1;
        for (Team team : wimRepository.getAllTeams()) {
            sb.append(String.format("%d. %s", counter, team.toString()))
                    .append(System.lineSeparator());
            counter++;
        }
        return sb.toString().trim();
      //  wimRepository.getAllTeams().stream().forEach(System.out::println);
    }
}
