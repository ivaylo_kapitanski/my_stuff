package com.company.commands;

import com.company.core.contracts.WIMFactory;
import com.company.core.contracts.WIMRepository;
import com.company.teams.Team;

import java.util.List;

public class CreateTeam implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;
    private static final String TEAM_EXISTS_ERROR_MESSAGE = "%s exist chose another name";
    private static final String TEAM_CREATED_SUCCESS_MESSAGE = "%s created!";
    private static final String INVALID_NUMBER_OF_ARGUMENTS = "Invalid numbers of arguments";
    private final WIMRepository wimRepository;
    private final WIMFactory wimFactory;

    public CreateTeam(WIMFactory wimFactory, WIMRepository wimRepository) {
        this.wimRepository = wimRepository;
        this.wimFactory = wimFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(INVALID_NUMBER_OF_ARGUMENTS);
        }
        String teamInput = parameters.get(0);
        return createTeam(teamInput);
    }

    private String createTeam(String teamName) {
        if (wimRepository.getAllTeams()
                .stream().anyMatch(team -> teamName.equals(team.getName()))) {

            return String.format(TEAM_EXISTS_ERROR_MESSAGE, teamName);
        }
        Team teams = wimFactory.createTeam(teamName);
        wimRepository.addTeam(teams);
        return String.format(TEAM_CREATED_SUCCESS_MESSAGE, teamName);
    }
}
