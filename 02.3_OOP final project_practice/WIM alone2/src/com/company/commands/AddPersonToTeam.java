package com.company.commands;

import com.company.core.contracts.WIMFactory;
import com.company.core.contracts.WIMRepository;
import com.company.teams.Member;
import com.company.teams.Team;

import java.util.List;

public class AddPersonToTeam implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;
    private final WIMRepository wimRepository;
    private final WIMFactory wimFactory;

    public AddPersonToTeam(WIMFactory wimFactory, WIMRepository wimRepository) {
        this.wimRepository = wimRepository;
        this.wimFactory = wimFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS)
            throw new IllegalArgumentException("Invalid number of arguments!");

        String personInput = parameters.get(0);
        String teamInput = parameters.get(1);

        if (wimRepository.getAllTeams()
                .stream().noneMatch(teamCheck -> teamInput.equals(teamCheck.getName()))) {
            throw new IllegalArgumentException(String.format("%s not a valid team name!", teamInput));
        }

        Team team = wimRepository.getAllTeams()
                .stream().filter(teamCheck -> teamCheck.getName()
                        .equals(teamInput)).findFirst().get();


        if (team.getMember().stream().anyMatch(personCheck -> personInput.equals(personCheck.getName()))) {
            throw new IllegalArgumentException(String.format("%s is already part of team: %s", personInput, teamInput));
        }
        Member member = wimFactory.createMembers(personInput);
        team.membersAdd(member);
        wimRepository.addMember(member);
        return String.format("%s added to team %s", personInput, teamInput);
    }
}
