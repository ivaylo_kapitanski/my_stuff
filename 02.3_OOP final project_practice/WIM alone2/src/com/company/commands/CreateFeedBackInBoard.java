package com.company.commands;

import com.company.core.contracts.WIMFactory;
import com.company.core.contracts.WIMRepository;
import com.company.models.Comments;
import com.company.models.Feedback;
import com.company.teams.Board;
import com.company.teams.Team;

import java.util.List;

public class CreateFeedBackInBoard implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS=5;

    private final WIMRepository wimRepository;
    private final WIMFactory wimFactory;

    public CreateFeedBackInBoard(WIMFactory wimFactory, WIMRepository wimRepository) {
        this.wimRepository = wimRepository;
        this.wimFactory = wimFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS)
            throw new IllegalArgumentException("Invalid number of arguments!");

        String teamInput = parameters.get(0);
        String boardInput = parameters.get(1);
        String feedbackTitle = parameters.get(2);
        String feedBackDescription = parameters.get(3);
        int rating = Integer.parseInt(parameters.get(4));

        return createFeedback(teamInput, boardInput, feedbackTitle, feedBackDescription, rating);
    }

        private String createFeedback(String teamInput, String boardInput,String feedbackTitle, String feedBackDescription, int rating ) {

            if (wimRepository.getAllTeams()
                    .stream().noneMatch(team -> teamInput.equals(team.getName()))) {

                return String.format("Team with name %s does not exist!", teamInput);
            }
            if (wimRepository.getAllBoards()
                    .stream().noneMatch(board -> boardInput.equals(board.getName()))) {

                return String.format("Board with name %s does not exist!", boardInput);
            }

            Team team = wimRepository.getAllTeams()
                    .stream().filter(teamCheck -> teamCheck.getName()
                            .equals(teamInput)).findFirst().get();
            Board board = wimRepository.getAllBoards()
                    .stream().filter(teamCheck -> teamCheck.getName()
                            .equals(boardInput)).findFirst().get();

        if(team.getBoard().contains(board)) {

            Feedback feedback = wimFactory.createFeedback(feedbackTitle, feedBackDescription, rating);
            board.addWorkItems(feedback);
            wimRepository.addFeedback(feedback);//or just addWorkitem??
            return String.format("Feedback with ID %s created in board %s", feedback.getItemID(), boardInput);
        }
        throw new IllegalArgumentException(String.format("%s not part of %s",boardInput,teamInput));
    }
}
