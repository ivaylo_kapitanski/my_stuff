package com.company.teams;

import com.company.utils.Validator;

import java.util.ArrayList;
import java.util.List;

public class TeamImpl implements Team{
    private String name;
    private final List<Member> members;
    private final List<Board> boards;

    public TeamImpl(String name) {
        setName(name);
        members=new ArrayList<>();
        boards=new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    @Override
    public List<Member> getMember() {
        return new ArrayList<>(members);
    }

    @Override
    public List<Board> getBoard() {
        return new ArrayList<>(boards);
    }

    public void setName(String name) {
        Validator.checkIfNull(name);
        Validator.checkLength(name,5,15, "Name must be between 5 and 15 symbols.");
        this.name = name;
    }
    @Override
    public void membersAdd(Member members){
        Validator.checkIfNull(members);
        this.members.add(members);
    }
    @Override
    public void boardsAdd(Board boards){
        Validator.checkIfNull(boards);
        this.boards.add(boards);
    }

    @Override
    public String toString() {
        return String.format("Team: %s%n",getName());
    }
}
