package com.company.teams;

import java.util.List;

public interface Team {
    String getName();

    List<Member> getMember();

    List<Board> getBoard();

    void boardsAdd(Board boards);

    void membersAdd(Member members);
}
