package com.company.teams;

import com.company.models.Workitem;

import java.util.List;

public interface Member{
    String getName();
    List<Workitem> getWorkItems();
    void addWorkItems(Workitem workItems);
    void removeWorkItems(Workitem workItems);
}
