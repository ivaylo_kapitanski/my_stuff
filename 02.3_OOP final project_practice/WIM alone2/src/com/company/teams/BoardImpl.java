package com.company.teams;

import com.company.models.Workitem;
import com.company.utils.Validator;

import java.util.ArrayList;
import java.util.List;

public class BoardImpl implements Board{
    private String name;
    private List<Workitem> workItems;

    public BoardImpl(String name) {
        setName(name);
        workItems=new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        Validator.checkIfNull(name);
        Validator.checkLength(name,5,15, "Name must be between 5 and 15 symbols.");
        this.name = name;
    }

    public List<Workitem> getWorkItems() {
        return new ArrayList<>(workItems);
    }

    public void addWorkItems(Workitem workItems){
        Validator.checkIfNull(workItems);
        this.workItems.add(workItems);
    }

}
