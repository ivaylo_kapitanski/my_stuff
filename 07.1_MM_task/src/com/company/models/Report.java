package com.company.models;

public class Report {

    private Long topPerformanceThreshold;
    private Boolean useExperienceMultiplier;
    private Long periodLimit;

    public Report() {
    }

    public Long getTopPerformanceThreshold() {
        return topPerformanceThreshold;
    }

    public void setTopPerformanceThreshold(Long topPerformanceThreshold) {
        this.topPerformanceThreshold = topPerformanceThreshold;
    }

    public Boolean getUseExperienceMultiplier() {
        return useExperienceMultiplier;
    }

    public void setUseExperienceMultiplier(Boolean useExperienceMultiplier) {
        this.useExperienceMultiplier = useExperienceMultiplier;
    }

    public Long getPeriodLimit() {
        return periodLimit;
    }

    public void setPeriodLimit(Long periodLimit) {
        this.periodLimit = periodLimit;
    }
}
