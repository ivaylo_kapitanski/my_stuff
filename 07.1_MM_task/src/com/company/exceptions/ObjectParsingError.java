package com.company.exceptions;

public class ObjectParsingError extends RuntimeException{
    public ObjectParsingError(String message) {
        super(message);
    }
}
