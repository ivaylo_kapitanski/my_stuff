package com.company.utils;

import com.company.exceptions.DuplicateFileException;
import com.company.models.Employee;
import com.company.models.Report;

import java.io.*;
import java.util.*;



public class CsvCreationHelper {
    public static void createCsvFile(Report report, List<Employee> employeeListst, String csvFileName) throws IOException {
        if (isFileWithSameNamePresent(csvFileName)) {
            throw new DuplicateFileException("Report with this name already exists");
        }
        File csvOutputFile = new File(csvFileName + ".csv");
        try (PrintWriter pw = new PrintWriter(csvOutputFile)) {
            pw.println("Name, Score");
            StringBuilder sb = new StringBuilder();
            List<Employee> eligibleEmployees = getEligibleEmployeesList(report, employeeListst);
            for (Employee employee : eligibleEmployees) {
                double score = calculateScore(report, employee);
                sb.append(employee.getName());
                sb.append(",");
                sb.append(Math.round(score*100.0)/100.0);
                pw.println(sb);
                sb.setLength(0);
            }
        } catch (IOException e) {
            throw new IOException(e.getMessage());
        }
    }

    private static boolean isFileWithSameNamePresent(String filename) throws IOException {
        String name = new File(".").getCanonicalPath();
        boolean isPresent = true;
        try {
            FileReader reader = new FileReader(name + "/" + filename + ".csv");
        } catch (FileNotFoundException e) {
            isPresent = false;
        }
        return isPresent;
    }

    private static List<Employee> getEligibleEmployeesList(Report report, List<Employee> employeeList) {
        int counter = 0;
        double threshold = (employeeList.size() * (double) report.getTopPerformanceThreshold() / 100);
        //adjust when file size is less than than the topPerformersThreshold
        if (threshold < 1) threshold = 1;
        employeeList.sort(Comparator.comparing(Employee::getTotalSales).reversed());
        Stack<Employee> updatedList = new Stack<>();

        for (Employee employee : employeeList) {
            if (employee.getSalesPeriod() > report.getPeriodLimit()) {
                continue;
            }
            if (counter >= Math.round(threshold)) {
                break;
            }
            if (updatedList.isEmpty()) {
                updatedList.add(employee);
                counter++;
            } else if (updatedList.peek().getTotalSales().equals(employee.getTotalSales())) {
                updatedList.add(employee);
            } else {
                updatedList.add(employee);
                counter++;
            }

        }
        return new ArrayList<>(updatedList);
    }

    private static double calculateScore(Report report, Employee employee) {
        double score;
        if (report.getUseExperienceMultiplier()) {
            score = ((double) employee.getTotalSales() / employee.getSalesPeriod())
                    * employee.getExperienceMultiplier();
        } else {
            score = (double) employee.getTotalSales() / employee.getSalesPeriod();
        }
        return score;
    }

}
