package com.company.utils;

import com.company.exceptions.ObjectParsingError;
import com.company.models.Employee;
import com.company.models.Report;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class JsonObjectsHelper {
    public static List<Employee> readEmployeesFromFile(String path) throws IOException {
        JSONParser jsonParser = new JSONParser();
        String name = new File(".").getCanonicalPath();
        List<Employee> employeeList = new ArrayList<>();

        try (FileReader reader = new FileReader(name + "/" + path + ".json")) {
            Object object = jsonParser.parse(reader);
            JSONArray objectList = (JSONArray) object;

            for (Object empObject : objectList) {
                employeeList.add(parseEmployeeObject((JSONObject) empObject));
            }
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException(e.getMessage());
        } catch (IOException e) {
            throw new IOException(e.getMessage());
        } catch (ParseException e) {
            throw new ObjectParsingError("Object parsing error");
        }
        return employeeList;
    }

    public static Report readDefinitionFile(String input) throws IOException {
        JSONParser jsonParser = new JSONParser();
        String name = new File(".").getCanonicalPath();
        Report report;
        try (FileReader reader = new FileReader(name + "/" + input + ".json")) {
            Object object = jsonParser.parse(reader);
            report = parseReportObject((JSONObject) object);
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException(e.getMessage());
        } catch (IOException e) {
            throw new IOException(e.getMessage());
        } catch (ParseException e) {
            throw new ObjectParsingError("Object parsing error");
        }
        return report;
    }

    private static Employee parseEmployeeObject(JSONObject emplObject) {
        Employee employee = new Employee();
        employee.setName((String) emplObject.get("name"));
        employee.setTotalSales((Long) emplObject.get("totalSales"));
        employee.setSalesPeriod((Long) emplObject.get("salesPeriod"));
        employee.setExperienceMultiplier((Double) emplObject.get("experienceMultiplier"));
        return employee;
    }

    private static Report parseReportObject(JSONObject object) {
        Report report = new Report();
        report.setTopPerformanceThreshold((Long) object.get("topPerformersThreshold"));
        report.setUseExperienceMultiplier((Boolean) object.get("useExperienceMultiplier"));
        report.setPeriodLimit((Long) object.get("periodLimit"));
        return report;
    }
}
