package com.company;

import com.company.exceptions.DuplicateFileException;
import com.company.exceptions.ObjectParsingError;
import com.company.models.Employee;
import com.company.models.Report;
import com.company.utils.CsvCreationHelper;
import com.company.utils.JsonObjectsHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);

        List<Employee> employeeList = new ArrayList<>();
        String fileName = "";
        while (fileName.isEmpty()) {
            try {
                System.out.println("Please enter the file from which to generate the report:");
                fileName = scanner.nextLine();
                employeeList = JsonObjectsHelper.readEmployeesFromFile(fileName);
            } catch (IOException | ObjectParsingError e) {
                System.out.println(e.getMessage());
                fileName = "";
            }
        }
        String defFile = "";
        Report selectedReport = new Report();
        while (defFile.isEmpty()) {
            try {
                System.out.println("Please enter the name of the report definition file:");
                defFile = scanner.nextLine();
                selectedReport = JsonObjectsHelper.readDefinitionFile(defFile);
            } catch (IOException | ObjectParsingError e) {
                System.out.println(e.getMessage());
                defFile = "";
            }
        }
        String csvFile = "";
        while (csvFile.isEmpty()) {
            try {

                System.out.println("Please enter the name of the csv report to be generated:");
                csvFile = scanner.nextLine();
                CsvCreationHelper.createCsvFile(selectedReport, employeeList, csvFile);
            } catch (IOException | DuplicateFileException e) {
                System.out.println(e.getMessage());
                csvFile = "";
            }
        }
    }
}
